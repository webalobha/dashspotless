<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


# User Registration Api
Route::prefix('user')->group(function() {
	Route::post('register', 'Api\User\UserRegisterController@register');
	Route::post('verify-otp', 'Api\User\UserRegisterController@verifyOtp');
    Route::get('send-notification','Api\User\SendTextMessage@sendOtp');
});

# user Authenticated Routes
Route::group(['prefix' => 'user', 'middleware' => ['user']], function () {
    Route::post('fetchProfile', 'Api\User\UserRegisterController@fetchProfile');
    Route::post('update-profile', 'Api\User\UserRegisterController@updateProfile');
    Route::post('update-location', 'Api\User\UserRegisterController@updateLocation');
    Route::post('categories', 'Api\User\UserRegisterController@categories');
    Route::post('sub-categories', 'Api\User\UserRegisterController@subCategories');
    Route::post('sub-category-info', 'Api\User\UserRegisterController@subCategoryInfo');

    # Services Routes of User
    Route::post('sub-category-services', 'Api\User\ServiceController@subCategoryServices');
    Route::post('variant-services', 'Api\User\ServiceController@variantServices');
    Route::post('service/getservicesforcart', 'Api\User\ServiceController@getServiceIncart');
    Route::post('service/addtocart', 'Api\User\ServiceController@addServiceToCart');
    Route::post('service/updatecartquantity', 'Api\User\ServiceController@updateCartQuantity');
    Route::post('service/deletefromcart', 'Api\User\ServiceController@deleteServiceFromCart');
    Route::post('service/search', 'Api\User\ServiceController@searchService');
    Route::post('service/detail', 'Api\User\ServiceController@serviceDetail');
    Route::post('fetchDashboardBanners', 'Api\User\ServiceController@fetchBanners');

    # Subscription Routes for user
    Route::post('subscription/get', 'Api\User\SubscriptionController@getSubscription');

    # Address Routes for user
    Route::post('address/get', 'Api\User\AddressController@fetch');
    Route::post('address/store', 'Api\User\AddressController@store');
    Route::post('address/remove', 'Api\User\AddressController@remove');
    Route::post('address/update', 'Api\User\AddressController@update');
    Route::post('address/fetchSingle', 'Api\User\AddressController@fetchSingleAddress');

    # fetch Country State and City Route
    Route::post('country/get', 'Api\User\CountryStateCityController@fetchCountries');
    Route::post('state/get', 'Api\User\CountryStateCityController@fetchStates');
    Route::post('city/get', 'Api\User\CountryStateCityController@fetchCities');

    # Routes for Booking Slots And Dates
    Route::get('bookingslot/getDates', 'Api\User\BookingSlotController@fetchDates');
    Route::post('bookingslot/get', 'Api\User\BookingSlotController@fetch');

    # Order Apis
    Route::post('order/submit', 'Api\User\OrderController@submitOrder');
    Route::post('order/upcoming', 'Api\User\OrderController@upcomingOrder');
    Route::post('order/past', 'Api\User\OrderController@pastOrders');
    Route::post('order/detail', 'Api\User\OrderController@orderDetail');
    Route::post('order/rescheduleCondition', 'Api\User\OrderController@rescheduleConditions');
    Route::post('order/fetchFreeSlots', 'Api\User\OrderController@fetchAllFreeTimeSlotsofProvider');
    Route::post('order/rescheduleConfirm', 'Api\User\OrderController@rescheduleConfirm');
    Route::post('order/fetchForCustomerSupport', 'Api\User\OrderController@fetchAllOrders');
    Route::post('order/cancel', 'Api\User\OrderController@cancelOrder');

    # Disputes Routes
    Route::post('orderDispute/fetchReasons', 'Api\User\OrderDisputeController@fetchDisputesReason');
    Route::post('orderDispute/raise', 'Api\User\OrderDisputeController@raiseDispute');
    Route::post('orderDispute/detail', 'Api\User\OrderDisputeController@disputeSolutionDetail');

    # Wallet Apis
    Route::post('wallet/fetch', 'Api\User\WalletController@fetchRemainingAmount');
    Route::post('wallet/history', 'Api\User\WalletController@fetchHistory');

    # User Need Help Static Content
     Route::post('needHelpContent/fetch', 'Api\User\NeedHelpContentController@fetchNeedHelpContent');

   
});

# Faq Routes
Route::group([], function () {
    Route::get('bookingServiceFaq', 'Api\Faqs\FaqController@getBookingService');
    Route::get('dashspotlessGuideFaq', 'Api\Faqs\FaqController@getDashspotlessGuide');
    Route::get('payingServiceFaq', 'Api\Faqs\FaqController@getPayingService');
    Route::get('aboutDashspotless', 'Api\Faqs\FaqController@aboutDashspotless');
    Route::get('termOfUse', 'Api\Faqs\FaqController@termOfUse');
    Route::get('howItWorks', 'Api\Faqs\FaqController@howItWorks');
    Route::get('policy', 'Api\Faqs\FaqController@privacyPolicy');
    Route::get('contactUs', 'Api\Faqs\FaqController@contactUs');
    Route::get('sendSms', 'Api\Twilio\SmsController@sendSms');
    Route::get('cancellationConditions/fetch', 'Api\User\NeedHelpContentController@fetchCancellationConditions');
    Route::get('rescheduleConditions/fetch', 'Api\User\NeedHelpContentController@fetchRescheduleconditions');
    Route::get('providerHelpContent/fetch', 'Api\User\NeedHelpContentController@fetchProviderHelpContent');
});

# Provider Registration Api
Route::prefix('provider')->namespace('Api\Provider')->group(function() {
	Route::post('register', 'ProviderRegisterController@register');
	Route::post('verify-otp', 'ProviderRegisterController@verifyOtp');
});

# Provider Authenticate routes
Route::group(['prefix' => 'provider', 'middleware' => ['provider']], function () {
    Route::post('update-profile', 'Api\Provider\ProviderRegisterController@updateProfile');
    Route::post('profile/fetch', 'Api\Provider\ProviderRegisterController@fetchProfile');
    Route::post('update-location', 'Api\Provider\ProviderRegisterController@updateLocation');
    Route::post('update-location-manual', 'Api\Provider\ProviderRegisterController@updateLocationManual');
    Route::post('categories', 'Api\Provider\ProviderRegisterController@categories');
    Route::post('store-category', 'Api\Provider\ProviderRegisterController@storeCategory');
    Route::post('store-sub-category', 'Api\Provider\ProviderRegisterController@storeSubCategory');
    Route::post('updateProfileImage', 'Api\Provider\ProviderRegisterController@updateProfileImage');

    # Routes for Providers Documents and bank Details
    Route::get('document/mandatory', 'Api\Provider\DocumentController@getMandatoryDocument');
    Route::post('document/upload', 'Api\Provider\DocumentController@uploadDocument');
    Route::post('document/getuploadedDocs', 'Api\Provider\DocumentController@getUploadDocument');
    Route::post('document/status', 'Api\Provider\DocumentController@documentStatus');
    Route::post('bankDetails/update', 'Api\Provider\DocumentController@updateBankDetails');
    Route::post('bankDetails/fetch', 'Api\Provider\DocumentController@fetchBankDetails');

    # Routes for Providers Award and Certificate
    Route::post('upload/workPhotos', 'Api\Provider\ProviderWorkCertificate@uploadWorkPhotos');
    Route::post('workPhotos/fetch', 'Api\Provider\ProviderWorkCertificate@fetchWorkPhotos');
    Route::post('upload/awardCertificate', 'Api\Provider\ProviderWorkCertificate@uploadAwardCertificate');
    Route::post('awardCertificate/fetch', 'Api\Provider\ProviderWorkCertificate@fetchAwardCertificate');

    # Routes for Credit Recharge 
    Route::post('creditPackage/fetch', 'Api\Provider\CreditPointController@fetchAllPackages');
    Route::post('creditPackage/buy', 'Api\Provider\CreditPointController@buyPackage');
    Route::post('creditPackage/fetchBought', 'Api\Provider\CreditPointController@fetchProviderBoughtPackages');
    Route::post('creditPackage/fetchExpenses', 'Api\Provider\CreditPointController@fetchExpensesCreditPoints');

    # Route for Leads
    Route::post('leads/fetch', 'Api\Provider\LeadsController@fetchLeads');
    Route::post('lead/fetchDetail', 'Api\Provider\LeadsController@fetchLeadDetail');
    Route::post('leads/accept', 'Api\Provider\LeadsController@acceptLead');
    Route::post('leads/reject', 'Api\Provider\LeadsController@rejectLead');

    # Route for Ongoing
    Route::post('ongoing/all', 'Api\Provider\OngoingController@fetchall');
    Route::post('ongoing/today', 'Api\Provider\OngoingController@fetchToday');
    Route::post('ongoing/tomorrow', 'Api\Provider\OngoingController@fetchTomorrow');
    Route::post('ongoing/weekly', 'Api\Provider\OngoingController@fetchWeekly');
    Route::post('ongoing/detail', 'Api\Provider\OngoingController@ongoingDetail');

    # Route for Training Centres
    Route::get('trainingCentre', 'Api\Provider\TrainingCentreController@fetch');

    # Route for Order
    Route::post('order/updateStatus', 'Api\Provider\OrderController@updateOrderStatus');
    Route::post('order/cancel', 'Api\Provider\OrderController@cancelOrder');
    Route::post('order/fetchCancel', 'Api\Provider\OrderController@fetchCancel');

    # Route for Penalties
    Route::post('penalty/fetch', 'Api\Provider\PenaltyController@fetchPenalty');
});

