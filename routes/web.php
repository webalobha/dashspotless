<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//      return redirect(route('login'));
// });

// Auth::routes();
Route::get('admin-login', 'AdminController\LoginController@index')->name('admin-login');
Route::post('admin-login', 'AdminController\LoginController@login');
Route::get('forgot-password', 'AdminController\LoginController@forgotpassword');
Route::post('sendemail', 'AdminController\LoginController@sendemail');
Route::get('reset_pass/{id}', 'AdminController\LoginController@reset_pass');
Route::post('post_reset_pass', 'AdminController\LoginController@post_reset_pass');

Route::group(['middleware' => 'admin'], function(){

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'AdminController\ProfileController@update');
Route::post('/edit-profile', 'AdminController\ProfileController@edit');

# category routes
Route::get('category', 'AdminController\CategoryController@index');
Route::post('add-category', 'AdminController\CategoryController@store');
Route::get('edit-category/{id}', 'AdminController\CategoryController@update');
Route::post('edit-category/{id}', 'AdminController\CategoryController@edit');
Route::delete('delete-category/{id}', 'AdminController\CategoryController@delete');
Route::post('status-category/{id}', 'AdminController\CategoryController@status');

# sub category routes
Route::get('sub-category', 'AdminController\SubCategoryController@index');
Route::post('add-sub-category', 'AdminController\SubCategoryController@store');
Route::get('edit-sub-category/{id}', 'AdminController\SubCategoryController@update');
Route::post('edit-sub-category/{id}', 'AdminController\SubCategoryController@edit');
Route::delete('delete-sub-category/{id}', 'AdminController\SubCategoryController@delete');
Route::post('status-sub-category/{id}', 'AdminController\SubCategoryController@status');


# Variants
Route::get('variant', 'AdminController\VariantController@index');
Route::get('variant-create', 'AdminController\VariantController@create');
Route::post('add-sub-variant', 'AdminController\VariantController@store');
Route::get('edit-variant/{id}', 'AdminController\VariantController@update');
Route::post('edit-variant', 'AdminController\VariantController@edit');
Route::get('variant-view/{id}', 'AdminController\VariantController@view');

# Variants
Route::any('service', 'AdminController\ServiceController@index');
Route::get('create-service', 'AdminController\ServiceController@create');
Route::post('add-service', 'AdminController\ServiceController@store');
Route::get('edit-service/{id}', 'AdminController\ServiceController@update');
Route::post('edit-service', 'AdminController\ServiceController@edit');
Route::get('view-service/{id}', 'AdminController\ServiceController@view');
Route::delete('delete-service/{id}', 'AdminController\ServiceController@delete');

Route::any('service-setting/{id?}', 'AdminController\ServiceSettingController@create_or_update');

Route::any('service-setting_update/{id?}', 'AdminController\ServiceSettingController@edit');



# Drop down data 
Route::get('subcategory-json','AdminController\SubCategoryController@subcategory');
Route::get('variant-json','AdminController\VariantController@variant_by_subcategory');
Route::get('search-variant-json','AdminController\VariantController@variant_by_subcategory_for_search');
Route::get('service-json','AdminController\ServiceController@service_by_variant');



# user section
Route::any('users', 'AdminController\UserController@index');
Route::get('users-status/{id?}', 'AdminController\UserController@status');
Route::delete('delete-user/{id}', 'AdminController\UserController@delete');
Route::get('view-user-history/{id}', 'AdminController\UserController@view');
Route::get('user-order-service-history/{id}', 'AdminController\UserController@orderServices');
Route::get('user-order-service-history-detail/{id}', 'AdminController\UserController@services');

# provider section
Route::any('providers', 'AdminController\ProviderController@index');
Route::get('provider-status/{id?}', 'AdminController\ProviderController@status');
Route::delete('delete-provider/{id}', 'AdminController\ProviderController@delete');
Route::get('view-provider-history/{id}', 'AdminController\ProviderController@view');

# approve document
Route::get('approve/{id}', 'AdminController\ProviderController@approvePage');
Route::get('reject/{id}', 'AdminController\ProviderController@rejectPage');
Route::get('post-status/{id}/{status}', 'AdminController\ProviderController@postStatus');

# document routes
Route::get('document', 'AdminController\DocumentController@index');
Route::any('add-document', 'AdminController\DocumentController@create');
Route::post('add-document', 'AdminController\DocumentController@store');
Route::get('edit-document/{id}', 'AdminController\DocumentController@update');
Route::post('edit-document/{id}', 'AdminController\DocumentController@edit');
Route::delete('delete-document/{id}', 'AdminController\DocumentController@delete');
Route::post('status-document/{id}', 'AdminController\DocumentController@status');

# Site Setting
Route::any('site-setting', 'AdminController\SiteSettingController@create_or_update');
Route::post('edit-site-setting/{id?}', 'AdminController\SiteSettingController@edit');

# subscription
Route::get('subscription', 'AdminController\SubscriptionController@index');
Route::get('add-subscription', 'AdminController\SubscriptionController@create');
Route::post('add-subscription', 'AdminController\SubscriptionController@store');
Route::get('edit-subscription/{id}', 'AdminController\SubscriptionController@update');
Route::post('edit-subscription/{id}', 'AdminController\SubscriptionController@edit');
Route::delete('delete-subscription/{id}', 'AdminController\SubscriptionController@delete');
Route::get('view-subscription/{id}', 'AdminController\SubscriptionController@view');

# credit-recharge routes
Route::get('credit-recharge', 'AdminController\CreditRechargeController@index');
Route::post('add-credit-recharge', 'AdminController\CreditRechargeController@store');
Route::get('edit-credit-recharge/{id}', 'AdminController\CreditRechargeController@update');
Route::post('edit-credit-recharge/{id}', 'AdminController\CreditRechargeController@edit');
Route::delete('delete-credit-recharge/{id}', 'AdminController\CreditRechargeController@delete');
Route::post('status-credit-recharge/{id}', 'AdminController\CreditRechargeController@status');

# role routes
Route::get('role', 'AdminController\RoleController@index');
Route::get('add-role', 'AdminController\RoleController@create');
Route::post('add-role', 'AdminController\RoleController@store');
Route::get('edit-role/{id}', 'AdminController\RoleController@update');
Route::post('edit-role/{id}', 'AdminController\RoleController@edit');
Route::delete('delete-role/{id}', 'AdminController\RoleController@delete');

# manage admin
Route::get('manage-admin', 'AdminController\AdminManageController@index');
Route::get('add-manage-admin', 'AdminController\AdminManageController@create');
Route::post('add-manage-admin', 'AdminController\AdminManageController@store');
Route::get('edit-manage-admin/{id}', 'AdminController\AdminManageController@update');
Route::post('edit-manage-admin/{id}', 'AdminController\AdminManageController@edit');


# Disbute routes
Route::get('dispute-reason', 'AdminController\DisputeReasonController@index');
Route::get('add-dispute-reason', 'AdminController\DisputeReasonController@create');
Route::post('stor-dispute-reason', 'AdminController\DisputeReasonController@store');
Route::get('edit-dispute-reason/{id}', 'AdminController\DisputeReasonController@update');
Route::get('view-dispute-reason/{id}', 'AdminController\DisputeReasonController@view');
Route::post('edit-dispute-reason/{id}', 'AdminController\DisputeReasonController@edit');
Route::delete('delete-dispute-reason/{id}', 'AdminController\DisputeReasonController@delete');
Route::post('status-dispute-reason/{id}', 'AdminController\DisputeReasonController@status');


# Disbute request
Route::get('dispute-request', 'AdminController\DisputeRequestController@index');
Route::get('add-dispute-request', 'AdminController\DisputeRequestController@create');
Route::post('stor-dispute-request', 'AdminController\DisputeRequestController@store');
Route::get('edit-dispute-request/{id}', 'AdminController\DisputeRequestController@update');
Route::get('view-dispute-request/{id}', 'AdminController\DisputeRequestController@view');
Route::post('edit-dispute-request/{id}', 'AdminController\DisputeRequestController@edit');
Route::delete('delete-dispute-request/{id}', 'AdminController\DisputeRequestController@delete');
Route::post('status-dispute-request/{id}', 'AdminController\DisputeRequestController@status');
Route::get('solution/{id}', 'AdminController\DisputeRequestController@solution');
Route::post('solution/{id}', 'AdminController\DisputeRequestController@postSolution');
Route::get('view-solution/{id}', 'AdminController\DisputeRequestController@viewSolution');
Route::any('user_or_provider', 'AdminController\DisputeRequestController@user_or_provider');
Route::any('user_or_provider_reasion', 'AdminController\DisputeRequestController@user_or_provider_reasion');
Route::any('orders_by_ajax', 'AdminController\DisputeRequestController@orders_by_ajax');








# Busness Country routes
Route::any('business-country', 'AdminController\BusinessCountryController@index');
Route::any('add-business-country', 'AdminController\BusinessCountryController@create');
Route::post('add-business-country', 'AdminController\BusinessCountryController@store');
Route::get('edit-business-country/{id}', 'AdminController\BusinessCountryController@update');
Route::post('edit-business-country/{id}', 'AdminController\BusinessCountryController@edit');
Route::delete('delete-business-country/{id}', 'AdminController\BusinessCountryController@delete');
Route::post('status-business-country/{id}', 'AdminController\BusinessCountryController@status');

# Busness City routes
Route::any('business-city', 'AdminController\BusinessCityController@index');
Route::any('add-business-city', 'AdminController\BusinessCityController@create');
Route::post('add-business-city', 'AdminController\BusinessCityController@store');
Route::get('edit-business-city/{id}', 'AdminController\BusinessCityController@update');
Route::post('edit-business-city/{id}', 'AdminController\BusinessCityController@edit');
Route::delete('delete-business-city/{id}', 'AdminController\BusinessCityController@delete');
Route::post('status-business-city/{id}', 'AdminController\BusinessCityController@status');
Route::any('master-state', 'AdminController\BusinessCityController@state');
Route::any('master-city', 'AdminController\BusinessCityController@city');



# category routes
Route::get('cms-page', 'AdminController\CMSPageController@index');
Route::get('add-cms-page', 'AdminController\CMSPageController@create');
Route::post('stor-cms-page', 'AdminController\CMSPageController@store');
Route::get('edit-cms-page/{id}', 'AdminController\CMSPageController@update');
Route::get('view-cms-page/{id}', 'AdminController\CMSPageController@view');
Route::post('edit-cms-page/{id}', 'AdminController\CMSPageController@edit');
Route::delete('delete-cms-page/{id}', 'AdminController\CMSPageController@delete');
Route::post('status-cms-page/{id}', 'AdminController\CMSPageController@status');

Route::get('assign-cms-page', 'AdminController\CMSPageController@assign');
Route::post('stor-assign-cms-page', 'AdminController\CMSPageController@store_assign');
Route::delete('delete-assign-cms-page/{id}', 'AdminController\CMSPageController@deleteAssign');

# orders route
Route::get('order', 'AdminController\OrderController@index');
Route::get('ordered-services/{id}', 'AdminController\OrderController@orderServices');
Route::get('view-order/{id}', 'AdminController\OrderController@view');

# content controller
Route::get('about-dashspotless', 'AdminController\ContentController@aboutDashspotless');
Route::post('about-dashspotless', 'AdminController\ContentController@storeAboutDashspotless');
Route::get('faq-paying-services', 'AdminController\ContentController@faqPayingService');
Route::post('faq-paying-services', 'AdminController\ContentController@storeFaqPayingService');
Route::get('faq-booking-service', 'AdminController\ContentController@faqBookingService');
Route::post('faq-booking-service', 'AdminController\ContentController@storeFaqBookingService');
Route::get('faq-dashspotless-guide', 'AdminController\ContentController@faqDashspotlessGuide');
Route::post('faq-dashspotless-guide', 'AdminController\ContentController@storeFaqDashspotlessGuide');
Route::get('how-it-works', 'AdminController\ContentController@howItWorks');
Route::post('how-it-works', 'AdminController\ContentController@storeHowItWorks');
Route::get('terms-of-use', 'AdminController\ContentController@termsOfUse');
Route::post('terms-of-use', 'AdminController\ContentController@storeTermsOfUse');
Route::get('privacy-policy', 'AdminController\ContentController@privacyPolicy');
Route::post('privacy-policy', 'AdminController\ContentController@storePrivacyPolicy');
Route::get('contact-us', 'AdminController\ContentController@contactUs');
Route::post('contact-us', 'AdminController\ContentController@storeContactUs');

//Credit points structure

Route::get('price-per-points', 'AdminController\CreditPointPackageController@PricePerPoint');
Route::post('storePricePerPoint', 'AdminController\CreditPointPackageController@storePricePerPoint');
Route::get('credit-points', 'AdminController\CreditPointPackageController@index');
Route::post('add-credit-points', 'AdminController\CreditPointPackageController@store');
Route::get('edit-credit-points/{id?}', 'AdminController\CreditPointPackageController@update');
Route::post('edit-credit-points/{id?}', 'AdminController\CreditPointPackageController@edit');
Route::delete('delete-credit-points/{id}', 'AdminController\CreditPointPackageController@delete');
Route::post('status-credit-points/{id}', 'AdminController\CreditPointPackageController@status');

# training center
Route::get('training-center', 'AdminController\TrainingCenterController@index');
Route::post('training-center', 'AdminController\TrainingCenterController@store');
Route::delete('training-center/{id}', 'AdminController\TrainingCenterController@delete');

# need-help routes
Route::get('need-help', 'AdminController\NeedHelpStaticContentController@index');
Route::get('add-need-help', 'AdminController\NeedHelpStaticContentController@create');
Route::post('add-need-help', 'AdminController\NeedHelpStaticContentController@store');
Route::get('edit-need-help/{id}', 'AdminController\NeedHelpStaticContentController@update');
Route::post('edit-need-help/{id}', 'AdminController\NeedHelpStaticContentController@edit');
Route::delete('delete-need-help/{id}', 'AdminController\NeedHelpStaticContentController@delete');
Route::post('status-need-help/{id}', 'AdminController\NeedHelpStaticContentController@status');

Route::get('user-notification', 'AdminController\NotificationController@userNotification');
Route::get('provider-notification', 'AdminController\NotificationController@providerNotification');
// logout session
Route::get('logout','AdminController\LoginController@logout');

});