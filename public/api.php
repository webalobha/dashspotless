<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


# User Registration Api
Route::prefix('user')->group(function() {
	Route::post('register', 'Api\User\UserRegisterController@register');
	Route::post('verify-otp', 'Api\User\UserRegisterController@verifyOtp');
});

# user Authenticated Routes
Route::group(['prefix' => 'user', 'middleware' => ['user']], function () {
    Route::post('update-profile', 'Api\User\UserRegisterController@updateProfile');
    Route::post('categories', 'Api\User\UserRegisterController@categories');
    Route::post('sub-categories', 'Api\User\UserRegisterController@subCategories');
    Route::post('sub-category-info', 'Api\User\UserRegisterController@subCategoryInfo');
});

# Provider Registration Api
Route::prefix('provider')->namespace('Api\Provider')->group(function() {
	Route::post('register', 'ProviderRegisterController@register');
	Route::post('verify-otp', 'ProviderRegisterController@verifyOtp');
});

# Provider Authenticate routes
Route::group(['prefix' => 'provider', 'middleware' => ['provider']], function () {
    Route::post('update-profile', 'Api\Provider\ProviderRegisterController@updateProfile');
    Route::post('upload-document', 'Api\Provider\ProviderRegisterController@uploadDocument');

});

