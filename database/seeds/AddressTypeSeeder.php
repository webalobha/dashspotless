<?php

use App\Models\AddressType;
use Illuminate\Database\Seeder;

class AddressTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # initialize Address Types
        $addressTypes = ['Home', 'Work', 'Other'];

        # Store Data to model
        foreach ($addressTypes as $key => $addressType) {
        	AddressType::updateOrCreate(['name' => $addressType]);
        }
    }
}
