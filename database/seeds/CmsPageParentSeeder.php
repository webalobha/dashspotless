<?php

use Illuminate\Database\Seeder;
use App\Models\CmsPageParentType;

class CmsPageParentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # initialize Address Types
        $cmsPagesParents = ['Header', 'Sidebar', 'Footer', 'App'];

        # Store Data to model
        foreach ($cmsPagesParents as $key => $cmsPagesParent) {
        	CmsPageParentType::updateOrCreate(['name' => $cmsPagesParent]);
        }
    }
}
