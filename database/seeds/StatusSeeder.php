<?php

use App\Models\Status;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # initialize Status Array
        $statuses = ['Pending', 'Accept', 'Reject'];

        # Store Data to model
        foreach ($statuses as $key => $status) {
        	Status::updateOrCreate(['name' => $status]);
        }
    }
}
