<?php

use App\Models\TransactionType;
use Illuminate\Database\Seeder;

class TransactionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # initialize Transaction Types
        $transactionTypes = ['Debit', 'Credit'];

        # Store Data to model
        foreach ($transactionTypes as $key => $transactionType) {
        	TransactionType::updateOrCreate(['name' => $transactionType]);
        }
    }
}
