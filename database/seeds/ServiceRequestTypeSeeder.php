<?php

use Illuminate\Database\Seeder;
use App\Models\ServiceRequestType;

class ServiceRequestTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # initialize Service Types
        $serviceTypes = ['Immediate', 'Before Service Time'];

        # Store Data to model
        foreach ($serviceTypes as $key => $serviceType) {
        	ServiceRequestType::updateOrCreate(['name' => $serviceType]);
        }
    }
}
