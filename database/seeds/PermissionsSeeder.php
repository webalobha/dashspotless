<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    $datas =[
             ['name' => 'add_category',         'guard_name' => 'admin', 'module_id'=>'1', 'action' => 'add'    ],
             ['name' => 'edit_category',        'guard_name' => 'admin', 'module_id'=>'1', 'action' => 'edit'   ],
             ['name' => 'view_category',        'guard_name' => 'admin', 'module_id'=>'1', 'action' => 'view'   ],
             ['name' => 'delete_category',      'guard_name' => 'admin', 'module_id'=>'1', 'action' => 'delete' ],
             ['name' => 'payment_category',     'guard_name' => 'admin', 'module_id'=>'1', 'action' => 'payment'],
             ['name' => 'status_category',      'guard_name' => 'admin', 'module_id'=>'1', 'action' => 'status' ],
             ['name' => 'add_sub_category',     'guard_name' => 'admin', 'module_id'=>'2', 'action' => 'add'    ],
             ['name' => 'edit_sub_category',    'guard_name' => 'admin', 'module_id'=>'2', 'action' => 'edit'   ],
             ['name' => 'view_sub_category',    'guard_name' => 'admin', 'module_id'=>'2', 'action' => 'view'   ],
             ['name' => 'delete_sub_category',  'guard_name' => 'admin', 'module_id'=>'2', 'action' => 'delete' ],
             ['name' => 'payment_sub_category', 'guard_name' => 'admin', 'module_id'=>'2', 'action' => 'payment'],
             ['name' => 'status_sub_category',  'guard_name' => 'admin', 'module_id'=>'2', 'action' => 'status' ],
       ];

       foreach ($datas as $key => $data) {
       	# code...
       	# Store Data to model
        Permission::updateOrCreate([
                                     'name'       => $data['name'],
                                     'guard_name' => $data['guard_name'],
                                     'module_id'  => $data['module_id'],
                                     'action'     => $data['action'],
        ]);
       }
    }
}
