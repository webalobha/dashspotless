<?php

use Illuminate\Database\Seeder;
use App\Models\SubscriptionDuration;

class SubscriptionDurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # initialize Subscription Duration Array
        $subscriptionDurations = ['Weekly', 'Monthly'];

        # Store Data to model
        foreach ($subscriptionDurations as $key => $subscriptionDuration) {
        	SubscriptionDuration::updateOrCreate(['name' => $subscriptionDuration]);
        }
    }
}
