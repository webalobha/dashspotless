<?php

use App\Models\DocumentStatus;
use Illuminate\Database\Seeder;

class DocumentStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # initialize Status Array
        $statuses = ['Pending', 'Accept', 'Reject'];

        # Store Data to model
        foreach ($statuses as $key => $status) {
        	DocumentStatus::updateOrCreate(['name' => $status]);
        }
    }
}
