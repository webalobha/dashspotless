<?php

use App\Models\PaymentType;
use Illuminate\Database\Seeder;

class PaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # initialize Payment Types
        $payemntTypes = ['New', 'Reschedule', 'Cancel'];

        # Store Data to model
        foreach ($payemntTypes as $key => $payemntType) {
        	PaymentType::updateOrCreate(['name' => $payemntType]);
        }
    }
}
