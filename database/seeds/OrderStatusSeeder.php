<?php

use App\Models\OrderStatus;
use Illuminate\Database\Seeder;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # initialize Status Types
        $statusTypes = ['Booked', 
            'Cancelled',
            'Rejected', 
            'Picked By Provider', 
            'On the Way', 
            'Completed',
            'Rescheduled',
            'Rejected By Provider',
            'Searching Provider',
            'Accepted By Provider',
            'In Progress',
        ];

        # Store Data to model
        foreach ($statusTypes as $key => $statusType) {
        	OrderStatus::updateOrCreate(['name' => $statusType]);
        }
    }
}
