<?php

use App\Models\DiscountType;
use Illuminate\Database\Seeder;

class DiscountTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	# initialize Status Array
        $discountTypes = ['No Discount', 'Percentage', 'Flat Discount'];

        # Store Data to model
        foreach ($discountTypes as $key => $discountType) {
        	DiscountType::updateOrCreate(['name' => $discountType]);
        }
    }
}
