<?php

use Illuminate\Database\Seeder;
use App\Models\AssignOrderProviderStatus;

class AssignOrderToProviderTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # initialize Assign Order Status history 
        $statuses = [
        'Searching',
        'Accepted', 
        'Initiated', 
        'Completed', 
        'Rejected', 
        'Assigned to Other', 
        'On the Way', 
        'In Progress', 
        'Canceled', 
    ];

        # Store Data to model
        foreach ($statuses as $key => $status) {
        	AssignOrderProviderStatus::updateOrCreate(['name' => $status]);
        }
    }
}
