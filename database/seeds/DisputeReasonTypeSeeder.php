<?php

use App\Models\DisputeReasonType;
use Illuminate\Database\Seeder;

class DisputeReasonTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # initialize Address Types
        $disputeReasonTypes = ['User', 'Provider'];

        # Store Data to model
        foreach ($disputeReasonTypes as $key => $disputeReasonType) {
        	DisputeReasonType::updateOrCreate(['name' => $disputeReasonType]);
        }
    }
}
