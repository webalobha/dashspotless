<?php

use App\Models\ProviderStatus;
use Illuminate\Database\Seeder;

class ProviderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # initialize Address Types
        $providerStatuses = ['Pending', 'Approved', 'On Hold', 'Rejected'];

        # Store Data to model
        foreach ($providerStatuses as $key => $providerStatus) {
        	ProviderStatus::updateOrCreate(['name' => $providerStatus]);
        }
    }
}
