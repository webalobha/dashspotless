<?php

use App\Models\SmsProvider;
use Illuminate\Database\Seeder;

class SmsProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # initialize Providers
        $smsProviders = [
            [
                'name'          => 'twilio',
                'account_sid'   => 'twilioDummyAccount',
                'auth_token'    => '72637273hehksnjqhedqhjeqejatxeuqeaee',
                'from_number'   => '8929372398'
            ],
            [
                'name'          => 'nexmo',
                'account_sid'   => 'nexmoDummy',
                'auth_token'    => 'sqkj.dsy72et2dq62f5e621e6ds552s',
                'from_number'   => '8929372398'
            ]
        ];

        # Store Data to model
        foreach ($smsProviders as $key => $smsProvider) {
        	SmsProvider::updateOrCreate($smsProvider);
        }
    }
}
