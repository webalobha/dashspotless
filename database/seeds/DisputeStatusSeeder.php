<?php

use App\Models\DisputeStatus;
use Illuminate\Database\Seeder;

class DisputeStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # initialize Dispute Stattus
        $disputeStatuses = ['Open', 'Closed'];

        # Store Data to model
        foreach ($disputeStatuses as $key => $disputeStatus) {
        	DisputeStatus::updateOrCreate(['name' => $disputeStatus]);
        }
    }
}
