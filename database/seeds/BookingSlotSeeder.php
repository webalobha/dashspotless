<?php

use Carbon\Carbon;
use App\Models\BookingSlot;
use Illuminate\Database\Seeder;

class BookingSlotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $startTime  = '09:00';
    
        # Store Data to model
        $count = 0;
        while ($count <= 22 ) {
            # start the Time form 09::00 AM
            $time = Carbon::createFromTimeString($startTime)->format('H:i:s');

            # create the Booking Slot
        	BookingSlot::updateOrCreate(['time' => $time]);

            # Add 30 Minute after every Slot
            $startTime = Carbon::createFromTimeString($startTime)->addMinute(30)->format('H:i:s');

            # increase in Count
            $count ++;
        }
    }
}
