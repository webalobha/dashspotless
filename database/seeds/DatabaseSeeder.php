<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
            AdminSeeder::class,
            StatusSeeder::class,
            ModulesSeeder::class,
            PermissionsSeeder::class,
            SmsProviderSeeder::class,
            BookingSlotSeeder::class,
            PaymentTypeSeeder::class,
            AddressTypeSeeder::class,
            OrderStatusSeeder::class,
         	DiscountTypeSeeder::class,
            DisputeStatusSeeder::class,
            CmsPageParentSeeder::class,
            ProviderStatusSeeder::class,
         	DocumentStatusSeeder::class,
            TransactionTypeSeeder::class,
            DisputeReasonTypeSeeder::class,
            OrderDisputeStatusSeeder::class,
            IosPushEnvironmentSeeder::class,
            ServiceRequestTypeSeeder::class,
         	SubscriptionDurationSeeder::class,
            AssignOrderToProviderTypeSeeder::class,
         ]);
    }
}
