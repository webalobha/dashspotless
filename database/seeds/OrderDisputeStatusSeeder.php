<?php

use App\Models\OrderDsiputeStatus;
use Illuminate\Database\Seeder;

class OrderDisputeStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # initialize Order Dsipute Status
        $orderDisputeStatuses = ['Open', 'Resolved'];

        # Store Data to model
        foreach ($orderDisputeStatuses as $key => $orderDisputeStatus) {
        	OrderDsiputeStatus::updateOrCreate(['name' => $orderDisputeStatus]);
        }
    }
}
