<?php

use Illuminate\Database\Seeder;
use App\Models\IosPushEnvironment;

class IosPushEnvironmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # initialize Environments Array
        $environments = ['Development', 'Production'];

        # Store Data to model
        foreach ($environments as $key => $environment) {
        	IosPushEnvironment::updateOrCreate(['name' => $environment]);
        }
    }
}
