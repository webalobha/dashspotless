<?php

use App\Models\Module;
use Illuminate\Database\Seeder;

class ModulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # initialize Modules Array
        $modules = ['Category', 'Sub Category'];

        # Store Data to model
        foreach ($modules as $key => $module) {
        	Module::updateOrCreate(['name' => $module]);
        }
    }
}
