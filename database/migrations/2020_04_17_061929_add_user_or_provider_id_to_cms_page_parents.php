<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserOrProviderIdToCmsPageParents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_page_parents', function (Blueprint $table) {
            $table->unsignedInteger('user_or_provider_id')->index()->after('parent_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_page_parents', function (Blueprint $table) {
            $table->dropColumn('user_or_provider_id');
        });
    }
}
