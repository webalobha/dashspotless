<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocumentIdToProviderDocumentStatusHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('provider_document_status_history', function (Blueprint $table) {
            $table->unsignedInteger('document_id')->default(0)->index()->after('provider_document_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('provider_document_status_history', function (Blueprint $table) {
            $table->dropColumn('document_id');
        });
    }
}
