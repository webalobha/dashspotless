<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrderRescheduling extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_reschedule', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('order_id')->index();
            $table->timestamp('date_time_of_service');
            $table->unsignedInteger('time_slot_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_reschedule');
    }
}
