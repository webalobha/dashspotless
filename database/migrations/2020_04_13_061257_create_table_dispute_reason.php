<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDisputeReason extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispute_reasons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('status')->index()->default(true);
            $table->unsignedInteger('dispute_reason_type_id')->index();
            $table->string('service')->index()->nullable();
            $table->string('title')->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispute_reasons');
    }
}
