<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin', function (Blueprint $table) {
            $table->string('is_admin')->default(0)->after('remember_token');
            $table->string('forgot_password_otp')->nullable()->after('is_admin');
            $table->boolean('forgot_password_status')->default(false)->after('forgot_password_otp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin', function (Blueprint $table) {
            $table->dropColumn(['is_admin', 'forgot_password_otp', 'forgot_password_status']);
        });
    }
}
