<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableServiceSettingCancellationData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_setting_cancellation_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('service_setting_id')->index();
            $table->string('description')->index()->nullable();
            $table->string('hours')->index()->nullable();
            $table->string('amount')->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_setting_cancellation_data');
    }
}
