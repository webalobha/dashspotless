<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderRescheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_reschedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('order_id')->index();
            $table->dateTime('reschedule_date_time')->index();
            $table->boolean('is_date_time_change')->default(false)->index();
            $table->boolean('is_address_change')->default(false)->index();
            $table->string('reschedule_penalty_paid')->default('0')->index();
            $table->string('penalty_paid_from_wallet')->index()->default('0');
            $table->string('penalty_paid_online')->index()->default('0');
            $table->string('address_of_service')->index()->nullable();
            $table->string('pin_code_service')->index()->nullable();
            $table->string('latitude')->index()->nullable();
            $table->string('longitude')->index()->nullable();
            $table->string('name')->index()->nullable();
            $table->string('mobile')->index()->nullable();
            $table->string('city')->index()->default(0);
            $table->string('state')->index()->default(0);
            $table->string('country')->index()->default(0);
            $table->boolean('status')->index()->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_reschedule');
    }
}
