<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableCmsPagesParentName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_pages_parent', function (Blueprint $table) {
            Schema::rename('cms_pages_parent', 'cms_page_parent_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_pages_parent', function (Blueprint $table) {
             Schema::rename( 'cms_page_parent_type', 'cms_pages_parent');
        });
    }
}
