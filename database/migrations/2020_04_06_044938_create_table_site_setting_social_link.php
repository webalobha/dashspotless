<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSiteSettingSocialLink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_setting_social_link', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('is_social_login')->default(false)->index();
            $table->string('facebook_app_id')->index()->nullable();
            $table->string('facebook_app_secret')->index()->nullable();
            $table->string('google_client_id')->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_setting_social_link');
    }
}
