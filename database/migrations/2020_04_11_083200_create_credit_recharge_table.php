<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditRechargeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_recharge', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('points')->nullable()->index();
            $table->string('amount_per_point')->nullable()->index();
            $table->boolean('status')->default(true)->index();
            $table->unsignedInteger('added_by')->index()->default(0);
            $table->unsignedInteger('updated_by')->index()->default(0);
            $table->unsignedInteger('deleted_by')->index()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_recharge');
    }
}
