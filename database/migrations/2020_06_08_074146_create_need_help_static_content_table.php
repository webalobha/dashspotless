<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNeedHelpStaticContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('need_help_static_content', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string('title')->nullable()->index();
            $table->longtext('content')->nullable();
            $table->boolean('status')->index()->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('need_help_static_content');
    }
}
