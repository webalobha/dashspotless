<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToAssignOrderToProvider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assign_order_to_provider', function (Blueprint $table) {
            $table->boolean('status')->default(true)->after('provider_id')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assign_order_to_provider', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
