<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProviderBankDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('provider_bank_details','confirm_account_number')) {
            Schema::dropIfExists('provider_bank_details');
        }

        Schema::create('provider_bank_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('provider_id')->index();
            $table->string('bank_name')->index()->nullable();
            $table->string('ifsc_code')->index()->nullable();
            $table->string('account_number')->index()->nullable();
            $table->string('account_holder_name')->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provider_bank_details');
    }
}
