<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProviderBankDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provider_bank_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('provider_id')->index();
            $table->string('account_name')->index();
            $table->string('account_number')->index();
            $table->string('confirm_account_number')->index()->nullable();
            $table->string('swift_code')->index()->nullable();
            $table->unsignedInteger('updated_by')->index()->default(0);
            $table->unsignedInteger('deleted_by')->index()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provider_bank_details');
    }
}
