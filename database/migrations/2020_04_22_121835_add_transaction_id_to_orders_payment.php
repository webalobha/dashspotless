<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionIdToOrdersPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders_payment', function (Blueprint $table) {
            $table->string('transaction_id')->after('order_id')->index()->nullable();
            $table->string('card_name')->after('transaction_id')->index()->nullable();
            $table->string('card_number')->after('card_name')->index()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders_payment', function (Blueprint $table) {
            $table->dropColumns(['transaction_id', 'card_name', 'card_number']);
        });
    }
}
