<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderIdToAssignOrderToProviderHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assign_order_to_provider_history', function (Blueprint $table) {
            $table->unsignedInteger('order_id')->after('id')->index()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assign_order_to_provider_history', function (Blueprint $table) {
            $table->dropColumn('order_id');
        });
    }
}
