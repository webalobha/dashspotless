<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableSubCategoriesExpertiseInAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sub_categories_expertise_in', function (Blueprint $table) {
            $table->unsignedInteger('sub_category_id')->after('id')->index()->nullable();
            $table->string('title')->after('sub_category_id')->index()->nullable();
            $table->string('avtar_name')->after('title')->index()->nullable();
            $table->string('avtar_path')->after('avtar_name')->index()->nullable();
            $table->unsignedInteger('added_by')->index()->default(0)->after('avtar_path');
            $table->unsignedInteger('updated_by')->index()->default(0)->after('added_by');
            $table->unsignedInteger('deleted_by')->index()->default(0)->after('updated_by');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sub_categories_expertise_in', function (Blueprint $table) {
            $table->dropColumn(['sub_category_id', 'title', 'avtar_name', 'avtar_path', 'added_by', 'updated_by', 'deleted_by']);
        });
    }
}
