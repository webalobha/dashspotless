<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSitePaymentSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_payment_setting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('is_cash')->default(false)->index();
            $table->boolean('is_card')->default(false)->index();
            $table->string('stripe_secret_key')->index()->nullable();
            $table->string('stripe_publishable_key')->index()->nullable();
            $table->string('stripe_currency')->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_payment_setting');
    }
}
