<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAwardAndCertificate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('awards_and_certificate', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('provider_id')->index();
            $table->string('image_name')->indeX()->nullable();
            $table->string('image_path')->indeX()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('awards_and_certificate');
    }
}
