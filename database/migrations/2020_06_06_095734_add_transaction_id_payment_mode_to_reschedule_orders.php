<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionIdPaymentModeToRescheduleOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_reschedules', function (Blueprint $table) {
            $table->string('transaction_id')->index()->nullable()->after('penalty_paid_online');
            $table->string('payment_mode')->index()->nullable()->after('transaction_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reschedule_orders', function (Blueprint $table) {
            $table->dropColumn(['transaction_id', 'payment_mode']);
        });
    }
}
