<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRadioOrCheckToSubCategoryVariantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sub_category_variants', function (Blueprint $table) {
            $table->unsignedinteger('radio_or_check')->default(0)->index()->after('label');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sub_category_variants', function (Blueprint $table) {
           $table->dropColumn('radio_or_check');
        });
    }
}
