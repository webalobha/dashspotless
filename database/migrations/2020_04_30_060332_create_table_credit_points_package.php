<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCreditPointsPackage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_points_package', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index()->nullable();
            $table->string('number_of_points')->index();
            $table->string('actual_price')->index();
            $table->string('dicount_title')->index()->nullable();
            $table->unsignedInteger('discount_type_id')->index()->default(0);
            $table->string('discount_given')->index();
            $table->string('discounted_amount')->index();
            $table->boolean('status')->index()->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_points_package');
    }
}
