<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnUserIdTiProviderIdToProviderAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('provider_addresses', 'user_id')) {
            Schema::table('provider_addresses', function (Blueprint $table) {
                $table->renameColumn('user_id', 'provider_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('provider_addresses', 'provider_id')) {
            Schema::table('provider_addresses', function (Blueprint $table) {
                $table->renameColumn('provider_id', 'user_id');
            });
        }
    }
}
