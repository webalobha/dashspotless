<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisputeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispute', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('disputable_id')->index();
            $table->string('disputable_type')->index();
            $table->unsignedInteger('dispute_status_id')->index();
            $table->string('order_id')->index();
            $table->string('reason')->index()->nullable();
            $table->string('comment')->index()->nullable();
            $table->string('refund_amount')->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispute');
    }
}
