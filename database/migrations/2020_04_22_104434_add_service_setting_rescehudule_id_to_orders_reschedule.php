<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddServiceSettingRescehuduleIdToOrdersReschedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders_reschedule', function (Blueprint $table) {
            $table->unsignedInteger('service_setting_rescedule_id')->default(0)->index()->after('time_slot_id');
            $table->string('reschedule_amount_charge')->index()->after('service_setting_rescedule_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders_reschedule', function (Blueprint $table) {
            $table->dropColumn(['service_setting_rescedule_id', 'reschedule_amount_charge']);
        });
    }
}
