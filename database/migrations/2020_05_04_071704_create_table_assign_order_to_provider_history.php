<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAssignOrderToProviderHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assign_order_to_provider_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('assign_orderprovider_id')->index();
            $table->unsignedInteger('assign_order_status_id')->index();
            $table->string('reason')->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assign_order_to_provider_history');
    }
}
