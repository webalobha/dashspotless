<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSiteSettingCompProfileLink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_setting_comp_profile_link', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('playstore_user_link')->index()->nullable();
            $table->string('playstore_provider_link')->index()->nullable();
            $table->string('appstore_user_link')->index()->nullable();
            $table->string('appstore_provider_link')->index()->nullable();
            $table->string('facebook_link')->index()->nullable();
            $table->string('twitter_link')->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_setting_comp_profile_link');
    }
}
