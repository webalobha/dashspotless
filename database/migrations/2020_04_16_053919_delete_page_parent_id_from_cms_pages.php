<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeletePageParentIdFromCmsPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('cms_pages', 'page_parent_id')) {
            Schema::table('cms_pages', function (Blueprint $table) {
                $table->dropColumn('page_parent_id');
            });
        }
     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(!Schema::hasColumn('cms_pages', 'page_parent_id')) {
            Schema::table('cms_pages', function (Blueprint $table) {
                $table->unsignedInteger('page_parent_id')->index();
            });
        }
    }
}
