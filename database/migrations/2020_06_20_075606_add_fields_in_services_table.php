<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->boolean('is_offer')->default(false)->index()->after('icon_path');
            $table->date('offer_from')->index()->nullable()->after('is_offer');
            $table->date('offer_to')->index()->nullable()->after('offer_from');
            $table->string('offer_banner')->index()->nullable()->after('offer_to');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->dropColumn(['is_offer', 'offer_from', 'offer_to', 'offer_banner']);
        });
    }
}
