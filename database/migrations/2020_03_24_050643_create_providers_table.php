<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name')->index()->nullable();
            $table->string('middle_name')->index()->nullable();
            $table->string('last_name')->index()->nullable();
            $table->string('country_code')->index()->nullable();
            $table->string('mobile_number')->index()->nullable();
            $table->string('email')->index()->nullable();
            $table->boolean('status')->default(true)->index();
            $table->string('age')->index()->nullable();
            $table->unsignedInteger('gender')->index()->nullable();
            $table->string('latitude')->index()->nullable();
            $table->string('longitude')->index()->nullable();
            $table->string('android_key')->index()->nullable();
            $table->string('ios_key')->index()->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('providers');
    }
}
