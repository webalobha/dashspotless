<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToSiteSettingMailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('site_setting_mail', function (Blueprint $table) {
            $table->string('twilio_account_sid')->index()->nullable()->after('sms_provider_id');
            $table->string('twilio_auth_token')->index()->nullable()->after('twilio_account_sid');
            $table->string('twilio_from_number')->index()->nullable()->after('twilio_auth_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('site_setting_mail', function (Blueprint $table) {
            $table->dropColumn(['twilio_account_sid', 'twilio_auth_token', 'twilio_from_number']);
        });
    }
}
