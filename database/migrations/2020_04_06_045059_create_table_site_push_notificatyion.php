<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSitePushNotificatyion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_setting_push_notification', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('is_send_mail')->default(false)->index();
            $table->boolean('is_send_sms')->default(false)->index();
            $table->string('ios_push_provider_pem')->index()->nullable();
            $table->string('ios_push_password')->index()->nullable();
            $table->string('android_push_key')->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_setting_push_notification');
    }
}
