<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->index();
            $table->string('payment_mode')->index()->nullable();
            $table->string('subscription_discount')->index()->nullable();
            $table->unsignedInteger('provider_id')->index()->default(0);
            $table->unsignedInteger('subscription_id')->index()->default(0);
            $table->unsignedInteger('user_address_id')->index();
            $table->string('date_of_service')->index();
            $table->unsignedInteger('time_slot_id')->index();
            $table->string('paid_amount')->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
