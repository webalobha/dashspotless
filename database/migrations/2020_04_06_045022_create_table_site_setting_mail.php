<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSiteSettingMail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_setting_mail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('is_send_mail')->default(false)->index();
            $table->boolean('is_send_sms')->default(false)->index();
            $table->boolean('status')->default(false)->index();
            $table->string('sms_provider_id')->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_setting_mail');
    }
}
