<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropFieldVariantIdFormConsultVisitBookingDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consult_visit_booking_details', function (Blueprint $table) {
             $table->dropColumn('variant_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consult_visit_booking_details', function (Blueprint $table) {
            $table->unsignedInteger('variant_id')->index();
        });
    }
}
