<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_countries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('country_id')->index();
            $table->boolean('status')->default(false)->index();
            $table->string('currency_code')->index()->nullable();
            $table->string('currency_symbol')->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_countries');
    }
}
