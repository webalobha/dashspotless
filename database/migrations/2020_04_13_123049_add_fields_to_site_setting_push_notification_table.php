<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToSiteSettingPushNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('site_setting_push_notification', function (Blueprint $table) {
            $table->string('ios_push_environment')->index()->nullable()->after('is_send_sms');
            $table->string('ios_push_user_pem')->index()->nullable()->after('ios_push_environment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('site_setting_push_notification', function (Blueprint $table) {
            $table->dropColumn(['ios_push_environment', 'ios_push_user_pem']);
        });
    }
}
