<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProviderAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provider_addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('country_id')->index()->default(0);
            $table->unsignedInteger('state_id')->index()->default(0);
            $table->string('city')->index()->nullable();
            $table->string('mobile')->index()->nullable();
            $table->string('pin_code')->index()->nullable();
            $table->string('landmark')->index()->nullable();
            $table->text('address')->nullable();
            $table->unsignedInteger('address_type')->default(0);
            $table->string('address_other_type_description')->index()->nullable();
            $table->boolean('status')->index()->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provider_addresses');
    }
}
