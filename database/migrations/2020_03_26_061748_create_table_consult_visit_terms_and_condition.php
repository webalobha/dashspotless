<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableConsultVisitTermsAndCondition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consult_visit_terms_and_condition', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('category_id')->index();
            $table->unsignedInteger('sub_category_id')->index();
            $table->unsignedInteger('variant_id')->index();
            $table->string('title')->index()->nullable();
            $table->text('description')->nullable();
            $table->unsignedInteger('added_by')->index()->default(0);
            $table->unsignedInteger('updated_by')->index()->default(0);
            $table->unsignedInteger('deleted_by')->index()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consult_visit_terms_and_condition');
    }
}
