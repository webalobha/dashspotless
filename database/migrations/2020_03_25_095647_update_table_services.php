<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->unsignedInteger('category_id')->index()->after('id');
            $table->unsignedInteger('sub_category_id')->index()->after('category_id');
            $table->unsignedinteger('variant_id')->default(0)->after('sub_category_id')->index();
            $table->string('description')->index()->after('title');
            $table->string('discount')->after('price')->nullable();
            $table->string('discounted_price')->after('discount')->nullable();
            $table->text('tool_tip_content')->after('time_taken_minutes')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->dropColumn([
                'category_id',
                'sub_category_id' ,
                'variant_id', 
                'description', 
                'discount', 
                'discounted_price', 
                'tool_tip_content'
            ]);
        });
    }
}
