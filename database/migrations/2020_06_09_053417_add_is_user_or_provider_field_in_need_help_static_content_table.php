<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsUserOrProviderFieldInNeedHelpStaticContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('need_help_static_content', function (Blueprint $table) {
            $table->integer('is_user_or_provider')->index()->default(0)->after('content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('need_help_static_content', function (Blueprint $table) {
            $table->dropColumn(['is_user_or_provider']);
        });
    }
}
