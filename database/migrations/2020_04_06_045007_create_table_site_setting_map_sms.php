<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSiteSettingMapSms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_setting_map_sms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('browser_map_key')->index()->nullable();
            $table->string('secret_map_key')->index()->nullable();
            $table->string('android_map_key')->index()->nullable();
            $table->string('ios_map_key')->index()->nullable();
            $table->string('fb_app_version')->index()->nullable();
            $table->string('fb_app_secret')->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_setting_map_sms');
    }
}
