<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProviderBoughtPackages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provider_bought_packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('provider_id')->index();
            $table->unsignedInteger('credit_package_id')->index()->default(0);
            $table->string('no_of_points')->index()->nullable();
            $table->string('package_name')->index()->nullable();
            $table->string('actual_price')->index()->nullable();
            $table->string('dicount_title')->index()->nullable();
            $table->string('discount_given')->index()->nullable();
            $table->string('discounted_amount')->index()->nullable();
            $table->string('transaction_id')->index()->nullable();
            $table->string('payment_type')->index()->nullable();
            $table->string('payment_amount')->index()->nullable();
            $table->string('card_type')->index()->nullable();
            $table->string('card_name')->index()->nullable();
            $table->string('account_holder_name')->index()->nullable();
            $table->string('bank_name')->index()->nullable();
            $table->string('card_number')->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provider_bought_packages');
    }
}
