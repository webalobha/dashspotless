<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableConsultVisitBookingDetailImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consult_visit_booking_details_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('category_id')->index();
            $table->unsignedInteger('sub_category_id')->index();
            $table->unsignedInteger('variant_id')->index();
            $table->string('reference_image_name')->index()->nullable();
            $table->string('reference_image_path')->index()->nullable();
            $table->unsignedInteger('added_by')->index()->default(0);
            $table->unsignedInteger('updated_by')->index()->default(0);
            $table->unsignedInteger('deleted_by')->index()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consult_visit_booking_details_images');
    }
}
