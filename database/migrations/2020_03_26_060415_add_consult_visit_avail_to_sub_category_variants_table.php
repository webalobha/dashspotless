<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConsultVisitAvailToSubCategoryVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sub_category_variants', function (Blueprint $table) {
            $table->boolean('consult_visit_avail')
                  ->default(false)
                  ->index()
                  ->after('radio_or_check');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sub_category_variants', function (Blueprint $table) {
            $table->dropColumn('consult_visit_avail');
        });
    }
}
