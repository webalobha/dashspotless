<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateVariantIdFieldNameToConsultVisitTermsAndCondition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('consult_visit_terms_and_condition', 'variant_id')) {
             Schema::table('consult_visit_terms_and_condition', function (Blueprint $table) {
                $table->renameColumn('variant_id', 'consultant_visit_id');
            });
        }
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('consult_visit_terms_and_condition', 'consultant_visit_id')) {
            Schema::table('consult_visit_terms_and_condition', function (Blueprint $table) {
                 $table->renameColumn( 'consultant_visit_id', 'variant_id');
            });
        }
    }
}
