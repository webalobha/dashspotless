<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSiteSettingService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_setting_service', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('provider_accept_timeout')->index()->nullable();
            $table->string('provider_search_radius')->index()->nullable();
            $table->boolean('serve_otp')->default(false)->index();
            $table->string('booking_id_prefix')->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_setting_service');
    }
}
