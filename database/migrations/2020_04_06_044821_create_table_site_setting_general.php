<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSiteSettingGeneral extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_setting_general', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index()->nullable();
            $table->string('site_icon_name')->index()->nullable();
            $table->string('site_icon_img_path')->index()->nullable(); 
            $table->string('site_logo_name')->index()->nullable();
            $table->string('site_logo_img_path')->index()->nullable();
            $table->string('contact_number')->index()->nullable();
            $table->string('contact_email')->index()->nullable();
            $table->string('sos_number')->index()->nullable();
            $table->text('copyright_content')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_setting_general');
    }
}
