<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableUserAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('user_addresses', 'city')) {
            Schema::table('user_addresses', function (Blueprint $table) {
                $table->renameColumn('city', 'city_id');    
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('user_addresses', 'city_id')) {
            Schema::table('user_addresses', function (Blueprint $table) {
                $table->renameColumn('city_id', 'city'); 
            });
        }
    }
}
