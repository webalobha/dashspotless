<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_setting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('service_request_type_id')->index();
            $table->unsignedInteger('day_or_hours')->index();
            $table->string('day_count')->index()->nullable();
            $table->string('hour_count')->index()->nullable();
            $table->string('proximate_range')->index()->nullable();
            $table->string('payment_limit_provider')->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_setting');
    }
}
