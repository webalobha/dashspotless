<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePackageDetailTitles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_detail_titles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('package_id')->index();
            $table->string('title')->index()->nullable();
            $table->unsignedInteger('added_by')->index();
            $table->unsignedInteger('updated_by')->index()->default(0);
            $table->unsignedInteger('deleted_by')->index()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_detail_titles');
    }
}
