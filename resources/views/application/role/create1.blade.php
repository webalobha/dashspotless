  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Role</h4>
      </div>
      <div class="modal-body">
               <form class="form-horizontal" method="post" action="{{ action('AdminController\RoleController@store') }}" id="add_model">
                @csrf
      <div class="row">
        <div class="col-md-12">
          
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Role </label>

                  <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" id="" required>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              
            </div>
      </div>
      
      <div class="box-footer text-right">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-main">Add</button>
              </div>
              <!-- /.box-footer -->
            </form>
      </div>
    </div>

  </div>