@extends('application.layouts.app')
@section('title','Update Role')
@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <h1>
        Provide Permission
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Role</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="heading-bg">
        <h3>Update Role </h3>
      </div>
      <!-- Info boxes -->
      <form class="form-horizontal" method="post" action="{{ action('AdminController\RoleController@edit', [$role->id]) }}" id="add_form">
                @csrf
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Role Name<span class="required">*</span></label>
                  <div class="col-sm-8">
                    <input type="text" name="name" class="form-control" id="" value="{{ $role->name ?? '' }}" required>
                  </div>
                </div>
                <div class="form-group">
                  <div class="table-responsive">
                  <table class="table table-bordered text-center permission_table">
                    <tr>
                      <td style="font-size: 16px; font-weight: 700;">Module</td>
                      <td style="font-size: 16px; font-weight: 700;">Add</td>
                      <td style="font-size: 16px; font-weight: 700;">Edit</td>
                      <td style="font-size: 16px; font-weight: 700;">View</td>
                      <td style="font-size: 16px; font-weight: 700;">Delete</td>
                      <td style="font-size: 16px; font-weight: 700;">Payment</td>
                      <td style="font-size: 16px; font-weight: 700;">Active/Inactive</td>
                    </tr>
                    @php $data = []; @endphp 
                    @foreach($roleHasPermissions as $roleHasPermission)
                    @php
                    $data[] =    $roleHasPermission->name;
                    @endphp       
                    @endforeach
                    @foreach($modules as $module)
                    <tr>
                      <td><b>{{ $module->name ?? '' }}</b></td>
                      @php
                       $permissions = Spatie\Permission\Models\Permission::where('module_id', $module->id)->orderBy('id', 'asc')->get();
                      @endphp
                      
                      @foreach($permissions as $permission)
                       <td><input type="checkbox" name="permission[]" value="{{ $permission->name ?? '' }}" @if(in_array($permission->name, $data)) checked @endif></td>
                      @endforeach
                    </tr>
                    @endforeach
                  </table>
                </div>
                </div>

              <div class="box-footer text-right">
                <button type="button" class="btn btn-default">Clear</button>
                <button type="submit" class="btn btn-main">Update</button>
              </div>
              </div>
              <!-- /.box-body -->
              
            </div>
        <div class="col-md-2"></div>
      </div>
      
              <!-- /.box-footer -->
            </form>



  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

    <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Images</h4>
      </div>
      <div class="modal-body">
               <form class="form-horizontal">
      <div class="row">
        <div class="col-md-12">
          
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-4 control-label">Choose Image</label>

                  <div class="col-sm-8">
                    <input type="file" name="" class="form-control" id="" placeholder="Description">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              
            </div>
      </div>
      
      <div class="box-footer text-right">
                <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-main">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
      </div>
    </div>

  </div>
</div>
@endsection
