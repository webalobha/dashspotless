@extends('application.layouts.app')
@section('title','Role')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
   
        

      <!-- <h1>
        Add Sub Category
      </h1> -->
     
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Role</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 text-right">
         <a href="{{ action('AdminController\RoleController@create') }}"  class="btn btn-main" ><i class="fa fa-plus"></i> Add Role</a>
        </div>
      </div>

          <div class="heading-bg" style="margin-top: 30px;">
            <h3></i>Role</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered table-striped display nowrap">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Role</th>
                  <!-- <th>Status</th> -->
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php $count = 1; @endphp
                @foreach($roles as $role)
                <tr>
                  <td>{{ $count++ }}</td>
                  <td>{{ $role->name ?? '' }}</td>
                  <!-- <td>Active</td> -->
                  <td>
                    <a href="{{ action('AdminController\RoleController@update',[$role->id]) }}" type="button" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i></a>
                    <a id="delete" href="{{ action('AdminController\RoleController@delete', [$role->id]) }}" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
                    <!-- <button type="button" class="btn btn-default btn-xs">Inactive</button> -->
                  </td>
                </tr>
                @endforeach
              </tbody>
              </table>
            </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection