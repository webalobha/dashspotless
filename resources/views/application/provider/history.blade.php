@extends('application.layouts.app')
@section('title','Provider History')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Provider
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Provider</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">  
       <div class="heading-bg" style="margin-bottom: 20px;">
            <h3>View Provider</h3>
          </div>
   <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="{{asset($provider->image_path ?? 'dist/img/avatar.png')}}" alt="User profile picture">

              <h3 class="profile-username text-center"><b>{{$provider->first_name??''}}</b></h3>
              <p class="text-muted text-center">{{$provider->email??''}}</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Mobile</b> <span class="pull-right">1234567890</span>
                </li>
                <li class="list-group-item">
                  <b>Status</b> <span class="pull-right"><button class="btn btn-xs btn-{{$provider->status=='1'?'success':'warning'}}">{{$provider->status=='1'?'Active':'De-Active'}}</button></span>    
                </li>
                <!-- <li class="list-group-item">
                  <b>User Rating</b> 
                  <span class="pull-right">
                    <ul class="rating_list">
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                    </ul>
                  </span>
                </li> -->
                <!-- <li class="list-group-item">
                  <b>Service Address</b>
                  <p class="text-justify">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  </p>
                </li> -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
           <!-- Profile Image -->
          <div class="box">
            <div class="box-body box-profile">
              <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#p_profile">Profile</a></li>
              <li><a data-toggle="tab" href="#p_service">Service</a></li>
              <li><a data-toggle="tab" href="#bankdetail">Bank Details</a></li>
              <li><a data-toggle="tab" href="#servicehistory">Service History</a></li>
            </ul>

            <div class="tab-content">
              <div id="p_profile" class="tab-pane fade in active">
                <h3>Documents</h3>
                <table class="table table-bordered">
                  
                  @foreach($providerDocuments as $providerDocument)
                  @php
                    $approveStatus = $providerDocument->histories->isNotEmpty() ?
                                     $providerDocument->histories->last()->status_approve : false;
                    $rejectStatus =  $providerDocument->histories->isNotEmpty() ?
                                      $providerDocument->histories->last()->status_reject : false;
                    $name = $providerDocument->documentName->name ?? '';
                  @endphp
                  @if($name != '')
                    <tr>
                      <td >{{ $providerDocument->documentName->name ?? '' }}</td>
                      <td>
                        <a class="btn btn-sm btn-primary" href="{{ asset($providerDocument->document_image_path ?? '') }}" target="__blank">Download</a> 
                        @if(!$approveStatus and !$rejectStatus)
                        <a type="button" href="{{ action('AdminController\ProviderController@postStatus', [$providerDocument->id, '2']) }}" class="btn btn-sm btn-success add_model" >Accept</a> 

                        <a type="button" href="{{ action('AdminController\ProviderController@postStatus', [$providerDocument->id, '3']) }}" class="btn btn-sm btn-danger add_model" >Reject</a>
                        @else
                        <a type="button" @if($rejectStatus) href="{{ action('AdminController\ProviderController@postStatus', [$providerDocument->id, '2']) }}" @endif class="btn btn-sm btn-success disabled">Accept</a> 

                        <a type="button" @if($approveStatus) href="{{ action('AdminController\ProviderController@postStatus', [$providerDocument->id, '3']) }}" @endif class="btn btn-sm btn-danger disabled">Reject</a>
                        @endif
                      </td>
                    </tr>
                  @endif
                  @endforeach
                </table>
                <h3>Awards & Certifications</h3>
                <div class="row">
                   @foreach($AwardAndCertificate as $Award)
                  <div class="col-md-4"><img src="{{asset($Award->image_path)}}" style="width: 100%;"></div>
                  @endforeach
                </div>
                <h3>Work Photos</h3>
                <div class="row">
                  @foreach($work_photos as $work)
                  <div class="col-md-4"><img src="{{asset($work->image_path)}}" style="width: 100%;"></div>
                  @endforeach
                </div>
              </div>
              <div id="p_service" class="tab-pane fade">
                <h3><i class="fa fa-bolt" style="margin-right: 10px;"></i>Service</h3>
                <table class="table table-bordered">
                  <thead>
                    <th>#</th>
                    <th>Category</th>
                    <th>Sub Category</th>
                    <th>Enrolled On</th>
                    <th>Status</th>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Cleaning</td>
                      <td>Sofa Cleaning</td>
                      <td>20 March 2020</td>
                      <td>
                        <div>
                        <label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label>
                      </div>
                      </td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>Cleaning</td>
                      <td>Room Cleaning</td>
                      <td>20 March 2020</td>
                      <td>
                        <div>
                        <label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label>
                      </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div id="bankdetail" class="tab-pane fade">
                <h3><i class="fa fa-university" style="margin-right: 10px;"></i>Bank Details</h3>
                <table class="table table-bordered">
                  <!-- <tr>
                    <td style="width: 30%;">Payee Name</td>
                    <td>{{$provider->email??''}}</td>
                  </tr> -->
                  <tr>
                    <td>Bank Name</td>
                    <td>{{$providerAccount->bank_name ??''}}</td>
                  </tr>
                  <tr>
                    <td>Account Number</td>
                    <td>{{$providerAccount->account_number ??''}}</td>
                  </tr>
                  <tr>
                    <td>IFSC Code</td>
                    <td>{{$providerAccount->ifsc_code??''}}</td>
                  </tr>
                   <tr>
                    <td>Account Holder Name</td>
                    <td>{{$providerAccount->account_holder_name??''}}</td>
                  </tr>
                </table>
              </div>
              <div id="servicehistory" class="tab-pane fade">
                <h3><i class="fa fa-university" style="margin-right: 10px;"></i>Service History</h3>
                <table class="table table-bordered">
              <thead>
                <th>S. No</th>
                <th>Customer Name</th>
                <th>Customer Mobile</th>
                <th>Order Number</th>
                <th>Services</th>
                <th>Date & Time</th>
                <th>Amount</th>
                <th>Action</th>
              </thead>
              <tbody>
                @php $count =1; @endphp
                @foreach($providerServiceHistories as $providerService)
                @foreach($providerService->orderServices as $providerServiceHistory)
                <tr>
                  <td>{{ $count++ }}</td>
                  <td>{{ $providerServiceHistory->serviceOrdersData->user->first_name ?? '' }}</td>
                  <td>{{ $providerServiceHistory->serviceOrdersData->user->mobile_number ?? '' }}</td>
                  <td>{{ $providerServiceHistory->serviceOrdersData->unique_order_id ?? '' }}</td>
                  <td>{{ $providerServiceHistory->service->title ?? '' }}</td>
                  <td>{{ $providerServiceHistory->serviceOrdersData->date_time_of_service ?? '' }}</td>
                  <td>{{ $providerServiceHistory->serviceOrdersData->paid_amount ?? '' }}</td>
                  <td><button class="btn btn-primary btn-sm">View</button></td>
                </tr>
                @endforeach
                @endforeach
              </tbody>
            </table>
             <div class="box-body box-profile">
              <div class="row service_row">
                <div class="col-sm-4"><b>Customer Name</b></div>
                <div class="col-sm-8">Saloni</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-4"><b>Mobile Number</b></div>
                <div class="col-sm-8">2324354556</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-4"><b>Email</b></div>
                <div class="col-sm-8">demo@gmail.com</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-12"><h4>Service Details</h4></div>
              </div>
              <div class="row service_row">
                <div class="col-sm-4"><b>Service Name</b></div>
                <div class="col-sm-8">lorem ipsum</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-4"><b>Service Duration</b></div>
                <div class="col-sm-8">lorem ipsum</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-4"><b>Categoy Name</b></div>
                <div class="col-sm-8">lorem ipsum</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-4"><b>Sub Category Name</b></div>
                <div class="col-sm-8">lorem ipsum</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-4"><b>Booking Amount</b></div>
                <div class="col-sm-8">lorem ipsum</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-4"><b>Booking Date & Time</b></div>
                <div class="col-sm-8">lorem ipsum</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-4"><b>Rescheduled Date & Time</b></div>
                <div class="col-sm-8">lorem ipsum</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-4"><b>Customer Service Address</b></div>
                <div class="col-sm-8">lorem ipsum</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-4"><b>Booking Status</b></div>
                <div class="col-sm-8">New/Completed/Cancelled</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-4"><b>Payment Mode:</b></div>
                <div class="col-sm-8">lorem ipsum</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-4"><b>Customer's Rating & Reviews</b></div>
                <div class="col-sm-8">lorem ipsum</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-4"><b>Provider's Rating & Reviews</b></div>
                <div class="col-sm-8">lorem ipsum</div>
              </div>
            </div>
            <!-- /.box-body -->
              </div>
            </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('js')
<script type="text/javascript">

function changeStatus($id) {

      var id = $id; 
       $.ajax({
         url: '{{ action("AdminController\UserController@status") }}?id='+id,
         type: "GET",
         data : {"_token":"{{ csrf_token() }}"},
         dataType: "json",
         success: function(response) { 
            if (response.success == 200) {
                toastr.success(response.message); 
               
                //location.reload();
            } else {
                toastr.error(response.message);
            }
          }

    })
  }
  
</script> 




@endsection