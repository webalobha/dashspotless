@extends('application.layouts.app')
@section('title','Category')
@section('content')
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Add Manager
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">View Dispute</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       
      <div class="heading-bg">
        <h3> View Dispute</h3>
      </div>

      <div class="box-body">
      <!-- Info boxes -->
       <form class="form-horizontal" action="{{ action('AdminController\DisputeReasonController@edit',[$DisputeReason->id]) }}" method="post" id="add_form">
        @csrf
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Service<span class="required">*</span></label>
                  <div class="col-sm-10">
                   <select class="form-control" name="service" required disabled>

                     <option value="" >Select Service</option>

                    @foreach($services as $services)
                     <option value="{{$services->id}}" {{$DisputeReason->service==$services->id?'selected':''}}>{{$services->title}}</option>
@endforeach  
                   </select>  
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Dispute Type<span class="required">*</span></label>
                  <div class="col-sm-10">
                    <select class="form-control" name="dispute_reason_type_id" required disabled>
                     <option value="">Select Dispute Type</option>

@foreach($DisputeReasonType as $reasonType)
<option value="{{$reasonType->id}}" {{$DisputeReason->dispute_reason_type_id==$reasonType->id?'selected':''}}>{{$reasonType->name}}</option>
@endforeach                    
                   </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Reason<span class="required">*</span></label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name = "title" value="{{$DisputeReason->title}}" required disabled>{{$DisputeReason->title}}</textarea>
                  </div> 
                </div>
                 <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Status<span class="required">*</span></label>
                  <div class="col-sm-10">
                   <select class="form-control" name="status" required disabled>
                     <option value="">Select Status</option>
                     <option value='1' {{$DisputeReason->status=='1'?'selected':''}}>Active</option>
                     <option value='0'  {{$DisputeReason->status=='0'?'selected':''}}>Inactive</option>
                   </select>
                  </div>
                </div>
                
              <!-- /.box-footer -->
              </div>
              <!-- /.box-body -->
              
            </div>
        <div class="col-md-2"></div>
      
            </form>
            </div>
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection