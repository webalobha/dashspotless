@extends('application.layouts.app')
@section('title','Category')
@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
   
        

      <!-- <h1>
        Add Sub Category
      </h1> -->
     
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Disputes</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 text-right">
         <a href="{{ url('add-dispute-reason') }}" class="btn btn-main"><i class="fa fa-plus"></i> Add Disputes</a>
        </div>
      </div>

          <div class="heading-bg" style="margin-top: 30px;">
            <h3>Disputes</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered table-striped display nowrap">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Dispute Reason</th>
                  <th>Dispute Type</th>
                  <th>Service</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
@php $i=1;    @endphp
 @foreach($DisputeReasons as $DisputeReason)

                <tr>
                  <td>{{$i}}</td>
                  <td>{{$DisputeReason->title}}
                  </td>
                  <td>{{$DisputeReason->DisputeReasonType->name??''}}</td>
                  <td>
                    {{$DisputeReason->serviceName->title??''}}</td>
                  <td>{{$DisputeReason->status=='1'?'Active':'Inactive'}}</td>
                  <td>
                    <a href="{{ action('AdminController\DisputeReasonController@view',[$DisputeReason->id]) }}" type="button" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a>
                    <a href="{{ action('AdminController\DisputeReasonController@update',[$DisputeReason->id]) }}" type="button" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></a>
                     <button type="button" href="{{ action('AdminController\DisputeReasonController@delete',[$DisputeReason->id]) }}" class="btn btn-danger btn-xs" id="delete"><i class="fa fa-trash-o"></i></button>
                    <button type="button" @if($DisputeReason->status == 1) id="deactivate" @else id="activate" @endif href="{{ action('AdminController\DisputeReasonController@status',[$DisputeReason->id]) }}" class="btn btn-primary btn-xs">@if($DisputeReason->status == 1) Active @else Deactive @endif</button>
                  </td>
                </tr>
            
                @php $i++ @endphp
                @endforeach  
              </tbody>
              </table>
            </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection