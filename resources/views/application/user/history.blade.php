@extends('application.layouts.app')
@section('title','User Orders')
@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">  

    <div class="row">
      <div class="col-md-12">
        <div class="box-body">
            <ul class="nav nav-tabs">
    <li class="active" style="width:50%;"><a data-toggle="tab" href="#home">Profile</a></li>
    <li style="width:50%;"><a data-toggle="tab" href="#menu1">Services History</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <div class="row">
        <div class="col-md-8">
          <div class="row profile_box">
            <div class="col-md-4">
              <img src="{{asset('dist/img/avatar.png')}}" style="width: 100%;">
            </div>
            <div class="col-md-8">
              <div class="content_box">
                <ul>
                    <li><b>User Name:</b> {{$user->first_name ?? ''}} {{$user->last_name ?? ''}}</li>
                    <li><b>User Email:</b> {{$user->email ?? ''}}</li>
                    <li><b>User phone:</b> {{$user->mobile_number ?? ''}}</li>
                  
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
   <div id="menu1" class="tab-pane fade">
      <div class="row">
        <div class="col-md-12">
          <div class="profile_box table-responsive">
            <table class="table table-bordered">
              <thead>
                <th>S. No</th>
                <th>Provider Name</th>
                <th>Provider Mobile</th>
                <th>Order Number</th>
                <th>Services</th>
                <th>Date & Time</th>
                <th>Amount</th>
                <th>Action</th>
              </thead>
              <tbody>
              @php
                $count = 1;
              @endphp
              @foreach($user->orders as $ordervalue) 
               {{--@foreach($ordervalue->orderServices as $orderServicesValue)  --}}
                 <tr>
                  <td>{{ $count++ }}</td>
                  <td>{{ $ordervalue->provider->first_name ?? 'NA' }}</td>
                  <td>{{ $ordervalue->provider->mobile_number ?? 'NA' }}</td>
                  <td>{{ $ordervalue->unique_order_id ?? '' }}</td>
                  <td>{{ count($ordervalue->orderServices ?? '' ) }}</td>
                  <td>{{ $ordervalue->date_time_of_service ?? '' }}</td>
                  <td>{{ $ordervalue->paid_amount ?? '' }}</td>
                  <td><a href="{{ action('AdminController\UserController@orderServices', [$ordervalue->id]) }}" class="btn btn-primary btn-sm">View</a></td>
                </tr>
               {{--@endforeach --}}
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
        </div>
      </div>
    </div>

  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection

@section('js')
<script type="text/javascript">

function changeStatus($id) {

      var id = $id; 
       $.ajax({
         url: '{{ action("AdminController\UserController@status") }}?id='+id,
         type: "GET",
         data : {"_token":"{{ csrf_token() }}"},
         dataType: "json",
         success: function(response) { 
            if (response.success == 200) {
                toastr.success(response.message); 
               
                //location.reload();
            } else {
                toastr.error(response.message);
            }
          }

    })
  }
  
</script> 




@endsection