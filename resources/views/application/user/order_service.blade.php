@extends('application.layouts.app')
@section('title','Ordered Services')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Order
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Order</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">  
     <!--  <form class="form-horizontal" action="" method="post">
        <div class="row">
      <div class="col-md-4">
        <label>Search by email or name</label>
        <input type="text" name="" class="form-control">
      </div>
       <div class="col-md-4">
         <label>Status</label>
        <select class="form-control">
          <option>Select Status</option>
          <option>Cancelled</option>
          <option>Completed</option>
          <option>Pending</option>
          <option>Active</option>
        </select>
       </div>
        <div class="col-md-4">
          <div style="margin-top: 24px;">
             <button class="btn btn-default">Clear</button>
          <button class="btn btn-main">Search</button>
          </div>
         
        </div>
    </div> 
      </form> -->
    <div class="heading-bg" style="margin-top: 30px;">
            <h3> Ordered Services</h3>
          </div>
      <!-- /.row -->
      <div class="row">
          
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Service Name</th>
                  <th>Status</th>
                  <th>Total Amount</th>
                  <th>Payble Amount</th>
                  <th>Payment Mode</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($services as $service)
                <tr>
                  <td>{{ $count++ }}</td>
                  <td>{{ $service->service->title ?? '' }}</td>
                  <td><button class="btn btn-sm btn-success">{{ $order->status_string ?? '' }}</button>
                  <td>{{ $service->total_amount ?? '' }}</td>
                  <td>{{ $service->payable_amount ?? '' }}</td>
                  <td>{{ $order->payment_mode ?? '' }}</td>
                  <td>
                    <a href="{{ action('AdminController\UserController@services', [$service->id]) }}" type="button" class="btn btn-primary btn-xs">View</a>
                    <!-- <button type="button" class="btn btn-danger btn-xs">Delete</button> -->
                  </td>
                </tr>
                @endforeach
              </tbody>
              </table>
            </div>
            </div>
            <!-- /.box-body -->
          </div>

  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection