@extends('application.layouts.app')
@section('title','Service Setting')
@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Update Profile
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Service Setting</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       
      <div class="heading-bg">
        <h3>Service Setting</h3>
      </div>

      <div class="box-body">
      <!-- Info boxes -->
      <form class="form-horizontal" action="{{ action('AdminController\ServiceSettingController@edit') }}" method="post" id="add_form">
        @csrf
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
                <div class="form-group">
                  <label for="" class="control-label">Service Request</label>
                    <div class="row">

 @php $i= 1;    @endphp

      @foreach($ServiceRequestType as $serviceReqtype)
@php
 $serviceReequestId = $ServiceSetting->service_request_type_id ?? '';
@endphp
<div class="col-md-{{ $i == '1' ? 3:9 }}"> <label class="radio-inline"><input type="radio" name="service_request_type_id" id="immedate" value="{{$serviceReqtype->id}}" onclick="javascript:Immedate();" 


  {{ $serviceReqtype->id == $serviceReequestId ? 'checked':'' }}



  >{{ $serviceReqtype->name ?? '' }}</label></div>


                      
                        

  <?php $i++ ?>
                      @endforeach

                    </div>
                 </div>


                 
                 <div class="ser_reqbox" style="border:1px solid #656565; padding:10px;" id="ser_reqbox">
                           <div class="row">
                            <div class="col-md-6"> <label class="radio-inline"><input type="radio" name="day_or_hours" checked value='1'>Day(In Count)</label>
                              <input type="text" name="day_count" value="{{$ServiceSetting->day_count ?? ''}}" class="form-control">
                            </div>
                             <div class="col-md-6"><label class="radio-inline"><input type="radio" name="day_or_hours" value='2'>Hours(In Count)</label>
                              <input type="text" name="hour_count" value="{{$ServiceSetting->hour_count ?? ''}}" class="form-control">
                             </div>
                          </div>
                        </div>
                <div class="form-group">
                  <label for="" class="control-label"> Proximate Range</label>
        <input type="text" class="form-control" id=""  name="proximate_range" value="{{$ServiceSetting->proximate_range ?? ''}}">
                </div>
                <div class="form-group">
                  <label for="" class="control-label">Payment limit for Provider</label>
                   <input type="text" class="form-control" id=""  name="payment_limit_provider" value="{{$ServiceSetting->payment_limit_provider ?? ''}}">
                </div>



                <div class="form-group">
                  <h3 style="margin-top: 0;color: #1597cb;">Reschedule Booking</h3>




        @if($ServiceSettingRescheduleData != '')
        <?php $n=1;  ?>
           @foreach($ServiceSettingRescheduleData as $reshedule)

                    <div class="row">
                      <div class="col-md-5">
                        <label for="" class="control-label"> Description</label>


 <input type="hidden"  name="reschedule[{{$n}}][reschedule_id]"   value="{{$reshedule->id ?? ''}}">

                   <input type="text" class="form-control" id=""  name="reschedule[{{$n}}][description]"   value="{{$reshedule->description ?? ''}}">
                      </div>
                       <div class="col-md-3">
                         <label for="" class="control-label"> Hours</label>
                   <input type="text" class="form-control" id=""  name="reschedule[{{$n}}][hour]"   value="{{$reshedule->hours ?? ''}}">
                       </div>
                       <div class="col-md-3">
                         <label for="" class="control-label"> Amount</label>
                   <input type="text" class="form-control" id=""  name="reschedule[{{$n}}][amount]"  value="{{$reshedule->amount ?? ''}}">
                       </div>
                       @if($n == 1)
                       <div class="col-md-1">
                         <a class="btn btn-success" id="add_resbook" style="margin-top: 26px;"><i class="fa fa-plus"></i></a>
                       </div>
                       @endif
                     </div>
    @php $n++ @endphp
@endforeach
@else
<div class="row">
                      <div class="col-md-5">
                        <label for="" class="control-label"> Description</label>



                   <input type="text" class="form-control" id=""  name="reschedule[0][description]"   >
                      </div>
                       <div class="col-md-3">
                         <label for="" class="control-label"> Hours</label>
                   <input type="text" class="form-control" id=""  name="reschedule[0][hour]"   >
                       </div>
                       <div class="col-md-3">
                         <label for="" class="control-label"> Amount</label>
                   <input type="text" class="form-control" id=""  name="reschedule[0][amount]"  >
                       </div>
                       <div class="col-md-1">
                         <a class="btn btn-success" id="add_resbook" style="margin-top: 26px;"><i class="fa fa-plus"></i></a>
                       </div>
                     </div>
@endif

                      <!--  <div class="col-md-1">
                         <a class="btn btn-success" id="add_resbook" style="margin-top: 26px;"><i class="fa fa-plus"></i></a>
                       </div> -->
                   
                    <div class="reschedule_box" id="reschedule_box"></div>
                 </div>
                 <div class="form-group">
                  <h3 style="margin-top: 0;color: #1597cb;">Cancellation of Bookings</h3>
@if($ServiceSettingCancellationData != '')
@php $n = 1; @endphp
 @foreach($ServiceSettingCancellationData as $CancellationData)
                    <div class="row">
                      <div class="col-md-5">

   <input type="hidden"  name="cancellation[{{$n}}][cancellation_id]"   value="{{$CancellationData->id ?? ''}}">                     
                        <label for="" class="control-label"> Description</label>
                   <input type="text" class="form-control" id=""  name="cancellation[{{$n}}][description]"  value="{{$CancellationData->description ?? ''}}">
                      </div>
                       <div class="col-md-3">
                         <label for="" class="control-label"> Hours</label>
                   <input type="text" class="form-control" id=""  name="cancellation[{{$n}}][hour]"  value="{{$CancellationData->hours ?? ''}}">
                       </div>
                       <div class="col-md-3">
                         <label for="" class="control-label"> Amount</label>
                   <input type="text" class="form-control" id=""  name="cancellation[{{$n}}][amount]"  value="{{$CancellationData->amount ?? ''}}">
                       </div>
                       @if($n == 1)
                       <div class="col-md-1">
                         <a class="btn btn-success" style="margin-top: 26px;" id="add_canbook"><i class="fa fa-plus"></i></a>
                       </div>
                       @endif
                    </div>
    @php $n++ @endphp
@endforeach
@else
<div class="row">
                      <div class="col-md-5">

                        <label for="" class="control-label"> Description</label>
                   <input type="text" class="form-control" id=""  name="cancellation[0][description]"  >
                      </div>
                       <div class="col-md-3">
                         <label for="" class="control-label"> Hours</label>
                   <input type="text" class="form-control" id=""  name="cancellation[0][hour]" >
                       </div>
                       <div class="col-md-3">
                         <label for="" class="control-label"> Amount</label>
                   <input type="text" class="form-control" id=""  name="cancellation[0][amount]"  >
                       </div>
                       <div class="col-md-1">
                         <a class="btn btn-success" style="margin-top: 26px;" id="add_canbook"><i class="fa fa-plus"></i></a>
                       </div>
                    </div>
@endif
                       <!-- <div class="col-md-1">
                         <a class="btn btn-success" style="margin-top: 26px;" id="add_canbook"><i class="fa fa-plus"></i></a>
                       </div> -->
                    
                    <div class="canbook_box" id="canbook_box"></div>
                 </div>


                  <div class="form-group">
                  <h3 style="margin-top: 0;color: #1597cb;">Penalty</h3>
@if($ServiceSettingPenaltyData != '')
@php $n = 1; @endphp
@foreach($ServiceSettingPenaltyData as $PenaltyData)
                    <div class="row">

  <input type="hidden"  name="penalty[{{$n}}][penalty_id]"   value="{{$PenaltyData->id ?? ''}}">                                       <div class="col-md-5">
                        <label for="" class="control-label"> Description</label>
                   <input type="text" class="form-control" id=""  name="penalty[{{$n}}][description]" value="{{$PenaltyData->description ?? ''}}">
                      </div>
                       <div class="col-md-3">
                         <label for="" class="control-label"> Hours</label>
                   <input type="text" class="form-control" id=""  name="penalty[{{$n}}][hour]" value="{{$PenaltyData->hours ?? ''}}">
                       </div>
                       <div class="col-md-3">
                         <label for="" class="control-label"> Amount</label>
                   <input type="text" class="form-control" id=""  name="penalty[{{$n}}][amount]"  value="{{$PenaltyData->amount ?? ''}}">
                       </div>
                       @if($n == 0)
                       <div class="col-md-1">
                         <a class="btn btn-success" style="margin-top: 26px;" id="add_penalty"><i class="fa fa-plus"></i></a>
                       </div>
                       @endif
                       </div>

                       @php $n++ @endphp

@endforeach
@else
 <div class="row">
<div class="col-md-5">
                        <label for="" class="control-label"> Description</label>
                   <input type="text" class="form-control" id=""  name="penalty[0][description]" >
                      </div>
                       <div class="col-md-3">
                         <label for="" class="control-label"> Hours</label>
                   <input type="text" class="form-control" id=""  name="penalty[0][hour]" >
                       </div>
                       <div class="col-md-3">
                         <label for="" class="control-label"> Amount</label>
                   <input type="text" class="form-control" id=""  name="penalty[0][amount]" >
                       </div>
                       <div class="col-md-1">
                         <a class="btn btn-success" style="margin-top: 26px;" id="add_penalty"><i class="fa fa-plus"></i></a>
                       </div>
                     </div>
@endif
                       

                       <!-- <div class="col-md-1">
                         <a class="btn btn-success" style="margin-top: 26px;" id="add_penalty"><i class="fa fa-plus"></i></a>
                       </div> -->
                    
                    <div class="penalty_box" id="penalty_box"></div>
                 </div>
                 <div class="box-footer text-right">
                <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
              </div>
              <!-- /.box-body -->
              </div>
            </div>
        <div class="col-md-2"></div>
      
            </form>
            </div>
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection
@section('js')


<!-- add reshedule booking -->

  <script type="text/javascript">
        $(document).ready(function(){
            var count = 100;
            $('#add_resbook').on('click', function(){

                var html = '<div class="row mt-10"><div class="col-md-5"><label for="" class="control-label"> Description</label><input type="text" class="form-control" id="" name="reschedule['+count+'][description]"></div><div class="col-md-3"><label for="" class="control-label"> Hours</label><input type="text" class="form-control" id=""  name="reschedule['+count+'][hour]"></div><div class="col-md-3"><label for="" class="control-label"> Amount</label><input type="text" class="form-control" id=""  name="reschedule['+count+'][amount]"></div><button type="button" class="btn btn-danger remove" style="margin-top:26px;"><i class="fa fa-trash-o"></i></button></div>';
                $('#reschedule_box').append(html);
                  count++;
            });
      $('.reschedule_box').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>


    <!-- add cancellation booking -->

  <script type="text/javascript">


        $(document).ready(function(){
            var count = 100;
            $('#add_canbook').on('click', function(){

                var html = '<div class="row mt-10"><div class="col-md-5"><label for="" class="control-label"> Description</label><input type="text" class="form-control" id="" name="cancellation['+count+'][description]"></div><div class="col-md-3"><label for="" class="control-label"> Hours</label><input type="text" class="form-control" id=""  name="cancellation['+count+'][hour]"></div><div class="col-md-3"><label for="" class="control-label"> Amount</label><input type="text" class="form-control" id=""  name="cancellation['+count+'][amount]"></div><button type="button" class="btn btn-danger remove" style="margin-top:26px;"><i class="fa fa-trash-o"></i></button></div>';
                $('#canbook_box').append(html);
                 count++;
            });
      $('.canbook_box').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>

    <!-- penalty -->
 <script type="text/javascript">
        $(document).ready(function(){
           var count = 100;
            $('#add_penalty').on('click', function(){

                var html = '<div class="row mt-10"><div class="col-md-5"><label for="" class="control-label"> Description</label><input type="text" class="form-control" id="" name="penalty['+count+'][description]"></div><div class="col-md-3"><label for="" class="control-label"> Hours</label><input type="text" class="form-control" id=""  name="penalty['+count+'][hour]"></div><div class="col-md-3"><label for="" class="control-label"> Amount</label><input type="text" class="form-control" id=""  name="penalty['+count+'][amount]"></div><button type="button" class="btn btn-danger remove" style="margin-top:26px;"><i class="fa fa-trash-o"></i></button></div>';
                $('#penalty_box').append(html);
                count++;
            });
      $('.penalty_box').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>














<!--------Inrtoduction------>
  <script type="text/javascript">
        $(document).ready(function(){
          var count = 100;
            $('#add_charges').on('click', function(){
      
                var html = '<div class="row addrow"><div class="col-sm-10"><input type="file" name="bannername['+count+'][banner_name]" id="banner123" class="form-control" accept="image/jpg, image/jpeg, image/png"></div>'+
                    '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#charges').append(html);
                count++;
            });
      
      $('.optionBox').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>
  
  <script type="text/javascript">
        $(document).ready(function(){
          var count = 100;
            $('#add_expert').on('click', function(){
      
                var html = '<div class="row addrow"><div class="col-sm-10"><input type="text" name="expertisetitle['+count+'][expertise_title]" id="expert_part" class="form-control" onkeypress="return alphanumeric(event)"></div>'+
                    '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#expert').append(html);
                count++;
            });
      $('.expert_new').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>
@endsection