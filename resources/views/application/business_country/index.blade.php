@extends('application.layouts.app')
@section('title','Business Country List')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
   
        

      <!-- <h1>
        Add Sub Category
      </h1> -->
     
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Business Country</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
         <div class="col-md-12 text-right">
        <button class="btn btn-main btn-xl edit_model" data-toggle="modal" data-href="{{ action('AdminController\BusinessCountryController@create') }}" data-toggle="modal" data-target="#add_model"  data-container=".add_model"><i class="fa fa-plus"></i> Add Business Country</button>
        </div>
      </div>

          <div class="heading-bg" style="margin-top: 30px;">
            <h3></i>Business Country</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered table-striped display nowrap">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Country</th>
                  <th>Currency Symbol</th>
                  <th>Currency Code</th>
                  <!--  <th>Status</th> -->
                   <th>Created</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                 @php $sr = 1 @endphp
                @foreach($BusinessCountries as $country)
                <tr>
                  <td>{{ $sr++ }}</td>
                  <td> {{ $country->Country->name ?? '' }} </td>
                  <td> {{ $country->currency_symbol  ?? '' }} </td>
                   <td> {{ $country->currency_code  ?? '' }} </td>
                     <!--  <td> {{ $country->created_at  ?? '' }} </td> -->
                         <td> {{ $country->updated_at  ?? '' }} </td>
                  
                  <td>
                    <button type="button" href="#" data-href="{{ action('AdminController\BusinessCountryController@update',[$country->id]) }}" class="btn btn-success btn-xs edit_model" data-toggle="modal" data-target="#edit_model"  data-container=".edit_model"><i class="fa fa-edit"></i></button>
                    <button type="button" href="{{ action('AdminController\BusinessCountryController@delete',[$country->id]) }}" class="btn btn-danger btn-xs" id="delete"><i class="fa fa-trash-o"></i></button>
                    
                  </td>
                </tr>
                @endforeach            
              </tbody>
              </table>
            </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection