@extends('application.layouts.app')
@section('title','Faq Booking Services')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <h1>
        Add Category
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Faq Booking Services</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="heading-bg">
        <h3>Faq Booking Services</h3>
      </div>
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <form class="form-horizontal" action="{{ action('AdminController\ContentController@storeFaqBookingService') }}" id="add_form" method="post">
          	@csrf
          	<input type="hidden" name="id" value="{{ $faqBookingService->id ?? '' }}">
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Title<span class="required">*</span></label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="" placeholder="Title" name="title" value="{{ $faqBookingService->title ?? '' }}" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Description<span class="required">*</span></label>

                  <div class="col-sm-9">
                    <textarea type="text" rows="10" name="description" class="form-control" id="" placeholder="Description" required>{{ $faqBookingService->description ?? '' }}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Clear</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
      </div>
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection