@extends('application.layouts.app')
@section('title','Category')
@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Update Profile
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Update Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       
      <div class="heading-bg">
        <h3>Update Profile</h3>
      </div>

      <div class="box-body">
      <!-- Info boxes -->
     <form class="form-horizontal" action="{{ action('AdminController\ProfileController@edit') }}" id="add_form" method="post">
            @csrf
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"> Name</label>
                  <div class="col-sm-10">
                   <input type="text" class="form-control" id=""  name="first_name" value="{{$profile->first_name}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Gender</label>

                  <div class="col-sm-10">
                   <label class="radio-inline"><input type="radio" name="optradio" checked>Male</label>
                    <label class="radio-inline"><input type="radio" name="optradio">Female</label>
                    <label class="radio-inline"><input type="radio" name="optradio">Other</label>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"> Email</label>
                  <div class="col-sm-10">
                   <input type="email" class="form-control" id=""  name="email" value="{{$profile->email}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Photo</label>

                  <div class="col-sm-10">
                    <input type="file" name="" class="form-control" id="" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Phone</label>
                  <div class="col-sm-10">
                   <input type="text" class="form-control" id=""  name="">
                  </div>
                </div>
                 <div class="box-footer text-right">
                <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-main">Update</button>
              </div>
              <!-- /.box-footer -->
              </div>
              <!-- /.box-body -->
              
            </div>
        <div class="col-md-2"></div>
      
            </form>
            </div>
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->





@endsection