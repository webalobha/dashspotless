@extends('application.layouts.app')
@section('title','Provider Notifications')
@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Provider Notifications</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="heading-bg" style="margin-top: 30px;">
            <h3>Provider Notifications</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered table-striped display nowrap">
                <thead>
                <tr>
                  <th>S.No</th>
                  <!-- <th>ProviderId</th> -->
                  <th>ProviderName</th>
                  <th>OrderId</th>
                  <th>Notification</th>
                  <th>Date/Time</th>
                </tr>
                </thead>
                <tbody>
                @php $i=1;    @endphp
                @foreach($providerNotifications as $providerNotification)

                <tr>
                  <td>{{ $i++ }}</td>
                  <!-- <td>{{ $providerNotification->provider_id ?? '' }}</td> -->
                  <td>{{ $providerNotification->provider->first_name ?? '' }}</td>
                  <td>{{ $providerNotification->order->unique_order_id ?? '' }}</td>
                  <td>{{ $providerNotification->notification ?? '' }}</td>
                  <td>{{ $providerNotification->created_at ?? '' }}</td>
                </tr>
                @endforeach  
              </tbody>
              </table>
            </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection