
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dash Spotless </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/style.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
  @include('application.layouts.partials.css')

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page" style="background: url(../public/dist/img/login-bg.jpg); background-position: center;background-size: cover; background-repeat: no-repeat;">
<div class="login-box">  
  <!-- /.login-logo -->
  <div class="login-box-body" style="border:4px solid #1597cb;box-shadow: 0px 0px 5px 0 #333;">
    <div class="login-logo">
      <a href="javascript:void(0);"><img src="{{asset('dist/img/Dash-Spotless.png')}}" style="width: 160px;"></a>
    </div>
    <p class="login-box-msg">Reset your password</p>
    <form class="form-horizontal" action="{{ action('AdminController\LoginController@post_reset_pass') }}" method="post" id="add_model1">
        @csrf
      <div class="form-group has-feedback">
         <input type="hidden" name="id" value="{{$id}}">
        <input type="password" name="password" class="form-control" placeholder="Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>    
      <div class="form-group has-feedback">
        <input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>     
      <div class="row">
        <div class="col-xs-4">
          <button type="submit" class="btn btn-main btn-block btn-flat">Update</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
 @include('application.layouts.partials.js')
<script type="text/javascript">
  
$(document).on('submit', 'form#add_model1', function(e) { 
      e.preventDefault();
      var data = new FormData(this);

      $.ajax({
          cache:false,
          contentType: false,
          processData: false,
          url: $(this).attr("action"),
          method: $(this).attr("method"),
          dataType: "json",
          data: data,
          success: function(response) { 
            if (response.success == 200) {
                toastr.success(response.message); 
                $('div.add_model').modal('hide');
               var redirect = " {{ url('admin-login')}}";
 window.location = redirect;
            } else {
                toastr.error(response.message);
            }
          }
      }); 
});




</script>
 

</body>
</html>








