@extends('application.layouts.app')
@section('title','Sub Category')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <h1>
        Add Sub Category
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Sub Category</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="heading-bg">
        <h3>Add Sub Category</h3>
      </div>
      <!-- Info boxes -->
      <!-- Info boxes -->
      <div class="box-body">
      <form class="form-horizontal" action="{{ action('AdminController\SubCategoryController@store') }}" method="post" id="add_form">
        @csrf
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">

                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Category Name<span class="required">*</span></label>

                  <div class="col-sm-10">
                    <select class="form-control" id="" name="category_id" required="">
                      <option value="">Select Categories</option>
                      @foreach($categories as $category)
                      <option value="{{ $category->id ?? '' }}">{{ $category->name ?? '' }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Sub Category Name<span class="required">*</span></label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="" placeholder="Sub Category Name" name="name" onkeypress="return alphaonly(event)">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Upload Icon Image<span class="required">*</span></label>

                  <div class="col-sm-10">
                    <input type="file" name="avtar_name" class="form-control" id="" placeholder="Description" accept="image/jpg, image/jpeg, image/png">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Upload Banner<span class="required">*</span></label>
                  <div class="col-sm-10" id="banner_row">
                    <div class="row addrow">
                      <div class="col-sm-10"><input type="file" name="bannername[0][banner_name]" id="banner12" class="form-control" accept="image/jpg, image/jpeg, image/png"></div>
                      <div class="col-sm-2 text-right">
                        <button type="button" class="btn btn-success m-t-32 mr-2" id="add_charges"><i class="fa fa-plus"></i></button>
                      </div>
                    </div>
                    <div id="charges" class="optionBox"></div>
                   </div>
                 </div>
                 <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Sub Category Title<span class="required">*</span></label>
                    <div class="col-sm-10">
                    <input type="text" name="title" id="" class="form-control" onkeypress="return alphaonly(event)">
                  </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Discount Label</label>
                    <div class="col-sm-10">
                    <input type="text" name="discount_label" id="" class="form-control" onkeypress="return alphanumericpercent(event)">
                  </div>
                  </div>
                  <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Expertise In</label>
                  <div class="col-sm-10" id="banner_row">
                    <div class="row addrow">
                      <div class="col-sm-10"><input type="text" name="expertisetitle[0][expertise_title]" id="banner123" class="form-control" onkeypress="return alphanumeric(event)"></div>
                      <div class="col-sm-2 text-right">
                           <button type="button" class="btn btn-success m-t-32 mr-2" id="add_expert"><i class="fa fa-plus"></i></button>
                      </div>
                      </div>
                  <div id="expert" class="expert_new"> </div>
           
          </div>
         </div>
                 <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
              </div>
              <!-- /.box-body -->
              
            </div>
        <div class="col-md-1"></div>
      
            </form>
</div>
        
      <!-- /.row -->
          <div class="heading-bg" style="margin-top: 30px;">
            <h3>Sub Categories</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Sub Category Name</th>
                  <th>Category Name</th>
                  <th>Image</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php $sr = 1 @endphp
                @foreach($subCategories as $subCategory)
                <tr>
                  <td>{{ $sr++ }}</td>
                  <td> {{ $subCategory->name ?? '' }} </td>
                  <td> {{ $subCategory->category->name ?? '' }} </td>
                  <td><img src="{{ asset($subCategory->avtar_path ?? 'dist/img/download.png') }}" style="width: 100px; height: 100px;"></td>
                  <td>
                    <a type="button" href="{{ action('AdminController\SubCategoryController@update',[$subCategory->id]) }}"  class="btn btn-success btn-xs "><i class="fa fa-edit"></i></a>
                    <button type="button" href="{{ action('AdminController\SubCategoryController@delete',[$subCategory->id]) }}" class="btn btn-danger btn-xs" id="delete"><i class="fa fa-trash-o"></i></button>
                    <button type="button" @if($subCategory->status == 1) id="deactivate" @else id="activate" @endif href="{{ action('AdminController\SubCategoryController@status',[$subCategory->id]) }}" class="btn btn-primary btn-xs">@if($subCategory->status == 1) Active @else Deactive @endif</button>
                  </td>
                </tr>
                @endforeach                
              </tbody>
              </table>
            </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- Modal -->
<div id="viewsubcat" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">View Sub Category</h4>
      </div>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-4">
              <img src="dist/img/avatar.png" style="width: 100%;">
            </div>
            <div class="col-md-8">
              <div class="detailview">
                <table class="table table-bordered">
                  <tr>
                    <td><b>Category Name</b></td>
                    <td>Car Wash</td>
                  </tr>
                  <tr>
                    <td><b>Sub Category Name</b></td>
                    <td>Full Wash</td>
                  </tr>
                  <tr>
                    <td><b>Status</b></td>
                    <td>Active</td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
      </div>
    </div>

  </div>
</div>
@endsection
@section('js')
<!--------Inrtoduction------>
  <script type="text/javascript">
        $(document).ready(function(){
          var count = 100;
            $('#add_charges').on('click', function(){
      
                var html = '<div class="row addrow"><div class="col-sm-10"><input type="file" name="bannername['+count+'][banner_name]" id="banner123" class="form-control" accept="image/jpg, image/jpeg, image/png"></div>'+
                    '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#charges').append(html);
                count++;
            });
      
      $('.optionBox').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>
  
  <script type="text/javascript">
        $(document).ready(function(){
          var count = 100;
            $('#add_expert').on('click', function(){
      
                var html = '<div class="row addrow"><div class="col-sm-10"><input type="text" name="expertisetitle['+count+'][expertise_title]" id="expert_part" class="form-control" onkeypress="return alphanumeric(event)"></div>'+
                    '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#expert').append(html);
                count++;
            });
      $('.expert_new').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>
@endsection