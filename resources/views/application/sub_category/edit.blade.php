@extends('application.layouts.app')
@section('title','Edit Sub Category')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Add Sub Category
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Sub Category</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="heading-bg">
        <h3>Edit Sub Category</h3>
      </div>
      <!-- Info boxes -->
      <!-- Info boxes -->
      <div class="box-body">
      <form class="form-horizontal" action="{{ action('AdminController\SubCategoryController@edit',[$subCategory->id]) }}" method="post" id="add_form">
        @csrf
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">

                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Category Name<span class="required">*</span></label>

                  <div class="col-sm-10">
                    <select class="form-control" id="" name="category_id" required="">
                      <option value="">Select Categories</option>
                      @foreach($categories as $category)
                      <option value="{{ $category->id ?? '' }}" @if($category->id == $subCategory->category_id) selected @endif>{{ $category->name ?? '' }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Sub Category Name<span class="required">*</span></label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="" placeholder="Sub Category Name" name="name" value="{{ $subCategory->name ?? '' }}" onkeypress="return alphaonly(event)">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Upload Icon Image<span class="required">*</span></label>

                  <div class="col-sm-10">
                    <input type="file" name="avtar_name" class="form-control" id="" placeholder="Description" accept="image/jpg, image/jpeg, image/png">
                  <img width="80" src="{{ asset($subCategory->avtar_path ?? '') }}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Upload Banner<span class="required">*</span></label>
                  <div class="col-sm-10" id="banner_row">
                    <div class="addrow">
                     @php $count = 0;
                     $subCategoryBanner = App\Models\SubCategoryBanner::where('sub_category_id', $subCategory->id)->get();
                     @endphp 
                     @if(count($subCategoryBanner) == 0)
                      <div class="addrow">
                        <div class="col-sm-10">
                          <input type="file" name="bannername[{{$count}}][banner_name]" id="banner123" class="form-control" accept="image/jpg, image/jpeg, image/png">
                        </div>
                        <div class="col-sm-2 text-right">
                          <button type="button" class="btn btn-success m-t-32 mr-2" id="add_charges"><i class="fa fa-plus"></i></button>
                        </div>
                      </div>
                     @endif   
                     @foreach( $subCategoryBanner as $subCategoryBanners) 
                       <input type="hidden" name="bannername[{{$count}}][banner_id]" value="{{ $subCategoryBanners->id }}">
                        <div class="row addrow">
                         <div class="col-sm-10">
                          <input type="file" name="bannername[{{$count}}][avtar_name]" id="banner123" class="form-control" accept="image/jpg, image/jpeg, image/png">
                          <img width="80" src="{{ asset($subCategoryBanners->avtar_path ?? '') }}">
                         </div>
                         @if($count != 0)
                         <!-- <div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button>  -->
                         @else
                          <div class="col-sm-2 text-right">
                           <button type="button" class="btn btn-success m-t-32 mr-2" id="add_charges"><i class="fa fa-plus"></i></button>
                          </div>
                         @endif
                        </div>
                     @php $count++; @endphp 
                     @endforeach

                      <!-- <div class="col-sm-10"><input type="file" name="bannername[0][banner_name]" id="banner12" class="form-control"></div> -->

                    </div>
                    <div id="charges" class="optionBox"></div>
                   </div>
                 </div>



                 <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Sub Category Title<span class="required">*</span></label>
                    <div class="col-sm-10">
                    <input type="text" name="title" id="" value="{{ $subCategory->title ?? '' }}" class="form-control" onkeypress="return alphaonly(event)">
                  </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Discount Label</label>
                    <div class="col-sm-10">
             <input type="text" name="discount_label" id="" value="{{ $subCategory->discount_label ?? '' }}" class="form-control" onkeypress="return alphanumeric(event)">
                  </div>
                  </div>
                  <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Expertise In</label>
                  <div class="col-sm-10" id="banner_row">
                    <div class="addrow">
                     @php $count = 0; 
                      $subCategoryExpertices = App\Models\SubCategoriesExpertise::where('sub_category_id', $subCategory->id)->get();
                     @endphp
                     @if(count($subCategoryExpertices) == 0)
                     <div class="addrow">
                      <div class="col-sm-10">
                        <input type="text" name="expertisetitle[0][expertise_title]" id="expert_part" class="form-control" onkeypress="return alphanumeric(event)">
                      </div>
                      <div class="col-sm-2 text-right">
                           <button type="button" class="btn btn-success m-t-32 mr-2" id="add_expert"><i class="fa fa-plus"></i></button>
                      </div>
                     </div>
                     @endif
                     @foreach($subCategoryExpertices as $subCategoryExpertise)
                     <input type="hidden" name="expertisetitle[{{$count}}][expertise_id]" value="{{ $subCategoryExpertise->id }}">
                      <div class="row addrow">
                        <div class="col-sm-10">
                          <input type="text" name="expertisetitle[{{$count}}][expertise_title]" id="expert_part" class="form-control" value="{{ $subCategoryExpertise->title }}" onkeypress="return alphanumeric(event)">
                        </div>
                        @if($count != 0)
                                            <!-- <div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button>                          -->
                        @else
                         <div class="col-sm-2 text-right">
                              <button type="button" class="btn btn-success m-t-32 mr-2" id="add_expert"><i class="fa fa-plus"></i></button>
                         </div>
                        @endif
                      </div>
                     @php $count++; @endphp 
                     @endforeach
                        <!-- <input type="text" name="expertisetitle[0][expertise_title]" id="banner123" class="form-control"> -->
                      </div>
                  <div id="expert" class="expert_new"> </div>
           
          </div>
         </div>
                 <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
              </div>
              <!-- /.box-body -->
              
            </div>
        <div class="col-md-1"></div>
      
            </form>
          </div>

        


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('js')
<!--------Inrtoduction------>
  <script type="text/javascript">
        $(document).ready(function(){
          var count = 100;
            $('#add_charges').on('click', function(){
      
                var html = '<div class="row addrow"><div class="col-sm-10"><input type="file" name="bannername['+count+'][banner_name]" id="banner123" class="form-control" accept="image/jpg, image/jpeg, image/png"></div>'+
                    '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#charges').append(html);
                count++;
            });
      
      $('.optionBox').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>
  
  <script type="text/javascript">
        $(document).ready(function(){
          var count = 100;
            $('#add_expert').on('click', function(){
      
                var html = '<div class="row addrow"><div class="col-sm-10"><input type="text" name="expertisetitle['+count+'][expertise_title]" id="expert_part" class="form-control" onkeypress="return alphanumeric(event)"></div>'+
                    '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#expert').append(html);
                count++;
            });
      $('.expert_new').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>
@endsection