@extends('application.layouts.app')
@section('title','Subscription')
@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Subscription
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Subscription</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">  
      <div class="row">
        <div class="col-md-12 text-right">
          <a href="{{ action('AdminController\SubscriptionController@create') }}" class="btn btn-main"><i class="fa fa-plus"></i> Add Subscription</a>
        </div>
      </div>

      <form class="form-horizontal" action="{{ action('AdminController\SubscriptionController@index') }}" method="get">
        <div class="row">
       <div class="col-md-4">
         <label>Search</label>
        <select class="form-control" name="discount_type_id">
          <option value="">Select</option>
          @foreach($discountTypes as $discountType)
          <option value="{{ $discountType->id ?? '' }}" @if($discountTypeId == $discountType->id) selected @endif>{{ $discountType->name ?? '' }}</option>
          @endforeach
        </select>
       </div>
        <div class="col-md-4">
          <div style="margin-top: 24px;">
             <!-- <button class="btn btn-default">Clear</button> -->
          <button class="btn btn-main">Search</button>
          </div>
         
        </div>
    </div> 
      </form>
    <div class="heading-bg" style="margin-top: 30px;">
            <h3>Subscription List</h3>
          </div>
      <!-- /.row -->
      <div class="row">
          
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Subscription Duration</th>
                  <th>Duration</th>
                  <th>Discount Type</th>
                  <th>Discount </th>
                  <th>Subscription Amount</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @php $count=1; @endphp
                @foreach($subscriptions as $subscription)
                <tr>
                  <td>{{ $count++ }}</td>
                  <td>{{ $subscription->subscriptionDuration->name ?? '' }}</td>
                  <td>{{ $subscription->duration ?? '' }}</td>
                  <td>{{ $subscription->discountType->name ?? '' }}</td>
                  <td>{{ $subscription->discount ?? '' }}</td>
                  <td>{{ $subscription->price ?? '' }} </td>
                  <td>
                    <a href="{{ action('AdminController\SubscriptionController@view', [$subscription->id]) }}" type="button" class="btn btn-success btn-xs">View</a>
                    <a href="{{ action('AdminController\SubscriptionController@update', [$subscription->id]) }}" type="button" class="btn btn-primary btn-xs">Edit</a>
                    <a href="{{ action('AdminController\SubscriptionController@delete', [$subscription->id]) }}" id="delete" type="button" class="btn btn-danger btn-xs">Delete</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
              </table>
            </div>
            </div>
            <!-- /.box-body -->
          </div>

  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 @endsection