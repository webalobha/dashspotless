@extends('application.layouts.app')
@section('title','Update Subscription')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Update Manager
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Update Subscription</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       
      <div class="heading-bg">
        <h3> Update Subscription</h3>
      </div>

      <div class="box-body">
      <!-- Info boxes -->
      <form class="form-horizontal" action="{{ action('AdminController\SubscriptionController@edit', [$subscription->id]) }}" method="post" >
        @csrf
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Subscription Duration</label>
                  <div class="col-sm-9">
                   <select class="form-control" name="subscription_duration_id" required>
                    <option value="">Select</option>
                    @foreach($subscriptionDurations as $subscriptionDuration)
                     <option value="{{ $subscriptionDuration->id ?? '' }}" @if($subscriptionDuration->id == $subscription->subscription_duration_id) selected @endif>{{ $subscriptionDuration->name ?? '' }}</option>
                    @endforeach
                   </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Week/Month Duration</label>
                  <div class="col-sm-9">
                   <input type="text" class="form-control" id=""  name="duration" placeholder="Week/Month Duration" value="{{ $subscription->duration ?? '' }}" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Subscription Discount Type</label>
                  <div class="col-sm-9">
                   <select class="form-control" name="discount_type_id" required>
                    <option value="">Select</option>
                    @foreach($discountTypes as $discountType)
                     <option value="{{ $discountType->id ?? '' }}" @if($discountType->id == $subscription->discount_type_id) selected @endif>{{ $discountType->name ?? '' }}</option>
                    @endforeach
                   </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label"> Percentage/Flat Discount</label>
                  <div class="col-sm-9">
                   <input type="text" class="form-control" id=""  name="discount" placeholder="Percentage/Flat Discount" value="{{ $subscription->discount ?? '' }}" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Discount Description</label>

                  <div class="col-sm-9">
                    <textarea class="form-control" name="description" placeholder="Discount Description" required>{{ $subscription->description ?? '' }}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Subscription Amount</label>
                  <div class="col-sm-9">
                   <input type="text" class="form-control" id=""  name="price" placeholder="Subscription Amount" value="{{ $subscription->price ?? '' }}" required>
                  </div>
                </div>
                 <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Clear</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
              </div>
              <!-- /.box-body -->
              
            </div>
        <div class="col-md-1"></div>
      
            </form>
            </div>
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 @endsection