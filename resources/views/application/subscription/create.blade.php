@extends('application.layouts.app')
@section('title','Add Subscription')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Add Manager
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add Subscription</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       
      <div class="heading-bg">
        <h3> Add Subscription</h3>
      </div>

      <div class="box-body">
      <!-- Info boxes -->
      <form class="form-horizontal" action="{{ action('AdminController\SubscriptionController@store') }}" method="post" onsubmit="return ValidateLoginForm();" name="form">
        @csrf
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Subscription Duration</label>
                  <div class="col-sm-9">
                   <select class="form-control" name="subscription_duration_id" required>
                    <option value="">Select</option>
                    @foreach($subscriptionDurations as $subscriptionDuration)
                     <option value="{{ $subscriptionDuration->id ?? '' }}">{{ $subscriptionDuration->name ?? '' }}</option>
                    @endforeach
                   </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Week/Month Duration</label>
                  <div class="col-sm-9">
                   <input type="text" class="form-control" id=""  name="duration" placeholder="Week/Month Duration" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Subscription Discount Type</label>
                  <div class="col-sm-9">
                   <select class="form-control" name="discount_type_id" id="discount_type" required>
                    <option value="">Select</option>
                    @foreach($discountTypes as $discountType)
                     <option value="{{ $discountType->id ?? '' }}">{{ $discountType->name ?? '' }}</option>
                    @endforeach
                   </select>
                  </div>
                </div>
                <div class="form-group discounted" id="discounted" style="display: none;">
                  <label for="" class="col-sm-3 control-label"> Percentage/Flat Discount</label>
                  <div class="col-sm-9">
                   <input type="text" class="form-control" id="discount"  name="discount" placeholder="Percentage/Flat Discount">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Discount Description</label>

                  <div class="col-sm-9">
                    <textarea class="form-control" name="description" placeholder="Discount Description" required></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Subscription Amount</label>
                  <div class="col-sm-9">
                   <input type="text" class="form-control" id=""  name="price" placeholder="Subscription Amount" required>
                  </div>
                </div>
                 <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Clear</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
              </div>
              <!-- /.box-body -->
              
            </div>
        <div class="col-md-1"></div>
      
            </form>
            </div>
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 @endsection
 @section('js')
  <script>
    $('#discount_type').change(function(){ 
      var discountId =    $( "#discount_type" ).val(); 
      if (discountId == '1') {
        $('.discounted').hide();
     } else {
      $('.discounted').show();
     }
    });
  </script>
  <script>
    function ValidateLoginForm()
    { 
     var discount = document.form.discount;
     var discountId =    $( "#discount_type" ).val();
      if (discountId != '1') {
      if (discount.value == "")
       {
        swal("Please enter discount amount");
        discount.focus();
        return false;
       }
       } else {
          return true;
       }
    }
  </script>

 @endsection