@extends('application.layouts.app')
@section('title','Subscription')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Add Manager
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">View Subscription</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       
      <div class="heading-bg">
        <h3> View Subscription</h3>
      </div>

      <div class="box-body">
      <!-- Info boxes -->
      <form class="form-horizontal">
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Subscription Duration</label>
                  <div class="col-sm-9">
                   <input type="text" class="form-control" id=""  name="" placeholder="" value="{{ $subscription->subscriptionDuration->name ?? '' }}" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Week/Month Duration</label>
                  <div class="col-sm-9">
                   <input type="text" class="form-control" id=""  name="" placeholder="" value="{{ $subscription->duration ?? '' }}" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Subscription Discount Type</label>
                  <div class="col-sm-9">
                   <input type="text" class="form-control" id=""  name="" placeholder="" value="{{ $subscription->discountType->name ?? '' }}" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label"> Percentage/Flat Discount</label>
                  <div class="col-sm-9">
                   <input type="text" class="form-control" id=""  name="" placeholder="" value="{{ $subscription->discount ?? '' }}" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Discount Description</label>

                  <div class="col-sm-9">
                    <textarea class="form-control" readonly="">{{ $subscription->description ?? '' }}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Subscription Amount</label>
                  <div class="col-sm-9">
                   <input type="text" class="form-control" id=""  name="" placeholder="999" readonly="" value="{{ $subscription->price ?? '' }}">
                  </div>
                </div>
              <!-- /.box-footer -->
              </div>
              <!-- /.box-body -->
              
            </div>
        <div class="col-md-1"></div>
      
            </form>
            </div>
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 @endsection