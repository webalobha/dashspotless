@extends('application.layouts.app')
@section('title','Service')
@section('content')

   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Add Service
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add Service</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="heading-bg">
            <h3>Add Service</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
               <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
         <form class="form-horizontal" action="{{ action('AdminController\ServiceController@store') }}" method="post" id="add_form">
        @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Category Name<span class="required">*</span></label>

                  <div class="col-sm-10">
                   <select class="form-control category" id="category_id" name="category_id">
                      <option>Select Categories</option>

                     @foreach($category as $cat)
                      <option value="{{$cat->id}}">{{$cat->name}}</option>
                     @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Sub Category Name<span class="required">*</span></label>

                  <div class="col-sm-10">
                      <select class="form-control subcategory" id="sub_category_id" name="sub_category_id">
                    
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>

                  <div class="col-sm-10 text-right">
                   <button type="submit" class="btn btn-default">Cancel</button>
                <button type="button" class="btn btn-main" id="openservice">Next</button>
                  </div>
                </div>

                
                <div class="boxhide" id="servicebox">
                  <div class="form-group checkvariant">
            <label for="" class="col-sm-2 control-label">Select Variant<span class="required">*</span></label>
            <div class="col-sm-10">
             <select class="form-control" id="variant_id" name="variant_id">
               
             </select>
          </div>
          </div>
        <div class="form-group">
            <label for="" class="col-sm-2 control-label">Service Name<span class="required">*</span></label>
            <div class="col-sm-10">
            <input type="text" name="title" id="title"  class="form-control" onkeypress="return alphaonly(event)">
          </div>
          </div>
          <div class="form-group">
          <label for="" class="col-sm-2 control-label">Upload Service Icon<span class="required">*</span></label>
          <div class="col-sm-10" id="banner_row">
           <input type="file" name="icon_name" class="form-control" accept="image/jpg, image/jpeg, image/png">
          </div>
         </div>
        <div class="form-group">
          <label for="" class="col-sm-2 control-label">Upload Banner</label>
          <div class="col-sm-10" id="banner_row">
            <div class="row addrow">
              <div class="col-sm-10"><input type="file" name="banner[0][banner_image]" id="banner" class="form-control" accept="image/jpg, image/jpeg, image/png"></div>
              <div class="col-sm-2 text-right">
         <button type="button" class="btn btn-success m-t-32 mr-2" id="add_details_banner"><i class="fa fa-plus"></i></button>

        </div>
            </div>
          <div id="service_part" class="details_service">
              
              </div>
          </div>
         </div>
         <div class="form-group">
            <label for="" class="col-sm-2 control-label">Descriptions<span class="required">*</span></label>
            <div class="col-sm-10">
            <textarea class="form-control" name="description" onkeypress="return alphanumeric(event)"></textarea>
          </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Duration(In minutes)</label>
            <div class="col-sm-10">
            <input type="text" name="time_taken_minutes" id="time_taken_minutes" class="form-control" onkeypress="return numbersonly(event)" maxlength="5">
          </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Amount<span class="required">*</span></label>
            <div class="col-sm-10">
            <input type="text" name="price" id="price" class="form-control" onInput="priceValue()" onkeypress="return numbersonly(event)">
            <!-- <span id="lblValue">The text box contains: </span> -->
          </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Discount Type</label>
            <div class="col-sm-10">
            <select class="form-control" name="discount_type" id="discount_type">
              @foreach($discountTypes as $discountType)
              <option value='{{ $discountType->id ?? "" }}'>{{ $discountType->name ?? '' }}</option>
              @endforeach
              <!-- <option value='percentage'>percentage</option> -->
            </select>
          </div>
          </div>
          <div class="form-group discounted" id="discounted" style="display: none;">
            <label for="" class="col-sm-2 control-label">Percentage/Flat Discount</label>
            <div class="col-sm-10">
            <input type="text" name="discount" id="discount" onInput="discountValue()" class="form-control">
          </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Discount Amount</label>
            <div class="col-sm-10">
            <input type="text" name="discounted_price" id="discounted_price" class="form-control" onkeypress="return numbersonly(event)" required="required" readonly>
          </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Tooltip Content<span class="required">*</span></label>
            <div class="col-sm-10">
            <textarea class="form-control" name="tool_tip_content" onkeypress="return alphaonly(event)"></textarea>
          </div>
          </div>
          <div class="form-group">
           <label for="" class="col-sm-2 control-label"><h4>Bullet Points</h4></label>
          <div class="col-sm-10">
            <label>Title</label>
            <input type="text" name="bullet_part[0][title]" id="banner" class="form-control" onkeypress="return alphaonly(event)">
            <label>Description</label>
            <textarea class="form-control" name="bullet_part[0][description]" onkeypress="return alphanumeric(event)"></textarea>
         </div>
         <div id="bullet_part" class="bullet_part"></div>
         </div>

       <div class="form-group text-right">
      <button type="button" class="btn btn-success m-t-32 mr-2" id="add_bullet">Add More</button>
          
           </div>
         <div class="form-group">
          <div class="col-sm-10">
            <label>Is Offer Available</label>
            <input id="is_offer" type="checkbox" name="is_offer" value="1">
         </div>
         <div id="offer"></div>
       </div>
         <div class="box-footer text-center">
           <button type="submit" class="btn btn-default">Cancel</button>
           <button type="submit" class="btn btn-main">Submit</button>
         </div>

                </div>

              </div>
            </form>
        </div>
        <div class="col-md-1"></div>
      </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('js')

<script type="text/javascript">

$(document).ready(function() { 
     $('.category').on('change', function() {         
      var categoryID = $(this).val(); 
      if(categoryID) {
        $.ajax({
         url: '{{ action("AdminController\SubCategoryController@subcategory") }}?category_id='+categoryID,
         type: "GET",
         data : {"_token":"{{ csrf_token() }}"},
         dataType: "json",
         success:function(data) {
          // console.log(data);
          if(data){
           $('.subcategory').empty();
           $('.subcategory').focus;
           $('.subcategory').append('<option value="">Select</option>'); 
           $.each(data, function(key, value){                         
           $('.subcategory').append('<option value="'+ value.id +'">' + value.name + '</option>');
          });
          } else {
            $('.subcategory').empty();
          }
         }
        });
      } else {
        $('.subcategory').empty();
      }
     });
   });
</script> 
       

<script type="text/javascript">


        $(document).ready(function(){
            $('#add_charges').on('click', function(){

      
                var html = '<div class="row addrow"><div class="col-sm-10"><input type="file" name="" id="banner123" class="form-control"></div>'+
                    '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#charges').append(html);
            });
      
      $('.optionBox').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>
  
  <script type="text/javascript">
        $(document).ready(function(){
            $('#add_expert').on('click', function(){
      
                var html = '<div class="row addrow"><div class="col-sm-10"><input type="text" name="" id="expert_part" class="form-control"></div>'+
                    '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#expert').append(html);
            });
      $('.expert_new').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>
  <!-----------Select Variant-->
  <script type="text/javascript">
        $(document).ready(function(){
            $('#add_more').on('click', function(){
      
                var html = '<div class="row addrow"><div class="form-group"><label for="" class="col-sm-2 control-label">Enter Statement</label><div class="col-sm-10"><input type="text" name="" id=""  class="form-control"></div></div>'+ 
          '<div class="form-group"><label for="" class="col-sm-2 control-label">Enter Label</label><div class="col-sm-10"><input type="text" name="" id="" class="form-control"></div></div>'+
                '<div class="form-group"><label for="" class="col-sm-2 control-label">Count</label><div class="col-sm-10"><button class="btn btn-default">1</button><button class="btn btn-default">2</button><button class="btn btn-default">3</button><button class="btn btn-default">4</button><button class="btn btn-default">5</button><button class="btn btn-default">6</button><button class="btn btn-default">7</button><button class="btn btn-default">8</button><button class="btn btn-default">9</button><button class="btn btn-default">10</button></div>  </div>'+
                '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#variant').append(html);
            });
      $('.variant_new').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    
    
    </script>
  <!----------Add Service Details-------->
    <script type="text/javascript">
      var count =1;
        $(document).ready(function(){
            $('#add_details_banner').on('click', function(){
      
                var html = '<div class="row addrow"><div class="col-sm-10"><input type="file" name="banner['+count+'][banner_image]" id="banner123" class="form-control"></div>'+
                    '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#service_part').append(html);
                count++;
            });
      $('.details_service').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>
  <script type="text/javascript">
        $(document).ready(function(){
            $('#add_bullet').on('click', function(){
      
                var html = '<div class=""> <label for="" class="col-sm-2 control-label"><h4>Bullet Points</h4></label><div class="col-sm-10"><label>Title</label><input type="text" name="bullet_part['+count+'][title]" id="banner" class="form-control">'+
                    '<label>Description</label><textarea name="bullet_part['+count+'][description]" class="form-control"></textarea><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#bullet_part').append(html);
                 count++;
            });
      $('.bullet_part').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>
    <!--   <script type="text/javascript">
  function onButtonClick(){
 var element = document.getElementById("servicebox");
  element.classList.remove("boxhide");
 element.classList.add("boxshow");
}
    </script> -->
    <script type="text/javascript">
      $("#openservice").click(function(){


          var e = document.getElementById("sub_category_id");
        var subcategoryID = e.options[e.selectedIndex].value;
     
      if(subcategoryID) {
        $.ajax({
         url: '{{ action("AdminController\VariantController@variant_by_subcategory") }}?sub_category_id='+subcategoryID,
         type: "GET",
         data : {"_token":"{{ csrf_token() }}"},
         dataType: "json",
         success:function(data) {
          // console.log(data);
          if(data){
           $('#variant_id').empty();
           $('#variant_id').focus;
           $('#variant_id').append('<option value="">Select</option>'); 
           $.each(data, function(key, value){                         
           $('#variant_id').append('<option value="'+ value.id +'">' + value.label + '</option>');
          });
      
        $("#servicebox").show();
      $('.checkvariant').show();


          } else {
           
             $("#servicebox").show();
            $('.checkvariant').hide();
          }
         }
        });
      } else {
        
         $("#servicebox").show();
        $('.checkvariant').hide();
      }
  
 





});
    </script>
<script type="text/javascript">
  function priceValue() {
    var price = document.getElementById("price");
    var s = price.value;
    $("#discounted_price").val(s);
    $("#discount").val('');
    //console.log(s);
    // var lblValue = document.getElementById("lblValue");
    // lblValue.innerText = "The text box contains: " + s;

    // var discount = document.getElementById("discount");
    // var s = discount.value; 
    // console.log  
  }
  function discountValue() {

  var discount =    $( "#discount" ).val();
  var price =    $("#price").val();
  var discountType =    $( "#discount_type" ).val();
         $("#discounted_price").val(price);
if(discountType=='1'){

  $("#discount").val('0');
  $("#discounted").hide();
}else if(discountType=='2'){

 $("#discounted_price").val(totalAmount);

var calculateDiscount = price*discount/100;

var totalAmount = price-calculateDiscount;


 $("#discounted_price").val(totalAmount);

  }else if(discountType=='3'){


var totalAmount = price-discount;


 $("#discounted_price").val(totalAmount);

 }

  }
  
</script>
<script type="text/javascript">
  $(function() {    // Makes sure the code contained doesn't run until
                  //     all the DOM elements have loaded
    
    $('#discount_type').change(function(){
      var price =  $("#price").val(); 
      var discountId =    $( "#discount_type" ).val(); 
       $("#discounted_price").val(price);
        $('#discount').val('');
      if (discountId == '1') {
        $('.discounted').hide();
       
         $("#discount").val('0');
         $("#discounted_price").val(price);
     } else {
      $('.discounted').show();
     }
           
    });

});
</script>
<script> 
  $("#is_offer").change(function() {
  
      if(this.checked) {
          
       var html = '<div id="newOffer" class="">\
                    <div class="col-sm-4">\
                      <label>Offer From</label>\
                       <input type="date" name="offer_from"  class="form-control"  required>\
                      <label>Offer To</label>\
                       <input type="date" name="offer_to" class="form-control" required>\
                      <label>Offer Banner</label>\
                       <input type="file" name="offer_banner" class="form-control">\
                    </div>\
                   </div>';
       $('#offer').append(html);
      } else {
        $('#newOffer').remove();
      }        
  });
</script>
@endsection

 
