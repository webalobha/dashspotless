@extends('application.layouts.app')
@section('title','Category')
@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Services
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Services</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">   
      
      <!-- /.row -->
      <div class="row">
        <div class="col-md-12 text-right">
          <a href="{{url('create-service')}}" class="btn btn-main"><i class="fa fa-plus"></i> Add Service</a>
        </div>
      </div>
      <div class="row">
      <div class="col-md-12">
        <form class="form-horizontal" action="{{ action('AdminController\ServiceController@index') }}" method="post">
          @csrf
          <div class="row">
            <div class="col-md-4">
              <label>Category</label>
              <select class="form-control category" name="category">
                <option value="">Select</option>
                @foreach($categoryList as $category)
                <option value="{{ $category->id ?? '' }}" @if($category->id == $category_id) selected @endif> {{ $category->name ?? '' }} </option>
                @endforeach
                <option>2</option>
              </select>
            </div>
            <div class="col-md-4">
              <label>Sub Category</label>
              <select class="form-control sub-category" name="sub_category">
                @if($subCategoryList == '')

                @else
                <option value="">Select</option>
                @foreach($subCategoryList as $subCategory)
                 <option value="{{ $subCategory->id ?? '' }}" @if($subCategory->id == $sub_category_id) selected @endif>{{ $subCategory->name ?? '' }}</option>
                @endforeach
                @endif
              </select>
            </div>
            <div class="col-md-4">
              <label>Variant</label>
              <select class="form-control variant" name="variant">
                @if($varientList == '')

                @else
                <option value="">Select</option>
                @foreach($varientList as $varient)
                 <option value="{{ $varient->id ?? '' }}" @if($varient->id == $variant_id) selected @endif>{{ $varient->label ?? '' }}</option>
                @endforeach
                @endif
              </select>
            </div>
            <div class="col-md-4">
              <label>Service</label>
              <select class="form-control service" name="service">
                @if($serviceslList == '')

                @else
                <option value="">Select</option>
                @foreach($serviceslList as $service)
                 <option value="{{ $service->id ?? '' }}" @if($service->id == $service_id) selected @endif>{{ $service->title ?? '' }}</option>
                @endforeach
                @endif
              </select>
            </div>
            <div class="col-md-4" style="margin-top: 24px;">
              <!-- <button class="btn btn-default" title="reset">Clear</button> -->
              <button class="btn btn-main">Submit</button>
            </div>
          </div>
        </form>
      </div>
      </div>
          <div class="heading-bg" style="margin-top: 30px;">
            <h3>Services</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Category Name</th>
                  <th>Sub Category Name</th>
                  <th>Service Name</th>
                  <th>Variant Name</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
@php $k=1; @endphp 
@foreach($services as $service)

                <tr>
                  <td>{{$k++}}</td>
                  <td>{{$service->category->name ?? ''}}
                  </td>
                  <td>{{$service->subCategory->name ?? ''}}</td>
                  <td>{{$service->title ?? ''}}</td>
@php
$i=0;
$variant = App\Models\Variant::where('id',$service->variant_id)->first();


@endphp


                  <td>{{$variant->label??'No'}}</td>
                  <td>
                    <a type="button" href="{{ action('AdminController\ServiceController@view',[$service->id]) }}"  class="btn btn-success btn-xs "><i class="fa fa-eye"></i></a>
                     
                  <a type="button" href="{{ action('AdminController\ServiceController@update',[$service->id]) }}"  class="btn btn-success btn-xs "><i class="fa fa-edit"></i></a>
                     <button type="button" href="{{ action('AdminController\ServiceController@delete',[$service->id]) }}" class="btn btn-danger btn-xs" id="delete"><i class="fa fa-trash-o"></i></button>
                    
                  </td>
                </tr>

@endforeach

               
              </tbody>
              </table>
            </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection
@section('js')
<script type="text/javascript">

$(document).ready(function() { 
     $('.category').on('change', function() {         
      var categoryID = $(this).val(); 
      if(categoryID) {
        $.ajax({
         url: '{{ action("AdminController\SubCategoryController@subcategory") }}?category_id='+categoryID,
         type: "GET",
         data : {"_token":"{{ csrf_token() }}"},
         dataType: "json",
         success:function(data) {
          // console.log(data);
          if(data){
           $('.sub-category').empty();
           $('.sub-category').focus;
           $('.sub-category').append('<option value="">Select</option>');
           $('.service').empty(); 
           $('.variant').empty();
           $.each(data, function(key, value){                         
           $('.sub-category').append('<option value="'+ value.id +'">' + value.name + '</option>');
          });
          } else {
            $('.sub-category').empty();
          }
         }
        });
      } else {
        $('.sub-category').empty();
        $('.service').empty();
        $('.variant').empty();
      }
     }); 

     $('.sub-category').on('change', function() {        
      var subCategoryID = $(this).val();  
      if(subCategoryID) {
        $.ajax({
         url: '{{ action("AdminController\VariantController@variant_by_subcategory_for_search") }}?sub_category_id='+subCategoryID,
         type: "GET",
         data : {"_token":"{{ csrf_token() }}"},
         dataType: "json",
         success:function(data) {
          // console.log(data);
          if(data){
           $('.variant').empty();
           $('.variant').focus;
           $('.variant').append('<option value="">Select</option>'); 
           $('.service').empty();
           $.each(data, function(key, value){                         
           $('.variant').append('<option value="'+ value.id +'">' + value.label + '</option>');
          });
          } else {
            $('.variant').empty();
          }
         }
        });
      } else {
        $('.variant').empty();
        $('.service').empty();
      }
     }); 

     $('.variant').on('change', function() {         
      var variantID = $(this).val(); 
      if(variantID) {
        $.ajax({
         url: '{{ action("AdminController\ServiceController@service_by_variant") }}?variant_id='+variantID,
         type: "GET",
         data : {"_token":"{{ csrf_token() }}"},
         dataType: "json",
         success:function(data) {
          // console.log(data);
          if(data){
           $('.service').empty();
           $('.service').focus;
           $('.service').append('<option value="">Select</option>'); 
           $.each(data, function(key, value){                         
           $('.service').append('<option value="'+ value.id +'">' + value.title + '</option>');
          });
          } else {
            $('.service').empty();
          }
         }
        });
      } else {
        $('.service').empty();
      }
     });
   });
</script>
@endsection