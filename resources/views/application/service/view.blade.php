  @extends('application.layouts.app')
@section('title','Category')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
      View Service
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">View Service</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="heading-bg" >
            <h3>View Service</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
               <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
          <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Category Name<span class="required">*</span></label>

                  <div class="col-sm-6">
                    <select class="form-control" id="" name="" disabled>
                      @foreach($category as $cat)
                      <option value="{{$cat->id}}" {{$cat->id==$service->category_id?'selected':''}}>{{$cat->name}}</option>
                     @endforeach
                    </select>
                  </div>

                  
<?php
$query = new App\Models\Category;


$cattt = $query->where('id', $service->category_id)->first();
?>

                  <div class="col-sm-4">
                    <img src="{{asset($cattt->avtar_path)}}" style="width: 100%;">
                  </div>
                </div>
<?php
$query1 = new App\Models\SubCategory;
$suvcattt = $query1->where('id', $service->sub_category_id)->first();
?>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Sub Category Name<span class="required">*</span></label>

                  <div class="col-sm-6">
                    <select class="form-control" id="" name="" disabled>
                      @foreach($SubCategory as $subcat)
                      <option value="{{$subcat->id}}" {{$subcat->id==$service->sub_category_id?'selected':''}}>{{$subcat->name}}</option>
                     @endforeach
                    </select>
                  </div>
                  <div class="col-sm-4">
                    <img src="{{asset($suvcattt->avtar_path)}}" style="width: 100%;">
                  </div>
                </div>
                <div class="" id="servicebox">
                  <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Select Variant</label>
                  <div class="col-sm-10">
                   <select class="form-control" disabled="">
                      @foreach($Variant as $vardata)
                      <option value="{{$vardata->id}}" {{$vardata->id==$service->variant_id?'selected':''}}>{{$vardata->label}}</option>
                     @endforeach
                   </select>
                </div>
                </div>
                <div class="form-group">
            <label for="" class="col-sm-2 control-label">Service Name</label>
            <div class="col-sm-10">
            <input type="text" name="" id="" value="{{$service->title}}"  class="form-control" value="Lorem ipsum" readonly>
          </div>
          </div>

@php
$i=0;
$query = new App\Models\ServiceBanner;
$banners = $query->where('service_id', $service->id)->get();
$query1 = new App\Models\ServiceBulletPoints;
$ServiceBulletPoints = $query1->where('service_id', $service->id)->get();



@endphp


        <div class="form-group">
          <label for="" class="col-sm-2 control-label">Upload Banner</label>
          <div class="col-sm-10" id="banner_row">
            <div class="row addrow">

                 @foreach($banners as $banner)
              <div class="col-sm-4"><img src="{{asset($banner->avtar_path)}}" style="width: 100%;"></div>
              
   @php $i++  @endphp
@endforeach
            </div>
          </div>
         </div>
         <div class="form-group">
            <label for="" class="col-sm-2 control-label">Discriptions</label>
            <div class="col-sm-10">
            <textarea class="form-control" readonly>{{$service->description}} </textarea>
          </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Duration</label>
            <div class="col-sm-10">
            <input type="text" name="" id="" class="form-control"  value="{{$service->time_taken_minutes}}" readonly>
          </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Amount</label>
            <div class="col-sm-10">
            <input type="text" name="" id="" class="form-control" value="{{$service->price}}" readonly="">
          </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Discount Type</label>
            <div class="col-sm-10">
            <select class="form-control" disabled="">
              <option value='amount' {{$service->discount_type=='amount'?'selected':''}}>Amount</option>
              <option value='percentage' {{$service->discount_type=='percentage'?'selected':''}}>percentage</option>
            </select>
          </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Percentage/Flat Discount</label>
            <div class="col-sm-10">
            <input type="text" name="" id="" class="form-control" value="{{$service->discount}}" readonly="">
          </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Discount Amount</label>
            <div class="col-sm-10">
            <input type="text" name="" id="" class="form-control" value="{{$service->discounted_price}}" readonly="">
          </div>
          </div>

          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Tooltip Content</label>
            <div class="col-sm-10">
            <textarea class="form-control" readonly>{{$service->tool_tip_content}}</textarea>
          </div>
          </div>
          <div class="form-group">

@foreach($ServiceBulletPoints as $ServiceBulletPoint)
           <label for="" class="col-sm-2 control-label"><h4>Bullet Points</h4></label>
          <div class="col-sm-10">
            <label>Title</label>
            <input type="text" name="" id="banner" class="form-control" value ="{{$ServiceBulletPoint->title}}" readonly>
            <label>Description</label>
            <textarea class="form-control" readonly>{{$ServiceBulletPoint->description}}</textarea>
         </div>

@php $i++  @endphp
@endforeach

       </div>
      </div>  
      </div>
      </form>
  </div>
        <div class="col-md-1"></div>
      </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



@endsection
