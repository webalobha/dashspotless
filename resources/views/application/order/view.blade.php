@extends('application.layouts.app')
@section('title','Service Detail')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Order
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">View Ordered Service</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">  
      <div class="heading-bg" style="margin-bottom: 20px;">
            <h3>View Ordered Service</h3>
          </div>
   <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="{{ asset($service->service->icon_path ?? '') }}" alt="User profile picture">

              <h3 class="profile-username text-center"><b>{{ $service->service->title ?? '' }}</b></h3>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Order ID</b> <span class="pull-right">{{ $service->serviceOrdersData->unique_order_id ?? '' }}</span>
                </li>
                <li class="list-group-item">
                  <b>Cost</b> <span class="pull-right">{{ $service->serviceOrdersData->paid_amount ?? '' }}</span>
                </li>
                <li class="list-group-item"> 
                  <b>Status</b> <span class="pull-right"><button class="btn btn-xs btn-success">{{ $service->serviceOrdersData->status_string ?? '' }}</button></span>
                </li>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
           <!-- Profile Image -->
          <div class="box">
            <div class="box-body box-profile">
              <div class="row">
                <div class="col-md-6">
                  <div class="row service_row">
                <div class="col-sm-6"><b>Customer Name</b></div>
                <div class="col-sm-6">{{ $service->serviceOrdersData->user->first_name ?? '' }}</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-6"><b>Mobile Number</b></div>
                <div class="col-sm-6">{{ $service->serviceOrdersData->user->mobile_number ?? '' }}</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-6"><b>Email</b></div>
                <div class="col-sm-6">{{ $service->serviceOrdersData->user->email ?? '' }}</div>
              </div>

              <div class="row service_row">
                <div class="col-sm-12"><h4><b>Service Details</b></h4></div>
              </div>
              <div class="row service_row">
                <div class="col-sm-6"><b>Order ID</b></div>
                <div class="col-sm-6">{{ $service->serviceOrdersData->unique_order_id ?? '' }}</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-6"><b>Service Name</b></div>
                <div class="col-sm-6">{{ $service->service->title ?? '' }}</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-6"><b>Service Duration</b></div>
                <div class="col-sm-6">{{ $service->service->time_taken_minutes ?? '' }}</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-6"><b>Categoy</b></div>
                <div class="col-sm-6">{{ $service->service->category->name ?? '' }}</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-6"><b>Sub Category</b></div>
                <div class="col-sm-6">{{ $service->service->subCategory->name ?? '' }}</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-6"><b>Booking Amount</b></div>
                <div class="col-sm-6">{{ $service->serviceOrdersData->paid_amount ?? '' }}</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-6"><b>Booking Date & Time</b></div>
                <div class="col-sm-6">{{ $service->serviceOrdersData->date_time_of_service ?? '' }}</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-6"><b>Rescheduled Date & Time</b></div>
                <div class="col-sm-6">lorem ipsum</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-6"><b>Customer Service Address</b></div>
                <div class="col-sm-6">{{ $service->serviceOrdersData->orderAddress->address ?? '' }}</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-6"><b>Booking Status</b></div>
                <div class="col-sm-6">{{ $service->serviceOrdersData->status_string ?? '' }}</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-6"><b>Payment Mode:</b></div>
                <div class="col-sm-6">{{ $service->serviceOrdersData->payment_mode ?? '' }}</div>
              </div>
              <!-- <div class="row service_row">
                <div class="col-sm-6"><b>Customer's Rating & Reviews</b></div>
                <div class="col-sm-6">lorem ipsum</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-6"><b>Provider's Rating & Reviews</b></div>
                <div class="col-sm-6">lorem ipsum</div>
              </div> -->
                </div>
                <div class="col-md-6">
                  <div class="row service_row">
                <div class="col-sm-6"><b>Provider Name</b></div>
                <div class="col-sm-6">{{ $service->serviceOrdersData->provider->first_name ?? '' }}</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-6"><b>Mobile Number</b></div>
                <div class="col-sm-6">{{ $service->serviceOrdersData->provider->mobile_number ?? '' }}</div>
              </div>
              <div class="row service_row">
                <div class="col-sm-6"><b>Email</b></div>
                <div class="col-sm-6">{{ $service->serviceOrdersData->provider->email ?? '' }}</div>
              </div>
                </div>
              </div>
              
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection