@extends('application.layouts.app')
@section('title','Orders')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Order
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Order</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">  
      <form class="form-horizontal" action="{{ action('AdminController\OrderController@index') }}" method="get">
        <div class="row">
      <div class="col-md-4">
        <label>Search by email or name</label>
        <input type="text" name="email" class="form-control" value="{{ $email }}">
      </div>
       <div class="col-md-4">
         <label>Status</label>
        <select class="form-control" name="status">
          <option value="">Select Status</option>
          @foreach($orderStatuses as $orderStatus)
          <option value="{{ $orderStatus->id ?? '' }}" @if($orderStatus->id == $status) selected @endif>{{ $orderStatus->name ?? '' }}</option>
          @endforeach
        </select>
       </div>
        <div class="col-md-4">
          <div style="margin-top: 24px;">
             <!-- <button type="reset" class="btn btn-default">Clear</button> -->
          <button class="btn btn-main">Search</button>
          </div>
         
        </div>
    </div> 
      </form>
    <div class="heading-bg" style="margin-top: 30px;">
            <h3> Order</h3>
          </div>
      <!-- /.row -->
      <div class="row">
          
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Order ID</th>
                  <th>Customer Name</th>
                  <th>Provider Name</th>
                  <th>Status</th>
                  <th>Payment Amount</th>
                  <th>Payment Status</th>
                  <th>Payment Mode</th>
                  <th>Provider Rating</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                <tr>
                  <td>{{ $count++ }}</td>
                  <td>{{ $order->unique_order_id ?? '' }}</td>
                  <td>{{ $order->user->first_name ?? '' }}</td>
                  <td>{{ $order->provider->first_name ?? 'Na' }}</td>
                  <td>
                    @php
                     if($status != ''){
                      $orderHistory = App\Models\OrderHistory::where('order_status_id', $status)
                                                             ->where('order_id', $order->id)
                                                             ->orderBy('created_at', 'desc')
                                                             ->first()
                                                             ->orderStatus
                                                             ->name;

                     }else{
                      $orderHistory = App\Models\OrderHistory::where('order_id', $order->id)->orderBy('created_at', 'desc')->first()->orderStatus->name;
                     }

                    @endphp

                    <button class="btn btn-sm btn-success">{{  $orderHistory }}</button>
                  </td>
                  <td>{{ $order->paid_amount ?? '' }}</td>
                  <td>{{  $orderHistory }}</td>
                  <td>{{ $order->payment_mode ?? '' }}</td>
                  <td>
                    <ul class="rating_list">
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                    </ul>
                  </td>
                  <td>
                    <a href="{{ action('AdminController\OrderController@orderServices', [$order->id]) }}" type="button" class="btn btn-primary btn-xs">View</a>
                    <!-- <button type="button" class="btn btn-danger btn-xs">Delete</button> -->
                  </td>
                </tr>
                @endforeach
              </tbody>
              </table>
            </div>
            </div>
            <!-- /.box-body -->
          </div>

  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection