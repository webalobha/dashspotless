@extends('application.layouts.app')
@section('title','Dashboard')
@section('content')
      
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

<!--       <div class="row">
        <div class="col-md-12">
        <ul class="nav nav-pills" style="justify-content: left;">
          <li><a href="{{ action('HomeController@index', ['all']) }}" class="active">All</a></li>
          <li><a href="{{ action('HomeController@index', ['today']) }}">Today</a></li>
          <li><a href="{{ action('HomeController@index', ['weekly']) }}">Weekly</a></li>
          <li><a href="{{ action('HomeController@index', ['monthly']) }}">Monthly</a></li>
          <li><a href="{{ action('HomeController@index', ['yearly']) }}">Yearly</a></li>
        </ul>
      </div>
      </div> -->
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-aqua">
            <div class="info-box-content">
              <span class="info-box-text">Total Orders</span>
              <span class="info-box-number">{{ count($orders) }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
    <!--     <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-red">
            <div class="info-box-content">
              <span class="info-box-text">Revenue</span>
              <span class="info-box-number">41,410</span>
            </div>
          </div>
        </div> -->
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-green">

            <div class="info-box-content">
              <span class="info-box-text">Order Completed</span>
              <span class="info-box-number">{{ count($completed) }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">

            <div class="info-box-content">
              <span class="info-box-text">Order Cancelled</span>
              <span class="info-box-number">{{ count($cancelled) }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
<!--         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
            <div class="info-box-content">
              <span class="info-box-text">Service Pending</span>
              <span class="info-box-number">90<small>%</small></span>
            </div>
          </div>
        </div> -->
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">
            <div class="info-box-content">
              <span class="info-box-text">Service Active</span>
              <span class="info-box-number">{{ $services }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-aqua">

            <div class="info-box-content">
              <span class="info-box-text">Total Provider</span>
              <span class="info-box-number">{{ $providers }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-red">

            <div class="info-box-content">
              <span class="info-box-text">Total Customers</span>
              <span class="info-box-number">{{ $users }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <!-- <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-red">
            <div class="info-box-content">
              <span class="info-box-text">Credit Recharge Amount</span>
              <span class="info-box-number">90<small>%</small></span>
            </div>
          </div>
        </div> -->
        <!-- /.col -->
        <!-- <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-aqua">
            <div class="info-box-content">
              <span class="info-box-text">Credit Recharge low Balance</span>
              <span class="info-box-number">41,410</span>
            </div>
          </div>
        </div> -->
        <!-- /.col -->

        <!-- fix for small devices only -->
        <!-- <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">

            <div class="info-box-content">
              <span class="info-box-text">Credit Recharge Expired</span>
              <span class="info-box-number">760</span>
            </div>
          </div>
        </div> -->
        <!-- /.col -->
        <!-- <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-green">

            <div class="info-box-content">
              <span class="info-box-text">Credit Recharge Penalty</span>
              <span class="info-box-number">2,000</span>
            </div>
          </div>
        </div> -->
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-aqua">
            <div class="info-box-content">
              <span class="info-box-text">Total Active Customer</span>
              <span class="info-box-number">{{ $activeUsers }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
            <div class="info-box-content">
              <span class="info-box-text">Total Active Provider</span>
              <span class="info-box-number">{{ $activeProviders }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection