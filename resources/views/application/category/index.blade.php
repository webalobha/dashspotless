@extends('application.layouts.app')
@section('title','Category')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Add Category
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Category</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="heading-bg">
        <h3>Add Category</h3>
      </div>
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <form class="form-horizontal" action="{{ action('AdminController\CategoryController@store') }}" id="add_form" method="post">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Category Name<span class="required">*</span></label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="" placeholder="Category Name" name="name" onkeypress="return alphaonly(event)">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Choose Image<span class="required">*</span></label>

                  <div class="col-sm-10">
                    <input type="file" name="avtar_name" class="form-control" id="" accept="image/jpg, image/jpeg, image/png" placeholder="Description" onchange="preview_image(event)">
                  </div>
                </div>
               <!--  <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>

                  <div class="col-sm-10">
                    
                     <img id="output_image" src="dist/img/download.png" style="width: 200px;height: 200px;"/>
                  </div>
                </div> -->
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <div class="col-md-2"></div>
      </div>
      <!-- /.row -->
          <div class="heading-bg" style="margin-top: 30px;">
            <h3>Categories</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Category Name</th>
                  <th>Image</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php $sr = 1 @endphp
                @foreach($categories as $category)
                <tr>
                  <td>{{ $sr++ }}</td>
                  <td> {{ $category->name ?? '' }} </td>
                  <td><img src="{{ asset($category->avtar_path ?? 'dist/img/download.png') }}" style="width: 100px; height: 100px;"></td>
                  <td>
                    <button type="button" href="#" data-href="{{ action('AdminController\CategoryController@update',[$category->id]) }}" class="btn btn-success btn-xs edit_model" data-toggle="modal" data-target="#edit_model"  data-container=".edit_model"><i class="fa fa-edit"></i></button>
                    <button type="button" href="{{ action('AdminController\CategoryController@delete',[$category->id]) }}" class="btn btn-danger btn-xs" id="delete"><i class="fa fa-trash-o"></i></button>
                    <button type="button" @if($category->status == 1) id="deactivate" @else id="activate" @endif href="{{ action('AdminController\CategoryController@status',[$category->id]) }}" class="btn btn-primary btn-xs">@if($category->status == 1) Active @else Deactive @endif</button>
                  </td>
                </tr>
                @endforeach                
              </tbody>
              </table>
            </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection