  @extends('application.layouts.app')
@section('title','Category')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <h1>
        View Variant
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">View Variant</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="heading-bg">
            <h3>View Variant</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
               <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
          <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Category Name</label>

                  <div class="col-sm-6">
                    <select class="form-control" id="" name="" disabled="">
                       @foreach($category as $cat)
                      <option value="{{$cat->id}}" {{$cat->id==$variants->category_id?'selected':''}}>{{$cat->name}}</option>
                     @endforeach
                    </select>
                  </div>
                   <div class="col-sm-4">
                    <img src="{{asset($cat->avtar_path)}}" style="width: 100%;" data-pagespeed-url-hash="2596483965" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Sub Category Name</label>

                  <div class="col-sm-6">
                    <select class="form-control" id="" name="" disabled="">
                        @foreach($SubCategory as $subcat)
                      <option value="{{$subcat->id}}" {{$subcat->id==$variants->sub_category_id?'selected':''}}>{{$subcat->name}}</option>
                     @endforeach
                    </select>
                  </div>
                  <div class="col-sm-4">

                    
                    <img src="{{asset($subcat->avtar_path)}}" style="width: 100%;" data-pagespeed-url-hash="2596483965" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Variant Subject Line</label>

                  <div class="col-sm-10">
                   <input type="text" name="" id="" class="form-control" value="{{$variants->title_or_subject}}" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Field Type</label>

                  <div class="col-sm-10">
                   <label class="radio-inline"><input type="radio" name="optradio" id="noCheck" disabled onclick="javascript:yesnoCheck();" {{$variants->radio_or_check=='1'?'checked':''}}>Radio Button</label>
                  <label class="radio-inline"><input type="radio" name="optradio" id="yesCheck" checked onclick="javascript:yesnoCheck();" {{$variants->radio_or_check=='2'?'checked':''}}>Checkbox</label>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Variant Name</label>
                  <div class="col-sm-10">
                    <div class="row">
                      <div class="col-sm-6">
                         <input type="text" name="" id="" value="{{$variants->label}}" class="form-control">
                      </div>
                      <div class="col-sm-4">
                        <label class="checkbox-inline"><input type="checkbox" name="" {{$variants->consult_visit_avail=='1'?'checked':''}}>Consultant Visit</label>
                      </div>
                    </div>
                   
                  </div>
                </div>

@php


$i=0;
$query = new App\Models\ConsultVisitBookingDetailVariant;
$consultant_booking_details_variant = $query->where('variant_id', $variants->id)->first();
$consult_visit_id =$consultant_booking_details_variant->consult_visit_id??'';



$query1 = new App\Models\ConsultVisitBookingDetails;
$consultant_booking_details = $query1->where('id', $consult_visit_id)->first();


$query2 = new App\Models\ConsultVisitBookingDetailsImages;
$consultant_imagerefrence = $query2->where('consult_visit_booking_id', $consult_visit_id)->get();


$query3 = new App\Models\ConsultVisitTermsAndCondition;
$consultant_terms = $query3->where('consultant_visit_id', $consult_visit_id)->get();




@endphp


                <div class="consultant_booking mt-30" id="consultant_booking" style="display: block;">
                <h3>Consultant Visit Booking Details & Conditions</h3>
                <div class="form-group">
            <label for="" class="col-sm-2 control-label">Subject Line</label>
            <div class="col-sm-10">
            <input type="text" name="" id="" class="form-control" value="{{$consultant_booking_details->subject_line??''}}" readonly="">
          </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Consultant Amount</label>
            <div class="col-sm-10">
            <input type="text" name="" id="" class="form-control"value="{{$consultant_booking_details->amount??''}}" readonly="">
          </div>
          </div>

           <div class="form-group">
            <label for="" class="col-sm-2 control-label">Discriptions</label>
            <div class="col-sm-10">
            <textarea class="form-control" readonly="">{{$consultant_booking_details->description??''}}</textarea>
          </div>
          </div>


        <div class="form-group">
          <label for="" class="col-sm-2 control-label">Reference Image</label>
          <div class="col-sm-10" id="banner_row">
            <div class="row addrow">

 @foreach($consultant_imagerefrence as $refimage)
              <div class="col-sm-4"><img src="{{asset($refimage->reference_image_path??'')}}" style="width: 100%;" data-pagespeed-url-hash="2596483965" onload="pagespeed.CriticalImages.checkImageForCriticality(this);"></div>

            @endforeach
              
            </div>
          <div id="service_part" class="details_service">
              
              </div>
          </div>
         </div>
        
          <h4>Terms And Conditions</h4>

@foreach($consultant_terms as $consultant_term)
          <div class="form-group">
           <label for="" class="col-sm-2 control-label">Title</label>
          <div class="col-sm-10">
            <input type="text" name="" id="banner" class="form-control"  value ="{{$consultant_term->title??''}}" readonly="">
         </div>
       </div>
       <div class="form-group">
           <label for="" class="col-sm-2 control-label">Description</label>
          <div class="col-sm-10">
            <textarea class="form-control" id="" name="" readonly="">{{$consultant_term->description??''}}</textarea>
         </div>

       </div>

   @endforeach   
              
              </div>
       
              </div>
              </div>
            </form>
               
                </div>
              </div>
            </form>
        </div>
        <div class="col-md-1"></div>
      </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection
