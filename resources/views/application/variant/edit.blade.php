@extends('application.layouts.app')
@section('title','Edit variant')
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Add Variant
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add Variant</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="heading-bg">
            <h3>Edit Variant</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
               <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
          <form class="form-horizontal" action="{{ action('AdminController\VariantController@edit') }}" method="post" id="add_form">
        @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Category Name<span class="required">*</span></label>

                  <div class="col-sm-10">
                    <select class="form-control category" id="category_id" name="category_id">
                      <option value="">Select Categories</option>

                     @foreach($category as $cat)
                      <option value="{{$cat->id}}" {{$cat->id == $variants->category_id ?'selected':''}}>{{$cat->name}}</option>
                     @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Sub Category Name<span class="required">*</span></label>

                  <div class="col-sm-10">
                    <select class="form-control subcategory" id="sub_category_id" name="sub_category_id">
                     @foreach($SubCategory as $subcat)
                      <option value="{{$subcat->id}}" {{$subcat->id==$variants->sub_category_id?'selected':''}}>{{$subcat->name}}</option>
                     @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Variant Subject Line<span class="required">*</span></label>

                  <div class="col-sm-10">
                   <input type="text" name="title_or_subject" id="title_or_subject" value="{{$variants->title_or_subject}}" class="form-control" onkeypress="return alphanumeric(event)" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Field Type</label>

                  <div class="col-sm-10">
                   <label class="radio-inline"><input type="radio" name="radio_or_check" value="1" id="noCheck" checked onclick="javascript:yesnoCheck();"   
                  {{$variants->radio_or_check=='1'?'checked':''}}
                    >Radio Button</label>
                  
                  <label class="radio-inline"><input type="radio" name="radio_or_check" value="2" id="yesCheck" onclick="javascript:yesnoCheck();"  {{$variants->radio_or_check=='2'?'checked':''}}>Checkbox</label>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Variant Name<span class="required">*</span></label>
                  <div class="col-sm-10">
                    @php
                     $count = 1;
                    @endphp
                   @if(!empty($variantNames))
                    @foreach($variantNames as $variantName)
                    
                    <div class="row">
                      <div class="col-sm-6">



                       <input type="hidden" name="label[{{ $count }}][variant]" value="{{$variantName->id}}" id="" class="form-control" onkeypress="return alphaonly(event)">

                         <input type="text" name="label[{{ $count }}][label]" value="{{$variantName->label}}" id="" class="form-control" onkeypress="return alphaonly(event)" required>
                      </div>
                      <div class="col-sm-4">
                        <label class="checkbox-inline"><input type="checkbox" name="label[{{ $count }}][consult_visit_avail]" class="consult_visit_avail" id="consult_visit_avail[{{ $count }}][consult_visit_avail]" value="1" onchange="valueChanged()"  {{$variantName->consult_visit_avail=='1'?'checked':''}}>Consultant Visit</label>   
                      </div>
                      @if($count == 1)
                      <div class="col-sm-2">
                        <button type="button" class="btn btn-success m-t-32 mr-2" id="add_variant">Add More</button>
                      </div>
                      @endif
                    </div>
                    @php $count++ @endphp
                    @endforeach
                   @else
                   <div class="row">
                      <div class="col-sm-6">
                      </div>
                      <div class="col-sm-4">
                      </div>
                      <div class="col-sm-2">
                        <button type="button" class="btn btn-success m-t-32 mr-2" id="add_variant">Add More</button>
                      </div>
                    </div>
                   @endif
                    <div id="add_variantbox" class="add_variantbox"></div>
                  </div>
                </div>

              

                <div class="consultant_booking mt-30" id="{{$variants->consult_visit_avail=='1'?'consultant_booking1':'consultant_booking'}}">



@php


$i=0;
$query = new App\Models\ConsultVisitBookingDetailVariant;
$consultant_booking_details_variant = $query->where('variant_id', $variants->id)->first();
$consult_visit_id =$consultant_booking_details_variant->consult_visit_id??'';



$query1 = new App\Models\ConsultVisitBookingDetails;
$consultant_booking_details = $query1->where('id', $consult_visit_id)->first();

//dd($consultant_booking_details,$consult_visit_id);

$query2 = new App\Models\ConsultVisitBookingDetailsImages;
$consultant_imagerefrence = $query2->where('consult_visit_booking_id', $consult_visit_id)->get();


$query3 = new App\Models\ConsultVisitTermsAndCondition;
$consultant_terms = $query3->where('consultant_visit_id', $consult_visit_id)->get();

   


@endphp

                <h3>Consultant Visit Booking Details & Conditions</h3>
                <div class="form-group">
                  <input type="hidden" name="consult_visit_id" value="{{ $consult_visit_id ?? '' }}">
            <label for="" class="col-sm-2 control-label">Subject Line<span class="required">*</span></label>
            <div class="col-sm-10">
            <input type="text" name="subject_line" id=""  class="form-control" value="{{$consultant_booking_details->subject_line??''}}" onkeypress="return alphanumeric(event)">
          </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Consultant Amount<span class="required">*</span></label>
            <div class="col-sm-10">
            <input type="text" name="amount" id="" value="{{$consultant_booking_details->amount??''}}"  class="form-control" onkeypress="return numbersonly(event)">
          </div>
          </div>
      
         <div class="form-group">
            <label for="" class="col-sm-2 control-label">Descriptions<span class="required">*</span></label>
            <div class="col-sm-10">
            <textarea class="form-control" name="description" value="{{$consultant_booking_details->description ??''}}" onkeypress="return alphanumeric(event)">{{$consultant_booking_details->description??''}}</textarea>
          </div>
          </div>

          <div class="form-group">
          <label for="" class="col-sm-2 control-label">Reference Image</label>
          <div class="col-sm-10" id="banner_row">



            <div class="row addrow">



          @foreach($consultant_imagerefrence as $refimage)

     <input type="hidden" name="reference_image_name[{{$i}}][refimage_id]" id="banner" value ="{{$refimage->id}}" class="form-control">


              <div class="col-sm-5"><input type="file" name="reference_image_name[{{$i}}][reference_image_name]" id="banner" class="form-control" accept="image/jpg, image/jpeg, image/png"></div>
          <div class="col-sm-5">
           <img src="{{asset($refimage->reference_image_path??'')}}" height= 150px; class="img-circle" alt="User Image">
        </div>

        @php $i++  @endphp
@endforeach

              <div class="col-sm-2 text-right">
         <button type="button" class="btn btn-success m-t-32 mr-2" id="add_details_banner"><i class="fa fa-plus"></i></button>

        </div>
            </div>

          <div id="service_part" class="details_service">
              
              </div>
          </div>
         </div>




          <h4>Terms And Conditions</h4>

@foreach($consultant_terms as $consultant_term)

          <div class="form-group">
           <label for="" class="col-sm-2 control-label">Title</label>
          <div class="col-sm-10">

<input type="hidden" name="title[{{$i}}][consultant_term_id]" id="banner" value ="{{$consultant_term->id??''}}" class="form-control" onkeypress="return alphaonly(event)">

            <input type="text" name="title[{{$i}}][title]" id="banner" value ="{{$consultant_term->title}}" class="form-control" onkeypress="return alphaonly(event)">
         </div>
       </div>
       <div class="form-group">
           <label for="" class="col-sm-2 control-label">Description</label>
          <div class="col-sm-10">
            <textarea class="form-control" id="" name="title[{{$i++}}][description]" value ="{{$consultant_term->description??''}}" onkeypress="return alphanumeric(event)">{{$consultant_term->description??''}}</textarea>
         </div>
@php $i++  @endphp
@endforeach


       <div class="col-sm-12 text-right mt-10">
      <button type="button" class="btn btn-success m-t-32 mr-2" id="add_bullet">Add More</button>
          
           </div>
       </div>
       <div id="bullet_part" class="bullet_part">
     
              
              </div>
       
              </div>


<input type ="hidden" name="variant_id" value="{{$variants->id}}">
<input type ="hidden" name="consult_visit_id" value="{{$consult_visit_id}}">



                
              </div>
              <div class="form-group mt-30">

                  <div class="col-sm-12 text-center">
                   <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-main">Submit</button>
                  </div>
                </div>
            </form>
               
                </div>
              </div>
            </form>
        </div>
        <div class="col-md-1"></div>
      </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('js')

<script type="text/javascript">
    function valueChanged()
    {
        if($('.consult_visit_avail').is(":checked"))   
       document.getElementById('consultant_booking').style.display = 'block';
        else
          document.getElementById('consultant_booking').style.display = 'none';
    }
</script>





<script type="text/javascript">
 // get subcategory by change category//


   $(document).ready(function() { 
     $('.category').on('change', function() {         
      var categoryID = $(this).val(); 
      if(categoryID) {
        $.ajax({
         url: '{{ action("AdminController\SubCategoryController@subcategory") }}?category_id='+categoryID,
         type: "GET",
         data : {"_token":"{{ csrf_token() }}"},
         dataType: "json",
         success:function(data) {
          // console.log(data);
          if(data){
           $('.subcategory').empty();
           $('.subcategory').focus;
           $('.subcategory').append('<option value="">Select</option>'); 
           $.each(data, function(key, value){                         
           $('.subcategory').append('<option value="'+ value.id +'">' + value.name + '</option>');
          });
          } else {
            $('.subcategory').empty();
          }
         }
        });
      } else {
        $('.subcategory').empty();
      }
     });
   });
</script> 


   
<!--------Inrtoduction------>
  <script type="text/javascript">

   
        $(document).ready(function(){
           var count = 100;
            $('#add_charges').on('click', function(){
      
                var html = '<div class="row addrow"><div class="col-sm-10"><input type="file"  id="banner123" name="bannername['+count+'][banner_name]" class="form-control"></div>'+
                    '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#charges').append(html);
            });
      
      $('.optionBox').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>
  
  <script type="text/javascript">
        $(document).ready(function(){
           var count = 100;
            $('#add_expert').on('click', function(){
      
                var html = '<div class="row addrow"><div class="col-sm-10"><input type="text" name="expertisetitle['+count+'][expertise_title]" id="expert_part" class="form-control"></div>'+
                    '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#expert').append(html);
            });
      $('.expert_new').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>
  <!-----------Select Variant-->
  <script type="text/javascript">
        $(document).ready(function(){
           var count = 100;
            $('#add_more').on('click', function(){
      
                var html = '<div class="row addrow"><div class="form-group"><label for="" class="col-sm-2 control-label">Enter Statement</label><div class="col-sm-10"><input type="text" name="" id=""  class="form-control"></div></div>'+ 
          '<div class="form-group"><label for="" class="col-sm-2 control-label">Enter Label</label><div class="col-sm-10"><input type="text" name="" id="" class="form-control"></div></div>'+
                '<div class="form-group"><label for="" class="col-sm-2 control-label">Count</label><div class="col-sm-10"><button class="btn btn-default">1</button><button class="btn btn-default">2</button><button class="btn btn-default">3</button><button class="btn btn-default">4</button><button class="btn btn-default">5</button><button class="btn btn-default">6</button><button class="btn btn-default">7</button><button class="btn btn-default">8</button><button class="btn btn-default">9</button><button class="btn btn-default">10</button></div>  </div>'+
                '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#variant').append(html);
            });
      $('.variant_new').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    
    
    </script>
  <!----------Add Service Details-------->
    <script type="text/javascript">
        $(document).ready(function(){
            $('#add_details_banner').on('click', function(){
               var count = 100;
      
                var html = '<div class="row addrow"><div class="col-sm-10"><input type="file"  name="reference_image_name['+count+'][reference_image_name]" id="banner123" class="form-control"></div>'+
                    '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#service_part').append(html);
            });
      $('.details_service').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>

  <!-- <script type="text/javascript">
        $(document).ready(function(){
            var count = 100;
            $('#add_bullet').on('click', function(){

                var html = '<div class="bullet_part"><div class="form-group"><label for="" class="col-sm-2 control-label">Title</label><div class="col-sm-10"><input type="text" name="title['+count+'][title]" id="banner" class="form-control"></div></div><div class="form-group"><label for="" class="col-sm-2 control-label">Description</label><div class="col-sm-10"><textarea class="form-control" id="" name="title['+count+'][description]"></textarea></div></div><div class="form-group text-right"><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#bullet_part').append(html);
            });
      $('.bullet_part').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script> -->
    <script type="text/javascript">
        $(document).ready(function(){
            var count = 100;
            $('#add_bullet').on('click', function(){

                var html = '<div class=""><div class="form-group"><label for="" class="col-sm-2 control-label">Title</label><div class="col-sm-10"><input type="text" name="title['+count+'][title]" id="banner" class="form-control"></div></div><div class="form-group"><label for="" class="col-sm-2 control-label">Description</label><div class="col-sm-10"><textarea class="form-control" id="" name="description['+count+'][description]"></textarea></div></div><button type="button" class="btn btn-danger pull-right remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#bullet_part').append(html);
            });
      $('.bullet_part').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>
  



  <!-- <script type="text/javascript">
        $(document).ready(function(){
            var count = 100;
            $('#add_variant').on('click', function(){
      
                var html = '<div class="row mt-10"><div class="col-sm-6"><input type="text" name="label['+count+'][label]" id="" class="form-control"></div><div class="col-sm-4"><label class="checkbox-inline"><input type="checkbox" name="label['+count+'][consult_visit_avail]" class="consult_visit_avail" value="1" onchange="valueChanged()">Consultant Visit</label></div><div class="col-sm-2"><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button></div></div>';
                $('#add_variantbox').append(html);
                count++;
            });
      $('.add_variantbox').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script> -->
    <script type="text/javascript">
        $(document).ready(function(){
            var count = 100;
            $('#add_variant').on('click', function(){
      
                var html = '<div class="row addrow mt-10"><div class="col-sm-6"><input type="text" name="label['+count+++'][label]" id="" class="form-control"></div><div class="col-sm-4"><label class="checkbox-inline"><input type="checkbox" name="consult_visit_avail['+count+++'][consult_visit_avail]" class="consult_visit_avail" value="1" onchange="valueChanged()">Consultant Visit</label></div><div class="col-sm-2"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button></div>';
                $('#add_variantbox').append(html);
            });
      $('.add_variantbox').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>

    <script type="text/javascript">
     function yesnoCheck() {
    if (document.getElementById('yesCheck').checked) {

   

        document.getElementById('consultant_booking').style.display = 'block';
    }
    else document.getElementById('consultant_booking').style.display = 'none';

}
    </script>


@endsection

 
