@extends('application.layouts.app')
@section('title','Category')
@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Variant
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Variant</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">   
      <!-- /.row -->
      <div class="row">
        <div class="col-md-12 text-right">
          <a href="{{ url('variant-create') }}" class="btn btn-main"><i class="fa fa-plus"></i> Add Variant</a>
        </div>
      </div>
          <div class="heading-bg" style="margin-top: 30px;">
            <h3>Variant</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Category Name</th>
                  <th>Sub Category Name</th>
                  <th>Variant Name</th>
                  <th>Consultant</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @php $i=1; @endphp
                @foreach($variants as $variant)  
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{ $variant->cat_name }}
                  </td>
                  <td>{{ $variant->subcat_name }}</td>
                  <td>{{ $variant->label }}</td>
                  <td>{{ $variant->consult_visit_avail?'Yes':'No' }}</td>
                  <td>
                   <a type="button" href="{{ action('AdminController\VariantController@view',[$variant->id]) }}"  class="btn btn-success btn-xs "><i class="fa fa-eye"></i></a>
                     
                  <a type="button" href="{{ action('AdminController\VariantController@update',[$variant->id]) }}"  class="btn btn-success btn-xs "><i class="fa fa-edit"></i></a>
                   <!--  <button type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>
                    <button type="button" class="btn btn-primary btn-xs">Active</button> -->
                  </td>
                </tr>

           @endforeach
                
              </tbody>
              </table>
            </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection