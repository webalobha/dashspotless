  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Business Country</h4>
      </div>
      <div class="modal-body">
         <form class="form-horizontal" action="{{ action('AdminController\BusinessCityController@create') }}" method="post" id="edit_model">
          @csrf
              <div class="box-body">

                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Country Name<span class="required">*</span></label>
                  <div class="col-sm-10">
                   <select class="form-control" name="country_id" id="country_id" required="">
                     <option>Select Country</option>
                     @foreach($BusinessCountry as $data)
                     <option value="{{$data->country_id}}">{{$data->Country->name }}</option>
                     @endforeach
                   </select>  
                  </div>
                </div>
                 <div class="form-group">
                  <label for="" class="col-sm-2 control-label">State<span class="required">*</span></label>
                  <div class="col-sm-10">
                    <select class="form-control state" name="state_id" id="state_id" required>
                     
                   </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">City<span class="required">*</span></label>
                  <div class="col-sm-10">
                   <select class="form-control city" name="city_id" id="city_id" required>
                    
                   </select>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Status<span class="required">*</span></label>
                  <div class="col-sm-10">
                   <select class="form-control" name="status" required>
                     <option>Select Status</option>
                     <option value="1">Active</option>
                     <option value="0">Inactive</option>
                   </select>
                  </div>
                </div>


                
               
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
      </div>
    </div>

  </div>


<script type="text/javascript">
 // get subcategory by change category//

   $(document).ready(function() { 
     $('#country_id').on('change', function() {         
      var country_id = $(this).val(); 
      $('.city').empty();
      if(country_id) {
        $.ajax({
         url: '{{ action("AdminController\BusinessCityController@state") }}?id='+country_id,
         type: "GET",
         data : {"_token":"{{ csrf_token() }}"},
         dataType: "json",
         success:function(data) {
          // console.log(data);
          if(data){
           $('.state').empty();
           $('.state').focus;
           $('.state').append('<option value="">Select</option>'); 
           $.each(data, function(key, value){                         
           $('.state').append('<option value="'+ value.id +'">' + value.name + '</option>');
          });
          } else {
            $('.state').empty();
          }
         }
        });
      } else {
        $('.state').empty();
      }
     });
   });


$(document).ready(function() { 
     $('#state_id').on('change', function() {         
      var state_id = $(this).val(); 
      if(state_id) {
        $.ajax({
         url: '{{ action("AdminController\BusinessCityController@city") }}?id='+state_id,
         type: "GET",
         data : {"_token":"{{ csrf_token() }}"},
         dataType: "json",
         success:function(data) {
          // console.log(data);
          if(data){
           $('.city').empty();
           $('.city').focus;
           $('.city').append('<option value="">Select</option>'); 
           $.each(data, function(key, value){                         
           $('.city').append('<option value="'+ value.id +'">' + value.name + '</option>');
          });
          } else {
            $('.city').empty();
          }
         }
        });
      } else {
        $('.city').empty();
      }
     });
   });



</script> 