@extends('application.layouts.app')
@section('title','Business City List')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
   
        

      <!-- <h1>
        Add Sub Category
      </h1> -->
     
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Business City</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
         <div class="col-md-12 text-right">
        <button class="btn btn-main btn-xl edit_model" data-toggle="modal" data-href="{{ action('AdminController\BusinessCityController@create') }}" data-toggle="modal" data-target="#add_model"  data-container=".add_model"><i class="fa fa-plus"></i> Add Business City</button>
        </div>
      </div>

          <div class="heading-bg" style="margin-top: 30px;">
            <h3></i>Business City</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered table-striped display nowrap">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Country</th>
                  <th>State</th>
                  <th>City</th>
                  <th>Status</th>
                   <th>Created</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                 @php $sr = 1 @endphp
                @foreach($BusinessCity as $busscity)
                <tr>
                  <td>{{ $sr++ }}</td>
                  <td> {{ $busscity->Country->name ?? '' }} </td>
                  <td> {{ $busscity->State->name  ?? '' }} </td>
                   <td> {{ $busscity->City->name  ?? '' }} </td>
                    <td> {{ $busscity->status=='1'?'Active':'InActive' }} </td>
                         <td> {{ $busscity->updated_at  ?? '' }} </td>
                  
                  <td>
                    <button type="button" href="#" data-href="{{ action('AdminController\BusinessCityController@update',[$busscity->id]) }}" class="btn btn-success btn-xs edit_model" data-toggle="modal" data-target="#edit_model"  data-container=".edit_model"><i class="fa fa-edit"></i></button>
                    <button type="button" href="{{ action('AdminController\BusinessCityController@delete',[$busscity->id]) }}" class="btn btn-danger btn-xs" id="delete"><i class="fa fa-trash-o"></i></button>
                    
                  </td>
                </tr>
                @endforeach            
              </tbody>
              </table>
            </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection


