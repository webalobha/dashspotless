@extends('application.layouts.app')
@section('title','Need Help')
@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Need Help Static Content</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 text-right">
         <a href="{{ action('AdminController\NeedHelpStaticContentController@create') }}" class="btn btn-main"><i class="fa fa-plus"></i> Add Need Help Content</a>
        </div>
      </div>

          <div class="heading-bg" style="margin-top: 30px;">
            <h3>Need Help Content List</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered table-striped display nowrap">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Need Help For</th>
                  <th>Title</th>
                  <th>Content</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php $i=1;    @endphp
                @foreach($needHelps as $needHelp)

                <tr>
                  <td>{{ $i++ }}</td>
                  <td> @if($needHelp->is_user_or_provider == '1') User @elseif($needHelp->is_user_or_provider == '2') Provider @else  @endif</td>
                  <td>{{$needHelp->title ?? ''}}</td>
                  <td>{{$needHelp->content ?? ''}}</td>
                  <td>
                    <a href="{{ action('AdminController\NeedHelpStaticContentController@update',[$needHelp->id]) }}" type="button" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></a>
                     <button type="button" href="{{ action('AdminController\NeedHelpStaticContentController@delete',[$needHelp->id]) }}" class="btn btn-danger btn-xs" id="delete"><i class="fa fa-trash-o"></i></button>
                     <button type="button" @if($needHelp->status == 1) id="deactivate" class="btn btn-primary btn-xs" @else id="activate" class="btn btn-danger btn-xs" @endif href="{{ action('AdminController\NeedHelpStaticContentController@status',[$needHelp->id]) }}" >@if($needHelp->status == 1) Active @else Deactive @endif</button>                    
                  </td>
                </tr>
                @endforeach  
              </tbody>
              </table>
            </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection