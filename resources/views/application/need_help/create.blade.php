@extends('application.layouts.app')
@section('title','Add Need Help Content')
@section('content')
     
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Add Manager
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Need Help Content</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
        <div class="col-md-12 text-right">
         <a href="{{ action('AdminController\NeedHelpStaticContentController@index') }}" class="btn btn-main"><i class="fa fa-arrow-left"></i> View Need Help Content</a>
        </div>
      </div><br>    

      <div class="heading-bg">
        <h3>Need Help Content</h3>
      </div>


      <div class="box-body">
      <!-- Info boxes -->
      <form class="form-horizontal" action="{{ action('AdminController\NeedHelpStaticContentController@store') }}" method="post" id="add_form">
        @csrf
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
              <label for="" class="control-label">Title<span class="required">*</span></label>
              <input type="text" name="title" class="form-control" placeholder="title" required>
          </div>

          <div class="form-group">
              <label for="" class="control-label">User</label>
              <input type="radio" name="is_user_or_provider" value="1" checked required>
              <label for="" class="control-label">Provider</label>
              <input type="radio" name="is_user_or_provider" value="2" required>
          </div>
         
          <div class="form-group">
            <label for="" class="control-label">Content<span class="required">*</span></label>
             <textarea name="content" rows="10" class="form-control" placeholder="Content" required></textarea>
          </div>
            <a href="<?php $_SERVER['PHP_SELF']; ?>" class="btn btn-default">Clear</a>
            <button type="submit" class="btn btn-main">Submit</button>
          </div>
        <!-- /.box-footer -->
        </div>
        <!-- /.box-body -->
        
      </div>

      </form>
    </div>
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection