  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Credit Recharge</h4>
      </div>
      <div class="modal-body">
         <form class="form-horizontal" action="{{ action('AdminController\CreditRechargeController@edit', [$creditRecharge->id]) }}" id="edit_model" method="post">
         	@csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-4 control-label">Points<span class="required">*</span></label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="" placeholder="" name="points" onkeypress="return numbersonly(event)" value="{{ $creditRecharge->points ?? '' }}" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-4 control-label">Amount Per Point<span class="required">*</span></label>

                  <div class="col-sm-8">
                    <input type="text" name="amount_per_point" class="form-control" id="" placeholder="" onkeypress="return numbersonly(event)" value="{{ $creditRecharge->amount_per_point ?? '' }}" required>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-main">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
      </div>
    </div>

  </div>