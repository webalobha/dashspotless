@extends('application.layouts.app')
@section('title','Credit Recharge')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <h1>
        Add Category
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Credit Recharge</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="heading-bg">
        <h3>Add Credit Recharge</h3>
      </div>
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <form class="form-horizontal" action="{{ action('AdminController\CreditRechargeController@store') }}" id="add_form" method="post">
          	@csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Points<span class="required">*</span></label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="" placeholder="Points" name="points" onkeypress="return numbersonly(event)" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Amount Per Point<span class="required">*</span></label>

                  <div class="col-sm-9">
                    <input type="text" name="amount_per_point" class="form-control" id="" placeholder="Amount Per Point" onkeypress="return numbersonly(event)" required>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Clear</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <div class="col-md-2"></div>
      </div>
      <!-- /.row -->
          <div class="heading-bg" style="margin-top: 30px;">
            <h3> Credit Recharge List</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Amount Per Point</th>
                  <th>Point</th>
                  <th>Amount</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php $count = 1;  @endphp
                @foreach($createcreditRecharges as $createcreditRecharge)
                <tr>
                  <td>{{ $count++ }}</td>
                  <td>{{ $createcreditRecharge->amount_per_point ?? '' }}</td>
                  <td>{{ $createcreditRecharge->points ?? '' }}</td>
                  <td>
                    @php 
                      $amount = $createcreditRecharge->amount_per_point * $createcreditRecharge->points;
                    @endphp
                  	{{ $amount ?? '' }}</td>
                  <td>
                    <button type="button" class="btn btn-success btn-xs edit_model" data-href="{{ action('AdminController\CreditRechargeController@update', [$createcreditRecharge->id]) }}" data-toggle="modal" data-target="#edit_model"><i class="fa fa-edit"></i></button>
                    <a id="delete" href="{{ action('AdminController\CreditRechargeController@delete', [$createcreditRecharge->id]) }}" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
              </table>
            </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection