 <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::guard('admin')->user()->first_name ?? ''}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
        <li class="treeview">
          <a href="{{ url('home') }}">     
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li><a href="{{ url('users') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Users</span></a></li>
        <li><a href="{{ url('providers') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Providers</span></a></li>
        <li class="treeview"><a href="javascript:void(0);"><i class="fa fa-circle-o text-aqua"></i> <span>Services</span></a>

           <ul class="treeview-menu">
          
            <li><a href="{{ url('category') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Categories</span></a></li>
            
            <li><a href="{{ url('sub-category') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Sub Categories</span></a></li>
             <li><a href="{{ url('variant') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Variant</span></a></li>
             <li><a href="{{ url('service') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Services</span></a></li>
          </ul>

        </li>
         <li><a href="{{ action('AdminController\OrderController@index') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Orders</span></a></li>

        <li class="treeview menu-open"><a href="javascript:void(0);"><i class="fa fa-circle-o text-aqua"></i> <span>Disputes</span></a>
          <ul class="treeview-menu" style="display: block;">
            <li><a href="{{ url('dispute-reason') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Dispute Reason</span></a></li>
            <li><a href="{{ url('dispute-request') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Service Dispute User Request</span></a></li>
          </ul>
        </li>


        <li class="treeview"><a href="javascript:void(0);"><i class="fa fa-circle-o text-aqua"></i> <span>Rating & Review</span></a>
          <ul class="treeview-menu">
            <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-aqua"></i> <span>User</span></a></li>
            <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-aqua"></i> <span>Provider</span></a></li>
          </ul>
        </li>
        <li><a href="{{ url('document') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Documents</span></a></li>
        <li class="treeview"><a href="javascript:void(0);"><i class="fa fa-circle-o text-aqua"></i> <span>Manage User</span></a>
          <ul class="treeview-menu">
            <li><a href="{{ action('AdminController\RoleController@index') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Role</span></a></li>
            <li><a href="{{ action('AdminController\AdminManageController@index') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Users</span></a></li>
          </ul>
        </li>


        <li><a href="{{ url('service-setting') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Service Setting</span></a></li>
        <li><a href="{{ action('AdminController\SubscriptionController@index') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Subscription</span></a></li>

        <li class="treeview"><a href="javascript:void(0);"><i class="fa fa-circle-o text-aqua"></i> <span>Credit Balance</span></a>

           <ul class="treeview-menu">
            <li><a href="{{ action('AdminController\CreditRechargeController@index') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Credit Recharge</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Credit Recharge History</span></a></li>
          </ul>

        </li>
        
 <li class="treeview"><a href="javascript:void(0);"><i class="fa fa-circle-o text-aqua"></i> <span>Credit Points</span></a>

           <ul class="treeview-menu">
            <li><a href="{{ action('AdminController\CreditPointPackageController@PricePerPoint') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Amount Per Points</span></a></li>
            <li><a href="{{ action('AdminController\CreditPointPackageController@index') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Credit Points</span></a></li>
          </ul>

        </li>






        <li><a href="{{ url('site-setting') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Site Setting</span></a></li>

       <li><a href="{{ url('business-country') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Business Country</span></a></li>

        <li><a href="{{ url('business-city') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Business City</span></a></li>


        <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-aqua"></i> <span>Manage Static content</span></a></li>

        <li><a href="{{ url('cms-page') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Manage CMS Page</span></a></li>

         <li><a href="{{ url('assign-cms-page') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Assign CMS Page</span></a></li>
         <li><a href="{{ action('AdminController\ContentController@aboutDashspotless') }}"><i class="fa fa-circle-o text-aqua"></i> <span>About Dashspotless</span></a></li>
         <li><a href="{{ action('AdminController\ContentController@faqBookingService') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Faq Booking Service</span></a></li>
         <li><a href="{{ action('AdminController\ContentController@faqPayingService') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Faq Paying For Service</span></a></li>
         <li><a href="{{ action('AdminController\ContentController@faqDashspotlessGuide') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Guide For Dashspotless</span></a></li>
         <li><a href="{{ action('AdminController\ContentController@howItWorks') }}"><i class="fa fa-circle-o text-aqua"></i> <span>How It Works</span></a></li>
         <li><a href="{{ action('AdminController\ContentController@termsOfUse') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Terms Of Use</span></a></li>
         <li><a href="{{ action('AdminController\ContentController@privacyPolicy') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Privacy Policy</span></a></li>
         <li><a href="{{ action('AdminController\ContentController@contactUs') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Contact Us</span></a></li>
         <li><a href="{{ action('AdminController\TrainingCenterController@index') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Training Center</span></a></li>
         <li><a href="{{ action('AdminController\NeedHelpStaticContentController@index') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Need Help</span></a></li>
         <li><a href="{{ action('AdminController\NotificationController@userNotification') }}"><i class="fa fa-circle-o text-aqua"></i> <span>User Notification</span></a></li>
         <li><a href="{{ action('AdminController\NotificationController@providerNotification') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Provider Notification</span></a></li>


      </ul>


<!--       <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Users</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Providers</span></a></li>
        <li><a href="{{ url('category') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Categories</span></a></li>
        <li><a href="{{ url('sub-category') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Sub Categories</span></a></li>
         <li><a href="{{ url('variant') }}"><i class="fa fa-circle-o text-aqua"></i> <span>Variant</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Orders</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Slider Images</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Settings</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Logout</span></a></li>
      </ul> -->
    </section>
    <!-- /.sidebar -->
  </aside>