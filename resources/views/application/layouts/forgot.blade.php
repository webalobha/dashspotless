
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dash Spotless </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/style.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    @include('application.layouts.partials.css')
</head>
<body class="hold-transition login-page" style="background: url(../public/dist/img/login-bg.jpg); background-position: center;background-size: cover; background-repeat: no-repeat;">
<div class="login-box">  
  <!-- /.login-logo -->
  <div class="login-box-body" style="border:4px solid #1597cb;box-shadow: 0px 0px 5px 0 #333;">
    <div class="login-logo">
      <a href="javascript:void(0);"><img src="{{asset('dist/img/Dash-Spotless.png')}}" style="width: 160px;"></a>
    </div>
    <p class="login-box-msg">Forgot your account</p>
    <form class="form-horizontal" action="{{ action('AdminController\LoginController@sendemail') }}" method="post" id="add_form">
        @csrf
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" placeholder="Email" required="">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>     
      <div class="row">
        <div class="col-xs-4">
          <button type="submit" class="btn btn-main btn-block btn-flat">Send</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  @include('application.layouts.partials.js')
</body>
</html>








