  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Document</h4>
      </div>
      <div class="modal-body">
         <form class="form-horizontal" action="{{ action('AdminController\DocumentController@edit',[$document->id]) }}" method="post" id="add_model">
          @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Name<span class="required">*</span></label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="" placeholder="Name" name="name" value="{{ $document->name ?? '' }}" onkeypress="return alphaonly(event)">
                  </div>
                </div>
                
               
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-main">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
      </div>
    </div>

  </div>