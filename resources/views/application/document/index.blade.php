@extends('application.layouts.app')
@section('title','Document List')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
         Document List
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Document List</li>
      </ol>
    </section>




    <!-- Main content -->
    <section class="content">
      <div class="col-md-12 text-right">
         

<button class="btn btn-main btn-xl edit_model" data-toggle="modal" data-href="{{ action('AdminController\DocumentController@create') }}" data-toggle="modal" data-target="#add_model"  data-container=".add_model"><i class="fa fa-plus"></i> Add Document</button>


        </div>
     <form class="form-horizontal" action="{{ action('AdminController\DocumentController@index') }}" method="get">
         
        <div class="row">
      <div class="col-md-4">
        <label>Search Document Name</label>
        <input type="text" name="name" class="form-control" value="{{ $name??'' }}">
      </div>
        <div class="col-md-4">
          <div style="margin-top: 24px;">
             <button type="reset" class="btn btn-default">Clear</button>
          <button class="btn btn-main">Search</button>
          </div>
         
        </div>
    </div> 
      </form>
          <div class="heading-bg" style="margin-top: 30px;">
            <h3>Document</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Name</th>
                   <th>Created by</th>
                   <th>Created</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php $sr = 1 @endphp
                @foreach($documents as $document)
                <tr>
                  <td>{{ $sr++ }}</td>
                  <td> {{ $document->name ?? '' }} </td>
                  <td> {{ $document->added_by  ?? '' }} </td>
                   <td> {{ $document->created_at  ?? '' }} </td>
                  
                  <td>
                    <button type="button" href="#" data-href="{{ action('AdminController\DocumentController@update',[$document->id]) }}" class="btn btn-success btn-xs edit_model" data-toggle="modal" data-target="#edit_model"  data-container=".edit_model"><i class="fa fa-edit"></i></button>
                    <button type="button" href="{{ action('AdminController\DocumentController@delete',[$document->id]) }}" class="btn btn-danger btn-xs" id="delete"><i class="fa fa-trash-o"></i></button>
                    
                  </td>
                </tr>
                @endforeach                
              </tbody>
              </table>
            </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection