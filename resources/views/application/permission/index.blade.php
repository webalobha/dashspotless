@extends('application.layouts.app')
@section('title','Role')
@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <h1>
        Provide Permission
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Permission</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="heading-bg">
        <h3>Provide Permission</h3>
      </div>
      <!-- Info boxes -->
      <form class="form-horizontal">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Select Role<span class="required">*</span></label>
                  <div class="col-sm-8">
                    <select class="form-control" required>
                      <option>select</option>
                       <option>Manager</option>
                        <option>Support</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Select User<span class="required">*</span></label>
                  <div class="col-sm-8">
                    <select class="form-control" required>
                      <option>select</option>
                       <option>1</option>
                        <option>2</option>
                    </select>
                  </div>
                  <div class="col-sm-2">
                    <input type="button" name="" class="btn btn-main" id="" value="Search">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Name<span class="required">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" name="" class="form-control" id="" value="Manager 1" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Email<span class="required">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" name="" class="form-control" id="" value="demo@gmail.com" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <div class="table-responsive">
                  <table class="table table-bordered text-center permission_table">
                    <tr>
                      <td style="font-size: 16px; font-weight: 700;">Module</td>
                      <td style="font-size: 16px; font-weight: 700;">Add</td>
                      <td style="font-size: 16px; font-weight: 700;">View</td>
                      <td style="font-size: 16px; font-weight: 700;">Edit</td>
                      <td style="font-size: 16px; font-weight: 700;">Delete</td>
                      <td style="font-size: 16px; font-weight: 700;">Payment</td>
                      <td style="font-size: 16px; font-weight: 700;">Active/Inactive</td>
                    </tr>
                    <tr>
                      <td><b>User/Customer</b></td>
                      <td></td>
                      <td><input type="checkbox" name=""></td>
                      <td></td>
                      <td><input type="checkbox" name=""></td>
                      <td></td>
                      <td><input type="checkbox" name=""></td>
                    </tr>
                    <tr>
                      <td><b>Provider</b></td>
                      <td></td>
                      <td><input type="checkbox" name=""></td>
                      <td></td>
                      <td><input type="checkbox" name=""></td>
                      <td><input type="checkbox" name=""></td>
                      <td><input type="checkbox" name=""></td>
                    </tr>
                    <tr>
                      <td><b>Category</b></td>
                      <td><input type="checkbox" name=""></td>
                      <td><input type="checkbox" name=""></td>
                      <td><input type="checkbox" name=""></td>
                      <td><input type="checkbox" name=""></td>
                      <td></td>
                      <td><input type="checkbox" name=""></td>
                    </tr>
                    <tr>
                      <td><b>Sub Category</b></td>
                      <td><input type="checkbox" name=""></td>
                      <td><input type="checkbox" name=""></td>
                      <td><input type="checkbox" name=""></td>
                      <td><input type="checkbox" name=""></td>
                      <td></td>
                      <td><input type="checkbox" name=""></td>
                    </tr>
                    <tr>
                      <td><b>Variant</b></td>
                      <td><input type="checkbox" name=""></td>
                      <td><input type="checkbox" name=""></td>
                      <td><input type="checkbox" name=""></td>
                      <td><input type="checkbox" name=""></td>
                      <td></td>
                      <td><input type="checkbox" name=""></td>
                    </tr>
                    <tr>
                      <td><b>Service</b></td>
                      <td><input type="checkbox" name=""></td>
                      <td><input type="checkbox" name=""></td>
                      <td><input type="checkbox" name=""></td>
                      <td><input type="checkbox" name=""></td>
                      <td></td>
                      <td><input type="checkbox" name=""></td>
                    </tr>
                    <tr>
                      <td><b>Order</b></td>
                      <td></td>
                      <td><input type="checkbox" name=""></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td><b>Documents</b></td>
                      <td><input type="checkbox" name=""></td>
                      <td><input type="checkbox" name=""></td>
                      <td><input type="checkbox" name=""></td>
                      <td><input type="checkbox" name=""></td>
                      <td></td>
                      <td><input type="checkbox" name=""></td>
                    </tr>
                    <tr>
                      <td><b>Review & Rating</b></td>
                      <td></td>
                      <td><input type="checkbox" name=""></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td><b>Promocode</b></td>
                      <td><input type="checkbox" name=""></td>
                      <td><input type="checkbox" name=""></td>
                      <td><input type="checkbox" name=""></td>
                      <td><input type="checkbox" name=""></td>
                      <td></td>
                      <td><input type="checkbox" name=""></td>
                    </tr>
                  </table>
                </div>
                </div>

              <div class="box-footer text-right">
                <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-main">Add</button>
              </div>
              </div>
              <!-- /.box-body -->
              
            </div>
        <div class="col-md-2"></div>
      </div>
      
              <!-- /.box-footer -->
            </form>



  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

    <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Images</h4>
      </div>
      <div class="modal-body">
               <form class="form-horizontal">
      <div class="row">
        <div class="col-md-12">
          
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-4 control-label">Choose Image</label>

                  <div class="col-sm-8">
                    <input type="file" name="" class="form-control" id="" placeholder="Description">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              
            </div>
      </div>
      
      <div class="box-footer text-right">
                <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-main">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
      </div>
    </div>

  </div>
</div>
@endsection
