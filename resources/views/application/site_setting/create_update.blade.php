@extends('application.layouts.app')
@section('title','Site Setting')
@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Update Profile
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Site Setting</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       
      <div class="heading-bg">
        <h3>Site Setting</h3>
      </div>

      <div class="box-body">
        <ul class="nav nav-tabs sitesettingtabs">
        <li class="active"><a data-toggle="tab" href="#general">General</a></li>
        <li><a data-toggle="tab" href="#c_profile_link">Comp. Profile Links</a></li>
        <li><a data-toggle="tab" href="#s_link">Social Link Config</a></li>
        <li><a data-toggle="tab" href="#map_sms">Map & SMS Config</a></li>
        <li><a data-toggle="tab" href="#mail">Mail Config</a></li>
        <li><a data-toggle="tab" href="#push_note">Push Notification</a></li>
        <li><a data-toggle="tab" href="#payment">Payment Setting</a></li>
        <li><a data-toggle="tab" href="#service">Service</a></li>
      </ul>

  <div class="tab-content" style="margin-top: 50px;">
    <div id="general" class="tab-pane fade in active">
     <form class="form-horizontal" action="{{ action('AdminController\SiteSettingController@edit') }}" method="post" id="add_form">
        @csrf
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="form-group">
            <label for="" class="control-label">Site Name</label>
             <input type="text" class="form-control" id=""  name="name" value="{{$SiteSettingGeneral->name ??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Site Icon</label>
             <input type="file" class="form-control" id=""  name="site_icon_name" >
             <img src="{{ asset($SiteSettingGeneral->site_icon_img_path ?? '') }}" height="60" width="60">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Site Logo</label>
             <input type="file" class="form-control" id=""  name="site_logo_name">
             <img src="{{ asset($SiteSettingGeneral->site_logo_img_path ?? '') }}" height="60" width="60">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Contact Number</label>
             <input type="text" class="form-control" id=""  name="contact_number" value="{{$SiteSettingGeneral->contact_number ?? ''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Contact Email</label>
             <input type="text" class="form-control" id=""  name="contact_email" value="{{$SiteSettingGeneral->contact_email??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">SOS Number</label>
             <input type="text" class="form-control" id=""  name="sos_number" value="{{$SiteSettingGeneral->sos_number??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Copyright Content</label>
             <input type="text" class="form-control" id=""  name="copyright_content" value="{{$SiteSettingGeneral->copyright_content??''}}">
          </div>

<input type ="hidden" name="general_setting" value="1">

           <div class="box-footer text-right">
          <button type="reset" class="btn btn-default">Clear</button>
          <button type="submit" name="general_setting" class="btn btn-main">Submit</button>
        </div>
        <!-- /.box-footer -->
        </div>
          
        <!-- /.box-body -->
      </div>
    <div class="col-md-2"></div>
  </form>
    </div>
    <div id="c_profile_link" class="tab-pane fade">
      <form class="form-horizontal" action="{{ action('AdminController\SiteSettingController@edit') }}" method="post" id="add_form">
        @csrf
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
        <div class="form-group">
            <label for="" class="control-label">Playstore User Link</label>
             <input type="text" class="form-control" id=""  name="playstore_user_link" value="{{$SiteSettingCompProfileLink->playstore_user_link ??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Playstore Provider Link</label>
             <input type="text" class="form-control" id=""  name="playstore_provider_link" value="{{$SiteSettingCompProfileLink->playstore_provider_link??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Apple store User Link</label>
             <input type="text" class="form-control" id=""  name="appstore_user_link" value="{{$SiteSettingCompProfileLink->appstore_user_link??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Apple store Provider Link</label>
             <input type="text" class="form-control" id=""  name="appstore_provider_link" value="{{$SiteSettingCompProfileLink->appstore_provider_link??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Facebook Link</label>
             <input type="text" class="form-control" id=""  name="facebook_link" value="{{$SiteSettingCompProfileLink->facebook_link??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Twitter</label>
             <input type="text" class="form-control" id=""  name="twitter_link" value="{{$SiteSettingCompProfileLink->twitter_link??''}}">
          </div>

          <input type ="hidden" name="company_profile_setting" value="1">
           <div class="box-footer text-right">
          <button type="reset" class="btn btn-default">Clear</button>
          <button type="submit" name="company_profile_setting" class="btn btn-main">Submit</button>
        </div>
        <!-- /.box-footer -->
        </div>
        <!-- /.box-body -->
      </div>
    <div class="col-md-2"></div>
  </form>
    </div>
    <div id="s_link" class="tab-pane fade">
     <form class="form-horizontal" action="{{ action('AdminController\SiteSettingController@edit') }}" method="post" id="add_form">
        @csrf
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="form-group">
            <div>
              <label for="" class="col-md-2" style="padding-left: 0;">Social Login</label>
              <label class="switch">
                @php
                 $isSocialLogin = $SiteSettingsocialLink->is_social_login ?? '';
                @endphp
                <input type="checkbox" name="is_social_login" value="1" {{$isSocialLogin =='1'?'checked':''}}>
                <span class="slider round"></span>
              </label>
            </div>
          </div>
          <div class="form-group">
            <label for="" class="control-label">Facebook App ID</label>
             <input type="text" class="form-control" id=""  name="facebook_app_id" value="{{$SiteSettingsocialLink->facebook_app_id??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Facebook App Secret </label>
             <input type="text" class="form-control" id=""  name="facebook_app_secret" value="{{$SiteSettingsocialLink->facebook_app_secret??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Google Client ID</label>
             <input type="text" class="form-control" id=""  name="google_client_id" value="{{$SiteSettingsocialLink->google_client_id??''}}">
          </div>

<input type ="hidden" name="social_link_setting" value="1">


           <div class="box-footer text-right">
          <button type="reset" class="btn btn-default">Clear</button>
          <button type="submit" name="social_link_setting" class="btn btn-main">Submit</button>
        </div>
        <!-- /.box-footer -->
        </div>
        <!-- /.box-body -->
      </div>
    <div class="col-md-2"></div>
  </form>
    </div>
    <div id="map_sms" class="tab-pane fade">
       <form class="form-horizontal" action="{{ action('AdminController\SiteSettingController@edit') }}" method="post" id="add_form">
        @csrf
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="form-group">
            <label for="" class="control-label">Browser Map Key</label>
             <input type="text" class="form-control" id=""  name="browser_map_key"  value="{{$SiteSettingMapSms->browser_map_key??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Server Map Key</label>
             <input type="text" class="form-control" id=""  name="secret_map_key" value="{{$SiteSettingMapSms->secret_map_key??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Android Map Key</label>
             <input type="text" class="form-control" id=""  name="android_map_key" value="{{$SiteSettingMapSms->android_map_key??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">IOS Map Key</label>
             <input type="text" class="form-control" id=""  name="ios_map_key" value="{{$SiteSettingMapSms->ios_map_key??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">FB App Version</label>
             <input type="text" class="form-control" id=""  name="fb_app_version" value="{{$SiteSettingMapSms->fb_app_version??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">FB App Secret</label>
             <input type="text" class="form-control" id=""  name="fb_app_secret" value="{{$SiteSettingMapSms->fb_app_secret??''}}">
          </div>
          <input type ="hidden" name="map_sms_setting" value="1">
           <div class="box-footer text-right">
          <button type="reset" class="btn btn-default">Clear</button>
          <button type="submit" name="map_sms_setting" class="btn btn-main">Submit</button>
        </div>
        <!-- /.box-footer -->
        </div>
        <!-- /.box-body -->
      </div>
    <div class="col-md-2"></div>
  </form>
    </div>
    <div id="mail" class="tab-pane fade">
    <form class="form-horizontal" action="{{ action('AdminController\SiteSettingController@edit') }}" method="post" id="add_form">
        @csrf
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="form-group">
            <div>
              <label for="" class="col-md-2" style="padding-left: 0;">Send Mail</label>
              <label class="switch">
                @php 
                $isSendMail = $SiteSettingMail->is_send_mail ?? '';
                @endphp
                <input type="checkbox" name="is_send_mail" value="1" @if($isSendMail == 1) checked @endif>
                <span class="slider round"></span>
              </label>
            </div>
          </div>
          <div class="form-group">
            <div>
              <label for="" class="col-md-2" style="padding-left: 0;">Send SMS</label>
             <label class="switch">
                @php 
                $isSendSms = $SiteSettingMail->is_send_sms ?? '';
                @endphp
                <input type="checkbox" name="is_send_sms" value="1" @if($isSendSms == 1) checked @endif>
                <span class="slider round"></span>
              </label>

            </div>
          </div>
          <div class="form-group">
            <label for="" class="control-label">SMS Provider</label>
             <input type="text" class="form-control" id=""  name="sms_provider_id" value="{{$SiteSettingMail->sms_provider_id??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Twilio Account SID </label>
             <input type="text" class="form-control" id=""  name="twilio_account_sid" value="{{$SiteSettingMail->twilio_account_sid??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Twilio Auth Token</label>
             <input type="text" class="form-control" id=""  name="twilio_auth_token" value="{{$SiteSettingMail->twilio_auth_token??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Twilio From Number</label>
             <input type="text" class="form-control" id=""  name="twilio_from_number" value="{{$SiteSettingMail->twilio_from_number??''}}">
          </div>
           <div class="box-footer text-right">
      <input type ="hidden" name="mail_setting" value="1">        
          <button type="reset" class="btn btn-default">Clear</button>
          <button type="submit" name="mail_setting" class="btn btn-main">Submit</button>
        </div>
        <!-- /.box-footer -->
        </div>
        <!-- /.box-body -->
      </div>
    <div class="col-md-2"></div>
  </form>
    </div>
    <div id="push_note" class="tab-pane fade">
    <form class="form-horizontal" action="{{ action('AdminController\SiteSettingController@edit') }}" method="post" id="add_form">
        @csrf
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="form-group">
            <label for="" class="control-label">IOS Push Environment</label>
             <input type="text" class="form-control" id=""  name="ios_push_environment" value="{{$SiteSettingPushNotification->ios_push_environment ??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">IOS Push User Pem</label>
             <input type="file" class="form-control" id=""  name="ios_push_user_pem">
             @php
             $iosPushUserPem = $SiteSettingPushNotification->ios_push_user_pem ?? '';
             @endphp
             @if($iosPushUserPem != '')
             <img src="{{ asset($SiteSettingPushNotification->ios_push_user_pem ?? '' ) }}" height="50" width="50">
             @endif
          </div>
          <div class="form-group">
            <label for="" class="control-label">IOS Push Provider Pem</label>
             <input type="file" class="form-control" id=""  name="ios_push_provider_pem">
             @php
             $iosPushProviderPem = $SiteSettingPushNotification->ios_push_provider_pem ?? '';
             @endphp
             @if($iosPushProviderPem != '')
              <img src="{{ asset($SiteSettingPushNotification->ios_push_provider_pem ?? '' ) }}" height="50" width="50">
             @endif
          </div>
          <div class="form-group">
            <label for="" class="control-label">IOS Push Password</label>
             <input type="text" class="form-control" id=""  name="ios_push_password" value="{{$SiteSettingPushNotification->ios_push_password??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Android Push Key</label>
             <input type="text" class="form-control" id=""  name="android_push_key" value="{{$SiteSettingPushNotification->android_push_key??''}}">
          </div>

  <input type ="hidden" name="push_note_setting" value="1">

           <div class="box-footer text-right">
          <button type="reset" class="btn btn-default">Clear</button>
          <button type="submit" name="push_note_setting" class="btn btn-main">Submit</button>
        </div>
        <!-- /.box-footer -->
        </div>
        <!-- /.box-body -->
      </div>
    <div class="col-md-2"></div>
  </form>
    </div>
    <div id="payment" class="tab-pane fade">
     <form class="form-horizontal" action="{{ action('AdminController\SiteSettingController@edit') }}" method="post" id="add_form">
        @csrf
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="form-group">
            <div>
              <label for="" class="col-md-2" style="padding-left: 0;">Cash</label>
              <label class="switch">
               @php
                $isCash = $SitePaymentSetting->is_cash ?? '';
               @endphp
                <input type="checkbox" name="is_cash" value="1" {{$isCash =='1'?'checked':''}}>
                <span class="slider round"></span>
              </label>
            </div>
          </div>
          <div class="form-group">
            <div>
              <label for="" class="col-md-2" style="padding-left: 0;">Card</label>
             <label class="switch">
               @php
                $isCard = $SitePaymentSetting->is_card ?? '';
               @endphp
                <input type="checkbox" name="is_card" value="1" {{$isCard =='1'?'checked':''}}>
                <span class="slider round"></span>
              </label>

            </div>
          </div>
          <div class="form-group">
            <label for="" class="control-label">Stripe Secret Key</label>
             <input type="text" class="form-control" id=""  name="stripe_secret_key" value="{{$SitePaymentSetting->stripe_secret_key??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Stripe Publishable Key </label>
             <input type="text" class="form-control" id=""  name="stripe_publishable_key" value="{{$SitePaymentSetting->stripe_publishable_key??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Stripe Currency</label>
             <input type="text" class="form-control" id=""  name="stripe_currency" value="{{$SitePaymentSetting->stripe_currency??''}}">
          </div>

           <input type ="hidden" name="payment_setting" value="1">
           <div class="box-footer text-right">
          <button type="reset" class="btn btn-default">Clear</button>
          <button type="submit" name="payment_setting" class="btn btn-main">Submit</button>
        </div>
        <!-- /.box-footer -->
        </div>
        <!-- /.box-body -->
      </div>
    <div class="col-md-2"></div>
  </form>
    </div>
    <div id="service" class="tab-pane fade">
   <form class="form-horizontal" action="{{ action('AdminController\SiteSettingController@edit') }}" method="post" id="add_form">
        @csrf
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="form-group">
            <label for="" class="control-label">Provider Accept Timeout(secs)</label>
             <input type="text" class="form-control" id=""  name="provider_accept_timeout" value="{{$SiteSettingService->provider_accept_timeout??''}}">
          </div>
          <div class="form-group">
            <label for="" class="control-label">Provider Search Radius(Kms) </label>
             <input type="text" class="form-control" id=""  name="provider_search_radius" value="{{$SiteSettingService->provider_search_radius??''}}">
          </div>
           <div class="form-group">
            <div>
              <label for="" class="col-md-2" style="padding-left: 0;">Serve OTP</label>
             <label class="switch">
                <input type="checkbox" name="serve_otp" value="1" {{$SiteSettingService->serve_otp??''=='1'?'checked':''}}>
                <span class="slider round"></span>
              </label>

            </div>
          </div>
          <div class="form-group">
            <label for="" class="control-label">Booking ID Prefix</label>
             <input type="text" class="form-control" id=""  name="booking_id_prefix" value="{{$SiteSettingService->booking_id_prefix??''}}">
          </div>

  <input type ="hidden" name="service_setting" value="1">         
           <div class="box-footer text-right">
          <button type="reset" class="btn btn-default">Clear</button>
          <button type="submit" name="service_setting" class="btn btn-main">Submit</button>
        </div>
        <!-- /.box-footer -->
        </div>
        <!-- /.box-body -->
      </div>
    <div class="col-md-2"></div>
  </form>
    </div>
  </div>
      </div>
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection