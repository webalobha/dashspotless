@extends('application.layouts.app')
@section('title','Category')
@section('content')
     
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Add Manager
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">CMS Pages</li>
      </ol>
    </section>

 <div class="row">
        <div class="col-md-12 text-right">
         <a href="{{ url('cms-page') }}" class="btn btn-main"><i class="fa fa-plus"></i> View CMS Page</a>
        </div>
      </div>
    <!-- Main content -->
    <section class="content">
       

      <div class="heading-bg">
        <h3> About us</h3>
      </div>


      <div class="box-body">
      <!-- Info boxes -->
      <form class="form-horizontal" action="{{ action('AdminController\CMSPageController@store') }}" method="post" id="add_form">
        @csrf
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
              <label for="" class="control-label">Page Name<span class="required">*</span></label>
              <input type="text" name="name" class="form-control" placeholder="Page title" required>
          </div>

          <div class="form-group">
            <label for="" class="control-label">Position<span class="required">*</span></label>
             <select class="form-control" name="position" required>
               <option>Select Position</option>
               <option value="1">1</option>
               <option value="2">2</option>
               <option value="3">3</option>
               <option value="4">4</option>
               <option value="5">5</option>
               <option value="6">6</option>
               <option value="7">7</option>
               
             
             </select>
          </div>
         
          <div class="form-group">
            <label for="" class="control-label">Description<span class="required">*</span></label>
             <textarea id="editor" name="page" rows="10" cols="80" class="form-control" placeholder="Page content" required>
            Page content
              </textarea>
          </div>
            <a href="<?php $_SERVER['PHP_SELF']; ?>" class="btn btn-default">Cancel</a>
            <button type="submit" class="btn btn-main">Submit</button>
          </div>
        <!-- /.box-footer -->
        </div>
        <!-- /.box-body -->
        
      </div>

      </form>
    </div>
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection