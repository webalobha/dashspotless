@extends('application.layouts.app')
@section('title','>CMS Page')
@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
   
        

      <!-- <h1>
       >CMS Page
      </h1> -->
     
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">CMS Page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 text-right">
         <a href="{{ url('add-cms-page') }}" class="btn btn-main"><i class="fa fa-plus"></i> Add CMS Page</a>
        </div>
      </div>

          <div class="heading-bg" style="margin-top: 30px;">
            <h3>CMC Page List</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered table-striped display nowrap">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Page Name</th>
                  <th>Slug</th>
                  <th>Position</th>
                 
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
@php $i=1;    @endphp
 @foreach($CmsPages as $CmsPage)

                <tr>
                  <td>{{$i}}</td>
                  <td>{{$CmsPage->name}}
                  </td>
                  <td>{{$CmsPage->slug??''}}</td>
                  <td>
                    {{$CmsPage->position??''}}</td>
                
                  <td>
                    <a href="{{ action('AdminController\CMSPageController@view',[$CmsPage->id]) }}" type="button" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a>
                    <a href="{{ action('AdminController\CMSPageController@update',[$CmsPage->id]) }}" type="button" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></a>
                     <button type="button" href="{{ action('AdminController\CMSPageController@delete',[$CmsPage->id]) }}" class="btn btn-danger btn-xs" id="delete"><i class="fa fa-trash-o"></i></button>
                    
                  </td>
                </tr>
            
                @php $i++ @endphp
                @endforeach  
              </tbody>
              </table>
            </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection