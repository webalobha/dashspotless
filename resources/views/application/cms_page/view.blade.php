@extends('application.layouts.app')
@section('title','Category')
@section('content')
     
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Add Manager
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Detail CMS Pages</li>
      </ol>
    </section>
<div class="row">
        <div class="col-md-12 text-right">
         <a href="{{ url('cms-page') }}" class="btn btn-main"><i class="fa fa-plus"></i> View CMS Page</a>
        </div>
      </div>
    <!-- Main content -->
    <section class="content">
       
      <div class="heading-bg">
        <h3> Detail CMS Pages</h3>
      </div>
      

      <div class="box-body">
      <!-- Info boxes -->
      <form class="form-horizontal" action="{{ action('AdminController\CMSPageController@edit',[$CmsPages->id]) }}" method="post" id="add_form">
        @csrf
      <div class="row">

         <input type="hidden" name="name" value="" required>
        <div class="col-md-12">
          <div class="form-group">
              <label for="" class="control-label">Page Name<span class="required">*</span></label>
              <input type="text" name="name" class="form-control" placeholder="Page title" value="{{$CmsPages->name}}" required disabled="">
          </div>

          <div class="form-group">
              <label for="" class="control-label">Slug<span class="required">*</span></label>
              <input type="text" name="slug" class="form-control" placeholder="Page slug" value="{{$CmsPages->slug}}" required readonly="" disabled="">
          </div>

          <div class="form-group">
            <label for="" class="control-label">Position<span class="required">*</span></label>
             <select class="form-control" name="position" required disabled="">
               <option>Select Position</option>
               <option value="1" {{$CmsPages->id=='1'?'selected':''}}>1</option>
               <option value="2"  {{$CmsPages->id=='2'?'selected':''}}>2</option>
               <option value="3"  {{$CmsPages->id=='3'?'selected':''}}>3</option>
               <option value="4"  {{$CmsPages->id=='4'?'selected':''}}>4</option>
               <option value="5"  {{$CmsPages->id=='5'?'selected':''}}>5</option>
               <option value="6"  {{$CmsPages->id=='6'?'selected':''}}>6</option>
               <option value="7"  {{$CmsPages->id=='7'?'selected':''}}>7</option>
               
             
             </select>
          </div>
         
          <div class="form-group">
            <label for="" class="control-label">Description<span class="required">*</span></label>
             <textarea id="editor" name="page" rows="10" cols="80" class="form-control" placeholder="Page content"  value="{{$CmsPages->page}}" required disabled="">
           {{$CmsPages->page}}
              </textarea>
          </div>
         
        <!-- /.box-footer -->
        </div>
        <!-- /.box-body -->
        
      </div>

      </form>
    </div>
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection