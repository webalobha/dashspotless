@extends('application.layouts.app')
@section('title','>CMS Page')
@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
   
        

      <!-- <h1>
       >CMS Page
      </h1> -->
     
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Assign Page List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12 text-right">
         <a href="{{ url('add-cms-page') }}" class="btn btn-main"><i class="fa fa-plus"></i> Add CMS Page</a>
        </div>
      </div>



 <div class="heading-bg">
        <h3> Assign Page</h3>
      </div>


      <div class="box-body">
      <!-- Info boxes -->
      <form class="form-horizontal" action="{{ action('AdminController\CMSPageController@store_assign') }}" method="post" id="add_form">
        @csrf
      <div class="row">
        <div class="col-md-12">
         

                                       
          <div class="form-group">
            <label for="" class="control-label">Select Page<span class="required">*</span></label>
             <select class="form-control" name="page_id" required>
               <option value="">Select</option>
               @foreach($CmsPages as $page)
               <option value="{{$page->id}}">{{$page->name}}</option>
               @endforeach
             </select>
          </div>


          <div class="form-check">
      <label class="form-check-label" for="check1" >
        @php $i=1; @endphp
         @foreach($CmsPageParentType as $cmstype)
        <input type="checkbox" class="form-check-input chkPassport" id="checkbox{{$cmstype->id}}" name="type[{{$i}}][type]" value="{{$cmstype->id}}">{{$cmstype->name}}
       @php $i++; @endphp
         @endforeach
      </label>
    </div>

<div class="form-group" id="usertype"  style="display: none">
            <label for="" class="control-label">Select app side<span class="required">*</span></label>
             <select class="form-control" name="user_or_provider_id" required>
               <option value="">Select</option>
              
               <option value="1">User</option>
               <option value="2">Provider</option>
              
             </select>
          </div>

  
         
        
          <div class="box-footer text-right">
            <button type="submit" class="btn btn-default">Cancel</button>
            <button type="submit" class="btn btn-main">Submit</button>
          </div>
        <!-- /.box-footer -->
        </div>
        <!-- /.box-body -->
        
      </div>

      </form>
    </div>








          <div class="heading-bg" style="margin-top: 30px;">
            <h3>Assign Page List</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered table-striped display nowrap">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Page Name</th>
                  <th>Assign Type</th>
                  <th>App Type</th>
                 @php $i=1;    @endphp
 @foreach($CmsPageParent as $CmsPagedata)

                <tr>
                  <td>{{$i}}</td>
                  <td>{{$CmsPagedata->PageName->name??''}}
                  </td>
                 
                  <td>
                    {{$CmsPagedata->PageType->name??''}}</td>
                    <td>
                    {{$CmsPagedata->UserType->name??''}}</td>
                  <td>
                 <button type="button" href="{{ action('AdminController\CMSPageController@deleteAssign',[$CmsPagedata->id]) }}" class="btn btn-danger btn-xs" id="delete"><i class="fa fa-trash-o"></i></button>
                   </td>
                </tr>
            
                @php $i++ @endphp
                @endforeach
                </tr>
                </thead>
                <tbody>
  
              </tbody>
              </table>
            </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection


@section('js')

<script type="text/javascript">
//

  $(document).ready(function(){
        $('input[type="checkbox"]').click(function(){
            if($(this).prop("checked") == true){
              var dd =  $(this).is(':checked');
             if(this.value=='4'){
               $("#usertype").show();
             }
             
            }
            else if($(this).prop("checked") == false){
               var dd =  $(this).is(':checked');

            $("#usertype").hide();
            }
        });
    });



 
</script>

 @endsection 