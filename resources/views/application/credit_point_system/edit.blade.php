@extends('application.layouts.app')
@section('title','Update Credit Point')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <h1>
        Add Category
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Update Credit Point</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="heading-bg">
        <h3>Update Credit Point</h3>
      </div>
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <form class="form-horizontal" action="{{ action('AdminController\CreditPointPackageController@edit',[$creditRecharge->id]) }}" id="add_form" method="post">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Title<span class="required">*</span></label>

                  <div class="col-sm-9">
                    <input type="text" name="name" class="form-control" id="" placeholder="Title " onkeypress="return numbersonly(event)" value="{{$creditRecharge->name}}" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Points<span class="required">*</span></label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="points" placeholder="Points" name="number_of_points" value="{{$creditRecharge->number_of_points}}" onkeypress="return numbersonly(event)" onInput="priceValue()" required>
                  </div>
                </div>
              <input type="hidden" name="price" id="price" value="{{$PricePerPoint->price}}">
               
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Amount<span class="required">*</span></label>
            <div class="col-sm-10">
            <input type="text" name="actual_price" id="total_price" value="{{$creditRecharge->actual_price}}" class="form-control" onInput="priceValue()"  onkeypress="return numbersonly(event)" readonly="">
            <!-- <span id="lblValue">The text box contains: </span> -->
          </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Discount Type</label>
            <div class="col-sm-10">

             
            <select class="form-control" name="discount_type_id" id="discount_type">
              @foreach($discountType as $discountTypes)
              <option value='{{ $discountTypes->id ?? "" }}' {{$discountTypes->id==$creditRecharge->discount_type_id?'selected':''}}>{{ $discountTypes->name ?? '' }}</option>
              @endforeach
              <!-- <option value='percentage'>percentage</option> -->
            </select>
          </div>
          </div>
          <div class="form-group discounted" id="discounted" style="display: ;">
            <label for="" class="col-sm-2 control-label">Percentage/Flat Discount</label>
            <div class="col-sm-10">
            <input type="text" name="discount_given" id="discount" onInput="discountValue()" class="form-control" value="{{$creditRecharge->discount_given}}">
          </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Discount Amount</label>
            <div class="col-sm-10">
            <input type="text" name="discounted_amount" id="discounted_price" value="{{$creditRecharge->discounted_amount}}" class="form-control" onkeypress="return numbersonly(event)"  required="required" readonly>
          </div>
          </div>
            
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Clear</button>
                <button type="submit" class="btn btn-main">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <div class="col-md-2"></div>
      </div>
      <!-- /.row -->
          

  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection


@section('js')

      


<script type="text/javascript">
  // alert('test');
  function priceValue() {
    var points = $( "#points" ).val();
     var price = $( "#price" ).val();
   
    var s = points*price;
    $("#total_price").val(s);
    //$("#discount").val('');
    //console.log(s);
    // var lblValue = document.getElementById("lblValue");
    // lblValue.innerText = "The text box contains: " + s;

    // var discount = document.getElementById("discount");
    // var s = discount.value; 
    // console.log  
  }
  function discountValue() {

  var discount =    $( "#discount" ).val();

  var points = $( "#points" ).val();
     var price = $( "#price" ).val();
   
    price = points*price;

  //alert(price);
  var discountType =    $( "#discount_type" ).val();
    //$("#discounted_price").val(price);
if(discountType=='1'){

  $("#discount").val('0');
  $("#discounted").hide();
}else if(discountType=='2'){

 //$("#discounted_price").val(totalAmount);

var calculateDiscount = price*discount/100;

var totalAmount = price-calculateDiscount;


 $("#discounted_price").val(totalAmount);

  }else if(discountType=='3'){


var totalAmount = price-discount;


 $("#discounted_price").val(totalAmount);

 }

  }
  
</script>
<script type="text/javascript">
  $(function() {    // Makes sure the code contained doesn't run until
                  //     all the DOM elements have loaded
    
    $('#discount_type').change(function(){
      var price =  $("#price").val(); 
      var discountId =    $( "#discount_type" ).val(); 
      // $("#discounted_price").val(price);
        $('#discount').val('');
      if (discountId == '1') {
        $('.discounted').hide();
       
         $("#discount").val('0');
        // $("#discounted_price").val(price);
     } else {
      $('.discounted').show();
     }
           
    });

});
</script>

@endsection

 
