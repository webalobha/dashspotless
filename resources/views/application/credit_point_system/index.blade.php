@extends('application.layouts.app')
@section('title','Credit Recharge')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <h1>
        Add Category
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Credit Recharge</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="heading-bg">
        <h3>Add Credit Recharge</h3>
      </div>
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <form class="form-horizontal" action="{{ action('AdminController\CreditPointPackageController@store') }}" id="add_form" method="post">
          	@csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Title<span class="required">*</span></label>

                  <div class="col-sm-9">
                    <input type="text" name="name" class="form-control" id="" placeholder="Title "  required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Points<span class="required">*</span></label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="points" placeholder="Points" name="number_of_points" onkeypress="return numbersonly(event)" onInput="priceValue()" required>
                  </div>
                </div>
              <input type="hidden" name="price" id="price" value="{{$PricePerPoint->price}}">
               
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Amount<span class="required">*</span></label>
            <div class="col-sm-10">
            <input type="text" name="actual_price" id="total_price" value="" class="form-control" onInput="priceValue()"  onkeypress="return numbersonly(event)" readonly="">
            <!-- <span id="lblValue">The text box contains: </span> -->
          </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Discount Type</label>
            <div class="col-sm-10">

             
            <select class="form-control" name="discount_type_id" id="discount_type">
              @foreach($discountType as $discountTypes)
              <option value='{{ $discountTypes->id ?? "" }}'>{{ $discountTypes->name ?? '' }}</option>
              @endforeach
              <!-- <option value='percentage'>percentage</option> -->
            </select>
          </div>
          </div>
          <div class="form-group discounted" id="discounted" style="display: none;">
            <label for="" class="col-sm-2 control-label">Percentage/Flat Discount</label>
            <div class="col-sm-10">
            <input type="text" name="discount_given" id="discount" onInput="discountValue()" class="form-control">
          </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Discount Amount</label>
            <div class="col-sm-10">
            <input type="text" name="discounted_amount" id="discounted_price" class="form-control" onkeypress="return numbersonly(event)" required="required" readonly>
          </div>
          </div>
          


                
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Clear</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <div class="col-md-2"></div>
      </div>
      <!-- /.row -->
          <div class="heading-bg" style="margin-top: 30px;">
            <h3> Credit Recharge List</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Title</th>
                  <th>Points</th>
                  <th>Amount</th>
                  <th>Discount Type</th>
                  <th>Discount</th>
                  <th>Amount After Discount</th>
                  <th>Action</th>
                </tr>
                </thead>
               <tbody>
                @php $count = 1;  @endphp
                @foreach($createcreditRecharges as $createcreditRecharge)
                <tr>
                  <td>{{ $count++ }}</td>
                  <td>{{ $createcreditRecharge->name ?? '' }}</td>
                  <td>{{ $createcreditRecharge->number_of_points ?? '' }}</td>
                  <td>{{ $createcreditRecharge->actual_price ?? '' }}</td>
                  <td>{{ $createcreditRecharge->discountType->name ?? '' }}</td>
                  <td>{{ $createcreditRecharge->discount_given ?? '' }}</td>
                   <td>{{ $createcreditRecharge->discounted_amount ?? '' }}</td>
                  <td>
                   

                   <a id="" href="{{ action('AdminController\CreditPointPackageController@update', [$createcreditRecharge->id]) }}" type="button" class="btn btn-success btn-xs edit_model"><i class="fa fa-edit"></i></a>



                    <a id="delete" href="{{ action('AdminController\CreditPointPackageController@delete', [$createcreditRecharge->id]) }}" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
              </table>
            </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection


@section('js')

      


<script type="text/javascript">
  // alert('test');
  function priceValue() {
    var points = $( "#points" ).val();
     var price = $( "#price" ).val();
   
    var s = points*price;
    $("#total_price").val(s);
    //$("#discount").val('');
    //console.log(s);
    // var lblValue = document.getElementById("lblValue");
    // lblValue.innerText = "The text box contains: " + s;

    // var discount = document.getElementById("discount");
    // var s = discount.value; 
    // console.log  
  }
  function discountValue() {

  var discount =    $( "#discount" ).val();

  var points = $( "#points" ).val();
     var price = $( "#price" ).val();
   
    price = points*price;

  //alert(price);
  var discountType =    $( "#discount_type" ).val();
    //$("#discounted_price").val(price);
if(discountType=='1'){

  $("#discount").val('0');
  $("#discounted").hide();
}else if(discountType=='2'){

 //$("#discounted_price").val(totalAmount);

var calculateDiscount = price*discount/100;

var totalAmount = price-calculateDiscount;


 $("#discounted_price").val(totalAmount);

  }else if(discountType=='3'){


var totalAmount = price-discount;


 $("#discounted_price").val(totalAmount);

 }

  }
  
</script>
<script type="text/javascript">
  $(function() {    // Makes sure the code contained doesn't run until
                  //     all the DOM elements have loaded
    
    $('#discount_type').change(function(){
      var price =  $("#price").val(); 
      var discountId =    $( "#discount_type" ).val(); 
      // $("#discounted_price").val(price);
        $('#discount').val('');
      if (discountId == '1') {
        $('.discounted').hide();
       
         $("#discount").val('0');
        // $("#discounted_price").val(price);
     } else {
      $('.discounted').show();
     }
           
    });

});
</script>

@endsection

 
