@extends('application.layouts.app')
@section('title','How It Works')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <h1>
        Add Category
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">How It Works</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="heading-bg">
        <h3>Amount Per points</h3>
      </div>
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <form class="form-horizontal" action="{{ action('AdminController\CreditPointPackageController@storePricePerPoint') }}" id="add_form" method="post">
          	@csrf
          	<input type="hidden" name="id" value="{{ $PricePerPoint->id ?? '' }}">
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Amount Per points<span class="required">*</span></label>

                  <div class="col-sm-9">
                    <input type="text" rows="10" name="price" value="{{ $PricePerPoint->price ?? '' }}" class="form-control" id="" placeholder="amount" onInput="priceValue()" onkeypress="return numbersonly(event)" required>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Clear</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
      </div>
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection