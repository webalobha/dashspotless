@extends('application.layouts.app')
@section('title','Managers')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <!--   <h1>
        Manager
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Manager</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">  
      <div class="row">
        <div class="col-md-12 text-right">
          <a href="{{ action('AdminController\AdminManageController@create') }}" class="btn btn-main"><i class="fa fa-plus"></i> Add Manager</a>
          <!-- <a href="add_permission.php" class="btn btn-main"><i class="fa fa-plus"></i> Add Permission</a> -->
        </div>
      </div>

      <form class="form-horizontal" action="" method="post">
        <div class="row">
      <div class="col-md-4">
        <label>Search by email or name</label>
        <input type="text" name="" class="form-control">
      </div>
       <div class="col-md-4">
         <label>Status</label>
        <select class="form-control">
          <option>Select Status</option>
          <option>On</option>
          <option>Off</option>
        </select>
       </div>
        <div class="col-md-4">
          <div style="margin-top: 24px;">
             <button class="btn btn-default">Clear</button>
          <button class="btn btn-main">Search</button>
          </div>
         
        </div>
    </div> 
      </form>
    <div class="heading-bg" style="margin-top: 30px;">
            <h3>Manager</h3>
          </div>
      <!-- /.row -->
      <div class="row">
          
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Role</th>
                  <!-- <th>Status</th> -->
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                $count = 1;
                @endphp
                @foreach($admins as $admin)
                <tr>
                  <td>{{ $count++ }}</td>
                  <td>{{ $admin->first_name ?? '' }} {{ $admin->last_name ?? '' }}</td>
                  <td>{{ $admin->email ?? '' }}</td>
                  <td>
                    @php
                     $roleName = $admin->getRoleNames() ?? '';
                    @endphp
                    {{ preg_replace('/[^A-Za-z0-9 ]/', '', $roleName) }}</td>
                  <!-- <td>
                       <label class="switch">
                <input type="checkbox" checked>
                <span class="slider round"></span>
              </label>
                  </td> -->
                  <td>
                    <a href="view_manager.php" type="button" class="btn btn-success btn-xs">View</a>
                    <a href="{{ action('AdminController\AdminManageController@update', [$admin->id]) }}" type="button" class="btn btn-primary btn-xs">Edit</a>
                    <button type="button" class="btn btn-danger btn-xs">Delete</button>
                  </td>
                </tr>
                @endforeach
               
              </tbody>
              </table>
            </div>
            </div>
            <!-- /.box-body -->
          </div>

  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection