@extends('application.layouts.app')
@section('title','Update Manager')
@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Update Manager
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Update Manager</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       
      <div class="heading-bg">
        <h3> Update Manager</h3>
      </div>

      <div class="box-body">
      <!-- Info boxes -->
      <form class="form-horizontal" method="post" action="{{ action('AdminController\AdminManageController@edit', [$admin->id]) }}" id="edit_form" autocomplete="off">
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
          <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Select Role<span class="required">*</span></label>
                  <div class="col-sm-10">
                   <select class="form-control" name="role" required>
                     <option value="">Select Role</option>
                     @foreach($roles as $role)
                     <option value="{{ $role->name ?? '' }}" @if($role->name == $roleName) selected @endif>{{ $role->name ?? '' }}</option>
                     @endforeach
                   </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">First Name<span class="required">*</span></label>
                  <div class="col-sm-10">
                   <input type="text" class="form-control" id=""  name="first_name" onkeypress="return alphaonly(event)" value="{{ $admin->first_name ?? '' }}" required>  
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Last Name</label>
                  <div class="col-sm-10">
                   <input type="text" class="form-control" id=""  name="last_name" onkeypress="return alphaonly(event)" value="{{ $admin->last_name ?? '' }}">
                  </div>
                </div>
               <!--  <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Gender<span class="required">*</span></label>

                  <div class="col-sm-10">
                   <label class="radio-inline"><input type="radio" name="optradio" checked>Male</label>
                    <label class="radio-inline"><input type="radio" name="optradio">Female</label>
                    <label class="radio-inline"><input type="radio" name="optradio">Other</label>
                  </div>
                </div> -->
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"> Email<span class="required">*</span></label>
                  <div class="col-sm-10">
                   <input type="email" class="form-control" id=""  name="email" onkeydown="validate(this.value);" value="{{ $admin->email ?? '' }}" required>
                   <span id="check" style="font-size: 10px;color: red;"></span>
                  </div>
                </div>
               <!--  <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Photo<span class="required">*</span></label>

                  <div class="col-sm-10">
                    <input type="file" name="" class="form-control" id="" accept="image/jpg, image/jpeg, image/png" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Phone<span class="required">*</span></label>
                  <div class="col-sm-10">
                   <input type="text" class="form-control" id=""  name="" onkeypress="return numbersonly(event)" required>
                  </div>
                </div> -->
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Password</span></label>
                  <div class="col-sm-10">
                   <input type="password" class="form-control" id=""  name="password">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Confirm Password</span></label>
                  <div class="col-sm-10">
                   <input type="password" class="form-control" id=""  name="confirm_password">
                  </div>
                </div>
                 <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Clear</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
              </div>
              <!-- /.box-body -->
              
            </div>
        <div class="col-md-1"></div>
      
            </form>
            </div>
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection