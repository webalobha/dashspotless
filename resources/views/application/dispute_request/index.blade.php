@extends('application.layouts.app')
@section('title','Disbute request')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
   
        

      <!-- <h1>
        Add Sub Category
      </h1> -->
     
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dispute Service Request</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 text-right">
         <a href="{{ url('add-dispute-request') }}" class="btn btn-main"><i class="fa fa-plus"></i> Add Service Dispute</a>
        </div>
      </div>

          <div class="heading-bg" style="margin-top: 30px;">
            <h3>Service Disputes</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered table-striped display nowrap">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Order ID</th>
                  <th>Raised By</th>
                  <th>Reason</th>
                  <th>Comment</th>
                  <th>Refund Amount</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=1; ?>
             @foreach($orderDisputes as $orderDispute)                  
                <tr>
                  <td>{{$i}}</td>
                  <td>{{$orderDispute->serviceOrders->unique_order_id??0}}
                  <td>{{$orderDispute->user->first_name??0}}</td>
                  <td>{{$orderDispute->Reason->title??'NA'}}</td>
                  <td>{{$orderDispute->comment??'NA'}}</td>
                   <td><!--order dispute refund amount--></td>
                  <td>{{$orderDispute->DisputeStatus->name??'NA'}}</td>
                  <td>
                   <!-- <a href="{{ action('AdminController\DisputeRequestController@view',[$orderDispute->id]) }}" type="button" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a> -->
                    <!-- <a href="{{ action('AdminController\DisputeRequestController@solution',[$orderDispute->id]) }}" type="button" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></a> -->
                    <button type="button" href="#" data-href="{{ action('AdminController\DisputeRequestController@viewSolution',[$orderDispute->id]) }}" class="btn btn-info btn-xs edit_model" data-toggle="modal" data-target="#edit_model"  data-container=".edit_model"><i class="fa fa-eye"></i></button>
                    <button type="button" href="#" data-href="{{ action('AdminController\DisputeRequestController@solution',[$orderDispute->id]) }}" class="btn btn-info btn-xs edit_model" data-toggle="modal" data-target="#edit_model"  data-container=".edit_model"><i class="fa fa-comment"></i></button>
                     <!-- <button type="button" href="{{ action('AdminController\DisputeRequestController@status',[$orderDispute->id]) }}" class="btn btn-danger btn-xs" id="delete"><i class="fa fa-trash-o"></i></button> -->
                     <!-- <button type="button" href="" class="btn btn-info btn-xs" title="Comment Solution"><i class="fa fa-comment"></i></button> -->
                     <button type="button" @if($orderDispute->dispute_status_id == 1) id="close" @else id="open" @endif href="{{ action('AdminController\DisputeRequestController@status',[$orderDispute->id]) }}" class="btn btn-primary btn-xs">@if($orderDispute->dispute_status_id == 1) Close @else Open @endif</button>
                  </td>
                </tr>
                 <?php $i++ ?>
                  @endforeach


              </tbody>
              </table>
            </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection