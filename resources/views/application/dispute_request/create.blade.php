@extends('application.layouts.app')
@section('title','Add Dispute Request')
@section('content')
   
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <!--  <h1>
        Add Manager
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add Dispute Request</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       
      <div class="heading-bg">
        <h3> Add Dispute Request</h3>
      </div>

      <div class="box-body">
      <!-- Info boxes -->
      <form class="form-horizontal" action="{{ action('AdminController\DisputeRequestController@store') }}" method="post" id="edit_model">
          @csrf
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Dispute Type<span class="required">*</span></label>
                  <div class="col-sm-10">
                    <select class="form-control" name="disputable_type" id="disputable_type" required>
                     <option value="">Select Dispute Type</option>
                     @foreach($DisputeReasonType as $reasonType)
                     <option value="{{$reasonType->id}}">{{$reasonType->name}}</option>
                     @endforeach                    
                   </select>                  
                 </div>
                </div>
                 <div class="form-group">
                  <label for="" class="col-sm-2 control-label">User/Provider<span class="required">*</span></label>
                   <div class="col-sm-10">
                  <select class="form-control users_or_providers" name="user_or_provider_id" id="users_or_providers" required>
                   
                   </select>
                    <div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Request Details<span class="required">*</span></label>
                  <div class="col-sm-10">
                    <table class="table table-bordered table-striped">
                      <thead>
                        <th>Order ID</th>
                        <th>Time Slot</th>
                        <th>Choose</th>
                      </thead>
                      <tbody id="orders">
                       
                        
                      </tbody>
                    </table>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Reason<span class="required">*</span></label>
                  <div class="col-sm-10">
                   <select class="form-control" required name="disputable_id" id="disputable_id">
                     
                   </select>
                  </div>
                </div>
                 <div class="box-footer text-right">
                
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
              </div>
              <!-- /.box-body -->
              
            </div>
        <div class="col-md-2"></div>
      
            </form>
            </div>
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection
@section('js')
<script type="text/javascript">
  
 $(document).ready(function() { 
     $('#disputable_type').on('change', function() {         
      var disputable_type = $(this).val(); 
      $('.city').empty();
      if(disputable_type) {
        $.ajax({
         url: '{{ action("AdminController\DisputeRequestController@user_or_provider") }}?disputable_type='+disputable_type,
         type: "GET",
         data : {"_token":"{{ csrf_token() }}"},
         dataType: "json",
         success:function(data) {
          // console.log(data);
          if(data){
           $('.users_or_providers').empty();
           $('.users_or_providers').focus;
           $('.users_or_providers').append('<option value="">Select</option>'); 
           $.each(data, function(key, value){                         
           $('.users_or_providers').append('<option value="'+ value.id +'">' + value.first_name + '</option>');
          });
          } else {
            $('.users_or_providers').empty();
          }
         }
        });
      } else {
        $('.users_or_providers').empty();
      }
     });
   });


//Resion list


 $(document).ready(function() { 
     $('#disputable_type').on('change', function() {         
      var disputable_type = $(this).val(); 
      $('.city').empty();
      if(disputable_type) {
        $.ajax({
         url: '{{ action("AdminController\DisputeRequestController@user_or_provider_reasion") }}?disputable_type='+disputable_type,
         type: "GET",
         data : {"_token":"{{ csrf_token() }}"},
         dataType: "json",
         success:function(data) {
          // console.log(data);
          if(data){
           $('#disputable_id').empty();
           $('#disputable_id').focus;
           $('#disputable_id').append('<option value="">Select</option>'); 
           $.each(data, function(key, value){                         
           $('#disputable_id').append('<option value="'+ value.id +'">' + value.title + '</option>');
          });
          } else {
            $('#disputable_id').empty();
          }
         }
        });
      } else {
        $('#disputable_id').empty();
      }
     });
   });

 //Resion list


 $(document).ready(function() { 
     $('#users_or_providers').on('change', function() {         
      var user_or_provider_id = $(this).val(); 
      var disputable_type = $('#disputable_type').val();
      $('#orders').empty();
      if(disputable_type) {
        $.ajax({
         url: '{{ action("AdminController\DisputeRequestController@orders_by_ajax") }}?disputable_type='+disputable_type,
         type: "GET",
         data : {"id":user_or_provider_id,

          "_token":"{{ csrf_token() }}"},
         dataType: "html",
         success:function(data) {
          
          


           $('#orders').html(data)

          
         }
        });
      } else {
        $('#orders').empty();
      }
     });
   });



</script>
@endsection