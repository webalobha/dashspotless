  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Solution</h4>
      </div>
      <div class="modal-body">
         @if(count($commentSolutions) != 0)
            @php $count = 1; @endphp
            @foreach($commentSolutions as $commentSolution)
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">[{{ $count++ }}]</label>

                  <div class="col-sm-10">
                    <textarea type="text" class="form-control" id="" placeholder="Comment your solution" name="comment" readonly>{{ $commentSolution->comment ?? '' }}</textarea>
                  </div>
                </div>
              </div> 
            @endforeach  
         @else
          <center>No Solutions</center>
         @endif       
      </div>
    </div>

  </div>