<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConsultVisitBookingDetailVariant extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='consult_visit_booking_detail_variant';
  
  # define fillable fildes
  protected $fillable = [
                      'id',
                      'consult_visit_id',
                      'variant_id',
                      'added_by', 
                      'updated_by'                  
  ];
}
