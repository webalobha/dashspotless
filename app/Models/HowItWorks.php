<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HowItWorks extends Model
{
  
  # define table
  protected $table ='how_it_works';
  
  # define fillable fields
  protected $fillable = [
  	                   'content',
  ];
}
