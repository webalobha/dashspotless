<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceSettingCancellationData extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='service_setting_cancellation_data';
  
  # define fillable fields
  protected $fillable = [
							'id',
							'service_setting_id',
							'description', 
							'hours', 
							'amount', 

						
                    
  ];

  
}
