<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SiteSettingGeneral extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='site_setting_general';
  
  # define fillable fildes
  protected $fillable = [
                  'id', 
                  'name', 
                  'site_icon_name', 
                  'site_icon_img_path',
                  'site_logo_name',
                  'site_logo_img_path',
                  'contact_number',
                  'contact_email', 
                  'sos_number',
                  'copyright_content',
                  'created_at',
                  'updated_at',
                  'deleted_at'                 
  ];
}
