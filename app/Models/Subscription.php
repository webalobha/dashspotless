<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='subscription';
  
  # define fillable fields
  protected $fillable = [
  	                   'subscription_duration_id',
  	                   'duration',
  	                   'discount_type_id',
  	                   'discount',
  	                   'price',
  	                   'description'
  ];

  /**
   * Modal has Many ProviderDocumentStatus
   * 
   * @retrun relation
   */
  public function subscriptionDuration()
  {
    return $this->belongsTo(\App\Models\SubscriptionDuration::class, 'subscription_duration_id', 'id');
  }

  /**
   * Modal has Many ProviderDocumentStatus
   * 
   * @retrun relation
   */
  public function discountType()
  {
    return $this->belongsTo(\App\Models\DiscountType::class, 'discount_type_id', 'id');
  }

  /**
   * Scope a query to only include monthly
   *
   * @param  \Illuminate\Database\Eloquent\Builder  $query
   * @return \Illuminate\Database\Eloquent\Builder
   */
  public function scopeMonthly($query)
  {
      return $query->where('subscription_duration_id', 2);
  }

  /**
   * Scope a query to only include weekly
   *
   * @param  \Illuminate\Database\Eloquent\Builder  $query
   * @return \Illuminate\Database\Eloquent\Builder
   */
  public function scopeWeekly($query)
  {
      return $query->where('subscription_duration_id', 1);
  }

  /**
   * @method fetch Expiry Date
   * @param
   * @retuen true
   */
  public function getExpiryDateAttribute()
  {
    $expiryDate = Carbon::now()->format('Y-m-d h:i:s');
    if($this->subscription_duration_id == 1 ) {
      $daysToAdd = $this->duration * 7;
      $expiryDate = Carbon::now()->addDays($daysToAdd)->endOfDay()->format('Y-m-d h:i:s');
    } elseif ($this->subscription_duration_id == 2) {
       $daysToAdd = $this->duration * 30;
       $expiryDate = Carbon::now()->addDays($daysToAdd)->endOfDay()->format('Y-m-d h:i:s');
    }

    return $expiryDate;
  }
}
