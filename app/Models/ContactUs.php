<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactUs extends Model
{
  
  # define table
  protected $table ='contact_us';
  
  # define fillable fields
  protected $fillable = [
  	                   'content',
  ];
}
