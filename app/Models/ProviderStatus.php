<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderStatus extends Model
{
  
  # define table
  protected $table ='providers_status';
  
  # define fillable fields
  protected $fillable = [
  	                   'name',
  ];
}
