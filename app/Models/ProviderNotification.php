<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderNotification extends Model
{
  
  # define table
  protected $table ='provider_notifications';
  
  # define fillable fields
  protected $fillable = [
                       'provider_id',
                       'order_id',
                       'notification',
  ];

  /**
   * Model has provider
   * @param
   * @return
   */
  public function provider()
  {
    return $this->belongsTo('\App\Models\Provider', 'provider_id', 'id');
  } 

  /**
  * Model has order
  * @param
  * @return
  */
  public function order()
  {
    return $this->belongsTo('\App\Models\Order', 'order_id', 'id');
  }
}
