<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserNotification extends Model
{
  
  # define table
  protected $table ='user_notifications';
  
  # define fillable fields
  protected $fillable = [
                       'user_id',
                       'order_id',
                       'notification',
  ];

  /**
   * Model has user
   * @param
   * @return
   */
  public function user()
  {
    return $this->belongsTo('\App\Models\User', 'user_id', 'id');
  } 

  /**
  * Model has order
  * @param
  * @return
  */
  public function order()
  {
    return $this->belongsTo('\App\Models\Order', 'order_id', 'id');
  }
}
