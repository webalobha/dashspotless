<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ProviderBankDetails extends Authenticatable
{
    use Notifiable;

    protected $table = 'provider_bank_details';

    protected $fillable = [
                        'id',
                        'provider_id',
                        'account_name',
                        'account_number',
                        'confirm_account_number',
                        'swift_code', 
                        'updated_by', 
                        'deleted_by',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

   
    
}