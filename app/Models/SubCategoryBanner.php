<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategoryBanner extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='sub_category_banners';
  
  # define fillable fildes
  protected $fillable = [
  	                   'sub_category_id',
  	                   'avtar_name',
  	                   'avtar_path',
  	                   'status',
  	                   'added_by',
  	                   'updated_by',
  	                   'deleted_by'
  ];
}
