<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CmsPageParent extends Model
{
  
  # define table
  protected $table ='cms_page_parents';
  
  # define fillable fields
  protected $fillable = [
  	                   'page_id',
  	                   'parent_id',
  	                   'user_or_provider_id',
  ];


  public function PageName()
  {
    return $this->belongsTo(\App\Models\CmsPages::class, 'page_id', 'id');
  }

  public function PageType()
  {
    return $this->belongsTo(\App\Models\CmsPageParentType::class, 'parent_id', 'id');
  }

   public function UserType()
  {
    return $this->belongsTo(\App\Models\DisputeReasonType::class, 'user_or_provider_id', 'id');
  }
}
