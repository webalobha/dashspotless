<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model
{
  
  # define table
  protected $table ='states';
  
  # define fillable fields
  protected $fillable = [
  	                   'name',
  	                   'country_id',
  ];

  /**
   * Model may have many Cities
   * @param
   * @return relation
   */
  public function cities()
  {
  	return $this->hasMany('App\Models\city');
  }
}
