<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConsultVisitBookingDetails extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='consult_visit_booking_details';
  
  # define fillable fildes
  protected $fillable = [
                    'id', 
                    'category_id',
                    'sub_category_id', 
                    'subject_line',
                    'amount',
                    'description',
                    'added_by',
                    'updated_by'                    
  ];
}
