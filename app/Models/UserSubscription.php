<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserSubscription extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='user_subscriptions';
  
  # define fillable fields
  protected $fillable = [
  	                   'user_id',
  	                   'subscription_id',
  	                   'subs_duration_type',
  	                   'duration',
  	                   'amount_for_subscription',
  	                   'discount_type',
  	                   'discount',
  	                   'status',
                       'expired_at',
  ];

	/**
	 * The attributes that should be cast.
	 *
	 * @var array
	 */
	protected $casts = [
	    'status' => 'boolean',
	];

  /**
   * The attributes that should be Dates.
   *
   * @var array
   */
  protected $dates = [
      'expired_at'
  ];
}
