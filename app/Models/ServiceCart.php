<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceCart extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='services_in_cart';
  
  # define fillable fields
  protected $fillable = [
  	                   'service_id',
  	                   'quantity',
                       'user_id'
  ];

  /**
   * Model belongs to Service
   * @param
   * @return relation
   */
  public function service()
  {
    return $this->belongsTo('App\Models\Service', 'service_id', 'id');
  }
  
}
