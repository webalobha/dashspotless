<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceSettingPenaltyData extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='service_setting_penalty_data';
  
  # define fillable fields
			protected $fillable = [
					'id',
					'service_setting_id',
					'description', 
					'hours', 
					'amount',

						
                    
  ];

  
}
