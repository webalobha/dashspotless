<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategoryVariants extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='sub_category_variants';
  
  # define fillable fildes
  protected $fillable = [
                       'category_id',
                       'sub_category_id',
                       'title_or_subject',
                       'label',
                       'radio_or_check',
                       'consult_visit_avail',
                       'avtar_path',
                       'status',
                       'is_variant_avail',
                       'added_by',
                       'updated_by',
                       'deleted_by'
  ];

  /**
   * Model has many Services 
   * @param
   * @return
   */
  public function services()
  {
    return $this->hasMany('\App\Models\Service', 'variant_id', 'id');
  }

  /**
   * Model belongs to Single Sub category
   * @param
   * @return relationship
   */
  public function subCategory()
  {
     return $this->belongsTo('\App\Models\SubCategory', 'sub_category_id', 'id');
  }
  
}