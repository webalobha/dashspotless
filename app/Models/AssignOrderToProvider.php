<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignOrderToProvider extends Model
{
  
  # define table
  protected $table ='assign_order_to_provider';
  
  # define fillable fields
  protected $fillable = [
  	                   'order_id',
  	                   'provider_id',
  	                   'status',
  ];

 	/**
    * The attributes that should be cast.
    *
    * @var array
    */
    protected $casts = [
        'status' => 'boolean',
    ];

  /**
   * Model has One order
   * @param 
   * @return Relation
   */
  public function order()
  {
    return $this->belongsTo('App\Models\Order', 'order_id', 'id');
  }

  /**
   * Model Belongs To Provider
   * @param 
   * @return Relation
   */
  public function provider()
  {
    return $this->belongsTo('App\Models\Provider', 'provider_id', 'id');
  }

  /**
   * Model Belongs To Assign Order Provider Status History
   * @param 
   * @return Relation
   */
  public function statusHistory()
  {
    return $this->hasMany('App\Models\AssignOrderToProviderHistory', 'assign_orderprovider_id', 'id');
  }

   /**
     * Scope a query to only include active Orders.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
