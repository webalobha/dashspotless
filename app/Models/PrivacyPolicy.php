<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrivacyPolicy extends Model
{
  # define table
  protected $table ='privacy_policy';
  
  # define fillable fields
  protected $fillable = [
  	                   'content',
  ];
}
