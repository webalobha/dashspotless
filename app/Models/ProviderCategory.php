<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ProviderCategory extends Authenticatable
{
    use Notifiable;

    protected $table = 'provider_category';

    protected $fillable = [
        'provider_id',
        'category_id',
    ];
}