<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TermOfUse extends Model
{
  
  # define table
  protected $table ='terms_of_use';
  
  # define fillable fields
  protected $fillable = [
  	                   'content',
  ];
}
