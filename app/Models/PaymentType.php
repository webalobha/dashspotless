<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentType extends Model
{
  
  # define table
  protected $table ='payment_type';
  
  # define fillable fields
  protected $fillable = [
  	                   'name',
  ];
}
