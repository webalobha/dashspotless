<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SiteSettingCompProfileLink extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='site_setting_comp_profile_link';
  
  # define fillable fildes
  protected $fillable = [
                        'id',
                        'playstore_user_link',
                        'playstore_provider_link',
                        'appstore_user_link',
                        'appstore_provider_link',
                        'facebook_link',
                        'twitter_link',
                        'created_at',
                        'updated_at'                    
  ];
}
