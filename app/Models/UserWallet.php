<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserWallet extends Model
{
  
  # define table
  protected $table ='user_wallet';
  
  # define fillable fields
  protected $fillable = [
  	                   'user_id',
  	                   'order_id',
  	                   'transaction_type',
  	                   'amount',
  ];
}
