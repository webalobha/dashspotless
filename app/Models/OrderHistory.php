<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderHistory extends Model 
{
  
  # define table
  protected $table ='order_histories';
  
  # define fillable fields
  protected $fillable = [
  	'order_id',
  	'order_status_id',
  	'comment',
  ];

  /**
   * Model has One TimeSlot
   * @param 
   * @return Relation
   */
  public function orderStatus()
  {
    return $this->belongsTo(\App\Models\OrderStatus::class, 'order_status_id', 'id');
  }
}
