<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
  
  # define table
  protected $table ='cities';
  
  # define fillable fields
  protected $fillable = [
  	                   'name',
  	                   'state_id',
  ];
}
