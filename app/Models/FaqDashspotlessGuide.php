<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FaqDashspotlessGuide extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='guide_for_dashspotless';
  
  # define fillable fields
  protected $fillable = [
					'title',
					'description',
  ];
}
