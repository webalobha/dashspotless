<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessCity extends Model
{
  //use SoftDeletes;
  
  # define table
  protected $table ='business_cities';
  
  # define fillable fields
  protected $fillable = [
					'id',
					'country_id',
					'state_id',
					'city_id',
					'status',
  	                      
  ];


public function Country()
  {
    return $this->belongsTo(\App\Models\Country::class, 'country_id', 'id');
  }

 public function State()
  {
    return $this->belongsTo(\App\Models\State::class, 'state_id', 'id');
  }
  
  public function City()
  {
    return $this->belongsTo(\App\Models\City::class, 'city_id', 'id');
  } 
 
}
