<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FaqPayingService extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='faq_paying_for_service';
  
  # define fillable fields
  protected $fillable = [
					'title',
					'description',
  ];
}
