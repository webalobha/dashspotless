<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrainingCenter extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='training_center';
  
  # define fillable fildes
  protected $fillable = [
                      'id',
                      'title',
                      'url',
  ];
}
