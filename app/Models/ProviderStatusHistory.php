<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProviderStatusHistory extends Model
{
  
  # define table
  protected $table ='provider_status_history';
  
  # define fillable fields
  protected $fillable = [
  	                   'provider_id',
  	                   'provider_status_id',
  	                   'comment',
  ];
}
