<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class   SiteSettingMapSms extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='site_setting_map_sms';
  
  # define fillable fildes
  protected $fillable = [
                'id', 
                'browser_map_key',
                'secret_map_key',
                'android_map_key',
                'ios_map_key',
                'fb_app_version',
                'fb_app_secret',
                'created_at', 
                'updated_at',
                'deleted_at'             
  ];
}
