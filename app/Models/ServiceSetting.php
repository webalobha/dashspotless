<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceSetting extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='service_setting';
  
  # define fillable fields
  protected $fillable = [
						'id',
						'service_request_type_id', 
						'day_or_hours', 'day_count', 
						'hour_count', 
						'proximate_range',
						'payment_limit_provider',
						
                    
  ];

  
}
