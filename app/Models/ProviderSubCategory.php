<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderSubCategory extends Model
{
    use SoftDeletes;
  
    protected $table = 'provider_sub_categories';

    protected $fillable = [
        'provider_id',
        'category_id',
        'sub_category_id',
    ];

}