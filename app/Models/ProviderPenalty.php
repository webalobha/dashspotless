<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderPenalty extends Model
{
  
  # define table
  protected $table ='provider_penalties_against_orders';
  
  # define fillable fields
  protected $fillable = [
  	                   'provider_id',
  	                   'order_id',
  	                   'penalty_points',
                       'comment',
  ];

  /**
     * Model belong to Order
     * 
     * @return relation
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id', 'id');
    }
}
