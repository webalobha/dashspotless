<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class   SiteSettingsocialLink extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='site_setting_social_link';
  
  # define fillable fildes
  protected $fillable = [
              'id', 
              'is_social_login',
              'facebook_app_id',
              'facebook_app_secret', 
              'google_client_id', 
              'created_at', 
              'updated_at', 
              'deleted_at'                 
  ];
}
