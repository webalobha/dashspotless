<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategory extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='sub_categories';
  
  # define fillable fildes
  protected $fillable = [
                       'category_id',
                       'name',
                       'title',
                       'discount_label',
                       'avtar_name',
                       'avtar_path',
                       'status',
                       'is_variant_avail',
                       'added_by',
                       'updated_by',
                       'deleted_by'
  ];

  /**
   * Model has many Categories
   * @param
   * @return
   */
  public function category()
  {
    return $this->belongsTo(\App\Models\Category::class, 'category_id', 'id');
  }

  /**
   * Model has many banners
   * @param
   * @return
   */
  public function banners()
  {
    return $this->hasMany('\App\Models\SubCategoryBanner', 'sub_category_id', 'id');
  }

  /**
   * Model has many variants
   * @param
   * @return
   */
  public function variants()
  {
    return $this->hasMany('\App\Models\SubCategoryVariants', 'sub_category_id', 'id');
  }

  /**
   * Model has many Services 
   * @param
   * @return
   */
  public function services()
  {
    return $this->hasMany('\App\Models\Service', 'sub_category_id', 'id');
  }

  /**
   * Model has many Expertise 
   * @param
   * @return
   */
  public function expertise()
  {
    return $this->hasMany('\App\Models\SubCategoriesExpertise', 'sub_category_id', 'id');
  }

  /**
    * Scope a query to only include active users.
    *
    * @param  \Illuminate\Database\Eloquent\Builder  $query
    * @return \Illuminate\Database\Eloquent\Builder
  */
  public function scopeActive($query)
  {
      return $query->where('status', 1);
  }
  
}
