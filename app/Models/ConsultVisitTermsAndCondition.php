<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConsultVisitTermsAndCondition extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='consult_visit_terms_and_condition';
  
  # define fillable fildes
  protected $fillable = [
                  'id', 
                  'category_id', 
                  'sub_category_id', 
                  'consultant_visit_id', 
                  'title',
                  'description',
                  'added_by', 
                  'updated_by'                   
  ];
}
