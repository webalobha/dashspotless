<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceSettingRescheduleData extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='service_setting_reschedule_data';
  
  # define fillable fields
  protected $fillable = [
							'service_setting_id',
							'description', 
							'hours', 
							'amount', 
  ];
}
