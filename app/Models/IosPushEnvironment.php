<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IosPushEnvironment extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='ios_push_environments';
  
  # define fillable fields
  protected $fillable = [
  	                   'name',
  ];
}
