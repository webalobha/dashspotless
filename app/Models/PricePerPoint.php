<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PricePerPoint extends Model
{
  //use SoftDeletes;
  
  # define table
  protected $table ='price_per_point';
  
  # define fillable fildes
  protected $fillable = [
                  'price', 
                 
  ];
}
