<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CmsPageParentType extends Model
{
  
  # define table
  protected $table ='cms_page_parent_type';
  
  # define fillable fields
  protected $fillable = [
						'name', 
  ];
}
