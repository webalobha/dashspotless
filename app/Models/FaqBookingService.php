<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FaqBookingService extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='faq_booking_service';
  
  # define fillable fields
  protected $fillable = [
					'title',
					'description',
  ];
}
