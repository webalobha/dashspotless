<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransactionType extends Model
{
  
  # define table
  protected $table ='transaction_types';
  
  # define fillable fields
  protected $fillable = [
  	                   'name',
  ];
}
