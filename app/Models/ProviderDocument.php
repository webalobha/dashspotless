<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ProviderDocument extends Authenticatable
{
    use Notifiable, SoftDeletes;

    //protected $guard = 'api';

    protected $fillable = [
        'provider_id',
        'document_id',
        'document_name',
        'document_image_path',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Model has Many document history
     * 
     * @return relation
     */
    public function histories()
    {
        return $this->hasMany('App\Models\ProviderDocumentStatusHistory', 'provider_document_id', 'id');
    }

    /**
     * Model has Many document name
     * 
     * @return relation
     */
    public function documentName()
    {
        return $this->belongsTo('App\Models\MandatoryDocumentsForProvider', 'document_id', 'id');
    }

    /**
     * Model has Provider
     * 
     * @return relation
     */
    public function provider()
    {
        return $this->belongsTo('App\Models\Provider', 'provider_id', 'id');
    }

    /**
     * Model has Many document name
     * 
     * @return relation
     */
    public function documentStatus()
    {
        return $this->belongsTo('App\Models\ProviderDocumentStatusHistory', 'id', 'provider_document_id');
    }
}