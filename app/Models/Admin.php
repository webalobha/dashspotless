<?php

namespace App\Models;

use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
  use HasRoles;

  use Notifiable;

  use SoftDeletes;
  
  # define table
  protected $table ='admin';
  
  # define fillable fildes
  protected $fillable = [
  	                   'first_name',
  	                   'middle_name',
  	                   'last_name',
  	                   'email',
  	                   'password',
  	                   'remember_token'
  ];

  /**
    * Scope a query to only not admin.
    *
    * @param  \Illuminate\Database\Eloquent\Builder  $query
    * @return \Illuminate\Database\Eloquent\Builder
  */
  public function scopeNotAdmin($query)
  {
      return $query->where('is_admin', '0');
  }
}
