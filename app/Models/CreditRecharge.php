<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreditRecharge extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='credit_recharge';
  
  # define fillable fildes
  protected $fillable = [
                  'points', 
                  'amount_per_point', 
                  'status', 
                  'added_by', 
                  'updated_by',
                  'deleted_by',
  ];
}
