<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use App\Http\Interfaces\TransactionTypeInterface;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements TransactionTypeInterface
{
    use Notifiable;

    //protected $guard = 'api';

    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name', 
        'mobile_number',
        'email',
        'status',
        'age',
        'gender',
        'latitude',
        'longitude',
        'device_type',
        'device_token',
        'android_key',
        'ios_key',
        'password',
        'api_token',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Model has Many Otp
     * 
     * @return relation
     */
    public function otp()
    {
        return $this->hasMany('App\Models\UserOtp', 'user_id', 'id');
    }

    /**
     * Model has Many Otp
     * 
     * @return relation
     */
    public function servicesInCart()
    {
        return $this->hasMany('App\Models\ServiceCart', 'user_id', 'id');
    }

    /**
     * Model has Many disputes
     * 
     * @return relation
     */
    public function disputes()
    {
        return $this->morphMany('App\Models\Dispute', 'disputable');
    }

    /**
     * Model has Many addresses
     * 
     * @return relation
     */
    public function address()
    {
        return $this->hasMany('App\Models\UserAddress');
    }

    /**
     * Model has Many Orders
     * 
     * @return relation
     */
    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    /**
     * Model has Many Subscription
     * 
     * @return relation
     */
    public function subscriptions()
    {
        return $this->hasMany('App\Models\UserSubscription');
    }


    /**
     * Model has Many User Wallet Transaction
     * 
     * @return relation
     */
    public function wallet()
    {
        return $this->hasMany('App\Models\UserWallet');
    }
    
    /**
     * @method to Find the Gender of User
     * @param 
     * @return string | ''
     */
    public function getGenderStringAttribute()
    {
        $genderString = '';
        if($this->gender != '') {
            if($this->gender == 1) {
                $genderString = 'Male';
            } elseif ($this->gender == 2) {
                $genderString = 'Female';
            }
        } 

        return $genderString;
    }
    /**
     * @method to count number of Services inside User Cart
     * @param 
     * @return number | 0
     */
    public function getServiceCountAttribute()
    {
        # get all the Service in Cart
        $servicesCount = $this->servicesInCart->isNotEmpty() ? $this->servicesInCart->count() : 0;

        return $servicesCount;
    }

    /**
     * @method to calculate total actual amount of user Services inside cart 
     * @param 
     * @return price | 0
     */
    public function getActualAmountOfServicesAttribute()
    {
        # Initialize Final Price
        $finalPrice = 0;

        # fetch all the User services
        $servicesInCart = $this->servicesInCart;

        if($servicesInCart->isNotEmpty()) {
            foreach ($servicesInCart as $key => $serviceIncart) {
                $finalPrice = $finalPrice + 
                              (($serviceIncart->service != '') ? ($serviceIncart->service->price * $serviceIncart->quantity) : 0);
            }
            return $finalPrice;
        } else {
            return $finalPrice;
        }
    }

    /**
     * @method to calculate total discount of user Services inside cart 
     * @param 
     * @return price | 0
     */
    public function getTotalDiscountOnServicesAttribute()
    {
        # Initialize Final Price
        $finalPrice = 0;

        # fetch all the User services
        $servicesInCart = $this->servicesInCart;

        if($servicesInCart->isNotEmpty()) {
            foreach ($servicesInCart as $key => $serviceIncart) {
                $service = $serviceIncart->service;
                $finalPrice += $service != '' ? $service->discount_given : 0;
            }
            return $finalPrice;
        } else {
            return $finalPrice;
        }
    }

    /**
     * @method to calculate total Price of user Services inside cart after discount
     * @param 
     * @return price | 0
     */
    public function getServicesFinalPriceInCartAttribute()
    {
        # Initialize Final Price
        $finalPrice = 0;

        # fetch all the User services
        $servicesInCart = $this->servicesInCart;

        if($servicesInCart->isNotEmpty()) {
            foreach ($servicesInCart as $key => $serviceIncart) {
                $service = $serviceIncart->service;
                if($service != '') {
                    $finalPrice += $service->price - $service->discount_given;
                } 
            }
            return $finalPrice;
        } else {
            return $finalPrice;
        }
    }

    /**
     * @method to fetch all the Valid Subscription 
     * @param
     * @return true|False
     */
    public function getValidSubscriptionsAttribute()
    {
        # fetch all the Subscription
       $subscriptions = $this->subscriptions->filter(function($subscription) {
                            $expirydate = $subscription->expired_at;
                            if($expirydate->gte(Carbon::now())) {
                                return $subscription;
                            }
                        });

       if($subscriptions->isNotEmpty()) {
            return true;
       } else{
            return false;
       }
       
    }

    /**
     * @method to calculate Wallet Remaining Amount
     * @param
     * @return Remainin Amount
     */
    public function getRemainingWalletAmountAttribute()
    {
        # FEtch all the Wallet Transactions
        $walletTransaction = $this->wallet;
        
        # Fetch Credit Amount
        $creditAmount = 0;
        if($walletTransaction->isNotEmpty()) {
            $creditAmount += $walletTransaction->where('transaction_type', TransactionTypeInterface::CREDIT)->sum('amount');
        }

        # Fetch Debit Amount
        $debitAmount = 0;
        if($walletTransaction->isNotEmpty()) {
            $debitAmount += $walletTransaction->where('transaction_type', TransactionTypeInterface::DEBIT)->sum('amount');
        }

        # reminaing Amount
        $remainingAmount = $creditAmount - $debitAmount;

        # retunr Amount
        return $remainingAmount;
    }
}