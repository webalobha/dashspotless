<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class   SiteSettingService extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='site_setting_service';
  
  # define fillable fildes
  protected $fillable = [
                    'id',
                    'provider_accept_timeout',
                    'provider_search_radius',
                    'serve_otp', 
                    'booking_id_prefix'                   
  ];
}
