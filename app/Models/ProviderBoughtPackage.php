<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderBoughtPackage extends Model
{
  
  # define table
  protected $table ='provider_bought_packages';
  
  # define fillable fields
  protected $fillable = [
  	                   'provider_id',
  	                   'credit_package_id',
  	                   'no_of_points',
  	                   'package_name',
  	                   'actual_price',
  	                   'dicount_title',
  	                   'discount_given',
  	                   'discounted_amount',
  	                   'transaction_id',
  	                   'payment_type',
  	                   'payment_amount',
  	                   'card_type',
  	                   'card_name',
  	                   'account_holder_name',
  	                   'bank_name',
  	                   'card_number',
  ];
}
