<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MandatoryDocumentsForProvider extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='mandatory_documents_for_provider';
  
  # define fillable fields
  protected $fillable = [
  	                   'name',
  	                   'added_by',
  	                   'updated_by',
  	                   'deleted_by'
  ];
}
