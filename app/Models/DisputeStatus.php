<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DisputeStatus extends Model
{
  
  # define table
  protected $table ='dispute_status';
  
  # define fillable fields
  protected $fillable = [
  	                   'name',
  ];
}
