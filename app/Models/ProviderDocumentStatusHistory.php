<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Interfaces\DocumentStatusInterface;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ProviderDocumentStatusHistory extends Authenticatable implements DocumentStatusInterface
{
    use Notifiable, SoftDeletes;

    protected $table = 'provider_document_status_history';

    protected $fillable = [
        'provider_document_id',
        'document_id',
        'document_status_id',
        'reason',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Attribute to Send Status of Provider Document
     * @param 
     * @return Status
     */
    public function getStatusAttribute()
    {
        # fetch the Current status 
        $status = $this->document_status_id;

        # initialize retiurn Status
        $statusString = '';

        # verify the Status
        if($status == DocumentStatusInterface::ACCEPTED) {
            $statusString = 'Accepted';
        } elseif ($status == DocumentStatusInterface::REJECTED) {
            $statusString = 'Rejected';
        }

        # return Status String
        return $statusString;
    }

    /**
     * Attribute check status is Approve
     * @param 
     * @return Status
     */
    public function getStatusApproveAttribute()
    {
        # fetch the Current status 
        $status = $this->document_status_id;

        # verify the Status
        if($status == DocumentStatusInterface::ACCEPTED) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Attribute check status is Reject
     * @param 
     * @return Status
     */
    public function getStatusRejectAttribute()
    {
        # fetch the Current status 
        $status = $this->document_status_id;

        # verify the Status
        if($status == DocumentStatusInterface::REJECTED) {
            return true;
        } else {
            return false;
        }
    }

  /**
   * Model has providerDocumentName
   * 
   * @return relation
   */
  public function providerDocumentName()
  {
    return $this->belongsTo(\App\Models\MandatoryDocumentsForProvider::class, 'provider_document_id', 'id');
  }
}