<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreditPointsPackage extends Model
{
 // use SoftDeletes;
  
  # define table
  protected $table ='credit_points_package';
  
  # define fillable fildes
  protected $fillable = [
                  'name',
                  'number_of_points', 
                  'actual_price', 
                  'dicount_title',
                  'discount_type_id', 
                  'discount_given',
                  'discounted_amount', 
                  'status',
  ];


  /**
   * Model has user
   * 
   * @return relation
   */
  public function discountType()
  {
    return $this->belongsTo('App\Models\DiscountType', 'discount_type_id', 'id');
  }

  /**
   * @method to fetch discount is PErcentage or Flat
   * @param
   * @return Discount
   */
  public function getDiscountStringAttribute()
  {
    $discount = '';
    if($this->discount_type_id == 2) {
      $discount = $this->discount_given.'%';
    } elseif ($this->discount_type_id == 3) {
      $discount = 'Rs '.$this->discount_given;
    } else {
      $discount = 'No Discount.';
    }
    return $discount;
  }
}
