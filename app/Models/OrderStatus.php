<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderStatus extends Model
{
  
  # define table
  protected $table ='order_statuses';
  
  # define fillable fields
  protected $fillable = [
  	                   'name',
  ];
}
