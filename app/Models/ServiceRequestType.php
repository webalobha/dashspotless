<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceRequestType extends Model
{
  //use SoftDeletes;
  
  # define table
  protected $table ='service_request_type';
  
  # define fillable fields
  protected $fillable = [
  	                   'name',
  ];
}
