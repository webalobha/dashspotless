<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignOrderProviderStatus extends Model
{
  
  # define table
  protected $table ='assign_order_to_provider_status';
  
  # define fillable fields
  protected $fillable = [
  	                   'name',
  ];
}
