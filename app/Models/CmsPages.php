<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CmsPages extends Model
{
  
  # define table
  protected $table ='cms_pages';
  
  # define fillable fields
  protected $fillable = [
						'position',
						'name', 
						'unchangeable_name',
						'slug', 
						'page',
  ];

  /**
   * Has Many Cms Pages parent
   * @params
   * @return Relation
   */
  public function parent()
  {
  	# code...
  }
  
}
