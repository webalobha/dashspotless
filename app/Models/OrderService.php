<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderService extends Model
{
  
  # define table
  protected $table ='order_services';
  
  # define fillable fields
  protected $fillable = [
  	'order_id',
  	'service_id',
    'quantity',
  	'service_time_taken',
  	'total_amount',
  	'discount_given',
  	'payable_amount',
  ];

  /**
   * Model belongs to  Services
   * @param 
   * @return Relation
   */
  public function service()
  {
    return $this->belongsTo('App\Models\Service');
  }

  /**
   * Model has many  order
   * @param 
   * @return Relation
   */
  public function serviceOrders()
  {
    return $this->hasMany('App\Models\Order', 'id', 'order_id');
  }

  /**
   * Model has many  order
   * @param 
   * @return Relation
   */
  public function serviceOrdersData()
  {
    return $this->belongsTo(\App\Models\Order::class, 'order_id', 'id');
  }
}
