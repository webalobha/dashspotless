<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class   SiteSettingMail extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='site_setting_mail';
  
  # define fillable fildes
  protected $fillable = [
                  'id', 
                  'is_send_mail',
                  'is_send_sms', 
                  'status', 
                  'sms_provider_id',
                  'twilio_account_sid',
                  'twilio_auth_token',
                  'twilio_from_number',
                  'created_at', 
                  'updated_at', 
                  'deleted_at'         
  ];
}
