<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Http\Interfaces\OrderStatusInterface;

class Provider extends Authenticatable implements OrderStatusInterface
{
    use Notifiable;

    protected $table = 'providers';

    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name', 
        'country_code', 
        'address', 
        'postal_code', 
        'last_name',
        'mobile_number',
        'email',
        'status',
        'image_name',
        'image_path',
        'age',
        'gender',
        'latitude',
        'longitude',
        'device_type',
        'device_token',
        'android_key',
        'ios_key',
        'device_type',
        'password',
        'api_token',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    # cast Fields
    protected $casts = [
        'status' => 'boolean',
    ];

    /**
     * Model has Many Otp
     * 
     * @return relation
     */
    public function otp()
    {
        return $this->hasMany('App\Models\ProviderOtp', 'provider_id', 'id');
    }

    /**
     * Model has Many disputes
     * 
     * @return relation
     */
    public function disputes()
    {
        return $this->morphMany('App\Models\Dispute', 'disputable');
    }

    /**
     * Model has Many documents
     * 
     * @return relation
     */
    public function documents()
    {
        return $this->hasMany('App\Models\ProviderDocument', 'provider_id', 'id');
    }

    /**
     * Model has One Category
     * 
     * @return relation
     */
    public function category()
    {
        return $this->hasOne('App\Models\ProviderCategory', 'provider_id', 'id');
    }

    /**
     * Model has Many SubCategory
     * 
     * @return relation
     */
    public function subCategory()
    {
        return $this->hasMany('App\Models\ProviderSubCategory', 'provider_id', 'id');
    }

    /**
     * Model has Many address
     * 
     * @return relation
     */
    public function addresses()
    {
        return $this->hasMany('App\Models\ProviderAddress', 'provider_id', 'id');
    }

    /**
     * Model has Many Work Photos
     * 
     * @return relation
     */
    public function workPhotos()
    {
        return $this->hasMany('App\Models\WorkPhotos', 'provider_id', 'id');
    } 

    /**
     * Model has Many Work Photos
     * 
     * @return relation
     */
    public function awardsCertificate()
    {
        return $this->hasMany('App\Models\AwardAndCertificate', 'provider_id', 'id');
    }

    /**
     * Model has One Bank Dteils
     * 
     * @return relation
     */
    public function bankDetail()
    {
        return $this->hasOne('App\Models\ProviderBankDetail', 'provider_id', 'id');
    }

    /**
     * Model has Many Provide Bought Package
     * 
     * @return relation
     */
    public function boughtPackages()
    {
        return $this->hasMany('App\Models\ProviderBoughtPackage', 'provider_id', 'id');
    }

    /**
     * Model has Many Assign Orders
     * 
     * @return relation
     */
    public function assignOrders()
    {
        return $this->hasMany('App\Models\AssignOrderToProvider', 'provider_id', 'id');
    }

    /**
     * Model has Many Orders
     * 
     * @return relation
     */
    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'provider_id', 'id');
    }

    /**
     * Model has Many Penalty
     * 
     * @return relation
     */
    public function penalties()
    {
        return $this->hasMany('App\Models\ProviderPenalty', 'provider_id', 'id');
    }

    /**
     * Scope a query to only include Verified Provider
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * @method to Fetch the remaining Credit Points of a Prtovider
     * @param 
     * @return remaining Credit Points
     */
    public function getRemainingCreditPointsAttribute()
    {
        # Fetch all the bought Credit Points of a Provider
        $totalCreditPoints = $this->boughtPackages->sum('no_of_points');

        # Fetch all the Orders of Provider
        $orders = $this->orders;

        # Fetch the Orders that are completed by Provider
        $fOrders = $orders->filter(function($order) {
            # fetch History of Order
            $orderHistory = $order->history;
            if($orderHistory->isNotEmpty()) {
                # fetch order History to check for Completed Order By Provider
                $completedOrders = $orderHistory->where('order_status_id', OrderStatusInterface::COMPLETED);

                if($completedOrders->isNotEmpty()) {
                    return $order;
                }
            }
        });
        
        # Fetch the No of Points Used
        $creditPointUsed = $fOrders->isNotEmpty() ? $fOrders->sum('credit_points') : 0;

        # Fetch No of Points whiuch are credited for cancelled Order
        $penaltyPoints = $this->penalties->sum('penalty_points');

        # Remaining Credit Points
        $remainingCreditPoints = $totalCreditPoints - ($creditPointUsed + $penaltyPoints);

        # Reyturn Credit points
        return $remainingCreditPoints;
    }
}