<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderReschedule extends Model
{
  
	# define table
	protected $table ='order_reschedules';
  
  	# define fillable fields
  	protected $fillable = [
  	                   'order_id',
  	                   'reschedule_date_time',
  	                   'is_date_time_change',
  	                   'is_address_change',
  	                   'reschedule_penalty_paid',
  	                   'penalty_paid_from_wallet',
  	                   'penalty_paid_online',
  	                   'address_of_service',
  	                   'pin_code_service',
  	                   'latitude',
  	                   'longitude',
  	                   'name',
  	                   'mobile',
  	                   'city',
  	                   'state',
  	                   'country',
  	                   'status',
  	                   'transaction_id',
  	                   'payment_mode',
  	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = [
	    'reschedule_date_time',
	];

	/**
	 * The attributes that should be cast.
	 *
	 * @var array
	 */
	protected $casts = [
	    'status' 				=> 'boolean',
	    'is_address_change' 	=> 'boolean',
	    'is_date_time_change' 	=> 'boolean',
	];
}
