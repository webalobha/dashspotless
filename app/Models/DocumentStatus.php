<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DocumentStatus extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='document_status';
  
  # define fillable fields
  protected $fillable = [
  	                   'name',
  ];
}
