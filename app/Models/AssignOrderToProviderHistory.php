<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignOrderToProviderHistory extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='assign_order_to_provider_history';
  
  # define fillable fields
  protected $fillable = [
  	                   'order_id',
  	                   'assign_orderprovider_id',
  	                   'assign_order_status_id',
  	                   'reason',
  ];
}
