<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AboutDashspotless extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='about_dashspotless';
  
  # define fillable fields
  protected $fillable = [
					'title',
					'description',
  ];
}
