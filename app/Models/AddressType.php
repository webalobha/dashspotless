<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AddressType extends Model
{
  
  # define table
  protected $table ='address_types';
  
  # define fillable fields
  protected $fillable = [
  	                   'name',
  ];
}
