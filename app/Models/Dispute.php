<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dispute extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='dispute';
  
  # define fillable fields
  protected $fillable = [
  	                   'disputable_id',
  	                   'disputable_type',
  	                   'dispute_status_id',
  	                   'order_id',
  	                   'reason',
  	                   'comment',
  	                   'refund_amount'
  ];

  /**
   * Morph to more than one Model
   * @relation with User or Provider
   * @return relation
   */
/*  public function disputable()
  {
      return $this->morphTo();
  }
*/
  /**
   * Model has user
   * 
   * @return relation
   */
  public function user()
  {
    return $this->belongsTo('App\Models\User', 'disputable_id', 'id');
  }

  /**
   * Model has provider
   * 
   * @return relation
   */
  public function provider()
  {
    return $this->belongsTo('App\Models\Provider', 'disputable_id', 'id');
  }


/**
   * Model has UserType
   * 
   * @return relation
   */
  public function UserType()
  {
    return $this->belongsTo('App\Models\DisputeReasonType', 'disputable_type', 'id');
  }



/**
   * Model has DisputeStatus
   * 
   * @return relation
   */
  public function DisputeStatus()
  {
    return $this->belongsTo('App\Models\DisputeStatus', 'dispute_status_id', 'id');
  }




  /**
   * Model has many  order
   * @param 
   * @return Relation
   */
  public function serviceOrders()
  {
    return $this->belongsTo('App\Models\Order', 'order_id', 'id');
  }


/**
   * Model has Reason
   * 
   * @return relation
   */
  public function Reason()
  {
    return $this->belongsTo('App\Models\DisputeReason', 'reason', 'id');
  }




}
