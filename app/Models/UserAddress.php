<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Interfaces\AddressTypeInterface;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAddress extends Model implements AddressTypeInterface
{
  use SoftDeletes;
  
  # define table
  protected $table ='user_addresses';
  
  # define fillable fields
  protected $fillable = [
  	'user_id',
  	'country_id',
  	'state_id',
  	'city_id',
  	'mobile',
    'name',
  	'pin_code',
  	'address',
    'address_type',
    'address_other_type_description',
    'status',
    'longitude',
    'latitude',
  ];

  /**
   * @attribute to fetch Address type
   * @params
   * @return address type String
   */
  public function getAddressTypeStringAttribute($value='')
  {
    # Intialize the Address Type
    $addressType = '';

    # Set the String Type Address Type
    if($this->address_type == AddressTypeInterface::HOME) {
      $addressType = 'Home';
    } elseif ($this->address_type == AddressTypeInterface::WORK) {
      $addressType = 'Work';
    } else {
      $addressType = 'Other';
    }

    # Return the String
    return $addressType;
  }

  /**
   * has relation with Country
   * @param
   * @return relation
   */
  public function country()
  {
    return $this->belongsTo('App\Models\Country', 'country_id', 'id');
  }

  /**
   * has relation with State
   * @param
   * @return relation
   */
  public function state()
  {
    return $this->belongsTo('App\Models\State', 'state_id', 'id');
  }

  /**
   * has relation with City
   * @param
   * @return relation
   */
  public function city()
  {
    return $this->belongsTo('App\Models\City', 'city_id', 'id');
  }
  

  /**
   * @method to fetch Country Name
   * @param
   * @return string | ''
   */
  public function getCountryNameAttribute()
  {
    $stringName = '';
    if($this->country_id != 0) {
      $stringName = $this->country->name;
    }

    return $stringName;
  }

   /**
   * @method to fetch State Name
   * @param
   * @return string | ''
   */
  public function getStateNameAttribute()
  {
    $stringName = '';
    if($this->state_id != 0) {
      $stringName = $this->state->name;
    }

    return $stringName;
  }

   /**
   * @method to fetch City Name
   * @param
   * @return string | ''
   */
  public function getCityNameAttribute()
  {
    $stringName = '';
    if($this->city_id != '' AND $this->city_id != 0) {
      $stringName = $this->city->name;
    }

    return $stringName;
  }
  
  
}
