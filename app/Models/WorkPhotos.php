<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkPhotos extends Model
{
  use SoftDeletes;

  # define table
  protected $table ='work_photos';
  
  # define fillable fields
  protected $fillable = [
  	                   'provider_id',
  	                   'image_name',
  	                   'image_path',
  ];
}
