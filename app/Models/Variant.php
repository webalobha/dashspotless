<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Variant extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='sub_category_variants';
  
  # define fillable fildes
  protected $fillable = [
                        'id', 
                        'category_id',
                        'sub_category_id',
                        'title_or_subject',
                        'label', 
                        'radio_or_check',
                        'consult_visit_avail',
                        'added_by',
                        'updated_by',
                        'deleted_by',
                        
  ];
}
