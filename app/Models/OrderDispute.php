<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDispute extends Model
{
  
  # define table
  protected $table ='user_order_related_disputes';
  
  # define fillable fields
  protected $fillable = [
  	                   'user_id',
  	                   'order_id',
  	                   'dispute_reason_id',
  	                   'dispute_status_id',
  	                   'comment',
  ];

  /**
   * Model has many  order
   * @param 
   * @return Relation
   */
  public function serviceOrders()
  {
    return $this->belongsTo('App\Models\Order', 'order_id', 'id');
  }

  /**
   * Model has user
   * 
   * @return relation
   */
  public function user()
  {
    return $this->belongsTo('App\Models\User', 'user_id', 'id');
  }

  /**
   * Model has Reason
   * 
   * @return relation
   */
  public function Reason()
  {
    return $this->belongsTo('App\Models\DisputeReason', 'dispute_reason_id', 'id');
  }

  /**
   * Model has DisputeStatus
   * 
   * @return relation
   */
  public function DisputeStatus()
  {
    return $this->belongsTo('App\Models\DisputeStatus', 'dispute_status_id', 'id');
  }

  /**
   * Model has DisputeSolutions
   * 
   * @return relation
   */
  public function solution()
  {
    return $this->hasMany('App\Models\OrderDisputeSolution', 'order_dispute_id', 'id');
  }
}
