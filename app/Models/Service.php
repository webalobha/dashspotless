<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Interfaces\DsicountTypeInterface;

class Service extends Model implements DsicountTypeInterface
{
  use SoftDeletes;
  
  # define table
  protected $table ='services';
  
  # define fillable fields
  protected $fillable = [
                'id',
                'category_id',
                'sub_category_id',
                'variant_id', 
                'status', 
                'title',
                'description',
                'price', 
                'discount_type', 
                'discount', 
                'discounted_price', 
                'final_price',
                'time_taken_minutes', 
                'tool_tip_content',
                'icon_name',
                'icon_path',
                'is_offer',
                'offer_from',
                'offer_to',
                'offer_banner',
                'added_by', 
                'updated_by',
                'deleted_by', 
                
  ];

  /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'status' => 'boolean',
    ];

  /**
   * relation with category
   * @param
   * @return \Illuminate\Http\Response
   */
  public function category()
  {
      # return relation with category
      return $this->belongsTo('App\Models\Category', 'category_id', 'id');
  }
  

  /**
   * relation with category
   * @param
   * @return \Illuminate\Http\Response
   */
  public function subCategory()
  {
      # return relation with category
      return $this->belongsTo('App\Models\SubCategory', 'sub_category_id', 'id');
  }

  /**
   * Modal has Many Bullet Points
   * 
   * @retrun relation
   */
  public function bulletPoints()
  {
    return $this->hasMany('App\Models\ServiceBulletPoints', 'service_id', 'id');
  }

  /**
   * Modal has Many Banners
   * 
   * @retrun relation
   */
  public function banners()
  {
    return $this->hasMany('App\Models\ServiceBanner', 'service_id', 'id');
  }

  
  /**
    * Scope a query to only include active users.
    *
    * @param  \Illuminate\Database\Eloquent\Builder  $query
    * @return \Illuminate\Database\Eloquent\Builder
  */
  public function scopeActive($query)
  {
      return $query->where('status', 1);
  }

  /**
   * @method to fetch the Discount on Sevice
   * @param
   * @return discount
   */
  public function getDiscountGivenAttribute()
  {
    # fetch DiscountType
    $discountType = $this->discount_type;
    $discountGiven = 0;
    if($discountType == DsicountTypeInterface::NO_DISCOUNT) {

    } elseif ($discountType == DsicountTypeInterface::PERCENTAGE) {
      $actualPriceOfService = $this->price;
      $discountInPercentage = $this->discount ?? 0;
      $discountGiven += (int)($actualPriceOfService * $discountInPercentage)/100;
    } elseif ($discountType == DsicountTypeInterface::FLAT_DISCOUNT) {
      $discountGiven += $this->discount ?? 0;
    }
    return $discountGiven;
  }

  /**
   * @method to fetch the Final Price of Sevice
   * @param
   * @return discount
   */
  public function getFinalPriceOfServiceAttribute()
  {
    # fethc Actual Price of Service
    $actualPrice = $this->price;

    #Fetch discountGiven
    $discountGiven = $this->discount_given;

    $finalPrice = $actualPrice  - $discountGiven;
    
    return $finalPrice;

  }

}