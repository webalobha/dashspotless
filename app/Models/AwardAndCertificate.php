<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AwardAndCertificate extends Model
{
	use SoftDeletes;
  
  # define table
  protected $table ='awards_and_certificate';
  
  # define fillable fields
  protected $fillable = [
  					   'provider_id',
  	                   'image_name',
  	                   'image_path',
  ];
}
