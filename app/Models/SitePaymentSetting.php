<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SitePaymentSetting extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='site_payment_setting';
  
  # define fillable fildes
  protected $fillable = [
                      'id',
                      'is_cash',
                      'is_card', 
                      'stripe_secret_key',
                      'stripe_publishable_key',
                      'stripe_currency'                 
  ];
}
