<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DisputeReasonType extends Model
{
  
  # define table
  protected $table ='dispute_reason_types';
  
  # define fillable fields
  protected $fillable = [
  	                   'name',
  ];
}
