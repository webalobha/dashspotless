<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Models\ServiceSettingRescheduleData;
use App\Http\Interfaces\OrderStatusInterface;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model implements OrderStatusInterface
{
  
  # define table
  protected $table ='orders';
  
  # define fillable fields
  protected $fillable = [
  	'user_id',
    'unique_order_id',
  	'provider_id',
  	'paid_amount',
  	'payment_mode',
  	'time_slot_id',
	  'date_of_service',
    'date_time_of_service',
    'wats_app_update',
  	'subscription_id',
  	'user_address_id',
  	'subscription_discount',
    'credit_points',
    'wallet_amount_used',
    'online_payment_amount',
    'total_minutes_taken_to_complete_order',
  ];

  protected $dates = [
    'date_of_service',
    'date_time_of_service'
  ];

  /**
   * Model has Many OrderServices
   * @param 
   * @return Relation
   */
  public function orderServices()
  {
    return $this->hasMany('App\Models\OrderService','order_id', 'id');
  }

  /**
   * Model has Many Order History
   * @param 
   * @return Relation
   */
  public function history()
  {
    return $this->hasMany('App\Models\OrderHistory', 'order_id', 'id');
  }

  /**
   * Model has Many Order History
   * @param 
   * @return Relation
   */
  public function orderHistory()
  {
    return $this->belongsTo(\App\Models\OrderHistory::class, 'id', 'order_id');
  }

  /**
   * Model has One TimeSlot
   * @param 
   * @return Relation
   */
  public function timeSlot()
  {
    return $this->hasOne('App\Models\BookingSlot', 'id', 'time_slot_id');
  }

  /**
   * Model has One user
   * @param 
   * @return Relation
   */
  public function user()
  {
    return $this->hasOne('App\Models\User', 'id', 'user_id');
  }

  /**
   * Model has One user
   * @param 
   * @return Relation
   */
  public function userData()
  {
    return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
  }

  /**
   * Model has One provider
   * @param 
   * @return Relation
   */
  public function provider()
  {
    return $this->hasOne('App\Models\Provider', 'id', 'provider_id');
  }

  /**
   * Model has Many Disputes
   * @param 
   * @return Relation
   */
  public function disputes()
  {
    return $this->hasMany('App\Models\OrderDispute', 'order_id', 'id');
  }

  /**
   * Model has One order address
   * @param 
   * @return Relation
   */
  public function orderAddress()
  {
    return $this->hasOne('App\Models\UserAddress', 'id', 'user_address_id');
  }

  /**
   * Attribute to Set String Status of POrder Last history
   * @params
   * @return string Status
   */
  public function getStatusStringAttribute()
  {
    # fetch the Last Status Id From Order History
    $statusId = $this->history->last()->order_status_id; 
    $status = '';
    if($statusId == OrderStatusInterface::BOOKED) {
      $status = 'Booked';
    } elseif ($statusId == OrderStatusInterface::CANCELLED) {
      $status = 'Cancelled';
    } elseif ($statusId == OrderStatusInterface::REJECTED) {
      $status = 'Rejected';
    } elseif ($statusId == OrderStatusInterface::PICKED_BY_PROVIDER) {
      $status = 'Accepted';
    } elseif ($statusId == OrderStatusInterface::ON_THE_WAY) {
      $status = 'On the Way';
    } elseif ($statusId == OrderStatusInterface::COMPLETED) {
      $status = 'Completed';
    } elseif ($statusId == OrderStatusInterface::REJECTED_BY_PROVIDER) {
      $status = 'Rejected By Provider';
    } elseif ($statusId == OrderStatusInterface::SEARCHING_PROVIDER) {
      $status = 'Searching Provider';
    } elseif ($statusId == OrderStatusInterface::ACCEPTED_BY_PROVIDER) {
      $status = 'Accepted';
    } elseif ($statusId == OrderStatusInterface::IN_PROGRESS) {
       $status = 'In Progress';
    }

    return $status;
  }
  
  /**
   * @method Atrribute to find which Reschedule has been Applied on Order
   * @param 
   * @return 
   */
  public function getRescheduleAppliedOnOrderAttribute()
  {
    # Fetch Order
    $order = $this;

    # fetch Service Date Time of Order
    $serviceDateTime = $this->date_time_of_service;

    # fetch Current Time
    $now = Carbon::now();

    # fetch Diffrence in Hour Between Current Time and Service Date time
    $hoursBetweenServiceFromNow = $serviceDateTime->diffInHours($now);

    # Set the Model
    $model = null;

    # Fetch the Model
    $serviceSettingRescheduleData = ServiceSettingRescheduleData::all();
    foreach ($serviceSettingRescheduleData as $key => $rescheduleModel) {
      $timeBetweenServiceAndRescheduleModel = $hoursBetweenServiceFromNow - $rescheduleModel->hours;
      if($key == 0) {
        $model = $rescheduleModel;
      } else {
        if(($timeBetweenServiceAndRescheduleModel > 0) AND ($timeBetweenServiceAndRescheduleModel < $lastTime)) {
          $model = $rescheduleModel;
        }
      }
      # save last Data
      $lastTime = $timeBetweenServiceAndRescheduleModel;
    }
    
    return $model;
  }


  /**
   * @method to check if order is Completed
   * @param
   * @return Boolean
   */
  public function getIsCompletedAttribute()
  {
    # FEtch all the History of Order
    $history = $this->history;
    if($history->isNotEmpty()){
      $isCompleted = $history->where('order_status_id', OrderStatusInterface::COMPLETED);
      if($isCompleted->isNotEmpty()) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  /**
   * @method to check if order is Completed
   * @param
   * @return Boolean
   */
  public function getIsNotCompletedAttribute()
  {
    # FEtch all the History of Order
    $history = $this->history;
    if($history->isNotEmpty()){
      $isCompleted = $history->where('order_status_id', OrderStatusInterface::COMPLETED);
      if($isCompleted->isEmpty()) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }
  

  /**
   * @method to check if order is Cancelled
   * @param
   * @return Boolean
   */
  public function getIsCancelledAttribute()
  {
    # FEtch all the History of Order
    $history = $this->history;
    if($history->isNotEmpty()){
      $isCancelled = $history->where('order_status_id', OrderStatusInterface::CANCELLED);
      if($isCancelled->isNotEmpty()) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}
