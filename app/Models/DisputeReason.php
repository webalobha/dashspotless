<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DisputeReason extends Model
{
  
  # define table
  protected $table ='dispute_reasons';
  
  # define fillable fields
  protected $fillable = [
						'id', 
						'status',
						'dispute_reason_type_id',
						'service', 
						'title',
  ];

  /**
   * Model has many Categories
   * @param
   * @return
   */
  public function DisputeReasonType()
  {
    return $this->belongsTo(\App\Models\DisputeReasonType::class, 'dispute_reason_type_id', 'id');
  }

  /**
   * Model has many banners
   * @param
   * @return
   */
  public function serviceName()
  {
    return $this->belongsTo(\App\Models\Service::class, 'service', 'id');
  }

  /**
     * Scope a query to only include user Type Reasons
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUserType($query)
    {
        return $query->where('dispute_reason_type_id', 1);
    }
}
