<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class   SiteSettingPushNotification extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='site_setting_push_notification';
  
  # define fillable fildes
  protected $fillable = [
            'id',
            'is_send_mail',
            'is_send_sms',
            'ios_push_provider_pem',
            'ios_push_environment',
            'ios_push_user_pem',
            'ios_push_password',
            'android_push_key',
            'created_at', 
            'updated_at', 
            'deleted_at'                
  ];
}
