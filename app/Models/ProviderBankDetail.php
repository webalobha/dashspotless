<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderBankDetail extends Model
{
  #use SoftDeletes;
  
  # define table
  protected $table ='provider_bank_details';
  
  # define fillable fields
  protected $fillable = [
					'provider_id',
					'bank_name',
					'ifsc_code',
					'account_number',
          'account_holder_name',
  ];
}
