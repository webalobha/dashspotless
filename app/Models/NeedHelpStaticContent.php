<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NeedHelpStaticContent extends Model
{
  # define table
  protected $table ='need_help_static_content';
  
  # define fillable fields
  protected $fillable = [
  	                   'title',
  	                   'content',
  	                   'is_user_or_provider',
  	                   'status',
  ];

  /**
     * Scope a query to only include active Help Content.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * Scope a query to only include for User
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUser($query)
    {
        return $query->where('is_user_or_provider', 1);
    }


    /**
     * Scope a query to only include for Provider
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeProvider($query)
    {
        return $query->where('is_user_or_provider', 2);
    }
}
