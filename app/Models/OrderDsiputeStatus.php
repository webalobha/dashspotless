<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDsiputeStatus extends Model
{
  
  # define table
  protected $table ='order_disputes_status';
  
  # define fillable fields
  protected $fillable = [
  	                   'name',
  ];
}
