<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubscriptionDuration extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='subscription_duration';
  
  # define fillable fields
  protected $fillable = [
  	                   'name',
  ];
}
