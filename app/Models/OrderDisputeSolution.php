<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDisputeSolution extends Model
{
  
  # define table
  protected $table ='order_disputes_solutions';
  
  # define fillable fields
  protected $fillable = [
  	                   'order_dispute_id',
  	                   'comment',
  ];
}
