<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiscountType extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='discount_type';
  
  # define fillable fields
  protected $fillable = [
  	                   'name',
  ];
}
