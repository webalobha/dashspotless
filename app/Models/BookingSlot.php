<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookingSlot extends Model
{
  
  # define table
  protected $table ='booking_slots';
  
  # define fillable fields
  protected $fillable = [
  		'time',
  ];

  
}
