<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessCountry extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='business_countries';
  
  # define fillable fields
  protected $fillable = [
					'id',
					'country_id',
					'status',
					'currency_code',
					'currency_symbol',

  ];


   /**
   * Model has one country
   * @param
   * @return
   */
  public function Country()
  {
    return $this->belongsTo(\App\Models\Country::class, 'country_id', 'id');
  }


}
