<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SmsProvider extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='sms_provider';
  
  # define fillable fields
  protected $fillable = [
  	                   'name',
  	                   'account_sid',
  	                   'auth_token',
                       'from_number',
  ];
}
