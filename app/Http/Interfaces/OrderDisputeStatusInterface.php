<?php

namespace App\Http\Interfaces;

interface OrderDisputeStatusInterface
{
	const OPEN 		= 1;
	const RESOLVED 	= 2;
}