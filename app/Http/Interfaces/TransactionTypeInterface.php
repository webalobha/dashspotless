<?php

namespace App\Http\Interfaces;

interface TransactionTypeInterface
{
	const DEBIT 		= 1;
	const CREDIT 		= 2;
}