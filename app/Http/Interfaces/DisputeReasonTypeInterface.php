<?php

namespace App\Http\Interfaces;

interface DisputeReasonTypeInterface
{
	const USER 			= 1;
	const PROVIDER 		= 2;
}