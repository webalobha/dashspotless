<?php

namespace App\Http\Interfaces;

interface OrderStatusInterface
{
	const BOOKED 						= 1;
	const CANCELLED 					= 2;
	const REJECTED 						= 3;
	const PICKED_BY_PROVIDER 			= 4;
	const ON_THE_WAY 					= 5;
	const COMPLETED 					= 6;
	const REJECTED_BY_PROVIDER 		    = 7;
	const SEARCHING_PROVIDER 		    = 8;
	const ACCEPTED_BY_PROVIDER 		    = 9;
	const IN_PROGRESS		 		    = 10;
}