<?php

namespace App\Http\Interfaces;

interface DsicountTypeInterface
{
	const NO_DISCOUNT 		= 1;
	const PERCENTAGE 		= 2;
	const FLAT_DISCOUNT 	= 3;
}