<?php

namespace App\Http\Interfaces;

interface AssignOrderProviderStatusInterface
{
	const SEARCHING 					= 1;
	const ACCEPTED 						= 2;
	const INITIATED 					= 3;
	const COMPLETE 						= 4;
	const REJECT 						= 5;
	const ASSIGNED_TO_OTHER 			= 6;
	const ON_TH_WAY		 				= 7;
	const IN_PROGRES		 			= 8;
	const CANCELED		 				= 9;
}