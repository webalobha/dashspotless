<?php

namespace App\Http\Interfaces;

interface AddressTypeInterface
{
	const HOME 		= 1;
	const WORK 		= 2;
	const OTHER 	= 3;
}