<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Order;
use App\Models\Service;
use App\Models\Provider;
use App\Models\OrderHistory;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    # bind Service protected
    protected $service;

    # bind Order protected
    protected $order;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
                                OrderHistory $orderHistory, 
                                Provider $provider,
                                Service $service, 
                                Order $order, 
                                User $user 
                              )
    {
      $this->historyOfOrder   = $orderHistory; 
      $this->provider       = $provider; 
      $this->service        = $service; 
      $this->order          = $order; 
      $this->user           = $user; 
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    # Bind view page
 protected $view = 'application.dashboard.';
 
 /**
  * show dashboard page
  * @param
  * @return \Illuminate\Http\Response
  */
 public function index($day='')
 {
    # get current date
    $date = Carbon::today();

    # fetch all services
    $services =  $this->service->where('status', '1')->count();;

    # fetch all services
    $serviceHistoryQuery = $this->order->with('history','orderHistory')->get();

    $orders             = [];
    $booked             = [];
    $rejected           = [];
    $onTheWay           = [];
    $cancelled          = [];
    $completed          = [];
    $inProgress         = [];
    $rescheduled        = [];
    $pickedByProvider   = [];
    $searchingProvider  = [];
    $rejectedByProvider = [];
    $acceptedByProvider = [];

    foreach ($serviceHistoryQuery as $serviceHistory) {
      $orders[] = $serviceHistory->history;

      $orderStatusId = $serviceHistory->orderHistory->order_status_id ?? '';


        if ($orderStatusId == 1) {
          $booked[] = $this->historyOfOrder->where('order_id', $serviceHistory->id)->orderBy('id', 'desc')->first()->order_status_id;
        } 
        if ($orderStatusId == 2){ 
         $cancelled[] = $this->historyOfOrder->where('order_id', $serviceHistory->id)->orderBy('id', 'desc')->first()->order_status_id;
        } 
        if ($orderStatusId == 3){ 
         $rejected[] = $this->historyOfOrder->where('order_id', $serviceHistory->id)->orderBy('id', 'desc')->first()->order_status_id;
        } 
        if ($orderStatusId == 4){ 
         $pickedByProvider[] = $this->historyOfOrder->where('order_id', $serviceHistory->id)->orderBy('id', 'desc')->first()->order_status_id;
        } 
        if ($orderStatusId == 5){  
         $onTheWay[] = $this->historyOfOrder->where('order_id', $serviceHistory->id)->orderBy('id', 'desc')->first()->order_status_id;
        } 
        if ($orderStatusId == 6){  
         $completed[] = $this->historyOfOrder->where('order_id', $serviceHistory->id)->orderBy('id', 'desc')->first()->order_status_id;
        } 
        if ($orderStatusId == 7){   
         $rescheduled[] = $this->historyOfOrder->where('order_id', $serviceHistory->id)->orderBy('id', 'desc')->first()->order_status_id;
        } 
        if ($orderStatusId == 8){  
         $rejectedByProvider[] = $this->historyOfOrder->where('order_id', $serviceHistory->id)->orderBy('id', 'desc')->first()->order_status_id;
        } 
        if ($orderStatusId == 9){
         $searchingProvider[] = $this->historyOfOrder->where('order_id', $serviceHistory->id)->orderBy('id', 'desc')->first()->order_status_id;
        } 
        if ($orderStatusId == 10){ 
         $acceptedByProvider[] = $this->historyOfOrder->where('order_id', $serviceHistory->id)->orderBy('id', 'desc')->first()->order_status_id;
        } 
        if ($orderStatusId == 11){ 
         $inProgress[] = $this->historyOfOrder->where('order_id', $serviceHistory->id)->orderBy('id', 'desc')->first()->order_status_id;
        }
    }

    # count activeProvider
    $activeProviders = $this->provider->where('status', '1')->count();

    # count activeUser
    $activeUsers = $this->user->where('status', '1')->count();

    # count provider
    $providers = $this->provider->count();

    # count user
    $users = $this->user->count();

//    dd(count($orders), count($booked), count($cancelled), count($rejected), count($pickedByProvider), count($onTheWay), count($completed), count($rescheduled), count($rejectedByProvider), count($searchingProvider), count($acceptedByProvider), count($inProgress));
    // # check fetch data by day
    // if ($day == 'all') {

    //   # fetch services
    //   $serviceQuery = $serviceQuery->where('status', '1');

    //   # fetch completed services
    //   $datas = $serviceHistoryQuery->get();
      
    //   foreach ($datas as $order) {
    //     if ($order->orderHistory->order_status_id == 1) {
    //       $booked[] = $order->orderHistory->order_status_id;
    //     }
    //     // if ($order->orderHistory->order_status_id == 2) {
    //     //   $canceled[] = $order->orderHistory->order_status_id;
    //     // }
    //   }
    //   dd(count($booked));
    // } elseif ($day == 'today') {

    //   # fetch services
    //   $serviceQuery = $serviceQuery->where('status', '1')->where('created_at','>=', $date->format("Y-m-d"));

    // } elseif ($day == 'weekly') {

    //   # first date of this week
    //   $firstDayOfWeek = $date->startOfWeek()->format('Y-m-d');

    //   # fetch services
    //   $serviceQuery = $serviceQuery->where('status', '1')->where('created_at','>=', $firstDayOfWeek); 

    // } elseif ($day == 'monthly') {

    //   # first date of this month
    //   $firstDayOfMonth = $date->startOfMonth()->format('Y-m-d');
      
    //   # fetch services
    //   $serviceQuery = $serviceQuery->where('status', '1')->where('created_at','>=', $firstDayOfMonth);
        
    // } elseif ($day == 'yearly') {

    //   # first date of this year
    //   $firstDayOfYear = $date->startOfYear()->format('Y-m-d');

    //   # fetch services
    //   $serviceQuery = $serviceQuery->where('status', '1')->where('created_at','>=', $firstDayOfYear);        
    // }

    # return the dashboard page
    return view($this->view.'index')->with([
                                            'users'           => $users,
                                            'orders'          => $orders,
                                            'services'        => $services,
                                            'completed'       => $completed,
                                            'cancelled'       => $cancelled,
                                            'providers'       => $providers,
                                            'activeUsers'     => $activeUsers,
                                            'activeProviders' => $activeProviders,
                                          ]);
 } 
}
