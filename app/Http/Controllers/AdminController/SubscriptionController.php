<?php

namespace App\Http\Controllers\AdminController;

use App\Models\Subscription;
use App\Models\DiscountType;
use Illuminate\Http\Request;
use App\Models\SubscriptionDuration;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;

class SubscriptionController extends Controller
{
 use MessageStatusTrait;

 # Bind Type
 protected $type = 'Subscription ';

 # Bind location
 protected $view = 'application.subscription.';

 # Bind subscription
 protected $subscription;

 # Bind discountType
 protected $discountType;

 # Bind subscriptionDuration
 protected $subscriptionDuration;

 /**
  * default constructor
  * @param
  * @return
  */
 function __construct(Subscription $subscription, DiscountType $discountType, SubscriptionDuration $subscriptionDuration)
 {
  $this->subscription         = $subscription;
  $this->discountType         = $discountType;
 	$this->subscriptionDuration = $subscriptionDuration;
 }
 
 /**
  * index page of subscription
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function index(Request $request)
 {
  # fetch discount type
  $discountTypes = $this->discountType
                        ->orderBy('name')
                        ->get();

 	# fetch subscription list
 	$query = $this->subscription
 	              ->orderBy('created_at','desc');

  if ($request->discount_type_id != '') {
    # code...
    $query = $query->where('discount_type_id', $request->discount_type_id);
  }

 	$subscriptions = $query->get();

 	# return to index page
 	return view($this->view.'index')->with([
                                          'subscriptions'         => $subscriptions,
                                          'discountTypes'         => $discountTypes,
                                          'discountTypeId'        => $request->discount_type_id,
                                        ]);
 }

 
 /**
  * create page of subscription
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function create()
 {
  # fetch discount type
  $discountTypes = $this->discountType
                        ->orderBy('id', 'asc')
                        ->get();

  # fetch subscription duration type
  $subscriptionDurations = $this->subscriptionDuration
                                ->orderBy('name')
                                ->get();

  # return to create page
  return view($this->view.'create')->with([
                                          'discountTypes'         => $discountTypes,
                                          'subscriptionDurations' => $subscriptionDurations,
                                        ]);;
 }

 /**
  * create subscription
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function store(Request $request)
 {
 	# request param
 	$arrayData = [
                  'subscription_duration_id'=> $request->subscription_duration_id ?? null,
                  'duration'                => $request->duration ?? null,
                  'discount_type_id'        => $request->discount_type_id ?? null,
                  'discount'                => $request->discount ?? null,
                  'price'                   => $request->price ?? null,
                  'description'             => $request->description ?? null,
 	];

    #store 
    $createsubscription = $this->subscription->create($arrayData);

    # check created or not
    # if created
    if ($createsubscription) {
     # return successs
     $output = ['success' => 200, 'message' => 'Subscription Added Successfully'];
    } else {
     # return error
     $output = ['error' => 100, 'message' => 'Something went wrong'];
    }
  return redirect('subscription');
 } 

 /**
  * edit  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function update($id)
 {
  # fetch discount type
  $discountTypes = $this->discountType
                        ->orderBy('id', 'asc')
                        ->get();

  # fetch subscription duration type
  $subscriptionDurations = $this->subscriptionDuration
                                ->orderBy('name')
                                ->get();

  # Fetch subscription by id
  $subscription = $this->subscription
                       ->where('id', $id)
                       ->first(); 
 	# code...
 	return view($this->view.'edit')->with([
                                        'subscription' => $subscription,
                                        'discountTypes'         => $discountTypes,
                                        'subscriptionDurations' => $subscriptionDurations,
                                       ]);
 }

 /**
  * edit subscription
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function edit(Request $request, $id)
 {
  # request param
  $arrayData = [
                  'subscription_duration_id'=> $request->subscription_duration_id ?? null,
                  'duration'                => $request->duration ?? null,
                  'discount_type_id'        => $request->discount_type_id ?? null,
                  'discount'                => $request->discount ?? null,
                  'price'                   => $request->price ?? null,
                  'description'             => $request->description ?? null,
  ];

    #store 
    $createsubscription = $this->subscription->where('id', $id)->update($arrayData);

    # check created or not
    # if created
    if ($createsubscription) {
     # return successs
     $output = ['success' => 200, 'message' => 'Subscription Updated Successfully'];
    } else {
     # return error
     $output = ['error' => 100, 'message' => 'Something went wrong'];
    }
  return redirect('subscription');
 }  

 /**
  * delete subscription
  * @param $id
  * @return \Illuminate\Http\Response
  */
 public function delete($id)
 {
   $query = $this->subscription;

   # delete subscription by id
   $query->where('id', $id)->delete();
   
   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $this->deleteMessage($this->type)];
 }

 /**
  * active deactive
  * @param $id
  * @return \Illuminate\Http\Response
  */ 
 public function view($id)
 {
  # Fetch subscription by id
  $subscription = $this->subscription
                       ->where('id', $id)
                       ->first(); 
  # code...
  return view($this->view.'view')->with([
                                        'subscription' => $subscription,
                                       ]);
 }
}
