<?php

namespace App\Http\Controllers\AdminController;

use App\Models\Admin;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;

class AdminManageController extends Controller
{
 # Bind view
 protected $view = 'application.manage_admin.';

 # Bind model
 protected $admin;

 /**
  * define constructor
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 function __construct(Admin $admin)
 {
 	$this->admin = $admin;
 }
 
 /**
  * managers index page
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function index()
 {
  # fetch admins
  $admins = $this->admin
                 //->NotAdmin()
                 ->orderby('id', 'desc')
                 ->get();

  # return index page
  return view($this->view.'index')->with(['admins' => $admins]);
 }

 /**
  * add page
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function create()
 {
  # fetch role
  $roles = Role::all();

  # return create page
  return view($this->view.'create')->with(['roles' => $roles]);
 }
 
 /**
  * add
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function store(Request $request)
 {
  # request data array
  $arrayData = [
                'first_name' => $request->first_name ?? null,
                'last_name'  => $request->last_name ?? null, 
                'email'      => $request->email ?? null, 
                'password'   => Hash::make($request->password) ?? null
  ];
 
  # check email existence
  $emailCount = $this->admin->where('email', $request->email)->count();
     
     # confirm password
     if ($request->password == $request->confirm_password) {
      # if count is 0
      if ($emailCount == 0) {
       	# then create admin
       	$createManager = $this->admin->create($arrayData);
        
        # if created
       	if ($createManager) {
       	 # assign role to created manager
         $assignRole = $createManager->assignRole($request->input('role'));
        
         # if role assigned 
         if ($assignRole) {
          # success message
          $output = ['success' => 200, 'message' => 'Added Successfully'];
         } else {
          $output = ['error' => 100, 'message' => 'Somethig went wrong.'];
         }
       	} else {
       	 $output = ['error' => 100, 'message' => 'Somethig went wrong.'];	
       	} 
       } else {
        $output = ['error' => 100, 'message' => 'Already Exist.'];
       } 
      } else {
      $output = ['error' => 100, 'message' => 'Password and Confirm Password are different'];
      } 
   return $output;
 }

 /**
  * edit page
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function update($id)
 {
  # fetch role
  $roles = Role::all();

  # fetch sub-admin
  $admin = $this->admin
                ->where('id', $id)
                ->first();

  $role = $admin->getRoleNames() ?? '';
  
  $roleName = preg_replace('/[^A-Za-z0-9 ]/', '', $role);

  # return create page
  return view($this->view.'edit')->with(['admin' => $admin, 'roles' => $roles, 'roleName' => $roleName]);
 }

 /**
  * edit
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function edit(Request $request ,$id)
 {
  $admin = $this->admin->where('id', $id)->first();

  # request data array
  $arrayData = [
                'first_name' => $request->first_name ?? null,
                'last_name'  => $request->last_name ?? null, 
                'email'      => $request->email ?? null, 
                'password'   => Hash::make($request->password) ?? $admin->password
  ];
 
  # check email existence
  $emailCount = $this->admin->where('email', $request->email)->where('id', '!=', $id)->count();
     
     # confirm password
     if ($request->password == $request->confirm_password) {
      # if count is 0
      if ($emailCount == 0) {
        # then update admin
        $updateManager = $this->admin->where('id', $id)->update($arrayData);

        # if updated
        if ($updateManager) {
         # assign role to updated manager
         $assignRole = $admin->syncRoles($request->input('role'));
        
         # if role assigned 
         if ($assignRole) {
          # success message
          $output = ['success' => 200, 'message' => 'Updated Successfully'];
         } else {
          $output = ['error' => 100, 'message' => 'Somethig went wrong.'];
         }
        } else {
         $output = ['error' => 100, 'message' => 'Somethig went wrong.']; 
        } 
       } else {
        $output = ['error' => 100, 'message' => 'Already Exist.'];
       } 
      } else {
      $output = ['error' => 100, 'message' => 'Password and Confirm Password are different'];
      } 
   return $output;
 }
}
