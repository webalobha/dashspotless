<?php

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;
use App\Models\CreditRecharge;
use App\Models\PricePerPoint;
use App\Models\CreditPointsPackage;
use App\Models\DiscountType;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;


class CreditPointPackageController extends Controller
{
 use MessageStatusTrait;

 # Bind Type
 protected $type = 'Credit Point Package ';

 # Bind location
 protected $view = 'application.credit_point_system.';

 # Bind creditRecharge
 protected $creditRecharge;

 /**
  * default constructor
  * @param
  * @return
  */
 function __construct(CreditRecharge $creditRecharge,PricePerPoint $PricePerPoint,CreditPointsPackage $CreditPointsPackage)
 {
 	$this->creditRecharge = $creditRecharge;
  $this->PricePerPoint = $PricePerPoint;
  $this->CreditPointsPackage = $CreditPointsPackage;
 }


 /**
  * how it works page
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function PricePerPoint()
 {
   # fetch how it works
   $PricePerPoint = $this->PricePerPoint
                      ->first();

   # return to howItWorks page
   return view($this->view.'per_point_amount')->with(['PricePerPoint' => $PricePerPoint]);
 }

 /**
  * store how it works
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function storePricePerPoint(Request $request)
 {
  # array data
  $arrayData = [
                  'price' => $request->price ?? '',
  ];

  if ($request->id != '') {
    # update
    $Data = $this->PricePerPoint->where('id', $request->id)->update($arrayData);
  } else {
    # create
    $Data = $this->PricePerPoint->create($arrayData);
  }

  if ($Data) {
    # output
    $output = ['success' => 200, 'message' => 'Success.'];
  } else {
    # output
    $output = ['error' => 100, 'message' => 'Something went wrong.'];
  }

  # return output
  return $output;
 } 


 /**
  * index page of creditRecharge
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function index()
 {
 	# fetch creditRecharge list
  $PricePerPoint = PricePerPoint::first();
    $discountType = DiscountType::get();
 	$createcreditRecharges = $this->CreditPointsPackage
 	                              ->orderBy('created_at', 'desc')
 	                              ->get();
//dd( $discountTypes);
 	# return to index page
 	return view($this->view.'index')->with(['createcreditRecharges' => $createcreditRecharges,'discountType'=>$discountType,'PricePerPoint'=>$PricePerPoint]);;
 }

 /**
  * create creditRecharge
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function store(Request $request)
 {
 	# current user
 	$user = Auth::guard('admin')->user();

    $query = $this->CreditPointsPackage;

 	# request param
 	$arrayData = [
                  'name'            => $request->name ?? null,
                  'number_of_points' => $request->number_of_points ?? null,
                  'actual_price'     => $request->actual_price ?? null,
                  'discount_type_id' => $request->discount_type_id ?? 0,
                  'discount_given' => $request->discount_given ?? 0,
                  'discounted_amount' => $request->discounted_amount ?? 0,
                   'status' => $request->status ?? 1,
                 
 	];

     #store 
    $createCreditRecharge = $query->create($arrayData);

    # check created or not
    # if created
    if ($createCreditRecharge) {
     # return successs
     $output = ['success' => 200, 'message' => 'Credit point Added Successfully'];
    } else {
     # return error
     $output = ['error' => 100, 'message' => 'Something went wrong'];
    }
  return $output;
 } 

 /**
  * edit  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function update($id)
 {
  # Fetch creditRecharge by id
   $PricePerPoint = PricePerPoint::first();
  $discountType = DiscountType::get();
  $creditRecharge = $this->CreditPointsPackage
                         ->where('id', $id)
                         ->first(); 
 	# code...
 	return view($this->view.'edit')->with(['creditRecharge' => $creditRecharge,'discountType'=>$discountType,'PricePerPoint'=>$PricePerPoint]);
 }

 /**
  * edit creditRecharge
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function edit(Request $request,$id)
 {
  # current user
  //dd($id);
  $user = Auth::guard('admin')->user();

  $query = $this->CreditPointsPackage;

 	# request param
$arrayData = [
                  'name'            => $request->name ?? null,
                  'number_of_points' => $request->number_of_points ?? null,
                  'actual_price'     => $request->actual_price ?? null,
                  'discount_type_id' => $request->discount_type_id ?? 0,
                  'discount_given' => $request->discount_given ?? 0,
                  'discounted_amount' => $request->discounted_amount ?? 0,
                   'status' => $request->status ?? 1,
                 
  ];

    #store 
    $updatecreditRecharge = $query->where('id', $id)->update($arrayData);

    # check created or not
    # if created
    if ($updatecreditRecharge) {
     # return successs
     $output = ['success' => 200, 'message' => 'Credit Recharge update Successfully'];
    } else {
     # return error
     $output = ['error' => 100, 'message' => 'Something went wrong'];
    }
  return $output;
 }  

 /**
  * delete creditRecharge
  * @param $id
  * @return \Illuminate\Http\Response
  */
 public function delete($id)
 {
   $query = $this->CreditPointsPackage;

   # current user
   $user = Auth::guard('admin')->user();

   #update deleted by
   //$query->where('id', $id)->update(['deleted_by' => $user->id]);

   # delete creditRecharge by id
   $query->where('id', $id)->delete();
   
   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $this->deleteMessage($this->type)];
 }

 /**
  * active deactive
  * @param $id
  * @return \Illuminate\Http\Response
  */ 
 public function status($id)
 {
   # initiate constructor
   $query =  $this->creditRecharge;

   # get the status 
   $status =  $query->where('id', $id)->first()->status;

   # check status, if active
   if ($status == '1') {
     #message
     $message = $this->inActiveMessage($this->type);

     # deactive( update status to zero)
     $statusCode = '0';
   } else {
     #message
     $message = $this->activeMessage($this->type);

     # active( update status to one)
     $statusCode = '1';
   }
   
   # update status code
   $query->where('id', $id)->update(['status' => $statusCode]);

   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $message];
 }
}
