<?php

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;
use App\Models\BusinessCountry;
use App\Models\BusinessCity;
use App\Models\Country;
use App\Models\State;
use App\Models\City;


class BusinessCityController extends Controller
{
 use MessageStatusTrait;

 # Bind Type
 protected $type = 'Business City';

 # Bind location
 protected $view = 'application.business_city.';

 # Bind document
 protected $document;

 /**
  * default constructor
  * @param
  * @return
  */
 function __construct(Country $Country,State $State,City $City,BusinessCity $BusinessCity,BusinessCountry $BusinessCountry)
 {
          $this->Country = $Country;
          $this->State = $State;
          $this->City = $City;
          $this->BusinessCity = $BusinessCity;
          $this->BusinessCountry = $BusinessCountry;
 }
 
 /**
  * index page of BusinessCountry
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function index(Request $request)
 {
 	# fetch BusinessCountry list


 $query = $this->BusinessCity->orderBy('id');

if ($request->name != '') {
       # code...
       $query = $query->where('name', $request->name);
               // ->orWhere('email', '=', $request->name);
     }

     if ($request->status != '') {
      //dd($request->status);
       # code...
       $query = $query->where('status', $request->status);
     }
     
     $BusinessCity = $query->get();

  return view($this->view.'index')->with([
                                        'BusinessCity' => $BusinessCity, 
                                    ]);
                         }






public function create()
 {
  # Fetch Country by id
  //$BusinessCountry = $this->BusinessCountry;
   $BusinessCountry = $this->BusinessCountry->get();
               
  return view($this->view.'create')->with([
                                        'BusinessCountry' => $BusinessCountry, 
                                    ]);
 }




 /**
  * create city
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function store(Request $request)
 {
 	# current user
 	$user = Auth::guard('admin')->user();

  $query = $this->BusinessCity;

 	
 	# request param
 	$arrayData = [
                  'country_id'       => $request->country_id ?? null,
                  'state_id'       => $request->state_id ?? 1,
                  'city_id'       => $request->city_id ?? null,
                   'status'       => $request->status ?? 1,
                  /* 'added_by'   => $user->id ?? null*/
 	];

  # check the requested document already exist or not
  $already = $query->where('city_id', $request->city_id)->count();
  
  if ($already == 0) {
    #store 
    $createBusnessCity = $query->create($arrayData);

    # check created or not
    # if created
    if ($createBusnessCity) {
     # return successs
     $output = ['success' => 200, 'message' => 'City Added Successfully'];
    } else {
     # return error
     $output = ['error' => 100, 'message' => 'Something went wrong'];
    }
  } else {
    # return exist message
    $output = ['error' => 100, 'message' => 'Already Exist'];
  }
  return $output;
 } 

 /**
  * edit  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function update($id)
 {
  # Fetch BusinessCountry by id
  $BusinessCountry = $this->BusinessCountry->get();
  $State = $this->State->get();
  $City = $this->City->get();
  


  $BusinessCity = $this->BusinessCity
                   ->where('id', $id)
                   ->first(); 


//dd($BusinessCity);

 	# code...
 	return view($this->view.'edit')->with(['BusinessCity' => $BusinessCity,
                                         'State' => $State,
                                         'BusinessCountry' => $BusinessCountry,
                                         'City' => $City,
                                       


                                       ]);
 }

 /**
  * edit BusinessCountry
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function edit(Request $request, $id)
 {
  # current user
  $user = Auth::guard('admin')->user();

  $query = $this->BusinessCity;

 
  # request param
  $arrayData = [
                  'country_id'       => $request->country_id ?? null,
                  'state_id'       => $request->state_id ?? 1,
                  'city_id'       => $request->city_id ?? null,
                   'status'       => $request->status ?? 1,
                  /* 'added_by'   => $user->id ?? null*/
  ];
  

  # check the requested document already exist or not
  $already = $query->where('city_id', $request->city_id)
                         ->where('id', '!=', $id)
                         ->count();
  
  if ($already == 0) {
    #store 
    $updateData = $query->where('id', $id)->update($arrayData);

    # check created or not
    # if created
    if ($updateData) {
     # return successs
     $output = ['success' => 200, 'message' => 'Business city update Successfully'];
    } else {
     # return error
     $output = ['error' => 100, 'message' => 'Something went wrong'];
    }
  } else {
    # return exist message
    $output = ['error' => 100, 'message' => 'Already Exist'];
  }
  return $output;
 }  

 /**
  * delete document
  * @param $id
  * @return \Illuminate\Http\Response
  */
 public function delete($id)
 {
   $query = $this->BusinessCity;

   # current user
   $user = Auth::guard('admin')->user();

   #update deleted by
   //$query->where('id', $id)->update(['deleted_by' => $user->id]);

   # delete document by id
   $query->where('id', $id)->delete();
   
   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $this->deleteMessage($this->type)];
 }

 /**
  * active deactive
  * @param $id
  * @return \Illuminate\Http\Response
  */ 
 public function status($id)
 {
   # initiate constructor
   $query =  $this->BusinessCity;

   # get the status 
   $status =  $query->where('id', $id)->first()->status;

   # check status, if active
   if ($status == '1') {
     #message
     $message = $this->inActiveMessage($this->type);

     # deactive( update status to zero)
     $statusCode = '0';
   } else {
     #message
     $message = $this->activeMessage($this->type);

     # active( update status to one)
     $statusCode = '1';
   }
   
   # update status code
   $query->where('id', $id)->update(['status' => $statusCode]);

   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $message];
 }






/**
  * json responce of state by country id
  * @param
  * @return \Illuminate\Http\Response in json
  */
  public function state(Request $request)
  {
    # fetch states 
    $states = $this->State
                   ->where('country_id',$request->id)
                  // ->where('status', '1')
                   ->get();

    # return json responce
    return response()->json($states);
  } 


  /**
  * json responce of city by state id
  * @param
  * @return \Illuminate\Http\Response in json
  */
  public function city(Request $request)
  {
    # fetch city 
    $city = $this->City
                   ->where('state_id',$request->id)
                  // ->where('status', '1')
                   ->get();

    # return json responce
    return response()->json($city);
  } 


















}
