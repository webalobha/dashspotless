<?php

namespace App\Http\Controllers\AdminController;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;
use App\Models\CmsPageParent;
use App\Models\CmsPages;
use App\Models\CmsPageParentType;
use App\Models\DisputeReasonType;

use DB;

class CMSPageController extends Controller
{
 use MessageStatusTrait;

 # Bind Type
 protected $type = 'CMS Page';

 # Bind location
 protected $view = 'application.cms_page.';

 # Bind CmsPages
 protected $CmsPages;

 # Bind CmsPageParentType
 protected $CmsPageParentType;

 # Bind CmsPageParent
 protected $CmsPageParent;

 /**
  * default constructor
  * @param
  * @return
  */
 function __construct(
                    CmsPages $CmsPages,
                    CmsPageParentType $CmsPageParentType,
                    CmsPageParent $CmsPageParent    

                  )
          {
  $this->CmsPages = $CmsPages;
  $this->CmsPageParentType = $CmsPageParentType;
  $this->CmsPageParent = $CmsPageParent;

 }
         
 


 /**
  * create page of CmsPages
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function create()
 {
  # DisputeReason Type
  $CmsPageParentType = $this->CmsPageParentType->get();


  
  return view($this->view.'create')->with(['CmsPageParentType' => $CmsPageParentType]);
 }


 /**
  * index page of DisputeReasonType
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function index(Request $request)
 {
  # fetch variant list
  $query = $this->CmsPages->orderBy('id');

    /* if ($request->category != '') {
       # code...
       $query = $query->where('category_id', $request->category);
     }

     if ($request->sub_category != '') {
       # code...
       $query = $query->where('sub_category_id', $request->sub_category);
     }
     if ($request->variant != '') {
       # code...
       $query = $query->where('variant_id', $request->variant);
     }
     if ($request->service != '') {
       # code...
       $query = $query->where('id', $request->service);
     }*/

  $services = $query->get();

  # fetch CmsPages name
  $CmsPages = $this->CmsPages->orderBy('id')->get(); 

//dd($CmsPages);
 	# return to index page
 	return view($this->view.'index')->with([
 		                                    'CmsPages'=> $CmsPages,
 		                                  ]);
 }


public function assign(Request $request)
 {
 
  $CmsPages = $this->CmsPages->orderBy('id')->get(); 
  $CmsPageParentType = $this->CmsPageParentType->get(); 
  $CmsPageParent =  $this->CmsPageParent->with('PageName','PageType','UserType')->orderBy('id')->get(); 



  # return to index page
  return view($this->view.'assign_page')->with([
                                        'CmsPages'=> $CmsPages,
                                        'CmsPageParentType'=> $CmsPageParentType,
                                        'CmsPageParent'=> $CmsPageParent,
                                      ]);
 }




 /**
  * create DisputeReason
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */

 public function store_assign(Request $request)
 {
  
  # current user
  $user = Auth::guard('admin')->user();

//dd(preg_replace('/[^A-Za-z0-9-]+/', '-', $request->name));

 

 $type = $request->type;

 if(empty($type)){
  $output = ['success' => 100, 'message' => 'Please select page type'];
  return $output;
 }

 if(empty($request->page_id)){
  $output = ['success' => 100, 'message' => 'Please select page'];
  return $output;
 }

 //dd($type);

foreach ($type as $key => $value) {
  


            $terms = [
               'user_or_provider_id' =>  $request->user_or_provider_id ?? 0,
            'page_id' =>  $request->page_id ?? '',
            'parent_id' =>  $value['type'] ?? '',
             
            ];


$check = $this->CmsPageParent->where('page_id',$request->page_id)->where('parent_id',$value['type'])->first();
if($check){

}else{
  $this->CmsPageParent->create($terms);
}



      
}



  $output = ['success' => 200, 'message' => 'Assin page Successfully'];
  return $output;
 } 





 /**
  * create DisputeReason
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */

 public function store(Request $request)
 {
 	
  # current user
 	$user = Auth::guard('admin')->user();

//dd(preg_replace('/[^A-Za-z0-9-]+/', '-', $request->name));

  $query = $this->CmsPages;

 $Data = [
            'position' => $request->position ,
            'name' => $request->name,
            'unchangeable_name'     => $request->unchangeable_name??0 ,
             'slug'     => preg_replace('/[^A-Za-z0-9-]+/', '-', $request->name) ,
             'page' => $request->page ,

          ];

   $postdata = $query->create($Data);



  $output = ['success' => 200, 'message' => 'Page created Successfully'];
  return $output;
 } 






/**
  * view details  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function view($id)
 {
 
                                        
  $CmsPages =  $this->CmsPages->where('id',$id)->first();

 
  return view($this->view.'view')->with(['CmsPages'=>$CmsPages]);
 }

 /**
  * edit  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function update($id)
 {
   $CmsPages =  $this->CmsPages->where('id',$id)->first();

 
  return view($this->view.'edit')->with(['CmsPages'=>$CmsPages]);
 }

 /**
  * edit DisputeReason
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function edit(Request $request)
 {
       


$query = $this->CmsPages;

$Data = [
            'position' => $request->position ,
            'name' => $request->name,
            'unchangeable_name'     => $request->unchangeable_name??0 ,
             'slug'     => preg_replace('/[^A-Za-z0-9-]+/', '-', $request->name) ,
             'page' => $request->page ,

          ];
$query->where('id',$request->id)->update($Data);
  


  $output = ['success' => 200, 'message' => 'CMS page update Successfully'];
  return $output;
 } 

 /**
  * delete variant
  * @param $id
  * @return \Illuminate\Http\Response
  */
 public function delete($id)
 {
   $query = $this->CmsPages;

   # current user
   $user = Auth::guard('admin')->user();

   #update deleted by
   //$query->where('id', $id)->update(['deleted_by' => $user->id]);

   # delete CMS by id
   $query->where('id', $id)->delete();
   
   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $this->deleteMessage($this->type)];
 }
     






 /**
  * delete deleteAssign
  * @param $id
  * @return \Illuminate\Http\Response
  */
 public function deleteAssign($id)
 {
   $query = $this->CmsPageParent;

   # current user
   $user = Auth::guard('admin')->user();

   #update deleted by
   //$query->where('id', $id)->update(['deleted_by' => $user->id]);

   # delete variant by id
   $query->where('id', $id)->delete();
   
   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $this->deleteMessage($this->type)];
 }


 /**
  * active deactive
  * @param $id
  * @return \Illuminate\Http\Response   
  */ 
 public function status($id)
 {
   # initiate constructor
   $query =  $this->CmsPages;

   # get the status 
   $status =  $query->where('id', $id)->first()->status;

   # check status, if active
   if ($status == '1') {
     #message
     $message = $this->inActiveMessage($this->type);

     # deactive( update status to zero)
     $statusCode = '0';
   } else {
     #message
     $message = $this->activeMessage($this->type);

     # active( update status to one)
     $statusCode = '1';
   }
   
   # update status code
   $query->where('id', $id)->update(['status' => $statusCode]);

   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $message];
 }


}
