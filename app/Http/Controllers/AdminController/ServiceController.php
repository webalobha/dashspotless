<?php

namespace App\Http\Controllers\AdminController;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;
use App\Models\Variant;
use App\Models\ConsultVisitBookingDetails;
use App\Models\ConsultVisitBookingDetailVariant;
use App\Models\ConsultVisitTermsAndCondition;
use App\Models\ConsultVisitBookingDetailsImages;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Service;
use App\Models\ServiceBanner;
use App\Models\DiscountType;
use App\Models\ServiceBulletPoints;
use DB;

class ServiceController extends Controller
{
 use MessageStatusTrait;

 # Bind Type
 protected $type = 'Service';

 # Bind location
 protected $view = 'application.service.';

 # Bind service
 protected $service;

 # Bind DiscountType
 protected $discountType;

 /**
  * default constructor
  * @param
  * @return
  */
 function __construct(
 	            Variant                          $variant,
                Service                          $service,
 	            Category                         $category,
              SubCategory                      $subCategory,
 	            DiscountType                      $discountType,
                ServiceBanner                    $ServiceBanner,
                ServiceBulletPoints              $ServiceBulletPoints,
                ConsultVisitBookingDetails       $ConsultVisitBookingDetails, 
                ConsultVisitTermsAndCondition    $ConsultVisitTermsAndCondition,
                ConsultVisitBookingDetailVariant $ConsultVisitBookingDetailVariant,
                ConsultVisitBookingDetailsImages $ConsultVisitBookingDetailsImages

 )
 {
                $this->variant                          = $variant;
                $this->service                          = $service;
                $this->category                         = $category;
                $this->subCategory                      = $subCategory;
                $this->discountType                     = $discountType;
                $this->ServiceBanner                    = $ServiceBanner;
                $this->ServiceBullet                    = $ServiceBulletPoints;
                $this->ConsultVisitBookingDetails       = $ConsultVisitBookingDetails;
                $this->ConsultVisitTermsAndCondition    = $ConsultVisitTermsAndCondition;
                $this->ConsultVisitBookingDetailVariant = $ConsultVisitBookingDetailVariant;
                $this->ConsultVisitBookingDetailsImages = $ConsultVisitBookingDetailsImages;

}
 


 /**
  * create page of Service
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function create()
 {
  # discount type
  $discountTypes = $this->discountType->all();

  # fetch variant list
  $category = Category::orderBy('name')
                     ->get();

  # return to create page
                    
  return view($this->view.'create')->with(['category' => $category, 'discountTypes' => $discountTypes]);;
 }


 /**
  * index page of variant
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function index(Request $request)
 {
  # fetch variant list
  $query = $this->service->orderBy('id', 'desc');

     if ($request->category != '') {
       # code...
       $query = $query->where('category_id', $request->category);
     }

     if ($request->sub_category != '') {
       # code...
       $query = $query->where('sub_category_id', $request->sub_category);
     }
     if ($request->variant != '') {
       # code...
       $query = $query->where('variant_id', $request->variant);
     }
     if ($request->service != '') {
       # code...
       $query = $query->where('id', $request->service);
     }

  $services = $query->get();

  # fetch category name
  $categoryList = $this->category->orderBy('name')->get();  

  $subCategoryList = '';
  if ($request->category) {
   # get subCategory name
   $subCategoryList =  $this->subCategory->where('category_id', $request->category)->get(); 
  }

  $varientList = '';
  if ($request->sub_category) {
   # get variant name
   $varientList =  $this->variant->where('sub_category_id', $request->sub_category)->get(); 
  }

  $serviceslList = '';
  if ($request->variant) {
   # get tehsil name
   $serviceslList =  $this->service->where('variant_id', $request->variant)->get(); 
  }
// dd($services);
 	# return to index page
 	return view($this->view.'index')->with([
 		                                    'services'        => $services,
 		                                    'category_id'     => $request->category,
 		                                    'categoryList'    => $categoryList,
 		                                    'sub_category_id' => $request->sub_category,
 		                                    'subCategoryList' => $subCategoryList,
 		                                    'variant_id'      => $request->variant,
 		                                    'varientList'     => $varientList,
 		                                    'service_id'      => $request->service,
 		                                    'serviceslList'   => $serviceslList,
 		                                  ]);
 }

 /**
  * create variant
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function store(Request $request)
 {
 	
  # current user
 	$user = Auth::guard('admin')->user();

  $amount = $request->price ?? '0';
  $discount = $request->discount ?? '0';
  $discountTypeId = $request->discount_type ?? '0';

  $discountAmount = '0';
  if ($discountTypeId == '1' OR $discountTypeId == '') {
    # code...
    $discountAmount = $amount;
    $finalPrice = $amount;
  } elseif($discountTypeId == '2') {
    $discountAmount = ($amount/100)*$discount;
    $finalPrice = $amount - $discountAmount;
  } elseif($discountTypeId == '3') {
    $discountAmount = $amount - $discount;
    $finalPrice = $amount - $discountAmount;
  }


  $query = $this->service;

  # upload avtar
    if($request->hasfile('icon_name')) {
     $file = $request->file('icon_name');
     $name = $file->getClientOriginalName(); // getting image name
     $date = date('y-m-d');              
     $randNumber = rand(0000,9999);              
     $newFile =$date.'_'.$randNumber.'_'.$name;
     $filename = str_replace(' ', '',$newFile); 
     $file->move('dist/img/service_icon/', $filename);
     $avtarPath ='dist/img/service_icon/'.$filename;
    } else {
     $avtarPath  = null;  
     $name = null;
    }
 
if ($request->is_offer == 1) {
   # offer detail
   $isOffer     = 1;
   $offerFrom   = $request->offer_from;
   $offerTo     = $request->offer_to;

  # upload avtar
    if($request->hasfile('offer_banner')) {
     $file = $request->file('offer_banner');
     $name = $file->getClientOriginalName(); // getting image name
     $date = date('y-m-d');              
     $randNumber = rand(0000,9999);              
     $newFile =$date.'_'.$randNumber.'_'.$name;
     $filename = str_replace(' ', '',$newFile); 
     $file->move('dist/img/offer_banner/', $filename);
     $offerBanner ='dist/img/offer_banner/'.$filename;
    } else {
     $offerBanner  = null;  
    }

 } else {
   # offer detail
   $isOffer     = 0;
   $offerFrom   = null;
   $offerTo     = null;
   $offerBanner = null;
 }

 $serviceData = [
            'category_id' => $request->category_id,
            'description' => $request->description,
            'sub_category_id'     => $request->sub_category_id,
            'variant_id' => $request->variant_id ?? 0,
            'title'     => $request->title,
            'time_taken_minutes'     => $request->time_taken_minutes,
            'price'     => $amount,
            'discount_type'     => $discountTypeId,
            'discount'     => $discount,
            'discounted_price'     => $discountAmount,
            'final_price'  => $finalPrice,
            'tool_tip_content'     => $request->tool_tip_content,
            'icon_name'     => $name,
            'icon_path'     => $avtarPath,
            'is_offer'           => $isOffer,
            'offer_from'         => $offerFrom,
            'offer_to'           => $offerTo,
            'offer_banner'       => $offerBanner,

          ];

   $postdata = $query->create($serviceData);



$bullet_part = $request->bullet_part;

foreach ($bullet_part as $key => $value) {
    if(isset($value['bullet_part_id'])){

$bullet_part_id =$value['bullet_part_id'];
        $terms = [
            'title' =>  $value['title'] ?? '',
            'description' =>  $value['description'] ?? '',
            'added_by'        => $user->id ?? null
          ];

 $this->ServiceBullet->where('id', $bullet_part_id)->update($terms);



    }else{


            $terms = [
            'service_id' =>  $postdata->id ?? '',
            'title' =>  $value['title'] ?? '',
            'description' =>  $value['description'] ?? '',
            'added_by'        => $user->id ?? null
            ];

            $this->ServiceBullet->create($terms);


    }
 
}


$banner= $request->banner;

//dd($reference_image);
if($banner != ''){

foreach ($banner as $key => $value) {




    if(isset($value['banner_image_id']) && isset($value['banner_image'])){

$banner_image_id =$value['banner_image_id'];
        # code...
          $file1 = $value['banner_image'];
          $name1 = $file1->getClientOriginalName(); // getting image name
          $date1 = date('y-m-d');              
          $randNumber1 = rand(0000,9999);              
          $newFile1 =$date1.'_'.$randNumber1.'_'.$name1;
          $filename1 = str_replace(' ', '',$newFile1); 
          $file1->move('dist/img/service/', $filename1);
          $bannerPath ='dist/img/service/'.$filename1;

          # Gather the blog content.
          $banner = [
           
            
            'avtar_name'     => $name1,
            'avtar_path'     => $bannerPath,
            'added_by'        => $user->id ?? null
          ];

          # create blog content.
          $this->ServiceBanner->where('id', $banner_image_id)->update($banner);



    }else{

 if(isset($value['banner_image'])){
            # code...
          $file1 = $value['banner_image'];
          $name1 = $file1->getClientOriginalName(); // getting image name
          $date1 = date('y-m-d');              
          $randNumber1 = rand(0000,9999);              
          $newFile1 =$date1.'_'.$randNumber1.'_'.$name1;
          $filename1 = str_replace(' ', '',$newFile1); 
          $file1->move('dist/img/service/', $filename1);
          $bannerPath ='dist/img/service/'.$filename1;

          # Gather the blog content.
          $banner = [
            'service_id' =>  $postdata->id ?? '',
             'avtar_name'     => $name1,
            'avtar_path'     => $bannerPath,
            'added_by'        => $user->id ?? null
          ];

          # create blog content.
          $this->ServiceBanner->create($banner);

         
}

    }
 
}

  
}









  $output = ['success' => 200, 'message' => 'Service Added Successfully'];
  return $output;
 } 




/**
  * view details  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function view($id)
 {
  # Fetch variant by id
  $category = Category::orderBy('name')->get();

  $SubCategory = SubCategory::orderBy('name')->get();

  $Variant = Variant::get();
                                         
  $service =  $this->service->where('id',$id)->first();
  # code...
 
  return view($this->view.'view')->with(['Variant'=>$Variant,'service' => $service,'category'=>$category,'SubCategory'=>$SubCategory]);
 }

 /**
  * edit  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function update($id)
 {
  # discount type
  $discountTypes = $this->discountType->all();

  # Fetch variant by id
  $category = Category::orderBy('name')->get();


  $SubCategory = SubCategory::orderBy('name')->get();
                     
  $service =  $this->service->where('id',$id)->first();
 	# code...

  $variants = $this->variant
                   ->where('radio_or_check','1')
                   ->where('consult_visit_avail','!=','1')
                   ->where('sub_category_id', $service->sub_category_id)
                   ->get();
 
 	return view($this->view.'edit')->with(['service' => $service,'category'=>$category,'SubCategory'=>$SubCategory,'discountTypes'=>$discountTypes, 'variants' => $variants]);
 }

 /**
  * edit variant
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function edit(Request $request)
 {
  $user = Auth::guard('admin')->user();
  $amount = $request->price ?? '';
  $discount = $request->discount ?? '';
  $discountTypeId = $request->discount_type ?? '';
  
  $discountAmount = '0';
  // if ($discountTypeId == '1' OR $discountTypeId == '') {
  //   # code...
  //   $discountAmount = $amount;
  // } elseif($discountTypeId == '2') {

  //   $discounting = $amount*$discount/100;
  //   $discountAmount = $amount - $discounting;

  // } elseif($discountTypeId == '3') {
  //   $discountAmount = $amount - $discount;
  // }
  if ($discountTypeId == '1' OR $discountTypeId == '') {
    # code...
    $discountAmount = $amount;
    $finalPrice = $amount;
  } elseif($discountTypeId == '2') {
    $discountAmount = ($amount/100)*$discount;
    $finalPrice = $amount - $discountAmount;
  } elseif($discountTypeId == '3') {
    $discountAmount = $amount - $discount;
    $finalPrice = $amount - $discountAmount;
  }

$query = $this->service;

  # upload avtar
    if($request->hasfile('icon_name')) {
     $file = $request->file('icon_name');
     $name = $file->getClientOriginalName(); // getting image name
     $date = date('y-m-d');              
     $randNumber = rand(0000,9999);              
     $newFile =$date.'_'.$randNumber.'_'.$name;
     $filename = str_replace(' ', '',$newFile); 
     $file->move('dist/img/service_icon/', $filename);
     $avtarPath ='dist/img/service_icon/'.$filename;
    } else {
     $avtarPath  = $query->where('id', $request->service_id)->first()->icon_path ?? null;  
     $name = $query->where('id', $request->service_id)->first()->icon_name ?? null;
    }

 if ($request->is_offer == 1) {
   # offer detail
   $isOffer     = 1;
   $offerFrom   = $request->offer_from;
   $offerTo     = $request->offer_to;

  # upload avtar
    if($request->hasfile('offer_banner')) {
     $file = $request->file('offer_banner');
     $name = $file->getClientOriginalName(); // getting image name
     $date = date('y-m-d');              
     $randNumber = rand(0000,9999);              
     $newFile =$date.'_'.$randNumber.'_'.$name;
     $filename = str_replace(' ', '',$newFile); 
     $file->move('dist/img/offer_banner/', $filename);
     $offerBanner ='dist/img/offer_banner/'.$filename;
    } else {
     $offerBanner  = $query->where('id', $request->service_id)->first()->offer_banner ?? null;  
    }

 } else {
   # offer detail
   $isOffer     = 0;
   $offerFrom   = null;
   $offerTo     = null;
   $offerBanner = null;
 }

 $serviceData = [
            'category_id'        => $request->category_id,
            'description'        => $request->description,
            'sub_category_id'    => $request->sub_category_id,
            'variant_id'         => $request->variant_id ?? 0,
            'title'              => $request->title,
            'time_taken_minutes' => $request->time_taken_minutes,
            'price'              => $amount,
            'discount_type'      => $discountTypeId,
            'discount'           => $discount,
            'discounted_price'   => $discountAmount,
            'final_price'        => $finalPrice,
            'tool_tip_content'   => $request->tool_tip_content,
            'icon_name'          => $name,
            'icon_path'          => $avtarPath,
            'is_offer'           => $isOffer,
            'offer_from'         => $offerFrom,
            'offer_to'           => $offerTo,
            'offer_banner'       => $offerBanner,
          ]; 

$query->where('id',$request->service_id)->update($serviceData);

$bullet_part = $request->bullet_part;
if ($bullet_part != '') {
foreach ($bullet_part as $key => $value) {
    if(isset($value['bullet_part_id'])){

     $bullet_part_id =$value['bullet_part_id'];
     $terms = [
               'title'       =>  $value['title'] ?? '',
               'description' =>  $value['description'] ?? '',
               'added_by'    => $user->id ?? null
              ];

     $this->ServiceBullet->where('id', $bullet_part_id)->update($terms);
    } else {
     $terms = [
               'service_id'  =>  $request->service_id ?? '',
               'title'       =>  $value['title'] ?? '',
               'description' =>  $value['description'] ?? '',
               'added_by'    => $user->id ?? null
              ];

     $this->ServiceBullet->create($terms);
    }
}
}
$banner= $request->banner;
if($banner != null){
foreach ($banner as $key => $value) {

    if(isset($value['banner_id']) && isset($value['banner_image'])){

     $banner_image_id =$value['banner_id'];
     # code...
     $file1 = $value['banner_image'];
     $name1 = $file1->getClientOriginalName(); // getting image name
     $date1 = date('y-m-d');              
     $randNumber1 = rand(0000,9999);              
     $newFile1 =$date1.'_'.$randNumber1.'_'.$name1;
     $filename1 = str_replace(' ', '',$newFile1); 
     $file1->move('dist/img/service/', $filename1);
     $bannerPath ='dist/img/service/'.$filename1;

     # Gather the blog content.
     $banner = [   
                'avtar_name' => $name1,
                'avtar_path' => $bannerPath,
                'added_by'   => $user->id ?? null
     ];

     # create blog content.
     $this->ServiceBanner->where('id', $banner_image_id)->update($banner);
    } else {

       if(isset($value['banner_image'])){
            # code...
          $file1 = $value['banner_image'];
          $name1 = $file1->getClientOriginalName(); // getting image name
          $date1 = date('y-m-d');              
          $randNumber1 = rand(0000,9999);              
          $newFile1 =$date1.'_'.$randNumber1.'_'.$name1;
          $filename1 = str_replace(' ', '',$newFile1); 
          $file1->move('dist/img/service/', $filename1);
          $bannerPath ='dist/img/service/'.$filename1;

          # Gather the blog content.
          $banner = [
            'service_id' =>  $request->service_id ?? '',
            'avtar_name' => $name1,
            'avtar_path' => $bannerPath,
            'added_by'   => $user->id ?? null
          ];

          # create blog content.
          $this->ServiceBanner->create($banner);
       }
    }
  }
}
  $output = ['success' => 200, 'message' => 'Service update Successfully'];
  return $output;
 } 

 /**
  * delete variant
  * @param $id
  * @return \Illuminate\Http\Response
  */
 public function delete($id)
 {
   $query = $this->service;

   # current user
   $user = Auth::guard('admin')->user();

   #update deleted by
   $query->where('id', $id)->update(['deleted_by' => $user->id]);

   # delete variant by id
   $query->where('id', $id)->delete();
   
   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $this->deleteMessage($this->type)];
 }

 /**
  * active deactive
  * @param $id
  * @return \Illuminate\Http\Response
  */ 
 public function status($id)
 {
   # initiate constructor
   $query =  $this->service;

   # get the status 
   $status =  $query->where('id', $id)->first()->status;

   # check status, if active
   if ($status == '1') {
     #message
     $message = $this->inActiveMessage($this->type);

     # deactive( update status to zero)
     $statusCode = '0';
   } else {
     #message
     $message = $this->activeMessage($this->type);

     # active( update status to one)
     $statusCode = '1';
   }
   
   # update status code
   $query->where('id', $id)->update(['status' => $statusCode]);

   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $message];
 }

 public function service_by_variant(Request $request)
 {
 	# code...
    # fetch service 
    $service = $this->service
                    ->where('variant_id',$request->variant_id)
                    ->get();

    # return json responce
    return response()->json($service);
 }
}
