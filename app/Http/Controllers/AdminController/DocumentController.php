<?php

namespace App\Http\Controllers\AdminController;

use App\Models\MandatoryDocumentsForProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;

class DocumentController extends Controller
{
 use MessageStatusTrait;

 # Bind Type
 protected $type = 'Document';

 # Bind location
 protected $view = 'application.document.';

 # Bind document
 protected $document;

 /**
  * default constructor
  * @param
  * @return
  */
 function __construct(MandatoryDocumentsForProvider $MandatoryDocumentsForProvider)
 {
 	$this->document = $MandatoryDocumentsForProvider;
 }
 
 /**
  * index page of document
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function index(Request $request)
 {
 	# fetch document list


 $query = $this->document->orderBy('id');

if ($request->name != '') {
       # code...
       $query = $query->where('name', $request->name);
               // ->orWhere('email', '=', $request->name);
     }

     if ($request->status != '') {
      //dd($request->status);
       # code...
       $query = $query->where('status', $request->status);
     }
     
     $documents = $query->get();

  return view($this->view.'index')->with([
                                        'documents' => $documents, 
                                        'name' => $request->name??'',
                                        'status' => $request->status??''
                                    ]);
                         }






public function create()
 {
  # Fetch document by id
  $document = $this->document;
                 
  # code...
  return view($this->view.'create');
 }




 /**
  * create document
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function store(Request $request)
 {
 	# current user
 	$user = Auth::guard('admin')->user();

  $query = $this->document;

 	
 	# request param
 	$arrayData = [
                  'name'       => $request->name ?? null,
                  'added_by'   => $user->id ?? null
 	];

  # check the requested document already exist or not
  $countdocument = $query->where('name', $request->name)->count();
  
  if ($countdocument == 0) {
    #store 
    $createdocument = $query->create($arrayData);

    # check created or not
    # if created
    if ($createdocument) {
     # return successs
     $output = ['success' => 200, 'message' => 'document Added Successfully'];
    } else {
     # return error
     $output = ['error' => 100, 'message' => 'Something went wrong'];
    }
  } else {
    # return exist message
    $output = ['error' => 100, 'message' => 'Already Exist'];
  }
  return $output;
 } 

 /**
  * edit  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function update($id)
 {
  # Fetch document by id
  $document = $this->document
                   ->where('id', $id)
                   ->first(); 
 	# code...
 	return view($this->view.'edit')->with(['document' => $document]);
 }

 /**
  * edit document
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function edit(Request $request, $id)
 {
  # current user
  $user = Auth::guard('admin')->user();

  $query = $this->document;

 
  # request param
  $arrayData = [
                  'name'       => $request->name ?? null,
                  'updated_by' => $user->id ?? null
  ];

  # check the requested document already exist or not
  $countdocument = $query->where('name', $request->name)
                         ->where('id', '!=', $id)
                         ->count();
  
  if ($countdocument == 0) {
    #store 
    $updatedocument = $query->where('id', $id)->update($arrayData);

    # check created or not
    # if created
    if ($updatedocument) {
     # return successs
     $output = ['success' => 200, 'message' => 'document update Successfully'];
    } else {
     # return error
     $output = ['error' => 100, 'message' => 'Something went wrong'];
    }
  } else {
    # return exist message
    $output = ['error' => 100, 'message' => 'Already Exist'];
  }
  return $output;
 }  

 /**
  * delete document
  * @param $id
  * @return \Illuminate\Http\Response
  */
 public function delete($id)
 {
   $query = $this->document;

   # current user
   $user = Auth::guard('admin')->user();

   #update deleted by
   $query->where('id', $id)->update(['deleted_by' => $user->id]);

   # delete document by id
   $query->where('id', $id)->delete();
   
   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $this->deleteMessage($this->type)];
 }

 /**
  * active deactive
  * @param $id
  * @return \Illuminate\Http\Response
  */ 
 public function status($id)
 {
   # initiate constructor
   $query =  $this->document;

   # get the status 
   $status =  $query->where('id', $id)->first()->status;

   # check status, if active
   if ($status == '1') {
     #message
     $message = $this->inActiveMessage($this->type);

     # deactive( update status to zero)
     $statusCode = '0';
   } else {
     #message
     $message = $this->activeMessage($this->type);

     # active( update status to one)
     $statusCode = '1';
   }
   
   # update status code
   $query->where('id', $id)->update(['status' => $statusCode]);

   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $message];
 }
}
