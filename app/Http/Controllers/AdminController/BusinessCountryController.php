<?php

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;
<<<<<<< HEAD
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;
use App\Models\BusinessCountry;
use App\Models\BusinessCity;
use App\Models\Country;
use App\Models\State;
use App\Models\City;


class BusinessCountryController extends Controller
{
 use MessageStatusTrait;

 # Bind Type
 protected $type = 'BusinessCountry';

 # Bind location
 protected $view = 'application.business_country.';

 # Bind document
 protected $document;

 /**
  * default constructor
  * @param
  * @return
  */
 function __construct(Country $Country,BusinessCountry $BusinessCountry)
 {
 	$this->Country = $Country;
  $this->BusinessCountry = $BusinessCountry;
 }
 
 /**
  * index page of BusinessCountry
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function index(Request $request)
 {
 	# fetch BusinessCountry list


 $query = $this->BusinessCountry->orderBy('id');

if ($request->name != '') {
       # code...
       $query = $query->where('name', $request->name);
               // ->orWhere('email', '=', $request->name);
     }

     if ($request->status != '') {
      //dd($request->status);
       # code...
       $query = $query->where('status', $request->status);
     }
     
     $BusinessCountries = $query->get();

  return view($this->view.'index')->with([
                                        'BusinessCountries' => $BusinessCountries, 
                                    ]);
                         }






public function create()
 {
  # Fetch Country by id
  $BusinessCountry = $this->BusinessCountry;
   $Country = $this->Country->get();
               
  return view($this->view.'create')->with([
                                        'Country' => $Country, 
                                    ]);
 }




 /**
  * create Country
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function store(Request $request)
 {
 	# current user
 	$user = Auth::guard('admin')->user();

  $query = $this->BusinessCountry;

 	
 	# request param
 	$arrayData = [
                  'country_id'       => $request->country_id ?? null,
                  'status'       => $request->status ?? 1,
                  'currency_code'       => $request->currency_code ?? null,
                   'currency_symbol'       => $request->currency_symbol ?? null,
                  /* 'added_by'   => $user->id ?? null*/
 	];

  # check the requested document already exist or not
  $already = $query->where('country_id', $request->country_id)->count();
  
  if ($already == 0) {
    #store 
    $createBusnessCountry = $query->create($arrayData);

    # check created or not
    # if created
    if ($createBusnessCountry) {
     # return successs
     $output = ['success' => 200, 'message' => 'Country Added Successfully'];
    } else {
     # return error
     $output = ['error' => 100, 'message' => 'Something went wrong'];
    }
  } else {
    # return exist message
    $output = ['error' => 100, 'message' => 'Already Exist'];
  }
  return $output;
 } 

 /**
  * edit  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function update($id)
 {
  # Fetch BusinessCountry by id
  $Country = $this->Country->get();
  $BusinessCountry = $this->BusinessCountry
                   ->where('id', $id)
                   ->first(); 
 	# code...
 	return view($this->view.'edit')->with(['BusinessCountry' => $BusinessCountry,'Country' => $Country]);
 }

 /**
  * edit BusinessCountry
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function edit(Request $request, $id)
 {
  # current user
  $user = Auth::guard('admin')->user();

  $query = $this->BusinessCountry;

 
 # request param
  $arrayData = [
                  'country_id'       => $request->country_id ?? null,
                  
                  'currency_code'       => $request->currency_code ?? null,
                   'currency_symbol'       => $request->currency_symbol ?? null,
                 /* 'added_by'   => $user->id ?? null*/
  ];

  # check the requested document already exist or not
  $already = $query->where('country_id', $request->country_id)
                         ->where('id', '!=', $id)
                         ->count();
  
  if ($already == 0) {
    #store 
    $updateData = $query->where('id', $id)->update($arrayData);

    # check created or not
    # if created
    if ($updateData) {
     # return successs
     $output = ['success' => 200, 'message' => 'Business country update Successfully'];
    } else {
     # return error
     $output = ['error' => 100, 'message' => 'Something went wrong'];
    }
  } else {
    # return exist message
    $output = ['error' => 100, 'message' => 'Already Exist'];
  }
  return $output;
 }  

 /**
  * delete document
  * @param $id
  * @return \Illuminate\Http\Response
  */
 public function delete($id)
 {
   $query = $this->BusinessCountry;

   # current user
   $user = Auth::guard('admin')->user();

   #update deleted by
   //$query->where('id', $id)->update(['deleted_by' => $user->id]);

   # delete document by id
   $query->where('id', $id)->delete();
   
   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $this->deleteMessage($this->type)];
 }

 /**
  * active deactive
  * @param $id
  * @return \Illuminate\Http\Response
  */ 
 public function status($id)
 {
   # initiate constructor
   $query =  $this->BusinessCountry;

   # get the status 
   $status =  $query->where('id', $id)->first()->status;

   # check status, if active
   if ($status == '1') {
     #message
     $message = $this->inActiveMessage($this->type);

     # deactive( update status to zero)
     $statusCode = '0';
   } else {
     #message
     $message = $this->activeMessage($this->type);

     # active( update status to one)
     $statusCode = '1';
   }
   
   # update status code
   $query->where('id', $id)->update(['status' => $statusCode]);

   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $message];
 }
=======
use App\Http\Controllers\Controller;

class BusinessCountryController extends Controller
{
    //
>>>>>>> a9884a839d6a668380f431b94b14667fa001ebc0
}
