<?php

namespace App\Http\Controllers\AdminController;


use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;
use App\Models\Service;
use App\Models\DisputeReason;
use App\Models\OrderDisputeSolution;
use App\Models\DisputeReasonType;
use App\Models\Dispute;
use App\Models\Provider;
use App\Models\User;
use App\Models\Order;
use App\Models\DisputeStatus;
use App\Models\OrderDispute;
use App\Models\UserNotification;
use App\Http\Controllers\Api\User\NotifyUser;

class DisputeRequestController extends Controller
{
 use MessageStatusTrait;

 # Bind Type
 protected $type = 'Order Dispute ';

 # Bind location
 protected $view = 'application.dispute_request.';

 # Bind DisputeReasonType
 protected $Dispute;

 # Bind OrderDisputeSolution
 protected $orderDisputeSolution;

 # Bind OrderDispute
 protected $orderDispute;

 # Bind DiscountType
 protected $DisputeReasonType;

 # Variable to Bind Model userNotification
 protected $userNotification;

 /**
  * default constructor
  * @param
  * @return
  */
 function __construct(
                    Service $service,
                    OrderDispute $orderDispute,
                    OrderDisputeSolution $orderDisputeSolution,
                    DisputeReason $disputeReason,
                    DisputeReasonType $DisputeReasonType,
                    UserNotification $userNotification,
                    Dispute $Dispute  

                  )
          {
  $this->service = $service;
  $this->orderDispute = $orderDispute;
  $this->orderDisputeSolution = $orderDisputeSolution;
  $this->DisputeReason = $disputeReason;
  $this->DisputeReasonType = $DisputeReasonType;
  $this->userNotification  = $userNotification;
  $this->Dispute = $Dispute;

 }
         
 


 /**
  * create page of DisputeReason
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function create()
 {
  # DisputeReason Type
  $DisputeReasonType = $this->DisputeReasonType->get();
 //dd($DisputeReasonType);
  $services = $this->service->get();

  
  return view($this->view.'create')->with(['DisputeReasonType' => $DisputeReasonType,'services' => $services]);
 }


 /**
  * index page of DisputeReasonType
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function index(Request $request)
 {
  # fetch variant list
  $DisputeList = Dispute::with(['user','UserType','serviceOrders','Reason','DisputeStatus'])->get();

  $orderDisputes = $this->orderDispute
                        ->orderBy('id', 'desc')
                        ->get();

 	# return to index page
 	return view($this->view.'index')->with([
                                        'DisputeList'  => $DisputeList,
 		                                    'orderDisputes'=> $orderDisputes,
 		                                  ]);
 }

 /**
  * create DisputeReason
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */

 public function store(Request $request)
 {
 	
  # current user
 	$user = Auth::guard('admin')->user();


//dd($request->all());


  $query = $this->Dispute;

 $Data = [
            'disputable_id' => $request->user_or_provider_id ,
            'reason' => $request->disputable_id ,
            'disputable_type' => $request->disputable_type,
            'dispute_status_id'     => 1 ,
            'order_id' => $request->unique_order_id ?? 0,


          ];

   $postdata = $query->create($Data);



  $output = ['success' => 200, 'message' => 'Dispute Request created Successfully'];
  return $output;
 } 




/**
  * view details  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function view($id)
 {
 
                                         
 $DisputeReasonType = $this->DisputeReasonType->get();
 //dd($id);
  $services = $this->service->get();
  $DisputeList = Dispute::where('id',$id)->with(['user','UserType','serviceOrders','Reason','DisputeStatus'])->first();

  if($DisputeList->disputable_type==1){
     $user_or_provider = User::get();
    }else{
       $user_or_provider = Provider::get();
    }

    $user_or_provider_reasion = $this->DisputeReason->where('dispute_reason_type_id',$DisputeList->disputable_type)->get();

  //dd($user_or_provider_reasion);
//dd($DisputeList->UserType->name);
  
  return view($this->view.'view')->with(['DisputeReasonType' => $DisputeReasonType,'services' => $services,'DisputeList'=>$DisputeList,'user_or_provider'=>$user_or_provider,'user_or_provider_reasion'=>$user_or_provider_reasion]);
 }

 /**
  * edit  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function update($id)
 {
   $DisputeReasonType = $this->DisputeReasonType->get();
 //dd($id);
  $services = $this->service->get();
  $DisputeList = Dispute::where('id',$id)->with(['user','UserType','serviceOrders','Reason','DisputeStatus'])->first();

  if($DisputeList->disputable_type==1){
     $user_or_provider = User::get();
    }else{
       $user_or_provider = Provider::get();
    }

    $user_or_provider_reasion = $this->DisputeReason->where('dispute_reason_type_id',$DisputeList->disputable_type)->get();

  //dd($user_or_provider_reasion);
//dd($DisputeList->UserType->name);
  
  return view($this->view.'edit')->with(['DisputeReasonType' => $DisputeReasonType,'services' => $services,'DisputeList'=>$DisputeList,'user_or_provider'=>$user_or_provider,'user_or_provider_reasion'=>$user_or_provider_reasion]);
 }

 /**
  * edit DisputeReason
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function edit(Request $request)
 {
       


$query = $this->Dispute;

 $Data = [
            'disputable_id' => $request->user_or_provider_id ,
            'reason' => $request->disputable_id ,
            'disputable_type' => $request->disputable_type,
            'dispute_status_id'     => 1 ,
            'order_id' => $request->unique_order_id ?? 0,


          ];

   $postdata = $query->where('id',$request->id)->update($Data);





  $output = ['success' => 200, 'message' => 'Dispute reason request update Successfully'];
  return $output;
 } 

 /**
  * delete variant
  * @param $id
  * @return \Illuminate\Http\Response
  */
 public function delete($id)
 {
   $query = $this->Dispute;
//dd($id);
   # current user
   $user = Auth::guard('admin')->user();

   #update deleted by
   //$query->where('id', $id)->update(['deleted_by' => $user->id]);

   # delete variant by id
   $query->where('id', $id)->delete();
   
   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $this->deleteMessage($this->type)];
 }
     
 /**
  * active deactive
  * @param $id
  * @return \Illuminate\Http\Response   
  */ 
 public function status($id)
 {
   # initiate constructor
   $query =  $this->orderDispute;

   $orderDisputeData =  $query->where('id', $id)->first();

   # get the status 
   $status = $orderDisputeData->dispute_status_id;
   
   # check status, if active
   if ($status == '1') {
     #message
     $finalMessage = 'Order Dispute Closed.';

     # deactive( update status to zero)
     $statusCode = '2';

      # Fetch User
      $userId = $orderDisputeData->serviceOrders->user_id;

      $user = User::where('id', $userId)->first();

      # Notify user for Order Successfully
      $notifyUser = new NotifyUser();

      # Set Title message
      $title = 'Mesee Order Dispute Notification';
      
      #fetch order id
      $orderId = $orderDisputeData->serviceOrders->unique_order_id;

      # Set Message
      $message = 'Your Order Dispute is closed for OrderId:'.$orderId;

      # Notify User
      $notifyUser->notify($user, $title, $message);

      # Save Send Notification 
      $data = [
          'user_id'       => $userId,
          'order_id'      => $orderId,
          'notification'  => $message,
      ];

      # Creta UserNotification
      $this->userNotification->create($data);

   } else {
     #message
     $finalMessage = 'Order Dispute Open.';

     # active( update status to one)
     $statusCode = '1';
   }
   
   # update status code
   $query->where('id', $id)->update(['dispute_status_id' => $statusCode]);

   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $finalMessage];
 }




/**
  * json responce of users or provider by type
  * @param
  * @return \Illuminate\Http\Response in json
  */
  public function user_or_provider(Request $request)
  {
    

    if($request->disputable_type==1){

$user_or_provider = User::get();

                 

    }else{
       $user_or_provider = Provider::get();

                  

    }
    




    # return json responce
    return response()->json($user_or_provider);
  } 




/**
  * json responce of users or provider by type
  * @param
  * @return \Illuminate\Http\Response in json
  */
  public function user_or_provider_reasion(Request $request)
  {
    

     $user_or_provider_reasion = $this->DisputeReason->where('dispute_reason_type_id',$request->disputable_type)->get();

    # return json responce
    return response()->json($user_or_provider_reasion);
  } 




 /**
  * edit  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function orders_by_ajax(Request $request)
 {
   

if($request->disputable_type==1){

$order = Order::where('user_id',$request->id)->get();              

    }else{
      $order = Order::where('provider_id',$request->id)->get();

                  

    }

  
  return view($this->view.'order_table')->with(['order'=>$order]);
 }

 /**
  * Solution Comment
  * @param
  * @return
  */
 public function solution($id)
 {
   # return solution comment page
   return view($this->view.'comment_solution')->with(['id' => $id]);
 }

 /**
  * post Solution Comment
  * @param
  * @return
  */
 public function postSolution(Request $request, $id)
 {
   # requested data
   $arrayData = [
                 'order_dispute_id' => $id,
                 'comment'          => $request->comment ?? null,
   ];

   #create
   $commentSolution = $this->orderDisputeSolution->create($arrayData);

   if ($commentSolution) {
     # return success
     $output = ['success' => 200, 'message' => 'Solution Commented Successfully'];
   } else {
     # return error
     $output = ['error' => 100, 'message' => 'Something went wrong'];
   }
   return $output;
 } 

 /**
  * view Solution Comment
  * @param
  * @return
  */
 public function viewSolution(Request $request, $id)
 {
   # fetch comments
   $commentSolutions = $this->orderDisputeSolution
                            ->where('order_dispute_id', $id)
                            ->orderBy('id', 'desc')
                            ->get();

   return view($this->view.'view_solution_comment')->with(['commentSolutions' => $commentSolutions]);
 } 
}
