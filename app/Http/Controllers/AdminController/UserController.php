<?php

namespace App\Http\Controllers\AdminController;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;
use App\Models\ConsultVisitBookingDetails;
use App\Models\ConsultVisitBookingDetailVariant;
use App\Models\ConsultVisitTermsAndCondition;
use App\Models\ConsultVisitBookingDetailsImages;
use App\Models\OrderHistory;
use App\Models\OrderService;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Order;
use App\Models\Variant;
use App\Models\User;

use DB;

class UserController extends Controller
{
 use MessageStatusTrait;

 # Bind Type
 protected $type = 'User';

 # Bind location
 protected $view = 'application.user.';

 # Bind variant
 protected $variant;

 # Bind order
 protected $order;

 # Bind order
 protected $orderService;

 /**
  * default constructor
  * @param
  * @return
  */
 function __construct(Variant $variant,
                ConsultVisitBookingDetails $ConsultVisitBookingDetails, 
                ConsultVisitBookingDetailVariant $ConsultVisitBookingDetailVariant,
                ConsultVisitTermsAndCondition $ConsultVisitTermsAndCondition,
                ConsultVisitBookingDetailsImages $ConsultVisitBookingDetailsImages,
                 User $user,
                 Order $order,
                 OrderService $orderService

 )
 {
                
                $this->user = $user;
                $this->order = $order;
                $this->orderService = $orderService;
}
 


 /**
  * create page of user
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function create()
 {
  # fetch user list
  $user = $this->user->orderBy('id')
                     ->get();

  # return to create page
                    
  return view($this->view.'create')->with(['category' => $category]);;
 }


 /**
  * index page of User
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function index(Request $request)
 {
 
     $query = $this->user->orderBy('id');

if ($request->name != '') {
       # code...
       $query = $query->where('first_name', $request->name)
                ->orWhere('email', '=', $request->name);
     }

     if ($request->status != '') {
      //dd($request->status);
       # code...
       $query = $query->where('status', $request->status);
     }
     
     $users = $query->get();

 	return view($this->view.'index')->with([
                                        'users' => $users, 
                                        'name' => $request->name??'',
                                        'status' => $request->status??''
                                    ]);
                         }


/**
  * view user profile and order history details  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function view($id)
 {
  
                                         
  $userData =  $this->user->where('id',$id)->first();
  $orders =  $this->order->all();

  $relations = [
            'orders.history',
            'orders.orderServices.service',
        ];

        # fetch the User
        $user = $this->user
                     //->with($relations)
                     ->find($id);  


  return view($this->view.'history')->with(['user'=>$user,'orders'=>$orders,'userData'=>$userData]);
 }

 /**
  * ordered services
  * @param
  * @return
  */
 public function orderServices($id)
 {
   # get order
   $order    = $this->order
                    ->where('id', $id)
                    ->first();
   
   # get services
   $services = $this->orderService
                    ->where('order_id', $id)
                    ->get();
   # code...
   return view($this->view.'order_service')->with([
                                                   'count'    => '1',
                                                   'order'    => $order,
                                                   'services' => $services,
                                                 ]);
 }

 /**
  * service  page
  * @param
  * @return 
  */
 public function services($id)
 {
  #fetch order detail by id
  $service = $this->orderService
                  ->where('id', $id)
                  ->first();

  # return to view page
  return view($this->view.'view')->with([
                                        'service' => $service
                                       ]);
 }
 


 /**
  * Slide butoon on/off page of User
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */


 public function status(Request $request)
 {
   # initiate constructor

 // dd($request->id);
   $query =  $this->user;

   # get the status 
   $status =  $query->where('id', $request->id)->first()->status;

   # check status, if active
   if ($status == '1') {
     # message
     $message = $this->inActiveMessage($this->type);

     # deactive( update status to zero)
     $statusCode = '0';
   } else {
     # message
     $message = $this->activeMessage($this->type);

     # active( update status to one)
     $statusCode = '1';
   }
   
   # update status code
   $query->where('id', $request->id)->update(['status' => $statusCode]);

   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $message];
 }

 /**
  * delete user
  * @param $id
  * @return \Illuminate\Http\Response
  */
 public function delete($id)
 {
   $query = $this->user;

   # current user
   $user = Auth::guard('admin')->user();

   #update deleted by
  // $query->where('id', $id)->update(['deleted_by' => $user->id]);

   # delete user by id
   $query->where('id', $id)->delete();
   
   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $this->deleteMessage($this->type)];
 }

/******** End Class ******************/
}