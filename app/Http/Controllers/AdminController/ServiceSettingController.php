<?php

namespace App\Http\Controllers\AdminController;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;
use App\Models\ServiceSetting;
use App\Models\ServiceSettingCancellationData;
use App\Models\ServiceSettingRescheduleData;
use App\Models\ServiceSettingPenaltyData;
use App\Models\ServiceRequestType;


class ServiceSettingController extends Controller
{
 use MessageStatusTrait;

 # Bind Type
 protected $type = 'Service Setting';

 # Bind location
 protected $view = 'application.service_setting.';

 # Bind service
 protected $service;

 /**
  * default constructor
  * @param
  * @return
  */
 function __construct(
 	            ServiceSetting                     $ServiceSetting,
              ServiceSettingCancellationData    $ServiceSettingCancellationData,
 	            ServiceSettingRescheduleData      $ServiceSettingRescheduleData,
 	            ServiceSettingPenaltyData         $ServiceSettingPenaltyData,
              ServiceRequestType               $ServiceRequestType   

 )
 {
                $this->ServiceSetting                   = $ServiceSetting;
                $this->ServiceSettingCancellationData   = $ServiceSettingCancellationData;
                $this->ServiceSettingRescheduleData     = $ServiceSettingRescheduleData;
                $this->ServiceSettingPenaltyData        = $ServiceSettingPenaltyData;
                $this->ServiceRequestType                    = $ServiceRequestType;
                

}
 




 
 /**
  * edit  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function create_or_update($id='')
 {
  # Fetch variant by id
  $ServiceRequestType =  ServiceRequestType::all();
 
  $ServiceSetting =  $this->ServiceSetting->first();

  if($ServiceSetting){
   $ServiceSettingCancellationData = $this->ServiceSettingCancellationData->where('service_setting_id',$ServiceSetting->id)->get();
  
  $ServiceSettingRescheduleData =  $this->ServiceSettingRescheduleData->where('service_setting_id',$ServiceSetting->id)->get();
  $ServiceSettingPenaltyData =  $this->ServiceSettingPenaltyData->where('service_setting_id',$ServiceSetting->id)->get();
}

  
 //dd($ServiceSettingCancellationData);
 	return view($this->view.'create_update')->with([
    'ServiceRequestType' => $ServiceRequestType,
    'ServiceSetting' => $ServiceSetting ??'',
    'ServiceSettingCancellationData' => $ServiceSettingCancellationData??'',
    'ServiceSettingRescheduleData' => $ServiceSettingRescheduleData??'',
    'ServiceSettingPenaltyData' => $ServiceSettingPenaltyData??''
  ]);
 }

 
/**
  * edit variant
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function edit(Request $request)
 {
       
    //dd($request->all());



       $user = Auth::guard('admin')->user();
$ServiceSetting =  $this->ServiceSetting->first();
$query = $this->ServiceSetting;

if($ServiceSetting){
//dd($ServiceSetting->id);
  $Setting_id= $ServiceSetting->id;

$ServiceSetting = [
            'service_request_type_id' => $request->service_request_type_id,
            'day_or_hours'     => $request->day_or_hours,
            'day_count' => $request->day_count??'',
            'hour_count'     => $request->hour_count??'',
            'proximate_range'     => $request->proximate_range??'',
            'payment_limit_provider' => $request->payment_limit_provider??'',

          ];
$query->where('id',$Setting_id)->update($ServiceSetting);
$servicesetting_id = $Setting_id;

}else{

$ServiceSetting = [
            'service_request_type_id' => $request->service_request_type_id,
            'day_or_hours'     => $request->day_or_hours,
            'day_count' => $request->day_count??'',
            'hour_count'     => $request->hour_count??'',
            'proximate_range'     => $request->proximate_range??'',
            'payment_limit_provider' => $request->payment_limit_provider??'',

          ];



$ServiceSetting=$query->create($ServiceSetting);

$servicesetting_id = $ServiceSetting->id;
}



 







  
//dd($servicesetting_id);


$reschedule = $request->reschedule;
if(!empty($reschedule)){
foreach ($reschedule as $key => $value) {
    if(isset($value['reschedule_id'])){

$reschedule_id =$value['reschedule_id'];
        $rescheduleData = [
           'hours' =>  $value['hour'] ?? '',
            'amount' =>  $value['amount'] ?? '',
            'description' =>  $value['description'] ?? '',
            //'added_by'        => $user->id ?? null
          ];

  $this->ServiceSettingRescheduleData->where('id', $reschedule_id)->update($rescheduleData);



    }else{


            $rescheduleData = [
            'service_setting_id' =>  $servicesetting_id?? '',
            'hours' =>  $value['hour'] ?? '',
            'amount' =>  $value['amount'] ?? '',
            'description' =>  $value['description'] ?? '',
            //'added_by'        => $user->id ?? null
            ];

  $this->ServiceSettingRescheduleData->create($rescheduleData);


    }
 
}

}
/*----------------------------------------------------------------------*/


foreach ($request->cancellation as $key => $value) {
    if(isset($value['cancellation_id'])){

        $cancellation_id =$value['cancellation_id'];
        $cancellationData = [
            'hours' =>  $value['hour'] ?? '',
            'amount' =>  $value['amount'] ?? '',
            'description' =>  $value['description'] ?? '',
             //'added_by'        => $user->id ?? null
          ];
        
       $this->ServiceSettingCancellationData->where('id', $cancellation_id)->update($cancellationData);

    

    }else{
     

            $cancellationData = [
           'service_setting_id' =>  $servicesetting_id?? '',
           'hours' =>  $value['hour'] ?? '',
            'amount' =>  $value['amount'] ?? '',
            'description' =>  $value['description'] ?? '',
            //'added_by'        => $user->id ?? null
          ];

        $this->ServiceSettingCancellationData->create($cancellationData);


    }
 
}


/*----------------------------------------------------------------------*/



foreach ($request->penalty as $key => $value) {
    if(isset($value['penalty_id'])){

$penalty_id =$value['penalty_id'];
        $penaltynData = [
           'hours' =>  $value['hour'] ?? '',
            'amount' =>  $value['amount'] ?? '',
            'description' =>  $value['description'] ?? '',
            //'added_by'        => $user->id ?? null
          ];

    $this->ServiceSettingPenaltyData->where('id', $penalty_id)->update($penaltynData);



    }else{


            $penaltynData = [
            'service_setting_id' =>  $servicesetting_id?? '',
           'hours' =>  $value['hour'] ?? '',
            'amount' =>  $value['amount'] ?? '',
            'description' =>  $value['description'] ?? '',
            //'added_by'        => $user->id ?? null
          ];
          
    $this->ServiceSettingPenaltyData->create($penaltynData);


    }
 
}

//dd('done');

  $output = ['success' => 200, 'message' => 'Service Setting update Successfully'];
  return $output;
 } 




}
