<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConsultVisitBookingDetailsImages extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='consult_visit_booking_details_images';
  
  # define fillable fildes
  protected $fillable = [
                    'id', 
                    'category_id', 
                    'sub_category_id', 
                    'consult_visit_booking_id', 
                    'reference_image_name',
                    'reference_image_path', 
                    'added_by', 'updated_by'                     
  ];
}
