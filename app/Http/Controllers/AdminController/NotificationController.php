<?php

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;
use App\Models\UserNotification;
use App\Models\ProviderNotification;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
 # bind model UserNotification
 protected $userNotification;

 # bind model ProviderNotification
 protected $providerNotification;

 /**
  * constructor
  * @param
  * @return
  */
 function __construct(UserNotification $userNotification, ProviderNotification $providerNotification)
 {
 	$this->userNotification      = $userNotification;
 	$this->providerNotification = $providerNotification;
 }
 
 /**
  * user notifications 
  * @param Request
  * @return Response
  */
 public function userNotification()
 {
 	# fetch user notifications
 	$userNotifications = $this->userNotification->orderBy('id', 'desc')->get();

 	return view('application.notification.user_notification')->with(['userNotifications' => $userNotifications]);
 }
 
 /**
  * provider notifications 
  * @param Request
  * @return Response
  */
 public function providerNotification()
 {
 	# fetch provider notifications
 	$providerNotifications = $this->providerNotification->orderBy('id', 'desc')->get();

 	return view('application.notification.provider_notification')->with(['providerNotifications' => $providerNotifications]);
 }
 
}
