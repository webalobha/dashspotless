<?php

namespace App\Http\Controllers\AdminController;

use App\Models\User;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\OrderHistory;
use App\Models\OrderService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
 # Bind location protected
 protected $view = 'application.order.';

 # Bind Order protected
 protected $order;

 # Bind user protected
 protected $user;

 # Bind OrderService protected
 protected $orderService;

 # Bind OrderHistory protected
 protected $orderHistory;

 # Bind OrderStatus protected
 protected $orderStatus;

 /**
  * default constructor
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 function __construct(Order $order, OrderService $orderService, OrderStatus $orderStatus, user $user, OrderHistory $orderHistory)
 {
  $this->user         = $user;
  $this->order        = $order;
  $this->orderStatus  = $orderStatus;
 	$this->orderHistory = $orderHistory;
  $this->orderService = $orderService;
 }

 /**
  * order index 
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function index(Request $request)
 {
  # get email
  $orderData = $this->order
                    ->all();
  
  $orderHistories = $this->orderHistory
                         ->where('order_status_id', $request->status)
                         //->orderBy('created_at', 'desc')
                         ->get(); 
  // dd($orderHistories);
  # fetch user by id
  $users = $this->user
                ->where('email', $request->email)
                ->get(); 
  
  # fetch orders by user
  $userId[] = '';
  foreach ($users as $user) {
    # code...
    $userId[] = $user->id ?? '';
  } 

  # fetch status of order
  $orderId[] = '';
  foreach ($orderHistories as $orderHistory) {
    # code...
    $orderId[] = $orderHistory->order_id ?? '';
  }
 
  # initiate
  $query = $this->order
                ->orderBy('id', 'desc');
 
  # if get email for search
  if ($request->email != '') {
    # return query
    $query = $query->whereIn('user_id', $userId);
  }

  if ($request->status != '') {
    # return query
    $query = $query->whereIn('id', $orderId);     
  }

 	# fetch orders
 	$orders = $query->get();

  # fetch order status
  $orderStatuses = $this->orderStatus
                        ->all();

 	# return to index page
 	return view($this->view.'index')->with([
 		                                    'count'         => '1',
                                        'orders'        => $orders,
                                        'orderStatuses' => $orderStatuses,
                                        'email'         => $request->email ?? '',
 		                                    'status'        => $request->status ?? '',
 		                                   ]);
 }

 /**
  * ordered services
  * @param
  * @return
  */
 public function orderServices($id)
 {
   # get order
   $order    = $this->order
                    ->where('id', $id)
                    ->first();
   
   # get services
   $services = $this->orderService
                    ->where('order_id', $id)
                    ->get();
   # code...
   return view($this->view.'order_service')->with([
                                                   'count'    => '1',
                                                   'order'    => $order,
                                                   'services' => $services,
                                                 ]);
 }
 
 
 /**
  * view page
  * @param
  * @return
  */
 public function view($id)
 {
 	#fetch order detail by id
  $service = $this->orderService
                  ->where('id', $id)
                  ->first();

 	# return to view page
 	return view($this->view.'view')->with([
 		                                    'service' => $service
 		                                   ]);
 } 
}
