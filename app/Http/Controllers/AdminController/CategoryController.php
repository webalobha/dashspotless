<?php

namespace App\Http\Controllers\AdminController;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;

class CategoryController extends Controller
{
 use MessageStatusTrait;

 # Bind Type
 protected $type = 'Category ';

 # Bind location
 protected $view = 'application.category.';

 # Bind Category
 protected $category;

 /**
  * default constructor
  * @param
  * @return
  */
 function __construct(Category $category)
 {
 	$this->category = $category;
 }
 
 /**
  * index page of category
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function index()
 {
 	# fetch category list
 	$categories = $this->category
 	                   ->orderBy('name')
 	                   ->get();

 	# return to index page
 	return view($this->view.'index')->with(['categories' => $categories]);;
 }

 /**
  * create category
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function store(Request $request)
 {
 	# current user
 	$user = Auth::guard('admin')->user();

  $query = $this->category;

 	# upload avtar
    if($request->hasfile('avtar_name')) {
     $file = $request->file('avtar_name');
     $name = $file->getClientOriginalName(); // getting image name
     $date = date('y-m-d');              
     $randNumber = rand(0000,9999);              
     $newFile =$date.'_'.$randNumber.'_'.$name;
     $filename = str_replace(' ', '',$newFile); 
     $file->move('dist/img/category/', $filename);
     $avtarPath ='dist/img/category/'.$filename;
    } else {
     $avtarPath  = null;  
     $name = null;
    }

 	# request param
 	$arrayData = [
                  'name'       => $request->name ?? null,
                  'avtar_name' => $name ?? null,
                  'avtar_path' => $avtarPath,
                  'added_by'   => $user->id ?? null
 	];

  # check the requested category already exist or not
  $countCategory = $query->where('name', $request->name)->count();
  
  if ($countCategory == 0) {
    #store 
    $createCategory = $query->create($arrayData);

    # check created or not
    # if created
    if ($createCategory) {
     # return successs
     $output = ['success' => 200, 'message' => 'Category Added Successfully'];
    } else {
     # return error
     $output = ['error' => 100, 'message' => 'Something went wrong'];
    }
  } else {
    # return exist message
    $output = ['error' => 100, 'message' => 'Already Exist'];
  }
  return $output;
 } 

 /**
  * edit  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function update($id)
 {
  # Fetch category by id
  $category = $this->category
                   ->where('id', $id)
                   ->first(); 
 	# code...
 	return view($this->view.'edit')->with(['category' => $category]);
 }

 /**
  * edit category
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function edit(Request $request, $id)
 {
  # current user
  $user = Auth::guard('admin')->user();

  $query = $this->category;

  # upload avtar
    if($request->hasfile('avtar_name')) {
     $file = $request->file('avtar_name');
     $name = $file->getClientOriginalName(); // getting image name
     $date = date('y-m-d');              
     $randNumber = rand(0000,9999);              
     $newFile =$date.'_'.$randNumber.'_'.$name;
     $filename = str_replace(' ', '',$newFile); 
     $file->move('dist/img/category/', $filename);
     $avtarPath ='dist/img/category/'.$filename;
    } else {
     $avtarPath = $query->where('id', $id)->first()->avtar_path; 
    }

  # request param
  $arrayData = [
                  'name'       => $request->name ?? null,
                  'avtar_name' => $request->avtar_name ?? null,
                  'avtar_path' => $avtarPath,
                  'updated_by' => $user->id ?? null
  ];

  # check the requested category already exist or not
  $countCategory = $query->where('name', $request->name)
                         ->where('id', '!=', $id)
                         ->count();
  
  if ($countCategory == 0) {
    #store 
    $updateCategory = $query->where('id', $id)->update($arrayData);

    # check created or not
    # if created
    if ($updateCategory) {
     # return successs
     $output = ['success' => 200, 'message' => 'Category update Successfully'];
    } else {
     # return error
     $output = ['error' => 100, 'message' => 'Something went wrong'];
    }
  } else {
    # return exist message
    $output = ['error' => 100, 'message' => 'Already Exist'];
  }
  return $output;
 }  

 /**
  * delete category
  * @param $id
  * @return \Illuminate\Http\Response
  */
 public function delete($id)
 {
   $query = $this->category;

   # current user
   $user = Auth::guard('admin')->user();

   #update deleted by
   $query->where('id', $id)->update(['deleted_by' => $user->id]);

   # delete category by id
   $query->where('id', $id)->delete();
   
   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $this->deleteMessage($this->type)];
 }

 /**
  * active deactive
  * @param $id
  * @return \Illuminate\Http\Response
  */ 
 public function status($id)
 {
   # initiate constructor
   $query =  $this->category;

   # get the status 
   $status =  $query->where('id', $id)->first()->status;

   # check status, if active
   if ($status == '1') {
     #message
     $message = $this->inActiveMessage($this->type);

     # deactive( update status to zero)
     $statusCode = '0';
   } else {
     #message
     $message = $this->activeMessage($this->type);

     # active( update status to one)
     $statusCode = '1';
   }
   
   # update status code
   $query->where('id', $id)->update(['status' => $statusCode]);

   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $message];
 }
}
