<?php

namespace App\Http\Controllers\AdminController;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;
use App\Models\Service;
use App\Models\DisputeReason;
use App\Models\DisputeReasonType;
use DB;

class DisputeReasonController extends Controller
{
 use MessageStatusTrait;

 # Bind Type
 protected $type = 'Dispute Reason';

 # Bind location
 protected $view = 'application.dispute_reason.';

 # Bind DisputeReasonType
 protected $DisputeReason;

 # Bind DiscountType
 protected $DisputeReasonType;

 /**
  * default constructor
  * @param
  * @return
  */
 function __construct(
                    Service $service,
                    DisputeReason $disputeReason,
                    DisputeReasonType $DisputeReasonType    

                  )
          {
  $this->service = $service;
  $this->DisputeReason = $disputeReason;
  $this->DisputeReasonType = $DisputeReasonType;

 }
         
 


 /**
  * create page of DisputeReason
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function create()
 {
  # DisputeReason Type
  $DisputeReasonType = $this->DisputeReasonType->get();
  $services = $this->service->get();

  
  return view($this->view.'create')->with(['DisputeReasonType' => $DisputeReasonType,'services' => $services]);
 }


 /**
  * index page of DisputeReasonType
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function index(Request $request)
 {
  # fetch variant list
  $query = $this->DisputeReason->orderBy('id');

    /* if ($request->category != '') {
       # code...
       $query = $query->where('category_id', $request->category);
     }

     if ($request->sub_category != '') {
       # code...
       $query = $query->where('sub_category_id', $request->sub_category);
     }
     if ($request->variant != '') {
       # code...
       $query = $query->where('variant_id', $request->variant);
     }
     if ($request->service != '') {
       # code...
       $query = $query->where('id', $request->service);
     }*/

  $services = $query->get();

  # fetch DisputeReason name
  $DisputeReasons = $this->DisputeReason->with('DisputeReasonType','serviceName')->orderBy('id')->get(); 

//dd($DisputeReasons);
 	# return to index page
 	return view($this->view.'index')->with([
 		                                    'DisputeReasons'=> $DisputeReasons,
 		                                  ]);
 }

 /**
  * create DisputeReason
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */

 public function store(Request $request)
 {
 	
  # current user
 	$user = Auth::guard('admin')->user();


  $query = $this->DisputeReason;

 $Data = [
            'dispute_reason_type_id' => $request->dispute_reason_type_id,
            'service' => $request->service,
            'title'     => $request->title ,
            'status' => $request->status ?? 0,

          ];
   $duplicateDispute = $query->where('title', $request->title)
                            ->where('service', $request->service)
                            //->where('dispute_reason_type_id', $request->dispute_reason_type_id)
                            ->count();
   
   if($duplicateDispute == 0){
    $postdata = $query->create($Data);
    $output = ['success' => 200, 'message' => 'Dispute reason created Successfully'];
   } else {
    $output = ['error' => 100, 'message' => 'Dispute reason Already exist'];
   }
  return $output;
 } 




/**
  * view details  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function view($id)
 {
 
                                         
  $DisputeReasonType = $this->DisputeReasonType->get();
  $services = $this->service->get();

  $DisputeReason =  $this->DisputeReason->where('id',$id)->first();

 
  return view($this->view.'view')->with(['DisputeReason'=>$DisputeReason,'DisputeReasonType'=>$DisputeReasonType,'services'=>$services]);
 }

 /**
  * edit  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function update($id)
 {
   $DisputeReasonType = $this->DisputeReasonType->get();
  $services = $this->service->get();

  $DisputeReason =  $this->DisputeReason->where('id',$id)->first();

 
 	return view($this->view.'edit')->with(['DisputeReason'=>$DisputeReason,'DisputeReasonType'=>$DisputeReasonType,'services'=>$services]);
 }

 /**
  * edit DisputeReason
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function edit(Request $request)
 {
       


$query = $this->DisputeReason;

$Data = [
            'dispute_reason_type_id' => $request->dispute_reason_type_id ,
            'service' => $request->service,
            'title'     => $request->title ,
            'status' => $request->status ?? 0,

          ];
$query->where('id',$request->id)->update($Data);
  





  $output = ['success' => 200, 'message' => 'Dispute reason update Successfully'];
  return $output;
 } 

 /**
  * delete variant
  * @param $id
  * @return \Illuminate\Http\Response
  */
 public function delete($id)
 {
   $query = $this->DisputeReason;

   # current user
   $user = Auth::guard('admin')->user();

   #update deleted by
   //$query->where('id', $id)->update(['deleted_by' => $user->id]);

   # delete variant by id
   $query->where('id', $id)->delete();
   
   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $this->deleteMessage($this->type)];
 }
     
 /**
  * active deactive
  * @param $id
  * @return \Illuminate\Http\Response   
  */ 
 public function status($id)
 {
   # initiate constructor
   $query =  $this->DisputeReason;

   # get the status 
   $status =  $query->where('id', $id)->first()->status;

   # check status, if active
   if ($status == '1') {
     #message
     $message = $this->inActiveMessage($this->type);

     # deactive( update status to zero)
     $statusCode = '0';
   } else {
     #message
     $message = $this->activeMessage($this->type);

     # active( update status to one)
     $statusCode = '1';
   }
   
   # update status code
   $query->where('id', $id)->update(['status' => $statusCode]);

   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $message];
 }


}
