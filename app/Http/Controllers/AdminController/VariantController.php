<?php

namespace App\Http\Controllers\AdminController;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;
use App\Models\Variant;
use App\Models\ConsultVisitBookingDetails;
use App\Models\ConsultVisitBookingDetailVariant;
use App\Models\ConsultVisitTermsAndCondition;
use App\Models\ConsultVisitBookingDetailsImages;
use App\Models\Category;
use App\Models\SubCategory;
use DB;

class VariantController extends Controller
{
 use MessageStatusTrait;

 # Bind Type
 protected $type = 'Variant';

 # Bind location
 protected $view = 'application.variant.';

 # Bind variant
 protected $variant;

 /**
  * default constructor
  * @param
  * @return
  */
 function __construct(Variant $variant,
                ConsultVisitBookingDetails $ConsultVisitBookingDetails, 
                ConsultVisitBookingDetailVariant $ConsultVisitBookingDetailVariant,
                ConsultVisitTermsAndCondition $ConsultVisitTermsAndCondition,
                ConsultVisitBookingDetailsImages $ConsultVisitBookingDetailsImages

 )
 {
                $this->ConsultVisitBookingDetails = $ConsultVisitBookingDetails;
                $this->ConsultVisitBookingDetailVariant = $ConsultVisitBookingDetailVariant;
                $this->ConsultVisitTermsAndCondition = $ConsultVisitTermsAndCondition;
                $this->ConsultVisitBookingDetailsImages = $ConsultVisitBookingDetailsImages;
                $this->variant = $variant;
}
 


 /**
  * create page of variant
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function create()
 {
  # fetch variant list
  $category = Category::orderBy('name')
                     ->get();

  # return to create page
                    
  return view($this->view.'create')->with(['category' => $category]);;
 }


 /**
  * index page of variant
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function index()
 {
 	# fetch variant list

   $variants = DB::table('sub_category_variants')
            ->join('categories', 'categories.id', '=', 'sub_category_variants.category_id')
            ->join('sub_categories', 'sub_categories.id', '=', 'sub_category_variants.sub_category_id')
            ->select('sub_category_variants.*', 'categories.name as cat_name', 'sub_categories.name as subcat_name')
          //  ->groupBy('sub_category_variants.title_or_subject')
            ->get();


 	/*$variants = $this->variant
 	                   ->orderBy('title_or_subject')
 	                   ->get();*/


 	# return to index page
 	return view($this->view.'index')->with(['variants' => $variants]);;
 }

 /**
  * create variant
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function store(Request $request)
 {
 	

   // dd($request->all());

  # current user

$newconsult_visit_avail = array();
  $newlabel= array();

 	$user = Auth::guard('admin')->user();

  $query = $this->variant;

if (!empty($request->label)) {

  $CountvariantName =  count($request->label);
  $variantName =  $request->label;

  $consult_visit_avail =  $request->consult_visit_avail;

 $Counttitle = count($request->title);

if( !empty($request->label)){
  foreach ( $request->label as $key => $value) {
  $label['label']  = $value['label'];
  $newlabel[]=$label;
  }

}
if( !empty($request->consult_visit_avail)){
   foreach ($request->consult_visit_avail as $key => $value) {
  $consult_visit_avail['consult_visit_avail']  = $value['consult_visit_avail'];
  $newconsult_visit_avail[]=$consult_visit_avail;
  }
}




     for($i=0; $i<$CountvariantName; $i++) {

          /*if($consult_visit_avail[$i]['consult_visit_avail']=='on'){

               $consult_visit_avail =1;

          }else{
            $consult_visit_avail =0;
          }*/

          $banner = [
            'label' =>  $newlabel[$i]['label']??'',
            'consult_visit_avail' => $newconsult_visit_avail[$i]['consult_visit_avail'] ?? 0,
            'category_id' => $request->category_id, 
            'sub_category_id'     => $request->sub_category_id,
            'title_or_subject'     => $request->title_or_subject,
            'radio_or_check'     => $request->radio_or_check,
            'sub_category_id'     => $request->sub_category_id,
            'added_by'        => $user->id ?? null
          ];

          # create Variant.
    $variantInsert[] =    $this->variant->create($banner);


  }    
}



$BookingDetails = [
            'category_id' => $request->category_id,
            'sub_category_id'     => $request->sub_category_id,
            'subject_line' => $request->subject_line,
            'amount'     => $request->amount,
            'description'     => $request->description,
          ];

          # create Variant.
    $BookingDetailsInsert =    $this->ConsultVisitBookingDetails->create($BookingDetails);


foreach ($variantInsert as $key => $value) {
             $InsertData = [
              'variant_id' =>  $value->id,
              'consult_visit_id' => $BookingDetailsInsert->id,
              'variant_id' => $value->id,
              'added_by'        => $user->id ?? null
              ];

$consult_visit_booking_detail_variant =    $this->ConsultVisitBookingDetailVariant->create($InsertData);

}







if (!empty($request->title)) {

  $Counttitle = count($request->title);
  foreach ($request->title as $key => $value) {
  $title['title']  = $value['title'];
  $newtitle[]=$title;
  }

  foreach ($request->description as $key => $value) {
  $description['description']  = $value['description'];
  $newdescription[]=$description;
  }


     for($j=0; $j<$Counttitle; $j++) {

          $terms = [
            'title' =>  $newtitle[$j]['title'] ?? '',
            'description' =>  $newdescription[$j]['description'] ?? '',
           'consultant_visit_id' => $BookingDetailsInsert->id,
            'category_id' => $request->category_id,
            'sub_category_id'     => $request->sub_category_id,
            'added_by'        => $user->id ?? null
          ];
          
$variantInsert =    $this->ConsultVisitTermsAndCondition->create($terms);


  }    
}

//dd($request->reference_image_name);
if (!empty($request->reference_image_name)) {

        # Store the blog Multiple content.
        foreach ($request->reference_image_name as $bannerImage) {

          # code...
          $file1 = $bannerImage['reference_image_name'];
          $name1 = $file1->getClientOriginalName(); // getting image name
          $date1 = date('y-m-d');              
          $randNumber1 = rand(0000,9999);              
          $newFile1 =$date1.'_'.$randNumber1.'_'.$name1;
          $filename1 = str_replace(' ', '',$newFile1);
          $file1->move('dist/img/variant/', $filename1);
          $bannerPath ='dist/img/variant/'.$filename1;

          # Gather the blog content.
          $banner = [
            'consult_visit_booking_id' => $BookingDetailsInsert->id,
            'category_id' => $request->category_id,
            'sub_category_id'     => $request->sub_category_id,
            'reference_image_name'     => $name1,
            'reference_image_path'     => $bannerPath,
            'added_by'        => $user->id ?? null
          ];

          # create blog content.
          $this->ConsultVisitBookingDetailsImages->create($banner);
        } 
       }


  $output = ['success' => 200, 'message' => 'variant Added Successfully'];
  return $output;
 } 




/**
  * view details  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function view($id)
 {
  # Fetch variant by id
  $category = Category::orderBy('name')->get();

  $SubCategory = SubCategory::orderBy('name')->get();
                     
  $variants = DB::table('sub_category_variants')
            ->where('sub_category_variants.id',$id)
            ->join('categories', 'categories.id', '=', 'sub_category_variants.category_id')
            ->join('sub_categories', 'sub_categories.id', '=', 'sub_category_variants.sub_category_id')
            ->select('sub_category_variants.*', 'categories.name as cat_name', 'sub_categories.name as subcat_name')
            ->groupBy('sub_category_variants.title_or_subject')
            ->first();
  # code...
  return view($this->view.'view')->with(['variants' => $variants,'category'=>$category,'SubCategory'=>$SubCategory]);
 }









 /**
  * edit  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function update($id)
 {
  # Fetch variant by id
  $category = Category::orderBy('name')->get();

  $SubCategory = SubCategory::orderBy('name')->get();
                     
  $variants = DB::table('sub_category_variants')
            ->where('sub_category_variants.id',$id)
            ->join('categories', 'categories.id', '=', 'sub_category_variants.category_id')
            ->join('sub_categories', 'sub_categories.id', '=', 'sub_category_variants.sub_category_id')
            ->select('sub_category_variants.*', 'categories.name as cat_name', 'sub_categories.name as subcat_name')
            ->groupBy('sub_category_variants.title_or_subject')
            ->first();
            //dd($variants);
  $variantNames =  $this->variant->where('title_or_subject', $variants->title_or_subject)->where('category_id', $variants->category_id)->where('sub_category_id', $variants->sub_category_id)->get();
 	# code...
 	return view($this->view.'edit')->with(['variants' => $variants,'category'=>$category,'SubCategory'=>$SubCategory,'variantNames'=>$variantNames]);
 }

 /**
  * edit variant
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function edit(Request $request)
 {
       

//dd($request->all());



       $user = Auth::guard('admin')->user();




$label = $request->label;
foreach ($label as $key => $value) {
    if(isset($value['variant'])){

$variant_id =$value['variant'];

        $variantdata = [
           'label' =>  $value['label']??'',
            'consult_visit_avail' =>$value['consult_visit_avail'] ?? 0,
            'category_id' => $request->category_id, 
            'sub_category_id'     => $request->sub_category_id,
            'title_or_subject'     => $request->title_or_subject,
            'radio_or_check'     => $request->radio_or_check,
            'sub_category_id'     => $request->sub_category_id,
            'added_by'        => $user->id ?? null
          ];
// dd($variantdata);
 $this->variant->where('id', $variant_id)->update($variantdata);



    }else{


           $variantdata = [
           'label' =>  $value['label']??'',
            'consult_visit_avail' =>$value['consult_visit_avail'] ?? 0,
            'category_id' => $request->category_id, 
            'sub_category_id'     => $request->sub_category_id,
            'title_or_subject'     => $request->title_or_subject,
            'radio_or_check'     => $request->radio_or_check,
            'sub_category_id'     => $request->sub_category_id,
            'added_by'        => $user->id ?? null
          ];


            $this->variant->create($variantdata);


    }
 
}














          $BookingDetails = [
            'category_id' => $request->category_id,
            'sub_category_id'     => $request->sub_category_id,
            'subject_line' => $request->subject_line,
            'amount'     => $request->amount,
            'description' => $request->description,
          ];
//dd($BookingDetails);
   $this->ConsultVisitBookingDetails->where('id', $request->consult_visit_id)->update($BookingDetails);
  
   

$title = $request->title;
foreach ($title as $key => $value) {
    if(isset($value['consultant_term_id'])){

$consultant_term_id =$value['consultant_term_id'];
        $terms = [
            'title' =>  $value['title'] ?? '',
            'description' =>  $value['description'] ?? '',
            'category_id' => $request->category_id,
            'sub_category_id'     => $request->sub_category_id,
            'added_by'        => $user->id ?? null
          ];

 $this->ConsultVisitTermsAndCondition->where('id', $consultant_term_id)->update($terms);



    }else{


            $terms = [

            'consultant_visit_id' => $request->consult_visit_id,
            'title' =>  $value['title'] ?? '',
            'description' =>  $value['description'] ?? '',
            'category_id' => $request->category_id,
            'sub_category_id'     => $request->sub_category_id,
            'added_by'        => $user->id ?? null
            ];

            $this->ConsultVisitTermsAndCondition->create($terms);


    }
 
}

    
$reference_image= $request->reference_image_name;

//dd($reference_image);
if ($reference_image != '') {
  # code...

foreach ($reference_image as $key => $value) {




    if(isset($value['refimage_id']) && isset($value['reference_image_name'])){

$refimage_id =$value['refimage_id'];
        # code...
          $file1 = $value['reference_image_name'];
          $name1 = $file1->getClientOriginalName(); // getting image name
          $date1 = date('y-m-d');              
          $randNumber1 = rand(0000,9999);              
          $newFile1 =$date1.'_'.$randNumber1.'_'.$name1;
          $filename1 = str_replace(' ', '',$newFile1);
          $file1->move('dist/img/variant/', $filename1);
          $bannerPath ='dist/img/variant/'.$filename1;

          # Gather the blog content.
          $banner = [
           
            'category_id' => $request->category_id,
            'sub_category_id'     => $request->sub_category_id,
            'reference_image_name'     => $name1,
            'reference_image_path'     => $bannerPath,
            'added_by'        => $user->id ?? null
          ];

          # create blog content.
          $this->ConsultVisitBookingDetailsImages->where('id', $refimage_id)->update($banner);



    }else{

 if(isset($value['reference_image_name'])){
            # code...
          $file1 = $value['reference_image_name'];
          $name1 = $file1->getClientOriginalName(); // getting image name
          $date1 = date('y-m-d');              
          $randNumber1 = rand(0000,9999);              
          $newFile1 =$date1.'_'.$randNumber1.'_'.$name1;
          $filename1 = str_replace(' ', '',$newFile1);
          $file1->move('dist/img/variant/', $filename1);
          $bannerPath ='dist/img/variant/'.$filename1;

          # Gather the blog content.
          $banner = [
            'consult_visit_booking_id' => $request->consult_visit_id,
            'category_id' => $request->category_id,
            'sub_category_id'     => $request->sub_category_id,
            'reference_image_name'     => $name1,
            'reference_image_path'     => $bannerPath,
            'added_by'        => $user->id ?? null
          ];

          # create blog content.
          $this->ConsultVisitBookingDetailsImages->create($banner);

         
}

    }
 
}

}




  $output = ['success' => 200, 'message' => 'variant updated Successfully'];
  return $output;
 } 

 /**
  * delete variant
  * @param $id
  * @return \Illuminate\Http\Response
  */
 public function delete($id)
 {
   $query = $this->variant;

   # current user
   $user = Auth::guard('admin')->user();

   #update deleted by
   $query->where('id', $id)->update(['deleted_by' => $user->id]);

   # delete variant by id
   $query->where('id', $id)->delete();
   
   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $this->deleteMessage($this->type)];
 }

 /**
  * active deactive
  * @param $id
  * @return \Illuminate\Http\Response
  */ 
 public function status($id)
 {
   # initiate constructor
   $query =  $this->variant;

   # get the status 
   $status =  $query->where('id', $id)->first()->status;

   # check status, if active
   if ($status == '1') {
     #message
     $message = $this->inActiveMessage($this->type);

     # deactive( update status to zero)
     $statusCode = '0';
   } else {
     #message
     $message = $this->activeMessage($this->type);

     # active( update status to one)
     $statusCode = '1';
   }
   
   # update status code
   $query->where('id', $id)->update(['status' => $statusCode]);

   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $message];
 }





 public function variant_by_subcategory(Request $request)
  {
    # fetch variant 
    $variant = $this->variant
                   ->where('sub_category_id',$request->sub_category_id)
                   ->where('radio_or_check','1')
                   ->where('consult_visit_avail','!=','1')
                   ->get();

    # return json responce

   if($variant->isNotEmpty()){

    return response()->json($variant);
}else{
  return response()->json(0);
}
}

 public function variant_by_subcategory_for_search(Request $request)
  {
    # fetch variant 
    $variant = $this->variant
                   ->where('sub_category_id',$request->sub_category_id)
                   ->get();

    # return json responce

 
    return response()->json($variant);

}

}