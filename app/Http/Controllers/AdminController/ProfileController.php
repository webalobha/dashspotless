<?php

namespace App\Http\Controllers\AdminController;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;

class ProfileController extends Controller
{
 use MessageStatusTrait;

 # Bind Type
 protected $type = 'Profile ';

 # Bind location
 protected $view = 'application.profile.';

 # Bind Admin
 protected $profile;

 /**
  * default constructor
  * @param
  * @return
  */
 function __construct(Admin $admin)
 {
  $this->admin = $admin;
 }
 
 

 /**
  * edit  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function update()
 {
  # Fetch Profile by id
  $user = Auth::guard('admin')->user();
  $profile = $this->admin
                   ->where('id', $user->id)
                   ->first(); 
  # code...
  return view($this->view.'edit')->with(['profile' => $profile]);
 }

 /**
  * edit Profile
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function edit(Request $request)
 {
  # current user
  $user = Auth::guard('admin')->user();

  $query = $this->admin;

 /* # upload avtar
    if($request->hasfile('avtar_name')) {
     $file = $request->file('avtar_name');
     $name = $file->getClientOriginalName(); // getting image name
     $date = date('y-m-d');              
     $randNumber = rand(0000,9999);              
     $filename =$date.'_'.$randNumber.'_'.$name;
     $file->move('dist/img/Profile/', $filename);
     $avtarPath ='dist/img/Profile/'.$filename;
    } else {
     $avtarPath = $query->where('id', $id)->first()->avtar_path; 
    }*/

  # request param
  $arrayData = [
                  'first_name'       => $request->first_name ?? null,
                  'last_name' => $request->last_name ?? null,
                  'email' => $request->email,
                
  ];

 
 
    #store 
    $updateProfile = $query->where('id', $user->id)->update($arrayData);

    # check created or not
    # if created
    if ($updateProfile) {
     # return successs
     $output = ['success' => 200, 'message' => 'profile update Successfully'];
    } else {
     # return error
     $output = ['error' => 100, 'message' => 'Something went wrong'];
    }

  return $output;
 }  

 
 
}
