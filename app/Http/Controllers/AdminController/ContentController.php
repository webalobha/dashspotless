<?php

namespace App\Http\Controllers\AdminController;

use App\Models\TermOfUse;
use App\Models\ContactUs;
use App\Models\HowItWorks;
use Illuminate\Http\Request;
use App\Models\PrivacyPolicy;
use App\Models\FaqPayingService;
use App\Models\AboutDashspotless;
use App\Models\FaqBookingService;
use App\Models\FaqDashspotlessGuide;
use App\Http\Controllers\Controller;

class ContentController extends Controller
{
 # bind termOfUse protected
 protected $termOfUse;

 # bind HowItWorks protected
 protected $howItWorks;

 # bind privacyPolicy protected
 protected $privacyPolicy;

 # bind contactUs protected
 protected $contactUs;

 # bind faqPayingService protected
 protected $faqPayingService;

 # bind AboutDashspotless protected
 protected $aboutDashspotless;

 # bind faqBookingService protected
 protected $faqBookingService;

 # bind faqDashspotlessGuide protected
 protected $faqDashspotlessGuide;

 /**
  * default constructor
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 function __construct(
                    TermOfUse            $termOfUse,
                    ContactUs            $contactUs,
                    HowItWorks           $howItWorks,
                    PrivacyPolicy        $privacyPolicy,
 	                  FaqPayingService     $faqPayingService,
 	                  AboutDashspotless    $aboutDashspotless,
 	                  FaqBookingService    $faqBookingService,
 	                  FaqDashspotlessGuide $faqDashspotlessGuide
 	                 )
 {
  $this->termOfUse            = $termOfUse;
  $this->contactUs            = $contactUs;
  $this->howItWorks           = $howItWorks;
  $this->privacyPolicy        = $privacyPolicy;
 	$this->faqPayingService     = $faqPayingService;
 	$this->aboutDashspotless    = $aboutDashspotless;
 	$this->faqBookingService    = $faqBookingService;
 	$this->faqDashspotlessGuide = $faqDashspotlessGuide;
 }

 /**
  * about Dashspotless page
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function aboutDashspotless()
 {
 	# fetch aboutDashspotless
 	$aboutDashspotless = $this->aboutDashspotless
 	                          ->first();

 	# return to aboutDashspotless page
 	return view('application.about_dashspotless.index')->with(['aboutDashspotless' => $aboutDashspotless]);
 }
 
 /**
  * post about Dashspotless
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function storeAboutDashspotless(Request $request)
 {
 	# array data
 	$arrayData = [
                  'title'       => $request->title ?? '',
                  'description' => $request->description ?? '',
 	];

 	if ($request->id != '') {
 		# update
 		$aboutDashspotless = $this->aboutDashspotless->where('id', $request->id)->update($arrayData);
 	} else {
 		# create
 		$aboutDashspotless = $this->aboutDashspotless->create($arrayData);
 	}

 	if ($aboutDashspotless) {
 		# output
 		$output = ['success' => 200, 'message' => 'Success.'];
 	} else {
 		# output
 		$output = ['error' => 100, 'message' => 'Something went wrong.'];
 	}

 	# return output
 	return $output;
 } 

 /**
  * faq Paying Service page
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function faqPayingService()
 {
 	# fetch faqPayingService
 	$faqPayingService = $this->faqPayingService
 	                          ->first();

 	# return to faqPayingService page
 	return view('application.faq_paying_for_service.index')->with(['faqPayingService' => $faqPayingService]);
 }
 
 /**
  * post faq Paying Service
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function storeFaqPayingService(Request $request)
 {
 	# array data
 	$arrayData = [
                  'title'       => $request->title ?? '',
                  'description' => $request->description ?? '',
 	];

 	if ($request->id != '') {
 		# update
 		$faqPayingService = $this->faqPayingService->where('id', $request->id)->update($arrayData);
 	} else {
 		# create
 		$faqPayingService = $this->faqPayingService->create($arrayData);
 	}

 	if ($faqPayingService) {
 		# output
 		$output = ['success' => 200, 'message' => 'Success.'];
 	} else {
 		# output
 		$output = ['error' => 100, 'message' => 'Something went wrong.'];
 	}

 	# return output
 	return $output;
 } 

 /**
  * faq Booking Service page
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function faqBookingService()
 {
 	# fetch faqBookingService
 	$faqBookingService = $this->faqBookingService
 	                          ->first();

 	# return to faqBookingService page
 	return view('application.faq_booking_service.index')->with(['faqBookingService' => $faqBookingService]);
 }
 
 /**
  * post faq Booking Service
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function storeFaqBookingService(Request $request)
 {
 	# array data
 	$arrayData = [
                  'title'       => $request->title ?? '',
                  'description' => $request->description ?? '',
 	];

 	if ($request->id != '') {
 		# update
 		$faqBookingService = $this->faqBookingService->where('id', $request->id)->update($arrayData);
 	} else {
 		# create
 		$faqBookingService = $this->faqBookingService->create($arrayData);
 	}

 	if ($faqBookingService) {
 		# output
 		$output = ['success' => 200, 'message' => 'Success.'];
 	} else {
 		# output
 		$output = ['error' => 100, 'message' => 'Something went wrong.'];
 	}

 	# return output
 	return $output;
 } 

 /**
  * faq Dashspotless Guide page
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function faqDashspotlessGuide()
 {
 	# fetch faqDashspotlessGuide
 	$faqDashspotlessGuide = $this->faqDashspotlessGuide
 	                          ->first();

 	# return to faqDashspotlessGuide page
 	return view('application.guide_for_dashspotless.index')->with(['faqDashspotlessGuide' => $faqDashspotlessGuide]);
 }
 
 /**
  * post faq Dashspotless Guide
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function storeFaqDashspotlessGuide(Request $request)
 {
 	# array data
 	$arrayData = [
                  'title'       => $request->title ?? '',
                  'description' => $request->description ?? '',
 	];

 	if ($request->id != '') {
 		# update
 		$faqDashspotlessGuide = $this->faqDashspotlessGuide->where('id', $request->id)->update($arrayData);
 	} else {
 		# create
 		$faqDashspotlessGuide = $this->faqDashspotlessGuide->create($arrayData);
 	}

 	if ($faqDashspotlessGuide) {
 		# output
 		$output = ['success' => 200, 'message' => 'Success.'];
 	} else {
 		# output
 		$output = ['error' => 100, 'message' => 'Something went wrong.'];
 	}

 	# return output
 	return $output;
 } 

 /**
  * how it works page
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function howItWorks()
 {
   # fetch how it works
   $howItWorks = $this->howItWorks
                      ->first();

   # return to howItWorks page
   return view('application.how_it_works.index')->with(['howItWorks' => $howItWorks]);
 }

 /**
  * store how it works
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function storeHowItWorks(Request $request)
 {
  # array data
  $arrayData = [
                  'content' => $request->content ?? '',
  ];

  if ($request->id != '') {
    # update
    $howItWorks = $this->howItWorks->where('id', $request->id)->update($arrayData);
  } else {
    # create
    $howItWorks = $this->howItWorks->create($arrayData);
  }

  if ($howItWorks) {
    # output
    $output = ['success' => 200, 'message' => 'Success.'];
  } else {
    # output
    $output = ['error' => 100, 'message' => 'Something went wrong.'];
  }

  # return output
  return $output;
 } 

 /**
  * term Of Use page
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function termsOfUse()
 {
   # fetch term Of Use
   $termsOfUse = $this->termOfUse
                      ->first();

   # return to termsOfUse page
   return view('application.terms_of_use.index')->with(['termsOfUse' => $termsOfUse]);
 }

 /**
  * store term Of Use
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function storeTermsOfUse(Request $request)
 {
  # array data
  $arrayData = [
                  'content' => $request->content ?? '',
  ];

  if ($request->id != '') {
    # update
    $termOfUse = $this->termOfUse->where('id', $request->id)->update($arrayData);
  } else {
    # create
    $termOfUse = $this->termOfUse->create($arrayData);
  }

  if ($termOfUse) {
    # output
    $output = ['success' => 200, 'message' => 'Success.'];
  } else {
    # output
    $output = ['error' => 100, 'message' => 'Something went wrong.'];
  }

  # return output
  return $output;
 } 

 /**
  * privacy Policy page
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function privacyPolicy()
 {
   # fetch privacy Policy
   $privacyPolicy = $this->privacyPolicy
                         ->first();

   # return to privacyPolicy page
   return view('application.privacy_policy.index')->with(['privacyPolicy' => $privacyPolicy]);
 }

 /**
  * store privacy Policy
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function storePrivacyPolicy(Request $request)
 {
  # array data
  $arrayData = [
                  'content' => $request->content ?? '',
  ];

  if ($request->id != '') {
    # update
    $privacyPolicy = $this->privacyPolicy->where('id', $request->id)->update($arrayData);
  } else {
    # create
    $privacyPolicy = $this->privacyPolicy->create($arrayData);
  }

  if ($privacyPolicy) {
    # output
    $output = ['success' => 200, 'message' => 'Success.'];
  } else {
    # output
    $output = ['error' => 100, 'message' => 'Something went wrong.'];
  }

  # return output
  return $output;
 } 

 /**
  * contact Us page
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function contactUs()
 {
   # fetch contact Us
   $contactUs = $this->contactUs
                      ->first();

   # return to contactUs page
   return view('application.contact_us.index')->with(['contactUs' => $contactUs]);
 }

 /**
  * store contact Us
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function storeContactUs(Request $request)
 {
  # array data
  $arrayData = [
                  'content' => $request->content ?? '',
  ];

  if ($request->id != '') {
    # update
    $contactUs = $this->contactUs->where('id', $request->id)->update($arrayData);
  } else {
    # create
    $contactUs = $this->contactUs->create($arrayData);
  }

  if ($contactUs) {
    # output
    $output = ['success' => 200, 'message' => 'Success.'];
  } else {
    # output
    $output = ['error' => 100, 'message' => 'Something went wrong.'];
  }

  # return output
  return $output;
 }
}
