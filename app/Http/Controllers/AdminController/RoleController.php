<?php

namespace App\Http\Controllers\AdminController;

use App\Models\Module;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;
use Spatie\Permission\Models\Permission;	

class RoleController extends Controller
{
 # Bind view
 protected $view = 'application.role.';

 # definr type
 protected $type = 'Role ';
 /**
  * index page
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function index()
 {
   # fetch roles
   $roles = Role::orderBy('id', 'desc')->get();

   # return index page
   return view($this->view.'index')->with(['roles' => $roles]);
 } 

 /**
  * add page
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function create()
 {
   # fetch role
   ///$roles = Role::all();
   # fetch permissions
   $permissions = Permission::all();

   # fetch modules
   $modules = Module::orderBy('id', 'asc')->get(); 

   # return add page
   return view($this->view.'create')->with([
   	                                        //'roles'       => $roles,
   	                                        'modules'     => $modules,
                                            'permissions' => $permissions,
   	                                       ]);
 }  

 /**
  * add
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function store(Request $request)
 {
   # fetch request role
   $roleName = $request->input('name');
   
   # check already exist or not
   $count = Role::where('name', $roleName)->count();
   
   # if not exist
   if ($count == 0) {
    
    # create the role
    $role = Role::create([
    	                  'name'       => $roleName,
    	                  'guard_name' => 'admin'
    	                ]);
    
    # if permission is requested
    if ($request->permission != '') {
     # assign permission to created role
     $assignPermiddion = $role->syncPermissions($request->input('permission'));
    }
    
    # if role is created
    if ($role) {
    	# success message
    	$output = ['success' => 200, 'message' => 'Role Added Successfully'];
    } else {
    	# if role is not created
    	# error message
    	$output = ['error' => 100, 'message' => 'Somethig went wrong.'];
    }
   } else {
   # if already exist
   # error message 
   $output = ['error' => 100, 'message' => 'Alredy Exist'];
   } 
   # return output
   return $output;
 }

 /**
  * edit page
  * @param Illuminate\Http\Request, $id
  * @return Illuminate\Http\Response
  */
 public function update($id)
 {
  # fetch permissions
  //$permissions = Permission::all();

  # fetch modules
  $modules = Module::orderBy('id', 'asc')->get(); 

 	# fetch role by id
 	$role = Role::where('id', $id)->first();

  $roleHasPermissions = $role->getAllPermissions();


 	# return to edit page
 	return view($this->view.'edit')->with(['modules' => $modules, 'role' => $role, 'roleHasPermissions' => $roleHasPermissions]);
 }

 /**
  * edit
  * @param
  * @return
  */
 public function edit(Request $request, $id)
 {
   # fetch request role
   $roleName = $request->input('name');
   
   # check already exist or not
   $count = Role::where('name', $roleName)->where('id', '!=', $id)->count();
   
   $role = Role::findOrFail($id); 
   $role->syncPermissions();

   # if not exist
   if ($count == 0) {
    
    # update the role
    $updateRole = Role::where('id', $id)->update([
                                            'name'       => $roleName,
                                            'guard_name' => 'admin'
                                          ]);
 
    if ($request->permission != '') {
     # assign permission to updated role
     $assignPermission = $role->syncPermissions($request->input('permission'));
    }

    # if role is updated
    if ($updateRole) {
    	# success message
    	$output = ['success' => 200, 'message' => 'Role Updated Successfully'];
    } else {
    	# if role is not updated
    	# error message
    	$output = ['error' => 100, 'message' => 'Somethig went wrong.'];
    }
   } else {
   # if already exist
   # error message 
   $output = ['error' => 100, 'message' => 'Alredy Exist'];
   } 
   # return output
   return $output;
 }
 
 /**
  * delete 
  * @param $id
  * @return \Illuminate\Http\Response
  */
 public function delete($id)
 {
   # delete role by id
   Role::where('id', $id)->delete();
   
   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $this->deleteMessage($this->type)];
 }
}
