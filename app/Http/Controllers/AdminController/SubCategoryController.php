<?php

namespace App\Http\Controllers\AdminController;


use App\Models\SubCategory;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\SubCategoryBanner;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\SubCategoriesExpertise;
use App\Http\Traits\MessageStatusTrait;

class SubCategoryController extends Controller
{
 use MessageStatusTrait;

 # Bind Type
 protected $type = 'Sub Category ';

 # Bind location
 protected $view = 'application.sub_category.';

 # Bind subCategory
 protected $subCategory;

 # Bind Category
 protected $category;

 # Bind sub Category banner
 protected $subCategoryBanner;

 # Bind sub Category experties
 protected $subCategoriesExpertise;

 /**
  * default constructor
  * @param
  * @return
  */
 function __construct(
                      Category               $category,
                      SubCategory            $subCategory,
                      SubCategoryBanner      $subCategoryBanner,
                      SubCategoriesExpertise $subCategoriesExpertise 
                     )
 {
 	$this->category               = $category;
  $this->subCategory            = $subCategory;
  $this->subCategoryBanner      = $subCategoryBanner;
 	$this->subCategoriesExpertise = $subCategoriesExpertise;
 }
 
 /**
  * index page of category
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function index()
 {
 	# fetch sub category list
 	$subCategories = $this->subCategory
 	                      ->with('category')
 	                      ->orderBy('name')
 	                      ->get();
 	# fetch category list
 	$categories = $this->category
 	                   ->where('status','1')
 	                   ->orderBy('name')
 	                   ->get();

 	# return to index page
 	return view($this->view.'index')->with(['subCategories' => $subCategories,'categories' => $categories]);;
 }

 /**
  * create sub category
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function store(Request $request)
 {
 	# current user
 	$user = Auth::guard('admin')->user();

    $query = $this->subCategory;

 	# upload avtar
    if($request->hasfile('avtar_name')) {
     $file = $request->file('avtar_name');
     $name = $file->getClientOriginalName(); // getting image name
     $date = date('y-m-d');              
     $randNumber = rand(0000,9999);              
     $newFile =$date.'_'.$randNumber.'_'.$name;
     $filename = str_replace(' ', '',$newFile); 
     $file->move('dist/img/sub_category/', $filename);
     $avtarPath ='dist/img/sub_category/'.$filename;
    } else {
     $avtarPath  = null;  
    }

 	# request param
 	$arrayData = [
                  'category_id'     => $request->category_id ?? null,
                  'name'            => $request->name ?? null,
                  'title'           => $request->title ?? null,
                  'discount_label'  => $request->discount_label ?? null,
                  'avtar_name'      => $name ?? null,
                  'avtar_path'      => $avtarPath,
                  'added_by'        => $user->id ?? null
 	];

  # check the requested sub category already exist or not
  $countubCategory = $query->where('name', $request->name)->count();
  
  if ($countubCategory == 0) {
    #store 
    $createSubCategory = $query->create($arrayData);

    # check created or not
    # if created
    if ($createSubCategory) {
    
     # create subcategory banner
     if (!empty($request->bannername)) {

        # Store the blog Multiple content.
        foreach ($request->bannername as $bannerImage) {

          # code...
          $file1 = $bannerImage['banner_name'];
          $name1 = $file1->getClientOriginalName(); // getting image name
          $date1 = date('y-m-d');              
          $randNumber1 = rand(0000,9999);              
          $newFile1 =$date1.'_'.$randNumber1.'_'.$name1;
          $filename1 = str_replace(' ', '',$newFile1); 
          $file1->move('dist/img/sub_category_banner/', $filename1);
          $bannerPath ='dist/img/sub_category_banner/'.$filename1;

          # Gather the blog content.
          $banner = [
            'sub_category_id' => $createSubCategory->id,
            'avtar_name'     => $name1,
            'avtar_path'     => $bannerPath,
            'added_by'        => $user->id ?? null
          ];

          # create blog content.
          $this->subCategoryBanner->create($banner);
        } 
       }

     # create sub category expertise
     if (!empty($request->expertisetitle)) {
      foreach ($request->expertisetitle as $expertiseTitle) {
        # code...
        $this->subCategoriesExpertise->create([
                                               'sub_category_id' => $createSubCategory->id,
                                               'title'           => $expertiseTitle['expertise_title'] ?? null,
                                               'added_by'        => $user->id ?? null
                                             ]);
      }
     }
    
     # return successs
     $output = ['success' => 200, 'message' => 'Sub Category Added Successfully'];
    } else {
     # return error
     $output = ['error' => 100, 'message' => 'Something went wrong'];
    }
  } else {
    # return exist message
    $output = ['error' => 100, 'message' => 'Already Exist'];
  }
  return $output;
 } 

 /**
  * edit  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function update($id)
 {
  # Fetch sub category by id
  $subCategory = $this->subCategory
                      ->where('id', $id)
                      ->first(); 

 	# fetch category list
 	$categories = $this->category
 	                   ->where('status','1')
 	                   ->orderBy('name')
 	                   ->get();
                     
 	# code...
 	return view($this->view.'edit')->with(['subCategory' => $subCategory, 'categories' => $categories]);
 }

 /**
  * edit category
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function edit(Request $request, $id)
 {
  # current user
  $user = Auth::guard('admin')->user();

  $query = $this->subCategory;

  $bannerImages = $this->subCategoryBanner;

  # upload avtar
  if($request->hasfile('avtar_name')) {
     $file = $request->file('avtar_name');
     $name = $file->getClientOriginalName(); // getting image name
     $date = date('y-m-d');              
     $randNumber = rand(0000,9999);              
     $newFile =$date.'_'.$randNumber.'_'.$name;
     $filename = str_replace(' ', '',$newFile);
     $file->move('dist/img/sub_category/', $filename);
     $avtarPath ='dist/img/sub_category/'.$filename;
  } else {
     $avtarPath = $query->where('id', $id)->first()->avtar_path; 
     $name      = $query->where('id', $id)->first()->avtar_name; 
  }

  # request param
  $arrayData = [
                  'category_id'     => $request->category_id ?? null,
                  'name'            => $request->name ?? null,
                  'title'           => $request->title ?? null,
                  'discount_label'  => $request->discount_label ?? null,
                  'avtar_name'      => $name ?? null,
                  'avtar_path'      => $avtarPath,
                  'added_by'        => $user->id ?? null
  ];

  # check the requested category already exist or not
  $countSubCategory = $query->where('name', $request->name)
                            ->where('id', '!=', $id)
                            ->count();

                           
  
  if ($countSubCategory == 0) {

        # Update Blog Content Row.
        foreach ($request->bannername as $bannerImage) {
                    
            if (isset($bannerImage['banner_id'])) {

                if (isset($bannerImage['banner_name'])) {

                    # code...
                    $file1 = $bannerImage['banner_name'];
                    $name1 = $file1->getClientOriginalName(); // getting image name
                    $date1 = date('y-m-d');              
                    $randNumber1 = rand(0000,9999);              
                    $newFile1 =$date1.'_'.$randNumber1.'_'.$name1;
                    $filename1 = str_replace(' ', '',$newFile1); 
                    $file1->move('dist/img/sub_category_banner/', $filename1);
                    $bannerPath ='dist/img/sub_category_banner/'.$filename1;

                    # Gather the blog content.
                    $banner = [
                      'sub_category_id' => $id,
                      'avtar_name'      => $name1,
                      'avtar_path'      => $bannerPath,
                      'added_by'        => $user->id ?? null
                    ];

                    # update code.
                    $bannerImages->where('id', $bannerImage['banner_id'])->update($banner);

                } else { 
                    $banners = $bannerImages->where('id', $bannerImage['banner_id'])->first();

                    # Gather the blog content.
                    $banner = [
                      'sub_category_id' => $id,
                      'avtar_name'     => $banners->avtar_name,
                      'avtar_path'     => $banners->avtar_path,
                      'added_by'        => $user->id ?? null
                    ];

                    # update code.
                    $bannerImages->where('id', $bannerImage['banner_id'])->update($banner);

                    # Soft Delete Row.
                   // $bannerImages->where('id', $bannerImage['banner_id'])->delete();
                }
              } else {
                
                    # code...
                    $file2 = $bannerImage['banner_name'];
                    $name2 = $file2->getClientOriginalName(); // getting image name
                    $date2 = date('y-m-d');              
                    $randNumber2 = rand(0000,9999);              
                    $newFile2 =$date2.'_'.$randNumber2.'_'.$name2;
                    $filename2 = str_replace(' ', '',$newFile2); 
                    $file2->move('dist/img/sub_category_banner/', $filename2);
                    $bannerPath2 ='dist/img/sub_category_banner/'.$filename2;

                    # Gather the blog content.
                    $banner2 = [
                      'sub_category_id' => $id,
                      'avtar_name'      => $name2,
                      'avtar_path'      => $bannerPath2,
                      'added_by'        => $user->id ?? null
                    ];

                    # create blog content.
                    $bannerImages->create($banner2);
            }
        }

        # Update Blog Content Row.
        foreach ($request->expertisetitle as $expertise) {
                    
            if (isset($expertise['expertise_id'])) {

                if (isset($expertise['expertise_title'])) {

                    # Gather the data.
                    $expertisetitles = [
                      'sub_category_id' => $id,
                      'title'           => $expertise['expertise_title'],
                      'added_by'        => $user->id ?? null
                    ];

                    # update code.
                    $this->subCategoriesExpertise->where('id',$expertise['expertise_id'])->update($expertisetitles);

                } else { 

                    # Soft Delete Row.
                    //$this->subCategoriesExpertise->where('id',$expertise['expertise_id'])->delete();
                }
              } else {
                
                # Gather the data.
                $expertisetitles = [
                      'sub_category_id' => $id,
                      'title'           => $expertise['expertise_title'],
                      'added_by'        => $user->id ?? null
                ];

                # Create New Row of Blog.
                $this->subCategoriesExpertise->create($expertisetitles);
            }
        }

    #store 
    $updateSubCategory = $query->where('id', $id)->update($arrayData);

    # check created or not
    # if created
    if ($updateSubCategory) {
     # return successs
     $output = ['success' => 200, 'message' => 'Sub Category update Successfully'];
    } else {
     # return error
     $output = ['error' => 100, 'message' => 'Something went wrong'];
    }
  } else {
    # return exist message
    $output = ['error' => 100, 'message' => 'Already Exist'];
  }
  return $output;
 }  

 /**
  * delete category
  * @param $id
  * @return \Illuminate\Http\Response
  */
 public function delete($id)
 {
   $query = $this->subCategory;

   # current user
   $user = Auth::guard('admin')->user();

   # update deleted by
   $query->where('id', $id)->update(['deleted_by' => $user->id]);

   # delete category by id
   $query->where('id', $id)->delete();
   
   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $this->deleteMessage($this->type)];
 }

 /**
  * active deactive
  * @param $id
  * @return \Illuminate\Http\Response
  */ 
 public function status($id)
 {
   # initiate constructor
   $query =  $this->subCategory;

   # get the status 
   $status =  $query->where('id', $id)->first()->status;

   # check status, if active
   if ($status == '1') {
     # message
     $message = $this->inActiveMessage($this->type);

     # deactive( update status to zero)
     $statusCode = '0';
   } else {
     # message
     $message = $this->activeMessage($this->type);

     # active( update status to one)
     $statusCode = '1';
   }
   
   # update status code
   $query->where('id', $id)->update(['status' => $statusCode]);

   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $message];
 }



 /**
  * json responce of subcategory by category id
  * @param
  * @return \Illuminate\Http\Response in json
  */
  public function subcategory(Request $request)
  {
    # fetch states 
    $states = $this->subCategory
                   ->where('category_id',$request->category_id)
                   ->where('status', '1')
                   ->get();

    # return json responce
    return response()->json($states);
  } 






}
