<?php

namespace App\Http\Controllers\AdminController;

use Validator;
use App\Models\Admin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Mail;
use App\Mail\SendMail;

class LoginController extends Controller
{
    public function index()
    {
    	return view('application.layouts.login');
    }

    public function login(Request $request)
    {
      
    	$v = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);
        
        if ($v->fails()) {  
            return 'Please Fill All Entries';
        }
        $credentials = $request->only('email', 'password');
        //dd($credentials);
        if (!Auth::guard('admin')->attempt($credentials)) {
            return redirect()->back()->with('invalid_msg','Given Credentials Are Not Matches');
        } else {
            $user = Auth::guard('admin')->user();
            //dd('success');
            if($user->deleted_at == Null)
                 {
              	 return redirect("home");
                 }
                 else{
                    return redirect()->back();
                 }
        }
          return 1;
    }


public function forgotpassword()
    {
       

      return view('application.layouts.forgot');
    }



 public function sendemail(Request $request)
 { 
   # request all data from the enquiry form 
  
   $email = $request->email ?? null;

   $check = Admin::where('email',$email)->first();
   
if($check){
    $check->forgot_password_status=1;
    $check->save();
  $subject = 'Reset password link';

$id = base64_encode($check->id);
  $message= url('reset_pass').'/'.$id;

  //dd($message);
$dd= Mail::to('pankaj.alobha@gmail.com')->send( new SendMail($subject, $message) );

 //dd($dd);
  
   $output = ['success' => 200, 'message' => 'Mail send Successfully'];
 




}else{
 $output = ['error' => 100, 'message' => 'Email id not exist!'];
 



}
   
 return($output);
       
  
 }
 


public function reset_pass(Request $request,$id)
    {
       
         // dd(base64_decode($id));
        $id =base64_decode($id);
      $post = Admin::where('id',$id)->where('forgot_password_status',0)->first();

            if($post){

        return redirect()->route('admin-login')->with('error','Link has been expired');

      }


      return view('application.reset')->with([
                                            'id'=> $id,
                                          ]);;
    }


    public function post_reset_pass(Request $request)
    {
       
         $post = Admin::where('id',$request->id)->first();

         //dd($post);
   
       if($request->password!=$request->confirm_password){

          

           $output = ['error' => 100, 'message' => 'Confirm password not metchad'];

           return $output;
       }


    $post->forgot_password_status='0';
    $post->password=Hash::make($request->password);
    if($post->save()){
    $output = ['success' => 200, 'message' => 'password update Successfully'];
        //return redirect()->route('admin-login')->with('success','New password update Successfully');
 

}
else{
 $output = ['error' => 100, 'message' => 'Some thing went wrong'];



    }

    return $output;

}











    public function logout()
    {
        Auth::guard('admin')->logout();

        return redirect('admin-login');
    }



}
