<?php

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;
use App\Models\CreditRecharge;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;

class CreditRechargeController extends Controller
{
 use MessageStatusTrait;

 # Bind Type
 protected $type = 'Credit Recharge ';

 # Bind location
 protected $view = 'application.credit_recharge.';

 # Bind creditRecharge
 protected $creditRecharge;

 /**
  * default constructor
  * @param
  * @return
  */
 function __construct(CreditRecharge $creditRecharge)
 {
 	$this->creditRecharge = $creditRecharge;
 }
 
 /**
  * index page of creditRecharge
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function index()
 {
 	# fetch creditRecharge list
 	$createcreditRecharges = $this->creditRecharge
 	                              ->orderBy('created_at', 'desc')
 	                              ->get();

 	# return to index page
 	return view($this->view.'index')->with(['createcreditRecharges' => $createcreditRecharges]);;
 }

 /**
  * create creditRecharge
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function store(Request $request)
 {
 	# current user
 	$user = Auth::guard('admin')->user();

    $query = $this->creditRecharge;

 	# request param
 	$arrayData = [
                  'points'           => $request->points ?? null,
                  'amount_per_point' => $request->amount_per_point ?? null,
                  'added_by'         => $user->id ?? null
 	];

     #store 
    $createCreditRecharge = $query->create($arrayData);

    # check created or not
    # if created
    if ($createCreditRecharge) {
     # return successs
     $output = ['success' => 200, 'message' => 'Credit Recharge Added Successfully'];
    } else {
     # return error
     $output = ['error' => 100, 'message' => 'Something went wrong'];
    }
  return $output;
 } 

 /**
  * edit  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function update($id)
 {
  # Fetch creditRecharge by id
  $creditRecharge = $this->creditRecharge
                         ->where('id', $id)
                         ->first(); 
 	# code...
 	return view($this->view.'edit')->with(['creditRecharge' => $creditRecharge]);
 }

 /**
  * edit creditRecharge
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function edit(Request $request, $id)
 {
  # current user
  $user = Auth::guard('admin')->user();

  $query = $this->creditRecharge;

 	# request param
 	$arrayData = [
                  'points'           => $request->points ?? null,
                  'amount_per_point' => $request->amount_per_point ?? null,
                  'updated_by'       => $user->id ?? null
    ];

    #store 
    $updatecreditRecharge = $query->where('id', $id)->update($arrayData);

    # check created or not
    # if created
    if ($updatecreditRecharge) {
     # return successs
     $output = ['success' => 200, 'message' => 'Credit Recharge update Successfully'];
    } else {
     # return error
     $output = ['error' => 100, 'message' => 'Something went wrong'];
    }
  return $output;
 }  

 /**
  * delete creditRecharge
  * @param $id
  * @return \Illuminate\Http\Response
  */
 public function delete($id)
 {
   $query = $this->creditRecharge;

   # current user
   $user = Auth::guard('admin')->user();

   #update deleted by
   $query->where('id', $id)->update(['deleted_by' => $user->id]);

   # delete creditRecharge by id
   $query->where('id', $id)->delete();
   
   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $this->deleteMessage($this->type)];
 }

 /**
  * active deactive
  * @param $id
  * @return \Illuminate\Http\Response
  */ 
 public function status($id)
 {
   # initiate constructor
   $query =  $this->creditRecharge;

   # get the status 
   $status =  $query->where('id', $id)->first()->status;

   # check status, if active
   if ($status == '1') {
     #message
     $message = $this->inActiveMessage($this->type);

     # deactive( update status to zero)
     $statusCode = '0';
   } else {
     #message
     $message = $this->activeMessage($this->type);

     # active( update status to one)
     $statusCode = '1';
   }
   
   # update status code
   $query->where('id', $id)->update(['status' => $statusCode]);

   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $message];
 }
}
