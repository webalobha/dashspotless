<?php

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
 # Bind view
 protected $view = 'application.permission.':

 /**
  * assign permission
  * @param Illuminate\Http\Request
  * @return Illuminate\Http\Response
  */
 public function index()
 {  
 	# fetch role
 	$roles = Role::all();

 	# fetch permissions
 	$permissions = Permission::all();

 	# return to assign permission page
 	return view($this->view.'index')->with([
 		                                    'roles'       => $roles, 
 		                                    'permissions' => $permissions
 		                                   ]);
 }
}
