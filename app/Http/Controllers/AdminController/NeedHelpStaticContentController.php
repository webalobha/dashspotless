<?php

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\NeedHelpStaticContent;
use App\Http\Traits\MessageStatusTrait;

class NeedHelpStaticContentController extends Controller
{
 use MessageStatusTrait;

 # Bind Type
 protected $type = 'Need Help Static Content ';

 # Bind location
 protected $view = 'application.need_help.';

 # Bind NeedHelpStaticContent
 protected $needHelpStaticContent;

 /**
  * default constructor
  * @param
  * @return
  */
 function __construct(NeedHelpStaticContent $needHelpStaticContent)
 {
  $this->needHelpStaticContent = $needHelpStaticContent;
 }


 /**
  * index page of needHelpStaticContent
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function index(Request $request)
 {
  # fetch needHelpStaticContent list
  $needHelps = $this->needHelpStaticContent->orderBy('id', 'desc')->get();

  # return to index page
  return view($this->view.'index')->with([
 		                                  'needHelps'=> $needHelps,
 		                                ]);
 }

 /**
  * create page of NeedHelpStaticContent
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function create()
 {
  # return create page
  return view($this->view.'create');
 }

 /**
  * create page of NeedHelpStaticContent
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function store(Request $request)
 {
  # request data
  $arrayData = [
                'title'               => $request->title ?? null,
                'content'             => $request->content ?? null,
                'is_user_or_provider' => $request->is_user_or_provider,
  ];

  # create
  $create = $this->needHelpStaticContent->create($arrayData);
  
  # if created
  if ($create) {
  	# return success
  	$output = ['success' => 200, 'message' => 'Created Successfully.'];
  } else {
  	# return error
  	$output = ['error' => 100, 'message' => 'Something Went Wrong.'];
  }
  # return
  return $output;
 }

 /**
  * edit  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function update($id)
 {
  # fetch need help by id
  $needHelp =  $this->needHelpStaticContent->where('id',$id)->first();

  # return to page
  return view($this->view.'edit')->with(['needHelp'=>$needHelp]);
 }

 /**
  * edit needHelpStaticContent
  * @param Illuminate\Http\Request, $id; 
  * @return Illuminate\Http\Response;
  */
 public function edit(Request $request, $id)
 {
  # request data
  $arrayData = [
                'title'               => $request->title ?? null,
                'content'             => $request->content ?? null,
                'is_user_or_provider' => $request->is_user_or_provider,
  ];

  # update
  $update = $this->needHelpStaticContent->where('id', $id)->update($arrayData);
  
  # if updated
  if ($update) {
  	# return success
  	$output = ['success' => 200, 'message' => 'Updated Successfully.'];
  } else {
  	# return error
  	$output = ['error' => 100, 'message' => 'Something Went Wrong.'];
  }
  # return
  return $output;
 } 

 /**
  * delete needHelpStaticContent
  * @param $id
  * @return \Illuminate\Http\Response
  */
 public function delete($id)
 {
   $this->needHelpStaticContent->where('id', $id)->delete();
   
   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $this->deleteMessage($this->type)];
 }

 /**
  * active deactive
  * @param $id
  * @return \Illuminate\Http\Response   
  */ 
 public function status($id)
 {
   # initiate constructor
   $query =  $this->needHelpStaticContent;

   # get the status 
   $status =  $query->where('id', $id)->first()->status;

   # check status, if active
   if ($status == '1') {
     #message
     $message = $this->inActiveMessage($this->type);

     # deactive( update status to zero)
     $statusCode = '0';
   } else {
     #message
     $message = $this->activeMessage($this->type);

     # active( update status to one)
     $statusCode = '1';
   }
   
   # update status code
   $query->where('id', $id)->update(['status' => $statusCode]);

   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $message];
 }


}
