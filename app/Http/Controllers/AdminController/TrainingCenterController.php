<?php

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;
use App\Models\TrainingCenter;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;

class TrainingCenterController extends Controller
{
  use MessageStatusTrait;

  # bind TrainingCenter to protected
  protected $trainingCenter;

  # view
  protected $view = 'application.training_center.';

  # type
  protected $type = 'Training Center ';

  /**
   * default constructor
   * @param
   * @return
   */
  function __construct(TrainingCenter $trainingCenter)
  {
  	$this->trainingCenter = $trainingCenter;
  }

  public function index()
  {
  	# fetch training center
  	$trainingCenters = $this->trainingCenter
  	                        ->orderBy('id', 'desc')
  	                        ->get();

  	# return to index page
  	return view($this->view.'index')->with(['trainingCenters' => $trainingCenters]);
  }
  
  /**
   * store
   * @param
   * @return
   */
  public function store(Request $request)
  {
  	# arrray data
  	$arrayData = [
                  'title' => $request->title ?? null,
                  'url'   => $request->url ?? null,
  	]; 

  	# create
  	$createTrainingCenter = $this->trainingCenter->create($arrayData);

  	if ($createTrainingCenter) {
  		# return success
  		$output = ['success' => 200, 'message' => 'Created Successfully'];
  	} else {
  		# return error
  		$output = ['error' => 100, 'message' => 'Something went wrong'];
  	}
    return $output;
  }

  /**
   * delete
   * @param
   * @return
   */
  public function delete($id)
  {
   $query = $this->trainingCenter;

   # delete trainingCenter by id
   $query->where('id', $id)->delete();
   
   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $this->deleteMessage($this->type)];
  }
  
  
}
