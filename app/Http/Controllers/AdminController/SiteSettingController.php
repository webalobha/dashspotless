<?php

namespace App\Http\Controllers\AdminController;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;
use App\Models\SitePaymentSetting;
use App\Models\SiteSettingCompProfileLink;
use App\Models\SiteSettingGeneral;
use App\Models\SiteSettingMail;
use App\Models\SiteSettingMapSms;
use App\Models\SiteSettingPushNotification;
use App\Models\SiteSettingsocialLink;
use App\Models\SiteSettingService;
use App\Models\SmsProvider;


class SiteSettingController extends Controller
{
 use MessageStatusTrait;

 # Bind Type
 protected $type = 'Site Setting';

 # Bind location
 protected $view = 'application.site_setting.';



 /**
  * default constructor
  * @param
  * @return
  */
 function __construct(
 	            SitePaymentSetting                     $SitePaymentSetting,
              SiteSettingCompProfileLink    $SiteSettingCompProfileLink,
 	            SiteSettingGeneral      $SiteSettingGeneral,
 	            SiteSettingMail         $SiteSettingMail,
              SiteSettingMapSms         $SiteSettingMapSms,  
              SiteSettingPushNotification         $SiteSettingPushNotification,
              SiteSettingsocialLink         $SiteSettingsocialLink,
              SiteSettingService         $SiteSettingService,
              SmsProvider         $SmsProvider

 )
 {
                $this->SitePaymentSetting           = $SitePaymentSetting;
                $this->SiteSettingCompProfileLink   = $SiteSettingCompProfileLink;
                $this->SiteSettingGeneral           = $SiteSettingGeneral;
                $this->SiteSettingMail              = $SiteSettingMail;
                $this->SiteSettingMapSms            = $SiteSettingMapSms;
                $this->SiteSettingPushNotification  = $SiteSettingPushNotification;
                $this->SiteSettingsocialLink        = $SiteSettingsocialLink;
                $this->SiteSettingService        = $SiteSettingService;
                 $this->SmsProvider        = $SmsProvider;

                          

}
 




 
 /**
  * edit  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function create_or_update($id='')
 {
  # Fetch variant by id
  $SitePaymentSetting =  $this->SitePaymentSetting->first();
  $SiteSettingCompProfileLink =  $this->SiteSettingCompProfileLink->first();
  $SiteSettingGeneral =  $this->SiteSettingGeneral->first();
  $SiteSettingMail =  $this->SiteSettingMail->first();
  $SiteSettingMapSms =  $this->SiteSettingMapSms->first();
  $SiteSettingPushNotification =  $this->SiteSettingPushNotification->first();
  $SiteSettingsocialLink =  $this->SiteSettingsocialLink->first();
  $SiteSettingService = $this->SiteSettingService->first();
  $SmsProvider =  $this->SmsProvider->first();

  

 
 //dd($ServiceSettingCancellationData);
 	return view($this->view.'create_update')->with([
    'SitePaymentSetting' => $SitePaymentSetting??'',
    'SiteSettingCompProfileLink' => $SiteSettingCompProfileLink ??'',
    'SiteSettingsocialLink' => $SiteSettingsocialLink??'',
    'SiteSettingGeneral' => $SiteSettingGeneral??'',
    'SiteSettingMail' => $SiteSettingMail??'',
    'SiteSettingMapSms' => $SiteSettingMapSms??'',
    'SiteSettingPushNotification' => $SiteSettingPushNotification??'',
     'SiteSettingService' => $SiteSettingService ?? '',
      'SmsProvider' => $SmsProvider??'',
    
  ]);
 }

 
/**
  * edit variant
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function edit(Request $request)
 {
       
   // dd($request->all());
 $user = Auth::guard('admin')->user();

  $SitePaymentSetting =  $this->SitePaymentSetting->first();
  $SiteSettingGeneral =  $this->SiteSettingGeneral->first();
  $SiteSettingCompProfileLink =  $this->SiteSettingCompProfileLink->first();
  $SiteSettingMail =  $this->SiteSettingMail->first();
  $SiteSettingMapSms =  $this->SiteSettingMapSms->first();
  $SiteSettingPushNotification =  $this->SiteSettingPushNotification->first();
  $SiteSettingsocialLink =  $this->SiteSettingsocialLink->first();
   $SiteSettingService =  $this->SiteSettingService->first();
   $SmsProvider =  $this->SmsProvider->first();
   //$SiteSettingMail =  $this->SiteSettingMail->first();



 // For genrel setting 

if($request->general_setting=='1') {

if($SiteSettingGeneral){

  $id= $SiteSettingGeneral->id;

$update =  $this->SiteSettingGeneral->where('id', $id)->first();
if($request->hasfile('site_icon_name')) {
     $file = $request->file('site_icon_name');
     $name = $file->getClientOriginalName(); // getting image name
     $date = date('y-m-d');              
     $randNumber = rand(0000,9999);              
     $newFile =$date.'_'.$randNumber.'_'.$name;
     $filename = str_replace(' ', '',$newFile);
     $file->move('dist/img/site_icon/', $filename);
     $avtarPath_icon ='dist/img/site_icon/'.$filename;

     $update->site_icon_img_path= $avtarPath_icon;
    } 
  if($request->hasfile('site_logo_name')) {
     $file = $request->file('site_logo_name');
     $name = $file->getClientOriginalName(); // getting image name
     $date = date('y-m-d');              
     $randNumber = rand(0000,9999);              
     $newFile =$date.'_'.$randNumber.'_'.$name;
     $filename = str_replace(' ', '',$newFile);
     $file->move('dist/img/site_logo/', $filename);
     $avtarPath_logo ='dist/img/site_logo/'.$filename;
     $update->site_logo_img_path= $avtarPath_logo;
    } 
    $update->name= $request->name;
    $update->contact_number=$request->contact_number;
    $update->contact_email= $request->contact_email;
    $update->sos_number= $request->sos_number;
    $update->copyright_content= $request->copyright_content;
   $update->save();
}else{
if($request->hasfile('site_icon_name')) {
     $file = $request->file('site_icon_name');
     $name = $file->getClientOriginalName(); // getting image name
     $date = date('y-m-d');              
     $randNumber = rand(0,99);              
     $newFile =$date.'_'.$randNumber.'_'.$name;
     $filename = str_replace(' ', '',$newFile);
     $file->move('dist/img/site_icon/', $filename);
     $avtarPath_icon ='dist/img/site_icon/'.$filename;
    } 


  if($request->hasfile('site_logo_name')) {
     $file = $request->file('site_logo_name');
     $name = $file->getClientOriginalName(); // getting image name
     $date = date('y-m-d');              
     $randNumber = rand(0,99);              
     $newFile =$date.'_'.$randNumber.'_'.$name;
     $filename = str_replace(' ', '',$newFile);
     $file->move('dist/img/site_logo/', $filename);
     $avtarPath_logo ='dist/img/site_logo/'.$filename;
    } 

$setting = [
            'namespace' => $request->name,
            'contact_number'     => $request->contact_number ,
            'contact_email' => $request->contact_email ??'',
            'sos_number'     => $request->sos_number??'',
            'copyright_content'     => $request->copyright_content??'',
            'site_icon_img_path'     =>  $avtarPath_icon,
            'site_logo_img_path'     => $avtarPath_logo,
          ];
$data=$this->SiteSettingGeneral->create($setting);
}

 $output = ['success' => 200, 'message' => 'general Setting update Successfully'];
  return $output;
}

// For company profile links setting 

if($request->company_profile_setting=='1') {

if($SiteSettingCompProfileLink){
  $id= $SiteSettingCompProfileLink->id;
         $update =  $this->SiteSettingCompProfileLink->where('id', $id)->first();
          $update->playstore_user_link= $request->playstore_user_link;
          $update->playstore_provider_link=$request->playstore_provider_link;
          $update->appstore_user_link= $request->appstore_user_link;
          $update->appstore_provider_link= $request->appstore_provider_link;
          $update->facebook_link= $request->facebook_link;
          $update->twitter_link= $request->twitter_link;
          $update->save();
}else{
$setting = [
          'playstore_user_link' => $request->playstore_user_link,
          'playstore_provider_link'     => $request->playstore_provider_link ,
          'appstore_user_link' => $request->appstore_user_link ??'',
          'appstore_provider_link'     => $request->appstore_provider_link??'',
          'facebook_link'     => $request->facebook_link??'',
          'twitter_link'     => $request->twitter_link??'',

          ];
       $this->SiteSettingCompProfileLink->create($setting);
}
 $output = ['success' => 200, 'message' => 'Company profile genrel Setting update Successfully'];
  return $output;
}



// For company cocial login links setting 


if($request->social_link_setting=='1') {

if($SiteSettingsocialLink){
  $id= $SiteSettingsocialLink->id;
         $update =  $this->SiteSettingsocialLink->where('id', $id)->first();
         
          $update->is_social_login  = $request->is_social_login;
          $update->facebook_app_id  = $request->facebook_app_id;
          $update->facebook_app_secret=$request->facebook_app_secret;
          $update->google_client_id= $request->google_client_id;
          $update->save();
}else{
$setting = [
           'is_social_login' => $request->is_social_login,
          'facebook_app_id' => $request->facebook_app_id,
          'facebook_app_secret'     => $request->facebook_app_secret ,
          'google_client_id' => $request->google_client_id ??'',

          ];
       $this->SiteSettingsocialLink->create($setting);
}
 $output = ['success' => 200, 'message' => 'Social login setting update Successfully'];
  return $output;
}



// For Map & SMS config setting 



if($request->map_sms_setting=='1') {

if($SiteSettingMapSms){
              $id= $SiteSettingMapSms->id;
              $update =  $this->SiteSettingMapSms->where('id', $id)->first();
              $update->browser_map_key  = $request->browser_map_key;
              $update->secret_map_key=$request->secret_map_key;
              $update->android_map_key= $request->android_map_key;
              $update->ios_map_key= $request->ios_map_key;
              $update->fb_app_version= $request->fb_app_version;
              $update->fb_app_secret= $request->fb_app_secret;
          $update->save();
}else{
$setting = [
          'browser_map_key' => $request->browser_map_key,
          'secret_map_key'     => $request->secret_map_key ,
          'android_map_key' => $request->android_map_key ??'',
          'ios_map_key' => $request->ios_map_key ??'',
          'fb_app_version' => $request->fb_app_version ??'',
          'fb_app_secret' => $request->fb_app_secret ??'',

          ];
       $this->SiteSettingMapSms->create($setting);
}
 $output = ['success' => 200, 'message' => 'Social login setting update Successfully'];
  return $output;
}



// Update push notigfication setting


if($request->push_note_setting=='1') {

if($SiteSettingPushNotification){
  $id= $SiteSettingPushNotification->id;

  $update =  $this->SiteSettingGeneral->where('id', $id)->first();
  if($request->hasfile('ios_push_user_pem')) {
     $file = $request->file('ios_push_user_pem');
     $name = $file->getClientOriginalName(); // getting image name
     $date = date('y-m-d');              
     $randNumber = rand(0000,9999);              
     $newFile =$date.'_'.$randNumber.'_'.$name;
     $filename = str_replace(' ', '',$newFile);
     $file->move('dist/img/ios_push_user_pem/', $filename);
     $iosPushUserPem ='dist/img/ios_push_user_pem/'.$filename;

     $update->ios_push_user_pem = $iosPushUserPem;
    } 
  if($request->hasfile('ios_push_provider_pem')) {
     $file = $request->file('ios_push_provider_pem');
     $name = $file->getClientOriginalName(); // getting image name
     $date = date('y-m-d');              
     $randNumber = rand(0000,9999);              
     $newFile =$date.'_'.$randNumber.'_'.$name;
     $filename = str_replace(' ', '',$newFile);
     $file->move('dist/img/ios_push_provider_pem/', $filename);
     $iosPushProviderPem ='dist/img/ios_push_provider_pem/'.$filename;
     $update->ios_push_provider_pem = $iosPushProviderPem;
    }

             // $id= $SiteSettingPushNotification->id;
              $update =  $this->SiteSettingPushNotification->where('id', $id)->first();
              $update->is_send_mail  = 1;
              $update->is_send_sms=1;
              $update->ios_push_environment= $request->ios_push_environment ?? null;
              $update->ios_push_user_pem= $iosPushUserPem ?? null;
              $update->ios_push_provider_pem= $iosPushProviderPem ?? null;
              $update->ios_push_password= $request->ios_push_password;
              $update->android_push_key= $request->android_push_key;           
              $update->save();
}else{
 if($request->hasfile('ios_push_user_pem')) {
     $file = $request->file('ios_push_user_pem');
     $name = $file->getClientOriginalName(); // getting image name
     $date = date('y-m-d');              
     $randNumber = rand(0,99);              
     $newFile =$date.'_'.$randNumber.'_'.$name;
     $filename = str_replace(' ', '',$newFile);
     $file->move('dist/img/ios_push_user_pem/', $filename);
     $iosPushUserPem ='dist/img/ios_push_user_pem/'.$filename;
    } 


  if($request->hasfile('ios_push_provider_pem')) {
     $file = $request->file('ios_push_provider_pem');
     $name = $file->getClientOriginalName(); // getting image name
     $date = date('y-m-d');              
     $randNumber = rand(0,99);              
     $newFile =$date.'_'.$randNumber.'_'.$name;
     $filename = str_replace(' ', '',$newFile);
     $file->move('dist/img/ios_push_provider_pem/', $filename);
     $iosPushProviderPem ='dist/img/ios_push_provider_pem/'.$filename;
    }

$setting = [
          'is_send_mail' => 1,
          'is_send_sms'     => 1 ,
          'ios_push_environment' => $request->ios_push_environment ?? null,
          'ios_push_user_pem'     => $iosPushUserPem ?? null,
          'ios_push_provider_pem' => $iosPushProviderPem ?? null,
          'ios_push_password' => $request->ios_push_password ??'',
          'android_push_key' => $request->android_push_key ??'',
          

          ];
       $this->SiteSettingPushNotification->create($setting);
}
 $output = ['success' => 200, 'message' => 'Push notification setting update Successfully'];
  return $output;
}


// Update payment setting


if($request->payment_setting=='1') {

if($SitePaymentSetting){
              $id= $SitePaymentSetting->id;
              $update =  $this->SitePaymentSetting->where('id', $id)->first();
              $update->is_cash= $request->is_cash;
              $update->is_card= $request->is_card;
              $update->stripe_secret_key= $request->stripe_secret_key;
               $update->stripe_publishable_key= $request->stripe_publishable_key;
              $update->stripe_currency= $request->stripe_currency;
                        
              $update->save();
}else{
$setting = [
           'is_cash' => $request->is_cash ??'',
           'is_card' => $request->is_card ??'',
          'stripe_secret_key' => $request->stripe_secret_key ??'',
          'stripe_publishable_key' => $request->stripe_publishable_key ??'',
          'stripe_currency' => $request->stripe_currency ??'',
          

          ];
       $this->SitePaymentSetting->create($setting);
}
 $output = ['success' => 200, 'message' => 'Payment setting update Successfully'];
  return $output;
}





// Update site service setting


if($request->service_setting=='1') {

if($SiteSettingService){
              $id= $SitePaymentSetting->id;
              $update =  $this->SiteSettingService->where('id', $id)->first();
              $update->provider_accept_timeout= $request->provider_accept_timeout;
               $update->provider_search_radius= $request->provider_search_radius;
              $update->serve_otp= $request->serve_otp;
               $update->booking_id_prefix= $request->booking_id_prefix;
                        
              $update->save();
}else{
$setting = [
         
          'provider_accept_timeout' => $request->provider_accept_timeout ??'',
          'provider_search_radius' => $request->provider_search_radius ??'',
          'serve_otp' => $request->serve_otp ??'',
           'booking_id_prefix' => $request->booking_id_prefix ??'',
          

          ];
       $this->SiteSettingService->create($setting);
}
 $output = ['success' => 200, 'message' => 'Site service setting update Successfully'];
  return $output;
}



// Update site mail sms  setting


if($request->mail_setting=='1') {

if($SiteSettingMail){
              $id= $SiteSettingMail->id;
              // $update =  $this->SmsProvider->where('id', $id)->first();

              // $update->name= $request->name;
              // $update->account_sid= $request->account_sid;
              // $update->auth_token= $request->auth_token;
              // $update->from_number= $request->from_number;        
              // $update->save();
              $otherupdate =  $this->SiteSettingMail->where('id', $id)->first();
              $otherupdate->is_send_mail= $request->is_send_mail?? 0;
              $otherupdate->is_send_sms= $request->is_send_sms ?? 0;
              $otherupdate->sms_provider_id= $request->sms_provider_id ?? '';
              $otherupdate->twilio_account_sid= $request->twilio_account_sid ?? '';
              $otherupdate->twilio_auth_token= $request->twilio_auth_token ?? '';
              $otherupdate->twilio_from_number= $request->twilio_from_number ?? '';
              $otherupdate->save();
}else{
$setting = [
         
          'is_send_mail' => $request->is_send_mail ?? 0,
          'is_send_sms' => $request->is_send_sms ?? 0,
          'sms_provider_id' => $request->sms_provider_id ??'',
          'twilio_account_sid' => $request->twilio_account_sid ??'',
          'twilio_auth_token' => $request->twilio_auth_token ??'',
           'twilio_from_number' => $request->twilio_from_number ??'',
          

          ];
       $id = $this->SiteSettingMail->create($setting);

            $otherupdate = new SiteSettingMail();
            $otherupdate->is_send_mail= $request->is_send_mail ?? 0;
            $otherupdate->is_send_sms= $request->is_send_sms ?? 0;
             $otherupdate->sms_provider_id=  $request->sms_provider_id ?? '';
             $otherupdate->twilio_account_sid= $request->twilio_account_sid ?? '';
              $otherupdate->twilio_auth_token= $request->twilio_auth_token ?? '';
              $otherupdate->twilio_from_number= $request->twilio_from_number ?? '';
            $otherupdate->save();





}
 $output = ['success' => 200, 'message' => 'Site service setting update Successfully'];
  return $output;
}








dd('500');




 } 




}
