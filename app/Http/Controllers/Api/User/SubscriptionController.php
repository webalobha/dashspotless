<?php

namespace App\Http\Controllers\Api\User;

use DB;
use Validator;
use App\Models\User; 
use App\Models\Service;
use App\Models\Category; 
use App\Models\ServiceCart; 
use App\Models\SubCategory;
use App\Models\Subscription;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Traits\StatusTrait;
use App\Models\SubCategoryVariants; 
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller; 

class SubscriptionController extends Controller
{
    use StatusTrait;

    # Variable to Bind Model
    protected $user;

    # bind UserOtp Model
    protected $variant;

    # bind UserOtp Model
    protected $service;

    # bind Service Cart Model
    protected $serviceCart;

    # bind Category Model
    protected $category;

    # bind Category Model
    protected $subCategory; 

    # bind Sub Category variant Model
    protected $subCategoryVariant;

    # bind Subscription Model
    protected $subscription;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        User $user,
        Service $service,
        Category $category, 
        ServiceCart $serviceCart,
        SubCategory $subCategory,
        Subscription $subscription, 
        subCategoryVariants $subCategoryVariant)
    {
        $this->user                     = $user;
        $this->service                  = $service;
        $this->category                 = $category;
        $this->serviceCart              = $serviceCart;
        $this->subCategory              = $subCategory;
        $this->subscription             = $subscription;
        $this->subCategoryVariant       = $subCategoryVariant;
    }

    /**
     * @method to get all the Subscription
     * @param Request $request
     * @return Subscription Json
     */
    public function getSubscription(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'subscription_type'  => 'required|string',
            'user_id'            => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # Fetch all the Subscription according to Subscription Type
        if($request->get('subscription_type') == 'monthly') {
            $subscriptions = $this->subscription
                                  ->with(['subscriptionDuration', 'discountType'])
                                  ->monthly()
                                  ->get();
        } elseif ($request->get('subscription_type') == 'weekly') {
            $subscriptions = $this->subscription
                                  ->with(['subscriptionDuration', 'discountType'])
                                  ->weekly()
                                  ->get();
        }

        # check Subscription if Empty
        $subscriptionData = [];
        if($subscriptions->isnotEmpty()) {
            foreach ($subscriptions as $key => $subscription) {
                $data = [
                    'subscription_id'       => (string)$subscription->id ?? '',
                    'subscription_type'     => (string)$subscription->subscriptionDuration->name ?? '',
                    'subscription_duration' => (string)$subscription->duration ?? '',
                    'total_price'           => (string)$subscription->price ?? '',
                    'discount_type'         => (string)$subscription->discountType->name ?? '',
                    'discount_given'        => (string)$subscription->discount ?? '',
                    'description'           => (string)$subscription->description ?? '',
                ];

                array_push($subscriptionData, $data);
            }
            # return response
            return response()->json([
               'code'      => (string)$this->successStatus, 
               'message'   => 'Subscription Found.',
               'data'      => $subscriptionData
            ]);
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'No Subscription Found.',
               'data'      => []
            ]); 
        }
    }
}
