<?php

namespace App\Http\Controllers\Api\User;

use DB;
use Validator;
use App\Models\User; 
use App\Models\UserOtp; 
use App\Models\Variant; 
use App\Models\Service;
use App\Models\Category; 
use App\Models\SubCategory;
use App\Models\SubCategoryVariants; 

use App\Http\Traits\StatusTrait;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller; 
use App\Http\Controllers\Api\Twilio\SmsController; 

class UserRegisterController extends Controller
{
    use StatusTrait;

    # Variable to Bind Model
    protected $user;

    # bind UserOtp Model
    protected $userOtp;

    # bind UserOtp Model
    protected $variant;

    # bind UserOtp Model
    protected $service;

    # bind Category Model
    protected $category;

    # bind Category Model
    protected $subCategory; 

    # bind Sub Category variant Model
    protected $subCategoryVariant;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        User $user,
        UserOtp $userOtp, 
        Variant $variant, 
        Service $service,
        Category $category, 
        SubCategory $subCategory,
        subCategoryVariants $subCategoryVariant)
    {
        $this->user                     = $user;
        $this->userOtp                  = $userOtp;
        $this->variant                  = $variant;
        $this->service                  = $service;
        $this->category                 = $category;
        $this->subCategory              = $subCategory;
        $this->subCategoryVariant       = $subCategoryVariant;
    }

    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'mobile_number'     => 'required|numeric',
            'device_type'       => 'required|string',
            'device_token'      => 'required|string',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        DB::beginTransaction();

        # Fetch all the inputs
        $input =  $request->all(); 

        # Check if email Already Exist
        $mobileNumber = $input['mobile_number'];

        # check user already Exist on that Email
        $users = $this->user->where('mobile_number', $mobileNumber)->get();

        
        # return response if User already exist on requested Email
        if($users->isNotEmpty()) {
            $otp = mt_rand(1000,9999);

            # fetch the First User
            $user = $users->first();

            # Update User Device Type
            $user->update([
                'device_type'   => $request->get('device_type'), 
                'device_token'  => $request->get('device_token'), 
            ]);

            # Set the success message after User creation 
            $data = [
                'user_id'           =>  (string)$user->id,
                'mobile_number'     =>  (string)$user->mobile_number,
                'otp'               =>  (string)$otp
            ];

            $otpData   = ['user_id' => $user->id, 'otp' => $otp];

            # Create User Otp Model
            $this->userOtp->create($otpData);

             # Instantiate Sms Controller
            #$smsController = new SmsController();

            # Call sendOtp Method
            #$smsController->sendSms($user->mobile_number, $otp);

            DB::commit();
           
            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'User already exist!!!',
                'data'      =>  $data
             ]); 
        } else {
            # Create User Model
            $user =  $this->user->create([
                'mobile_number' => $mobileNumber,
                'device_type'   => $request->get('device_type'),
                'device_token'  => $request->get('device_token'), 
            ]);

            #Set data for User Otp
            $otp    = mt_rand(1000,9999);
            $data   = ['user_id' => $user->id, 'otp' => $otp];

            # Create User Otp Model
            $this->userOtp->create($data);

            # Set the success message after User creation 
            $data = [
                'mobile_number'     =>  $user->mobile_number,
                'userId'            =>  (string)$user->id,
                'otp'               =>  (string)$otp
            ];

            # Instantiate Sms Controller
            #$smsController = new SmsController();

            # Call sendOtp Method
            #$smsController->sendSms($user->mobile, $otp);

            DB::commit();

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'User has been register Successfully!!!',
                'data'      => $data
             ]); 

        }
    }

    /**
     * @method to verify Users Request Otp
     * 
     * @return Otp Verified or Not
     */
    public function verifyOtp(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'mobile_number'     => 'required|numeric',
            'otp'               => 'required|string',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # Fetch all the inputs
        $input =  $request->all(); 

        # Check if email Already Exist
        $mobileNumber = $input['mobile_number'];

        # check user already Exist on that Email
        $users = $this->user
                      ->with('otp')
                      ->where('mobile_number', $mobileNumber)
                      ->get();

        # return response if User already exist on requested Email
        if($users->isNotEmpty()) {
            # fetch User
            $user = $users->first();

            # fetch the Otp of Users
            $userOtps = $user->otp;
            if($userOtps->isNotEmpty() ) {
                $lastOtp = $userOtps->last()->otp;
                if($lastOtp == $request->get('otp')) {
                    $token = $this->generateToken();

                    # Update uSer token
                    $user->update(['api_token' => $token]);

                    # Set the Data
                    $data = [
                        'token'             => $token,
                        'mobile_number'     => $mobileNumber,
                        'user_id'           => $user->id,
                        'first_name'        => $user->first_name??'',
                        'mobile_number'     => $user->mobile_number??'',
                        'email'             => $user->email??'',
                        'latitude'          => $user->latitude??'',
                        'longitude'         => $user->longitude??'',  
                       
                    ];

                    # return response
                    return response()->json([
                        'code'      => (string)$this->successStatus, 
                        'message'   => 'Otp has been Verified.',
                        'data'      => $data
                     ]);
                } else {
                    # return response
                    return response()->json([
                        'code'      => (string)$this->failedStatus, 
                        'message'   => 'Otp does not match.',
                        'data'      => []
                     ]);
                }
            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'Otp Not Found.',
                    'data'      => []
                 ]);
            }
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'User not Found with this Mobile Number.',
                'data'      => []
             ]); 
        }
    }
    
    /**
     * function to generate the Token
     * 
     * @return Token
     */
    public function generateToken()
    {
        # Set the token 
        $token = Str::random(60);

        # Hash Token
        $hashToken = hash('sha256', $token);

        # return the Hash Token 
        return $hashToken;
    }

    /**
     *
     * Block comment
     *
     */
    public function fetchProfile(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id'     => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # fetch the User
        $user = $this->user->find($request->get('user_id'));

        if($user != '') {
            $data = [
                'user_id'           => (string)$user->id ?? '',
                'name'              => (string)$user->first_name ?? '',
                'mobile_number'     => (string)$user->mobile_number ?? '',
                'email'             => (string)$user->email ?? '',
                'age'               => (string)$user->age ?? '',
                'gender'            => (string)$user->gender_string ?? '',
            ];

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'User Found .',
                'data'      => $data
             ]); 
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'User not Found with provided Id.',
                'data'      => []
             ]); 
        }
    }    


        /**
     * @method to Update Provider Profile
     * @param $request
     * @return Json 
     */
    public function updateProfile(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id'     => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # fetch the User
        $user = $this->user->find($request->get('user_id'));

        if($user != '') {
            $emailExist = $this->user
                               ->where('id','<>', $request->get('user_id'))
                               ->where('email',$request->get('email'))
                               ->get();

            if($emailExist->isEmpty()) {
                # Fetch the field
                $data = [
                            'first_name' => $request->get('name') ?? '',
                            'email'      => $request->get('email') ?? '',
                            'gender'     => $request->get('gender') ?? ''
                        ];

                # Update Provider
                $user->update($data);
                $data['user_id'] = $user->id;
                # return response
                    return response()->json([
                        'code'      => (string)$this->successStatus, 
                        'message'   => 'User Data has been Updated',
                        'data'      => $data
                     ]);
                } else {
                    # return response
                    return response()->json([
                        'code'      => (string)$this->failedStatus, 
                        'message'   => 'This Email already in Use.',
                        'data'      => []
                     ]); 
                }
        } else {
             # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'User not Found with provided Id.',
                'data'      => []
             ]); 
        }
    }

        /**
     * @method to update the location of User
     * @param request
     * @return json 
     */
    public function updateLocation(Request $request)
    {
       # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id'       => 'required|numeric',
            'latitude'      => 'required|string',
            'longitude'     => 'required|string',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # fetch the User
        $user = $this->user->find($request->get('user_id'));

        if($user != '') {
            $data = [
                'latitude'  => $request->get('latitude'),
                'longitude' => $request->get('longitude'),
            ];

            # Update the User Location
            $user->update($data);
            $data['user_id'] = $user->id;
            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'User location has been Updated Successfully.',
                'data'      => $data
             ]);
        } else {
             # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'User not Found with User Id.',
                'data'      => []
             ]); 
        }
    }

    /**
     * @method to fetch all the Categories
     * 
     * @return Json data
     */
    public function categories(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # feth the user
        $user = $this->user->find($request->get('user_id'));

        # feth the user
        $user = $this->user
                     ->with(['servicesInCart.service'])
                     ->find($request->get('user_id'));

        # check for user
        if($user != '') {
            $categories = $this->getCategories();
            if($categories->isNotEmpty()) {
                $categoriesData = [];
                foreach ($categories as $key => $category) {
                    $data = [
                        'category_id'   => (string)$category->id,
                        'name'          => (string)$category->name ?? '',
                        'avtar_path'    => (string)$category->avtar_path ?? '',
                    ];

                    array_push($categoriesData, $data);
                }

                # return response
                return response()->json([
                    'code'      => (string)$this->successStatus, 
                    'message'   => 'Categories Found.',
                    'service_count'      => (string)$user->service_count,
                    'data'      => $categoriesData
                 ]); 

            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'No category Found.',
                    'data'      => []
                ]); 
            }
        } else {
           # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'User not Found with provided Id.',
                'data'      => []
             ]); 
        }
    }

    /**
     * fetch all the categories avail in System
     * 
     * @return Category Collection
     */
    public function getCategories()
    {
        # Fetch all the Active Categories
        $categories = $this->category->active()->get();

        # return 
        return $categories;
    }

    /**
     * @method to Get th eSub Catgeories on basis of Category
     * 
     * @return Json
     */
    public function subCategories(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id'       => 'required|numeric',
            'category_id'   => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # Fetch the Category
        $category = $this->category->find($request->get('category_id'));

        # check Category is found or not
        if($category != '') {
            $subCategories = $this->subCategory
                                  ->where('category_id', $request->get('category_id'))
                                  ->with(['variants'])
                                  ->active()
                                  ->get();

            #array to store the subcategories array
            $subCategoryData = [];

            # check for Sub categories
            if($subCategories->isNotEmpty()) {
                foreach ($subCategories as $key => $subCategory) {
                    $variantAvail = 'false';
                    if($subCategory->variants->isNotEmpty()) {
                        $variantAvail = 'true';
                    }
                    $data = [
                        'sub_category_id'       => (string)$subCategory->id ?? '',
                        'variant_available'     => (string)$variantAvail ?? '',
                        'avtar_path'            => $subCategory->avtar_path  ?? '',
                        'sub_category_name'     => $subCategory->name ?? ''
                    ];

                    array_push($subCategoryData, $data);

                }

                #return the success response
                return response()->json([
                    'code'      => (string)$this->successStatus, 
                    'message'   => 'Sub Categories Found.',
                    'data'      => $subCategoryData
                ]); 
            } else {
                # return response
                return response()->json([
                   'code'      => (string)$this->failedStatus, 
                   'message'   => 'No Sub category Found on provided category id',
                   'data'      => []
                ]); 
            }
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'No category Found on provide category id',
               'data'      => []
            ]); 
        }
    }

    /**
     * @method to Get the Sub Catgeory info on basis of Sub-category id
     * 
     * @return Json
     */
    public function subCategoryInfo(Request $request)
    {
         # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id'           => 'required|numeric',
            'sub_category_id'   => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # feth the user
        $user = $this->user->find($request->get('user_id'));

        # relations
        $relations = [
            'banners', 
            'expertise', 
            'services.banners', 
            'services.bulletPoints', 
            'variants.services'
        ];

        # Fetch the Category
        $subCategory = $this->subCategory
                            ->with($relations)
                            ->find($request->get('sub_category_id'));

        # check Sub-category is found or not
        if($user != '') {
            if($subCategory != '') {
                # fetch all the Banners of Sub Categories.
                $subcategoryBanners = $subCategory->banners;

                # fetch the Sub Category Expertise
                $subcategoryExpertises = $subCategory->expertise;

                # fetch the Variants of Sub Category
                $subCategoryVariants = $subCategory->variants;

                # fetch all the Banners of Sub categories
                $subCatgeorybannersData = [];
                foreach ($subcategoryBanners as $key => $subcategoryBanner) {
                    $data = [
                        'avtar_name' => (string)$subcategoryBanner->avtar_name ?? '',
                        'avtar_path' => (string)$subcategoryBanner->avtar_path ?? ''
                    ];

                    # push Data
                    array_push($subCatgeorybannersData, $data);
                }

                # fetch all the Expertise of Sub categories
                $subCatgeoryExpertiseData = [];
                foreach ($subcategoryExpertises as $key => $subcategoryExpertise) {
                    $data = [
                        'title' => (string)$subcategoryExpertise->title ?? '',
                    ];

                    # push Data
                    array_push($subCatgeoryExpertiseData, $data);
                }

                # get Variants and Services
                $variantAvailable = 'false';
                $variantData = [];
                $apiResponseData = [];
                if($subCategoryVariants->isNotEmpty()) {
                    $variantAvailable = 'true';
                    foreach ($subCategoryVariants as $key => $subCategoryVariant) {
                       $data = [
                            'variant_id'        => (string)$subCategoryVariant->id ?? '',
                            'title_or_subject'  => (string)$subCategoryVariant->title_or_subject ?? '',
                            'label'             => (string)$subCategoryVariant->label ?? '',
                       ];

                       #push Variant data
                       array_push($variantData, $data);
                    }
                } 

                # Set Api Response Data
                $apiResponseData = [
                    'sub_category_id'   => (string)$subCategory->id ?? '',
                    'name'              => (string)$subCategory->name ?? '',
                    'title'             => (string)$subCategory->title ?? '',
                    'discount_label'    => (string)$subCategory->discount_label ?? '',
                    'banners'           => $subCatgeorybannersData ?? '',
                    'expertise_in'      => $subCatgeoryExpertiseData ?? '',
                    'expertise_in'      => $subCatgeoryExpertiseData ?? '',
                    'variant_available' => (string)$variantAvailable ?? '',
                    'variant_data'      => $variantData ?? '',
                ];

                # return response
                return response()->json([
                   'code'      => (string)$this->successStatus, 
                   'message'   => 'All data related to Sub category.',
                   'data'      => $apiResponseData
                ]);
            } else {
                # return response
                return response()->json([
                   'code'      => (string)$this->failedStatus, 
                   'message'   => 'No Sub Category Found on Provided Id.',
                   'data'      => []
                ]); 
            }
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'No User Found on Provided User Id',
               'data'      => []
            ]); 
        }
    }

    /**
     * fetch all the Sub-categories avail in System
     * 
     * @return Sub Category Collection
     */
    public function getSubCategories($id)
    {
        # Set Relation
        $relations = ['subCategoryBanner', 'subCategoryExpertise'];

        # Fetch all the Active Categories
        $subCategories = $this->subCategory->with($relations)->find($id);

        # return 
        return $subCategories;
    }
}
