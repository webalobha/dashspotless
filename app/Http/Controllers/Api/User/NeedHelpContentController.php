<?php

namespace App\Http\Controllers\Api\User;

use DB;
use Validator;
use App\Models\User; 
use App\Models\NeedHelpStaticContent;
use App\Models\ServiceSettingRescheduleData;
use App\Models\ServiceSettingCancellationData;

use App\Http\Traits\StatusTrait;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller; 

class NeedHelpContentController extends Controller
{
  use StatusTrait;

  # Variable to Bind Model
  protected $user;

  # Variable to Bind Model NeedHelpStaticContent
  protected $needHelpStaticContent;

  # Variable to Bind Model ServiceSettingRescheduleData
  protected $serviceSettingRescheduleData;

  # Variable to Bind Model ServiceSettingCancellationData
  protected $serviceSettingCancellationData;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct(User $user, NeedHelpStaticContent $needHelpStaticContent,
    ServiceSettingCancellationData $serviceSettingCancellationData,
    ServiceSettingRescheduleData $serviceSettingRescheduleData)
  {
    $this->user                               = $user;
    $this->needHelpStaticContent              = $needHelpStaticContent;
    $this->serviceSettingRescheduleData       = $serviceSettingRescheduleData;
    $this->serviceSettingCancellationData     = $serviceSettingCancellationData;
  }

  /**
   * @method to Fetch fetchNeedHelpContent
   * @param Request $request
   * @return Address Json
   */
  public function fetchNeedHelpContent(Request $request) 
  {
    # Validate request data
    $validator = Validator::make($request->all(), [ 
        'user_id' => 'required|numeric',
    ]);

    # If validator fails return response
    if ($validator->fails()) { 
        return response()->json(['error'=>$validator->errors()], 401);            
    }

    # feth the user
    $user = $this->user
                 ->find($request->get('user_id'));

    # Fetch the Remaining Amount of User Wallet
    if($user != '') {
      # Fetch all the Need Help Content
      $needHelpContent = $this->needHelpStaticContent->active()->user()->get();

      $contentArray = [];
      if($needHelpContent->isNotEmpty()){
        foreach ($needHelpContent as $key => $content) {
          $data = [
            'id'      => (string)$content->id ?? '',
            'title'   => (string)$content->title ?? '',
            'content' => (string)$content->content ?? '',
          ];

          array_push($contentArray, $data);
        }

        # return response
        return response()->json([
           'code'           => (string)$this->successStatus, 
           'message'        => 'Content Found',
           'data'           => $contentArray
        ]); 
      } else {
        # return response
        return response()->json([
           'code'           => (string)$this->failedStatus, 
           'message'        => 'No Content Found',
           'data'           => []
        ]);  
      }
      
    } else {
      # return response
      return response()->json([
         'code'      => (string)$this->failedStatus, 
         'message'   => 'No User Found.',
         'data'      => []
      ]); 
    }
  }

   /**
   * @method to Fetch fetchNeedHelpContent
   * @param Request $request
   * @return Address Json
   */
  public function fetchProviderHelpContent(Request $request) 
  {
    # Fetch the Remaining Amount of User Wallet
    # Fetch all the Need Help Content
    $needHelpContent = $this->needHelpStaticContent->active()->provider()->get();

    $contentArray = [];
    if($needHelpContent->isNotEmpty()){
      foreach ($needHelpContent as $key => $content) {
        $data = [
          'id'      => (string)$content->id ?? '',
          'title'   => (string)$content->title ?? '',
          'content' => (string)$content->content ?? '',
        ];

        array_push($contentArray, $data);
      }

      # return response
      return response()->json([
         'code'           => (string)$this->successStatus, 
         'message'        => 'Content Found',
         'data'           => $contentArray
      ]); 
    } else {
      # return response
      return response()->json([
         'code'           => (string)$this->failedStatus, 
         'message'        => 'No Content Found',
         'data'           => []
      ]);  
    }
  }

  /**

   * @method to Fetch fetchCancellationConditions
   * @param Request $request
   * @return Address Json
   */
  public function fetchCancellationConditions(Request $request) 
  {
    # Fetch all the Cancellations Conditions
    $cancellationSettings = $this->serviceSettingCancellationData->get();

    if($cancellationSettings->isNotEmpty()) {
      $cancellationSettingArray = [];
      foreach ($cancellationSettings as $key => $setting) {
        $data = [
          'id'              => (string)$setting->id ?? '',
          'description'     => (string)$setting->description ?? '',
          'penalty_amount'  => (string)$setting->amount ?? '',
        ];

        array_push($cancellationSettingArray, $data);
      }
       # return response
      return response()->json([
         'code'      => (string)$this->successStatus, 
         'message'   => 'Cancellation Condition Found.',
         'data'      => $cancellationSettingArray
      ]); 
    } else {
      # return response
      return response()->json([
         'code'      => (string)$this->failedStatus, 
         'message'   => 'Cancellation Condition not Found.',
         'data'      => []
      ]); 
    }
  }

  /**
   * @method to Fetch fetchCancellationConditions
   * @param Request $request
   * @return Address Json
   */
  public function fetchRescheduleconditions(Request $request) 
  {
    # Fetch all the Reschhedule Conditions
    $rescheduleSettings = $this->serviceSettingRescheduleData->get();

    if($rescheduleSettings->isNotEmpty()) {
      $rescheduleSettingArray = [];
      foreach ($rescheduleSettings as $key => $setting) {
        $data = [
          'id'              => (string)$setting->id ?? '',
          'description'     => (string)$setting->description ?? '',
          'penalty_amount'  => (string)$setting->amount ?? '',
        ];

        array_push($rescheduleSettingArray, $data);
      }
       # return response
      return response()->json([
         'code'      => (string)$this->successStatus, 
         'message'   => 'Reschedule Condition Found.',
         'data'      => $rescheduleSettingArray
      ]); 
    } else {
      # return response
      return response()->json([
         'code'      => (string)$this->failedStatus, 
         'message'   => 'Reschedule Condition not Found.',
         'data'      => []
      ]); 
    }
  } 

  /**
   * @method to Fetch fetchProviderHelpContent
   * @param Request $request
   * @return Json
   */
 /* public function fetchProviderHelpContent(Request $request) 
  {
    # Fetch all the Reschhedule Conditions
    $rescheduleSettings = $this->serviceSettingRescheduleData->get();

    if($rescheduleSettings->isNotEmpty()) {
      $rescheduleSettingArray = [];
      foreach ($rescheduleSettings as $key => $setting) {
        $data = [
          'id'              => (string)$setting->id ?? '',
          'description'     => (string)$setting->description ?? '',
          'penalty_amount'  => (string)$setting->amount ?? '',
        ];

        array_push($rescheduleSettingArray, $data);
      }
       # return response
      return response()->json([
         'code'      => (string)$this->successStatus, 
         'message'   => 'Reschedule Condition Found.',
         'data'      => $rescheduleSettingArray
      ]); 
    } else {
      # return response
      return response()->json([
         'code'      => (string)$this->failedStatus, 
         'message'   => 'Reschedule Condition not Found.',
         'data'      => []
      ]); 
    }
  }*/
}
