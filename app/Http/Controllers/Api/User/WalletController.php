<?php

namespace App\Http\Controllers\Api\User;

use DB;
use Validator;
use App\Models\User; 
use App\Models\UserWallet; 
use App\Models\UserAddress; 
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Traits\StatusTrait;
use App\Models\SubCategoryVariants; 
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller; 

class WalletController extends Controller
{
  use StatusTrait;

  # Variable to Bind Model
  protected $user;

  # Variable to Bind Model UserWallet
  protected $userWallet;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct(User $user, UserWallet $userWallet)
  {
    $this->user         = $user;
    $this->userWallet   = $userWallet;
  }

  /**
   * @method to Fetch Remaining Amount in Wallet
   * @param Request $request
   * @return Address Json
   */
  public function fetchRemainingAmount(Request $request) 
  {
    # Validate request data
    $validator = Validator::make($request->all(), [ 
        'user_id' => 'required|numeric',
    ]);

    # If validator fails return response
    if ($validator->fails()) { 
        return response()->json(['error'=>$validator->errors()], 401);            
    }

    # feth the user
    $user = $this->user
                 ->with(['wallet'])
                 ->find($request->get('user_id'));

    # Fetch the Remaining Amount of User Wallet
    if($user != '') {
      # return response
      return response()->json([
         'code'           => (string)$this->successStatus, 
         'message'        => 'Wallet Remaining Amount',
         'walletAmount'   => (string)$user->remaining_wallet_amount,
         'data'           => []
      ]); 
    } else {
      # return response
      return response()->json([
         'code'      => (string)$this->failedStatus, 
         'message'   => 'No User Found.',
         'data'      => []
      ]); 
    }
  }

  /**
   * @method to Fetch Wallet History
   * @param Request $request
   * @return Address Json
   */
  public function fetchHistory(Request $request) 
  {
    # Validate request data
    $validator = Validator::make($request->all(), [ 
        'user_id' => 'required|numeric',
    ]);

    # If validator fails return response
    if ($validator->fails()) { 
        return response()->json(['error'=>$validator->errors()], 401);            
    }

    # feth the user
    $user = $this->user
                 ->with(['wallet'])
                 ->find($request->get('user_id'));

    # Fetch the Remaining Amount of User Wallet
    if($user != '') {
      # fetch Wallet Transaction
      $walletTransactions = $user->wallet;

      if($walletTransactions->isNotEmpty()) {
        $walletTransactionArray = [];
        $$walletTransactions = $$walletTransactions->sortByDesc('id');
        foreach ($walletTransactions as $key => $walletTransaction) {
          $data = [
            'id'                => (string)$walletTransaction->id ?? '',
            'transaction_type'  => ($walletTransaction->transaction_type == 1) ? 'Debit' :  'Credit',
            'amount'            => (string)$walletTransaction->amount ?? '',
            'date'              => (string)$walletTransaction->created_at->format('d M Y') ?? '',
            'time'              => (string)$walletTransaction->created_at->format('h:i A') ?? '',
          ];

          array_push($walletTransactionArray, $data);
        }

        # return response
        return response()->json([
           'code'           => (string)$this->successStatus, 
           'message'        => 'Wallet Transaction History found',
           'walletAmount'   => (string)$user->remaining_wallet_amount,
           'data'           => $walletTransactionArray
         ]);
      } else {
        # return response
        return response()->json([
           'code'           => (string)$this->failedStatus, 
           'message'        => 'No Wallet Transaction History found',
           'walletAmount'   => (string)0,
           'data'           => []
         ]); 
      }
     
    } else {
      # return response
      return response()->json([
         'code'      => (string)$this->failedStatus, 
         'message'   => 'No User Found.',
         'data'      => []
      ]); 
    }
  }
}
