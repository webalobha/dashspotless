<?php

namespace App\Http\Controllers\Api\User;

use DB;
use Validator;
use App\Models\User; 
use App\Models\UserOtp; 
use App\Models\Variant; 
use App\Models\Service;
use App\Models\Category; 
use App\Models\ServiceCart; 
use App\Models\SubCategory;
use App\Models\SubCategoryBanner;
use App\Models\SubCategoryVariants; 

use App\Http\Traits\StatusTrait;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller; 

class ServiceController extends Controller
{
    use StatusTrait;

    # Variable to Bind Model
    protected $user;

    # bind UserOtp Model
    protected $variant;

    # bind UserOtp Model
    protected $service;

    # bind Service Cart Model
    protected $serviceCart;

    # bind Category Model
    protected $category;

    # bind Category Model
    protected $subCategory;  

    # bind subCategoryVariant Model
    protected $subCategoryVariant; 

    # bind SubCategoryBanner Model
    protected $subCategoryBanner;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        User $user,
        Variant $variant, 
        Service $service,
        Category $category, 
        ServiceCart $serviceCart,
        SubCategory $subCategory,
        SubCategoryBanner $subCategoryBanner,
        subCategoryVariants $subCategoryVariant)
    {
        $this->user                     = $user;
        $this->variant                  = $variant;
        $this->service                  = $service;
        $this->category                 = $category;
        $this->serviceCart              = $serviceCart;
        $this->subCategory              = $subCategory;
        $this->subCategoryBanner        = $subCategoryBanner;
        $this->subCategoryVariant       = $subCategoryVariant;
    }

    /**
     * @method to get Services on Sub category
     * @param Request $request
     * @return Service json
    */
    public function subCategoryServices(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id'            => 'required|numeric',
            'sub_category_id'    => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # feth the user
        $user = $this->user
                    ->with(['servicesInCart.service'])
                    ->find($request->get('user_id'));

        # relations
        $relations = [
            'variants',
            'services.banners', 
            'services.bulletPoints', 
        ];

        # Fetch the Category
        $subCategory = $this->subCategory
                            ->with($relations)
                            ->find($request->get('sub_category_id'));

        # check Sub-category is found or not
        if($user != '') {
            if($subCategory != '') {
                # sub category baneers
                $banners = $subCategory->banners;

                # fetch the variants
                $variants = $subCategory->variants;
                if($variants->isEmpty()) {
                    # Fetch all Services on Sub Category
                    $serviceData = [];
                    $services = $subCategory->services;

                    # fetch The Active Service Only
                    $activeService = $services->filter(function($service) {
                                                if($service->status) {
                                                    return $service;
                                                }
                    });
                    foreach ($activeService as $key => $service) {
                        # fetch the Bullet Point related Data belongs to Service
                        $bulletPointsData = [];
                        $bulletPoints = $service->bulletpoints;
                        foreach ($bulletPoints as $key => $bulletPoint) {
                            $data = [
                                'id'            => (string)$bulletPoint->id ?? '',
                                'title'         => (string)$bulletPoint->title ?? '',
                                'description'   => (string)$bulletPoint->description ?? '',
                            ];

                            # Push the Service banners Data
                            array_push($bulletPointsData, $data);
                        }

                        # Set data for Service
                        $data = [
                            'service_id'            => (string)$service->id ?? '',
                            'category_id'           => (string)$service->category_id ?? '',
                            'sub_category_id'       => (string)$service->sub_category_id ?? '',
                            'title'                 => (string)$service->title ?? '',
                            'description'           => (string)$service->description ?? '',
                            'price'                 => (string)$service->price ?? '', 
                            'discount_type'         => (string)$service->discount_type ?? '', 
                            'discount'              => (string)$service->discount_given ?? '',
                            'discounted_price'      => (string)$service->final_price_of_service ?? '',
                            'time_taken_minutes'    => (string)$service->time_taken_minutes ?? '', 
                            'tool_tip_content'      => (string)$service->tool_tip_content ?? '',
                            'icon_path'      =>         $service->icon_path ?? '',
                            'bullet_points'         =>  $bulletPointsData ?? [],
                            #'banners'               =>  $bannerData ?? [],
                        ];
                       
                       # Push Service data
                        array_push($serviceData, $data);
                    }

                    # merge banners inside Data
                    $bannerImage = $banners->isNotEmpty() ? 
                                    (string)$banners->last()->avtar_path : '';

                    $user = $this->user
                             ->with(['servicesInCart.service'])
                             ->find($request->get('user_id'));

                    # return response
                    return response()->json([
                       'code'               => (string)$this->successStatus, 
                       'message'            => 'Services Data on Sub category',
                       'service_count'      => (string)$user->service_count,
                       'actual_amount'      => (string)$user->actual_amount_of_services,
                       'discount'           => (string)$user->total_discount_on_services,
                       'discounted_amount'  => (string)$user->services_final_price_in_cart,
                       'banner_image'       => $bannerImage,
                       'data'               => array_values($serviceData)
                    ]); 
                } else {
                    # return response
                    return response()->json([
                       'code'      => (string)$this->failedStatus, 
                       'message'   => 'Variant Available on this Sub Category.',
                       'data'      => []
                    ]); 
                }
            } else {
                # return response
                return response()->json([
                   'code'      => (string)$this->failedStatus, 
                   'message'   => 'No Sub Category Found on Provided Id.',
                   'data'      => []
                ]); 
            }
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'No User Found on Provided User Id',
               'data'      => []
            ]); 
        }
    }

    /**
     * @method to get Services on Sub category
     * @param Request $request
     * @return Service json
     */
    public function variantServices(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id'            => 'required|numeric',
            'variant_id'         => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # feth the user
        $user = $this->user
                     ->with(['servicesInCart.service'])
                     ->find($request->get('user_id'));

        # relations
        $relations = [
            'subCategory.banners',
            'services.banners', 
            'services.bulletPoints', 
        ];

        # Fetch the Category
        $variant = $this->subCategoryVariant
                            ->with($relations)
                            ->find($request->get('variant_id'));

        # check Sub-category is found or not
        if($user != '') {
            if($variant != '') {
                # fetch banners
                $banners = $variant->subCategory ? $variant->subCategory->banners : collect([]);

                # Fetch all Services on Sub Category
                $serviceData = [];
                $services = $variant->services;
                if($services->isNotEmpty()) {
                    foreach ($services as $key => $service) {
                        # fetch the Bullet Point related Data belongs to Service
                        $bulletPointsData = [];
                        $bulletPoints = $service->bulletpoints;
                        foreach ($bulletPoints as $key => $bulletPoint) {
                            $data = [
                                'id'            => (string)$bulletPoint->id ?? '',
                                'title'         => (string)$bulletPoint->title ?? '',
                                'description'   => (string)$bulletPoint->description ?? '',
                            ];

                            # Push the Service banners Data
                            array_push($bulletPointsData, $data);
                        }

                        # Set data for Service
                        $data = [
                            'service_id'            => (string)$service->id ?? '',
                            'category_id'           => (string)$service->category_id ?? '',
                            'sub_category_id'       => (string)$service->sub_category_id ?? '',
                            'title'                 => (string)$service->title ?? '',
                            'description'           => (string)$service->description ?? '',
                            'price'                 => (string)$service->price ?? '', 
                            'discount_type'         => (string)$service->discount_type ?? '', 
                            'discount'              => (string)$service->discount_given ?? '', 
                            'discounted_price'      => (string)$service->final_price_of_service ?? '', 
                            'time_taken_minutes'    => (string)$service->time_taken_minutes ?? '', 
                            'tool_tip_content'      => (string)$service->tool_tip_content ?? '',
                            'icon_path'             => (string)$service->icon_path ?? '',
                            'bullet_points'         =>  $bulletPointsData ?? [],
                            #'banners'               =>  $bannerData ?? [],
                        ];
                       
                       # Push Service data
                        array_push($serviceData, $data);
                    }

                    # merge banners inside Data
                    $bannerImage = $banners->isNotEmpty() ? 
                                    (string)$banners->last()->avtar_path : '';

                    $user = $this->user
                             ->with(['servicesInCart.service'])
                             ->find($request->get('user_id'));
                                                   
                    # return response
                    return response()->json([
                       'code'               => (string)$this->successStatus, 
                       'message'            => 'Services Data on Sub category',
                       'variant_label'     => (string)$variant->label ?? '',
                       'service_count'      => (string)$user->service_count,
                       'actual_amount'      => (string)$user->actual_amount_of_services,
                       'discount'           => (string)$user->total_discount_on_services,
                       'discounted_amount'  => (string)$user->services_final_price_in_cart,
                       'banner_image'       => $bannerImage,
                       'data'               => array_values($serviceData) ?? []
                    ]); 
                } else {
                    # return response
                    return response()->json([
                       'code'      => (string)$this->failedStatus, 
                       'message'   => 'No Service Found on variant.',
                       'data'      => []
                    ]); 
                }
            } else {
                # return response
                return response()->json([
                   'code'      => (string)$this->failedStatus, 
                   'message'   => 'No variant Found on Provided Id.',
                   'data'      => []
                ]); 
            }
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'No User Found on Provided User Id',
               'data'      => []
            ]); 
        }
    }
    
    /**
     * @method to get all the Services of User in Cart
     * @param Request $request
     * @return Services in Cart
     */
    public function getServiceIncart(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # feth the user
        $user = $this->user
                     ->with(['servicesInCart.service'])
                     ->find($request->get('user_id'));

        # check Usewr is found or not
        if($user != '') {
            # fetch user Services
            $servicesInCart = $user->servicesInCart;
            $serviceData = [];

            # If service is Not Empty
            if($servicesInCart->isNotEmpty()) {
                foreach ($servicesInCart as $key => $serviceInCart) {
                   $service = $serviceInCart->service;

                   $data = [
                        'service_id'            => (string)$service->id ?? '',
                        'service_cart_id'       => (string)$serviceInCart->id ?? '',
                        'title'                 => (string)$service->title ?? '',
                        'description'           => (string)$service->description ?? '',
                        'actual_price'          => (string)$service->price ?? '',
                        'quantity'              => (string)$serviceInCart->quantity ?? '',
                        'discount'              => (string)$service->discount_given ?? '',
                        'discounted_price'      => (string)$service->final_price_of_service ?? '',
                        'time_taken_minutes'    => (string)$service->time_taken_minutes ?? '',
                        'tool_tip_content'      => (string)$service->tool_tip_content ?? '',
                   ];

                   # push Data in Service Data
                   array_push($serviceData, $data);
                }

                $user = $this->user
                             ->with(['servicesInCart.service'])
                             ->find($request->get('user_id'));
                # return response
                return response()->json([
                   'code'                   => (string)$this->successStatus, 
                   'message'                => 'Services in Cart.',
                   'service_count'          => (string)$user->service_count,
                   'actual_amount'          => (string)$user->actual_amount_of_services,
                   'discount'               => (string)$user->total_discount_on_services,
                   'discounted_amount'      => (string)$user->services_final_price_in_cart,
                   'subscription_avilable'  => $user->valid_subscriptions ? "true" : "false",
                   'data'                   => $serviceData
                ]);
            } else {
                # return response
                return response()->json([
                   'code'      => (string)$this->failedStatus, 
                   'message'   => 'Cart is Empty.',
                   'data'      => []
                ]); 
            }
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'No User Found on Provided User Id',
               'data'      => []
            ]); 
        }     
    }

    /**
     * @method to update Cart Quantity
     * @param Request $request
     * @return json Cart Data
     */
    public function updateCartQuantity(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id'            => 'required|numeric',
            'service_cart_id'    => 'required|numeric',
            'quantity'           => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # feth the user
        $user = $this->user
                     ->with(['servicesInCart.service'])
                     ->find($request->get('user_id'));

        # fetch the Service in Cart
        $serviceInCart = $this->serviceCart
                              ->find($request->get('service_cart_id'));

         # check Sub-category is found or not
        if($user != '') {
          if($serviceInCart != '') {
            $serviceInCart->update(['quantity' => $request->get('quantity')]);

            $user = $this->user
                     ->with(['servicesInCart.service'])
                     ->find($request->get('user_id'));

            # return response
            return response()->json([
               'code'                   => (string)$this->successStatus, 
               'message'                => 'Quantity Updated.',
               'actual_amount'          => (string)$user->actual_amount_of_services,
               'discount'               => (string)$user->total_discount_on_services,
               'discounted_amount'      => (string)$user->services_final_price_in_cart,
               'data'                   => []
            ]); 
          } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'No Service Found in cart type Provided.',
               'data'      => []
            ]); 
          }
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'No User Found on Provided User Id',
               'data'      => []
            ]); 
        }
    }
    
    /**
     * @method to get Services on Sub category
     * @param Request $request
     * @return Service json
     */
    public function addServiceToCart(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id'            => 'required|numeric',
            'service_id'         => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # feth the user
        $user = $this->user
                     ->with(['servicesInCart.service.subCategory'])
                     ->find($request->get('user_id'));

        # fetch the SubcategoryId of Requested Service_id
        $service = $this->service
                        ->with('subCategory')
                        ->find($request->get('service_id'));

        if($service != '') {
            $subCategoryIdRequestedService = $service->subCategory->id;
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'Service Not Found.',
               'data'      => []
            ]); 
        }

        # Fetch SubCategory Of Previous Added Service
        $servicesInCart = $user->servicesInCart;
        $subCategoryIds = [];
        if($servicesInCart->isNotEmpty()) {
            foreach ($servicesInCart as $key => $serviceInCart) {
                $subCategoryId = $serviceInCart->service->subCategory->id;
                array_push($subCategoryIds, $subCategoryId);
            }
            if(!in_array($subCategoryIdRequestedService, $subCategoryIds)) {
                $message = 'A different kind of Service already Exist in cart. Please complete it or Remove it from cart.';
                # return response
                return response()->json([
                   'code'      => (string)$this->failedStatus, 
                   'message'   => $message,
                   'data'      => []
                ]); 
            }
        }
        # relations
        $relations = [
            
        ];

        # check Sub-category is found or not
        if($user != '') {
           $serviceInCart = $this->serviceCart
                                        ->where('user_id', $user->id)
                                        ->where('service_id', $request->get('service_id'))
                                        ->get();

            if($serviceInCart->isEmpty()) {
                $data = [
                    'user_id'       => $request->get('user_id'),
                    'quantity'      => 1,
                    'service_id'    => $request->get('service_id'),
                ];

                # create service inside User Cart
                $this->serviceCart->create($data);

                $user = $this->user
                             ->with(['servicesInCart.service'])
                             ->find($request->get('user_id'));

                # return response
                return response()->json([
                   'code'           => (string)$this->successStatus, 
                   'message'        => 'Added to cart.',
                   'service_count'  => (string)$user->service_count,
                   'service_price'  => (string)$user->services_final_price_in_cart,
                   'data'           => []
                ]); 
            } else {
               # return response
                return response()->json([
                   'code'               => (string)$this->failedStatus, 
                   'message'            => 'Already in Cart.',
                   'service_count'      => (string)$user->service_count,
                   'service_price'      => (string)$user->services_final_price_in_cart,
                   'data'               => []
                ]);  
            }
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'No User Found on Provided User Id',
               'data'      => []
            ]); 
        }
    }

    /**
     * @method to give Serach Result 
     * @param Request $request
     * @return Search Json | Empty 
     */
    public function searchService(Request $request)
    {
        # fetch search Term
        $searchTerm = $request->get('search_term');

        # fetch the Services
        $services = $this->service->where('title', 'like', '%'.$searchTerm.'%')->get();
        
        if($services->isNotEmpty()) {
            $searchData = [];
            foreach ($services as $key => $service) {
                $data = [
                    'service_id'    => (string)$service->id,
                    'name'          => (string)$service->title ?? '',
                    'actual_price'  => (string)$service->price ?? '',
                     'icon_path'      =>       $service->icon_path ?? '',
                ];

                array_push($searchData, $data);
            }
            
            # return response
            return response()->json([
               'code'      => (string)$this->successStatus, 
               'message'   => 'Search Term Found.',
               'data'      => $searchData
            ]);
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'Search Term not match.',
               'data'      => []
            ]); 
        }
    }

    /**
     * @method to get ServiceDetail of a service
     * @param Request $request
     * @return Json | Empty
     */
    public function serviceDetail(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'service_id' => 'required|numeric',
            'user_id'    => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # feth the user
        $user = $this->user
                     ->with(['servicesInCart.service.subCategory'])
                     ->find($request->get('user_id'));

        # fetch the SubcategoryId of Requested Service_id
        $service = $this->service
                        ->with(['subCategory', 'bulletPoints'])
                        ->find($request->get('service_id'));
                        
        if($user != '') {
            if($service != '') {
                $bulletPointsData = [];
                if($service->bulletpoints->isNotEmpty()) {
                    foreach ($service->bulletpoints as $key => $bulletpoint) {
                        $data = [
                            'id'            => (string)$bulletpoint->id ?? '',
                            'title'         => (string)$bulletpoint->title ?? '',
                            'description'   => (string)$bulletpoint->description ?? '',
                        ];

                        array_push($bulletPointsData, $data);
                    }
                }

                $data = [
                    'id'                => (string)$service->id,
                    'subCategory_name'  => (string)$service->subCategory->name,
                    'service_count'     => (string)$user->service_count,
                    'title'             => (string)$service->title ?? '',
                    'icon_path'         => (string)$service->icon_path ?? '',
                    'description'       => (string)$service->description ?? '',
                    'actual_price'      => (string)$service->price ?? '',
                    'discount_given'    => (string)$service->discount_given ?? '',
                    'discounted_price'  => (string)$service->final_price_of_service ?? '',
                    'time_taken'        => (string)$service->time_taken_minutes ?? '',
                    'tool_tip_content'  => (string)$service->tool_tip_content ?? '',
                    'bullet_points'     => $bulletPointsData,
                ];

                # return response
                return response()->json([
                   'code'      => (string)$this->successStatus, 
                   'message'   => 'Service Found.',
                   'data'      => $data
                ]); 
            } else {
                # return response
                return response()->json([
                   'code'      => (string)$this->failedStatus, 
                   'message'   => 'Service Not Found.',
                   'data'      => []
                ]); 
            }
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'User Not Found.',
               'data'      => []
            ]);
        }
       
    }
    
    

    /**
     * @method to get Services on Sub category
     * @param Request $request
     * @return Service json
     */
    public function deleteServiceFromCart(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id'            => 'required|numeric',
            'service_id'         => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # feth the user
        $user = $this->user
                     ->with(['servicesInCart'])
                     ->find($request->get('user_id'));

        # relations
        $relations = [
            
        ];

        # check Sub-category is found or not
        if($user != '') {
           $serviceInCart = $this->serviceCart
                                        ->where('user_id', $user->id)
                                        ->where('service_id', $request->get('service_id'))
                                        ->get();

            if($serviceInCart->isNotEmpty()) {
                $serviceInCart->first()->delete();

                # return response
                return response()->json([
                   'code'      => (string)$this->successStatus, 
                   'message'   => 'Deleted from cart.',
                   'data'      => []
                ]); 
            } else {
               # return response
                return response()->json([
                   'code'      => (string)$this->failedStatus, 
                   'message'   => 'Service Not Found Inside Cart.',
                   'data'      => []
                ]);  
            }
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'No User Found on Provided User Id',
               'data'      => []
            ]); 
        }
    }

    /**
     * @method to ftehc the Sub categories Banners
     * @param Request $request
     * @return json
     */
    public function fetchBanners(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id'   => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # feth the user
        $user = $this->user
                     ->find($request->get('user_id'));

        if($user != '') {
            #FEtch all the Banners Image
            $allBanners = $this->subCategoryBanner->all();
            if($allBanners->isNotEmpty()) {
                $bannerArray = [];
                $bannerGrouped = $allBanners->groupBy('sub_category_id');
                foreach ($bannerGrouped as $key => $banner) {
                   $banner = $banner->first();
                   $data = [
                    'id'            => (string)$banner->id ?? '',
                    'avtar_name'    => (string)$banner->avtar_name ?? '',
                    'avtar_path'    => (string)$banner->avtar_path ?? '',
                   ];

                   array_push($bannerArray, $data);
                }


                # return response
                return response()->json([
                   'code'      => (string)$this->successStatus, 
                   'message'   => 'Banner found',
                   'data'      => $bannerArray
                ]);
            } else{
                # return response
                return response()->json([
                   'code'      => (string)$this->failedStatus, 
                   'message'   => 'No banner found',
                   'data'      => []
                ]); 
            }
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'No User Found on Provided User Id',
               'data'      => []
            ]); 
        }
    }
    
}
