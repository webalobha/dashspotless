<?php

namespace App\Http\Controllers\Api\User;

use Exception;
use App\Models\User; 
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller; 

class NotifyUser extends Controller
{
  # Variable to Bind Model
  protected $user;

  # Variable to Bind title
  protected $title;

  # Variable to Bind message
  protected $message;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
  }

  public function notify($user, $title = '', $message= '')
  {
    # define the Server Api Key for Sending N
    define('SERVER_API_KEY', env('SERVER_ANDROID_USER_API_KEY','AAAA71GSrtg:APA91bExULGMYeGSLmxlhcq7RDsu6MOLf2h15OL6JNjOCLw0GvKfuhVK8AABpOdjVhBwBbu2K8SYagM-gneZHJN2e2hxV1ae4fZWufrRRwrc3DwNaazkWVV1mah56hr97Qc7X9BTywr6'));


    # Fetch The User Device Token
    $tokens = [$user->device_token];

    #dd($tokens, $user, $title, $message);
    # Set th Header
    $header = [ 
      'Authorization: Key=' . SERVER_API_KEY,
      'Content-Type: Application/json' 
    ];

    # Set the Message
    $msg = [
      'title' => (string)$title,
      'body'  => (string)$message,
      'icon'  => asset('logo/logo.PNG'),
      'image' => asset('logo/logo.PNG'),
    ];
    #dd($msg);
    # Set the Payload
    $payload = [
      'registration_ids'  => $tokens,
      'data'              => $msg,
    ];

    # initialize the Curl
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($payload),
      CURLOPT_HTTPHEADER => $header
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return ;
  }
}
