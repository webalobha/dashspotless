<?php

namespace App\Http\Controllers\Api\User;

use DB;
use Validator;
use Carbon\Carbon;

use App\Models\User; 
use App\Models\Order; 
use App\Models\OrderDispute; 
use App\Models\DisputeReason; 

use App\Http\Traits\StatusTrait;

# Interfaces
use App\Http\Interfaces\OrderDisputeStatusInterface;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller; 

class OrderDisputeController extends Controller implements OrderDisputeStatusInterface
{
    use StatusTrait;

    # Variable to Bind Model
    protected $user;

    # Variable to Bind Model
    protected $order;

    # Variable to Bind Model
    protected $orderDispute; 

    # Variable to Bind Model
    protected $disputeReason;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        User $user, 
        Order $order, 
        OrderDispute $orderDispute,
        DisputeReason $disputeReason)
    {
        $this->user              = $user;
        $this->order             = $order;
        $this->orderDispute      = $orderDispute;
        $this->disputeReason     = $disputeReason;
    }

    /**
     * @method to get Services on Sub category
     * @param Request $request
     * @return Service json
    */
    public function fetchDisputesReason(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id'               => 'required|numeric',
            'order_id'              => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # fetch User
        $user = $this->user->find($request->get('user_id'));
        if($user != '') {
            $order = $this->order
                          ->with('disputes')
                          ->find($request->get('order_id'));

            if($order != '') {
                # fetch all te User related Disputes
                $disputeReasons = $this->disputeReason->userType()->get();

                # Fetch if Discpute Raise on 
                $isDisputeRaised = $order->disputes->isNotEmpty() ? "true" : "false";
                
                if($disputeReasons->isNotEmpty()) {
                    $reasonArray = [];
                    foreach ($disputeReasons as $key => $reason) {
                        $data = [
                            'id'    => (string)$reason->id,
                            'title' => (string)$reason->title,
                        ];

                        array_push($reasonArray, $data);
                    }
                    # return response
                    return response()->json([
                       'code'               => (string)$this->successStatus, 
                       'message'            => 'Dispute Reason Found',
                       'is_dispute_raise'   => $isDisputeRaised,
                       'data'               => $reasonArray
                    ]); 
                } else {
                    # return response
                    return response()->json([
                       'code'      => (string)$this->failedStatus, 
                       'message'   => 'No Dispute Reason Found',
                       'data'      => []
                    ]); 
                }
            } else {
                # return response
                return response()->json([
                   'code'      => (string)$this->failedStatus, 
                   'message'   => 'Order Not Found',
                   'data'      => []
                ]); 
            }
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'User Not Found',
               'data'      => []
            ]); 
        }

    }

    /**
     * @method to get Services on Sub category
     * @param Request $request
     * @return Service json
    */
    public function raiseDispute(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id'               => 'required|numeric',
            'order_id'              => 'required|numeric',
            'dispute_reason_id'     => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # Set the Discpute
        $data = [
            'user_id'           => $request->get('user_id'),
            'order_id'          => $request->get('order_id'),
            'dispute_reason_id' => $request->get('dispute_reason_id'),
            'dispute_status_id' => OrderDisputeStatusInterface::OPEN,
            'comment'           => $request->get('comment') ?? '',
        ];

        # Raise the Dispute
        $this->orderDispute->create($data);

        # return response
        return response()->json([
           'code'      => (string)$this->successStatus, 
           'message'   => 'Your Dispute will solve Soon.',
           'data'      => []
        ]); 
    }


    /**
     * @method to get Services on Sub category
     * @param Request $request
     * @return Service json
    */
    public function disputeSolutionDetail(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id'               => 'required|numeric',
            'order_id'              => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # fethc uSer
        $user = $this->user->find($request->get('user_id')); 

        if($user != '') {
            # Fetch the Order
            $order = $this->order
                          ->with(['disputes.solution','disputes.DisputeStatus'])
                          ->find($request->get('order_id'));

            if($order != '') {
                # Fetch Dispute on Order
                $disputes = $order->disputes;
                
                if($disputes->isNotEmpty()) {
                    $disputeArray = [];
                    /*foreach ($disputes as $key => $dispute) {
                        $disputeSolutionArray = [];
                        foreach ($dispute->solution as $solution) {
                           $data = [
                            'id'        => (string)$solution->id,
                            'solution'  => (string)$solution->comment,
                           ];

                           array_push($disputeSolutionArray, $data);
                        }
                        */

                        # FEtch the first Dispute
                        $dispute = $disputes->first();
                        $disputeData = [
                            'id'                => (string)$dispute->id ?? '',
                            'question'          => (string)$dispute->comment ?? '',
                            'status'            => (string)$dispute->DisputeStatus->name ?? '',
                            'disputeSolution'   => $dispute->solution->isNotEmpty() ? $dispute->solution->first()->comment : ''
                        ];
                      /*  array_push($disputeArray, $disputeData);
                    }*/

                    # return response
                    return response()->json([
                       'code'               => (string)$this->successStatus, 
                       'message'            => ' Dispute Found On Order',
                       'unique_order_id'    => (string)$order->unique_order_id,
                       'amount'             => (string)$order->paid_amount,
                       'date_time'          => (string)$order->date_time_of_service->format('l, jS M H:i A'),
                       'order_status'       => (string)$order->status_string,
                       'data'               => $disputeData
                    ]);
                } else {
                    # return response
                    return response()->json([
                       'code'      => (string)$this->failedStatus, 
                       'message'   => 'No Dispute Found On Order',
                       'data'      => []
                    ]);
                }
            } else {
                # return response
                return response()->json([
                   'code'      => (string)$this->failedStatus, 
                   'message'   => 'Order Not Found',
                   'data'      => []
                ]);
            }
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'User Not Found',
               'data'      => []
            ]); 
        }
    }
}
