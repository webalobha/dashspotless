<?php

namespace App\Http\Controllers\Api\User;

use DB;
use Validator;
use Carbon\Carbon;

use App\Models\User; 
use App\Models\BookingSlot; 

use App\Http\Traits\StatusTrait;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller; 

class BookingSlotController extends Controller
{
    use StatusTrait;

    # Variable to Bind Model
    protected $user;

    # Variable to Bind Booking Slot Model
    protected $bookingSlot;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user, BookingSlot $bookingSlot)
    {
        $this->user           = $user;
        $this->bookingSlot    = $bookingSlot;
    }

    /**
     * @method to get Services on Sub category
     * @param Request $request
     * @return Service json
    */
    public function fetchDates(Request $request)
    {
        # fetch Today Date
        $startDate = Carbon::now();
        $dateData = [];
        $count = 0;
        while($count <= 4 ) {

            $date = Carbon::now()->addDay($count)->format('m-d-Y');
            $showDate = 
            $data = [
                'date' => (string)$date
            ];
            array_push($dateData, $data);
            $count++;
        }
        # return response
        return response()->json([
           'code'      => (string)$this->successStatus, 
           'message'   => 'Booking slot found.',
           'data'      => $dateData
        ]); 
    }

    /**
     * @method to get Services on Sub category
     * @param Request $request
     * @return Service json
    */
    public function fetch(Request $request)
    {
        # Fetch all the Booking Slots
        $bookingSlots = $this->bookingSlot->get();

        $bookingSlotData = [];
        if($bookingSlots->isNotEmpty()) {
            foreach ($bookingSlots as $key => $bookingSlot) {
                $data = [
                    'id'        => (string)$bookingSlot->id ?? '',
                    'time'      => (string)$bookingSlot->time ?? '',
                ];

                # Push the data
                array_push($bookingSlotData, $data);
            }
            # return response
            return response()->json([
               'code'      => (string)$this->successStatus, 
               'message'   => 'Booking slot found.',
               'data'      => $bookingSlotData
            ]); 
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'Booking slot not found.',
               'data'      => []
            ]); 
        }
    }
}
