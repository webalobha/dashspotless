<?php

namespace App\Http\Controllers\Api\User;

use DB;
use Validator;
use Carbon\Carbon;

use App\Models\User; 
use App\Models\Order; 
use App\Models\Provider; 
use App\Models\CmsPages; 
use App\Models\UserWallet; 
use App\Models\BookingSlot; 
use App\Models\ServiceCart; 
use App\Models\Subscription; 
use App\Models\OrderService; 
use App\Models\OrderHistory; 
use App\Models\OrderReschedule; 
use App\Models\UserSubscription; 
use App\Models\UserNotification;
use App\Models\ProviderNotification;
use App\Models\AssignOrderToProvider; 
use App\Models\AssignOrderToProviderHistory; 
use App\Models\ServiceSettingRescheduleData; 

use App\Http\Traits\StatusTrait;
use App\Http\Interfaces\OrderStatusInterface;
use App\Http\Interfaces\TransactionTypeInterface;
use App\Http\Interfaces\AssignOrderProviderStatusInterface;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller; 
use App\Http\Controllers\Api\User\NotifyUser;
use App\Http\Controllers\Api\Provider\NotifyProvider;
use App\Http\Controllers\Api\User\BookingSlotController; 

class OrderController extends Controller implements OrderStatusInterface, TransactionTypeInterface, AssignOrderProviderStatusInterface
{
    use StatusTrait;

    # Variable to Bind Model
    protected $user;

    # Variable to Bind Order Model
    protected $order;  

    # Variable to Bind Provider Model
    protected $provider;

    # Variable to Bind UserWallet Model
    protected $userWallet;  

    # Variable to Bind Booking Slot Model
    protected $bookingSlot; 

    # Variable to Bind OrderService Model
    protected $orderService;

    # Variable to Bind Subscription Model
    protected $subscription;

    # Variable to Bind AssignOrderToProvider Model
    protected $orderHistory;

    # Variable to Bind Model userNotification
    protected $userNotification;

    # Variable to Bind Model providerNotification
    protected $providerNotification;

    # Variable to Bind OrderReschedule Model
    protected $orderReschedule;

    # Variable to Bind AssignOrderToProvider Model
    protected $assignOrderToProvider;

    # Variable to Bind UserSubscription Model
    protected $userSubscription;

    # Variable to Bind AssignOrderToProviderHistory Model
    protected $assignOrderToProviderHistory;

    # Variable to Bind Service Cart Model
    protected $serviceCart;

    # Variable to Bind CMS Pages
    protected $cmsPage;

    # Variable to Bind Service Setting Reschedule Data Model
    protected $serviceSettingRescheduleData;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user, Order $order, 
                                CmsPages $cmsPage,
                                Provider $provider,
                                UserWallet $userWallet,
                                BookingSlot $bookingSlot,
                                ServiceCart $serviceCart,
                                Subscription $subscription,
                                OrderService $orderService, 
                                OrderHistory $orderHistory,
                                OrderReschedule $orderReschedule,
                                UserNotification $userNotification,
                                UserSubscription $userSubscription,
                                ProviderNotification $providerNotification,
                                AssignOrderToProvider $assignOrderToProvider,
                                AssignOrderToProviderHistory $assignOrderToProviderHistory,
                                ServiceSettingRescheduleData $serviceSettingRescheduleData)
    {
        $this->user                              = $user;
        $this->order                             = $order;
        $this->cmsPage                           = $cmsPage;
        $this->provider                          = $provider;
        $this->userWallet                        = $userWallet;
        $this->serviceCart                       = $serviceCart;
        $this->bookingSlot                       = $bookingSlot;
        $this->subscription                      = $subscription;
        $this->orderService                      = $orderService;
        $this->orderHistory                      = $orderHistory;
        $this->orderReschedule                   = $orderReschedule;
        $this->userSubscription                  = $userSubscription;
        $this->userNotification                  = $userNotification;
        $this->providerNotification              = $providerNotification;
        $this->assignOrderToProvider             = $assignOrderToProvider;
        $this->assignOrderToProviderHistory      = $assignOrderToProviderHistory;
        $this->serviceSettingRescheduleData      = $serviceSettingRescheduleData;
    }

    /**
     * @method to get Services on Sub category
     * @param Request $request
     * @return Service json
    */
    public function submitOrder(Request $request)
    {
      # Validate request data
      $validator = Validator::make($request->all(), [ 
          'user_id'                 => 'required|numeric',
          'payment_mode'            => 'required|string',
          'user_address_id'         => 'required|numeric',
          'date_of_service'         => 'required|string',
          'time_slot_id'            => 'required|numeric',
          'paid_amount'             => 'required|numeric',
          'order_list'              => 'required|array',
          'buy_new_subscription'    => 'required|string',
          'wallet_amount_used'      => 'required|string',
          'online_payment_amount'   => 'required|string'
      ]);

      # If validator fails return response
      if ($validator->fails()) { 
          return response()->json(['error'=>$validator->errors()], 401);            
      }
      
      # fetch the Time Slot
      $bookingSlot = $this->bookingSlot->find($request->get('time_slot_id'));


      # data for Order
      $data = [
          'user_id'                   => $request->get('user_id'),
          'unique_order_id'           => '#'.mt_rand(100000000, 999999999),
          'provider_id'               => 0,
          'paid_amount'               => $request->get('paid_amount') ?? '',
          'payment_mode'              => $request->get('payment_mode') ?? '',
          'time_slot_id'              => $request->get('time_slot_id') ?? '',
          'date_of_service'           => $this->formatDate($request->get('date_of_service')),
          'date_time_of_service'      => $this->formatDate($request->get('date_of_service')).' '.
                                          $bookingSlot->time,
          'subscription_id'           => ($request->has('subscription_id') AND ($request->get('subscription_id') != '')) ?
                                          $request->get('subscription_id') : 0,
          'user_address_id'           => $request->get('user_address_id'),
          'subscription_discount'     => ($request->has('subscription_discount') AND 
                                         ($request->get('subscription_discount') != '')) ? 
                                          $request->get('subscription_discount') : 0,
          'credit_points'             => '50',
          'wallet_amount_used'        => $request->get('wallet_amount_used'),
          'online_payment_amount'     => $request->get('online_payment_amount')
      ];

      DB::beginTransaction();


      # Create Order 
      $order = $this->order->create($data);

      # Save Wallet amount Used
      if($request->get('wallet_amount_used') != '0') {
        $data = [
          'user_id'           => $request->get('user_id'),
          'order_id'          => $order->id,
          'transaction_type'  => 1,
          'amount'            => $request->get('wallet_amount_used'),
        ];

        # CreatebWallet Amount USed
        $this->userWallet->create($data);
      }

      # Save user Subscription if available
      if($request->get('buy_new_subscription') == 'true') {
        if($request->get('subscription_id') != '') {
          $relations = ['subscriptionDuration','discountType'];
          $subscription = $this->subscription
                               ->with($relations)
                               ->find($request->get('subscription_id'));

          if($subscription != '') {
            $data = [
              'user_id'                 => $request->get('user_id'),
              'subscription_id'         => $subscription->id,
              'subs_duration_type'      => $subscription->subscriptionDuration->name,
              'duration'                => $subscription->duration,
              'amount_for_subscription' => $subscription->price,
              'discount'                => $subscription->discount,
              'discount_type'           => $subscription->discountType->name,
              'expired_at'              => $subscription->expiry_date,
            ];

            $this->userSubscription->create($data);
          }
        }
      }

      # Fetch all the Order List
      $orderServices = $request->get('order_list');
      $serviceIds = [];
      $completionMinutesTaken = 0;
      foreach ($orderServices as $key => $service) {
         $serviceInCart = $this->serviceCart
                         ->where('user_id', $request->get('user_id'))
                         ->where('service_id', $service['service_id'])
                        ->get();
                       
          $data = [
              'order_id'              => $order->id,
              'service_id'            => $service['service_id'] ?? 0,
              'quantity'              => $serviceInCart->isNotEmpty() ? $serviceInCart->last()->quantity : 1,
              'service_time_taken'    => $service['service_time_taken'] ?? '',
              'total_amount'          => $service['total_amount'] ?? '',
              'discount_given'        => $service['discount_given'] ?? '',
              'payable_amount'        => $service['payable_amount'] ?? '',
          ];

          array_push($serviceIds, $service['service_id']);
          $this->orderService->create($data);
          $completionMinutesTaken += $service['service_time_taken'] * ($serviceInCart->isNotEmpty() ? $serviceInCart->last()->quantity : 1);
      }

      # Update total Minutes taken to complete Order
      $order->update(['total_minutes_taken_to_complete_order' => $completionMinutesTaken]);

      # create Order History
      $data = [
          'order_id'          => $order->id,
          'order_status_id'   => OrderStatusInterface::BOOKED,
          'comment'           => 'Booked',
      ];

      $this->orderHistory->create($data);

      # Delete the Orders from Cart after booked
      $serviceCart = $this->serviceCart
                         ->where('user_id', $request->get('user_id'))
                         ->whereIn('service_id', $serviceIds)
                         ->get();

      # delete Service from cart
      if($serviceCart->isNotEmpty()) {
          foreach ($serviceCart as $key => $service) {
              $service->delete();
          }
      }

      $relations = [
          'orderServices.service.subCategory',
          'timeSlot',
          'user',
          'orderAddress'
      ];

      # ftehc the Updated Order
      $order = $this->order
                    ->with($relations)
                    ->find($order->id);

      # fetch the Time Slot of Order
      $timeSlot = carbon::parse($order->timeSlot->time)->format('H:i A');

      # fetch Order Service
      $orderService = $order->orderServices->first();

      # fetch SubCategory Of Services
      $subCategory = $orderService->service->subCategory;

      # Set Booking message
      $bookingMessage = 'Your '.($subCategory->name ?? ' ').
                        ' will be assigned 1 hour Before the scheduled time.';

      # Set Arrival time Statement
      $date = $order->date_of_service->format('l, jS M');

      $arrivalDateTime = $date.' '.$timeSlot;

      # Assign Order to Providers in Seraching Status
      $this->searchProviderForOrder($order);

      # Fetch User
      $user = $this->user->find($request->get('user_id'));

      # Notify user for Order Successfully
      $notifyUser = new NotifyUser();

      # Set Title message
      $title = 'Mesee Order Notification';

      # Set Message
      $message = 'Your Order placed Successfully.';

      # Notify User
      $notifyUser->notify($user, $title, $message);

      # Save Send Notification 
      $data = [
          'user_id'       => $user->id,
          'order_id'      => $order->id,
          'notification'  => $message,
      ];

      # Creta UserNotification
      $this->userNotification->create($data);

      DB::commit();

      # return response
      return response()->json([
         'code'               => (string)$this->successStatus, 
         'user_id'            => (string)$request->get('user_id'), 
         'order_id'           => (string)$order->id, 
         'unique_order_id'    => (string)$order->unique_order_id, 
         'booking_message'    => (string)$bookingMessage,
         'arrival_time'       => (string)$arrivalDateTime,
         'booking_amount'     => (string)$order->paid_amount,
         'sub_total'          => (string)$order->paid_amount,
         'amount_paid'        => (string)$order->paid_amount,
         'message'            => 'Order has been Placed Succesfully.We will notify you once Provider will accept request.',
         'data'               => []
      ]); 
    }

    /**
     * @method to assign Provider to User Order
     * @param $order
     * @return 
     */
    public function searchProviderForOrder($order = '')
    {
      # fetch Address on which Servise will Provider by Provider
      $orderAddress = $order->orderAddress;

      # Fetch the Lat long from Order address 
      $latitude = $orderAddress->latitude;
      $longitude = $orderAddress->longitude;
      $distance = 20;

      # fetch Providers
      $providers = $this->provider
                        ->active()
                        #->whereRaw("(1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
                       # ->limit(5)
                        ->get();

      # fetch all the Providers who are Free
      //Code Here

      # Assign the Providers
      if($providers->isNotEmpty()) {
        foreach ($providers as $key => $provider) {
           $data = [
             'order_id'    => $order->id,
             'provider_id' => $provider->id,
           ];

           # Searching provider and Set order to Provider 
           $assignedOrderProvider = $this->assignOrderToProvider->create($data);

           # Set Assigned Provider Status to Searching
           $data = [
             'order_id'                => $order->id,
             'assign_orderprovider_id' => $assignedOrderProvider->id,
             'assign_order_status_id'  => AssignOrderProviderStatusInterface::SEARCHING,
             'reason'                  => 'Searching Provider with Order',
           ];

           # Create Status for searching Provider for Status
           $this->assignOrderToProviderHistory->create($data);

            # Notify Providder for lEad
            $notifyProvider = new NotifyProvider();

            # Set Title message
            $title = 'Mesee Lead Notification';

            # Set Message
            $message = 'New Lead Found';

            # Notify User
            $notifyProvider->notify($provider, $title, $message);

            # Save Send Notification 
            $data = [
                'provider_id'   => $provider->id,
                'order_id'      => $order->id,
                'notification'  => $message,
            ];

            # Creta UserNotification
            $this->providerNotification->create($data);
         }
           return;
      } else {

      }
     
    }
    

    /**
     * @method to get all the Upcoming Orders
     * @param Request
     * @retrun Order Json
     */
    public function upcomingOrder(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id'            => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $relations = [
            'orders.history',
            'orders.orderServices.service.subCategory',
        ];

        # fetch the User
        $user = $this->user
                     ->with($relations)
                     ->find($request->get('user_id'));

        if($user != '') {
            $orders = $user->orders;
            if($orders->isNotEmpty()) {
                $upcomingOrders = $orders->filter(function($order) {
                    /*if($order->date_time_of_service->addHours(1)->gte(Carbon::now())) {
                        $statusId =$order->history->last()->order_status_id;
                        #if($statusId == OrderStatusInterface::BOOKED OR 
                           # $statusId == OrderStatusInterface::ON_THE_WAY) {
                            return $order;
                        #}
                    }*/

                    if($order->is_not_completed) {
                      return $order;
                    }
                });

                # Set Upcomingdata
                $upcomingData = [];
                if($upcomingOrders->count()) {
                    foreach ($upcomingOrders as $key => $order) {
                        # fetch Order Service
                        $orderService = $order->orderServices->first();

                        # fetch SubCategory Of Services
                        $subCategory = $orderService->service->subCategory;

                        # Set Arrival time Statement
                        $dateTime = $order->date_time_of_service->format('l, jS M H:i A');

                        $data = [
                            'order_id'          => (string)$order->id ?? '',
                            'unique_order_id'   => (string)$order->unique_order_id ?? '',
                            'order_amount'      => (string)$order->paid_amount ?? '',
                            'subcategory_name'  => (string)$subCategory->name ?? '',
                            'service_date_time' => (string)$dateTime ?? '',
                            'order_status'      => (string)$order->status_string ?? '',
                        ];

                        array_push($upcomingData, $data);
                    }
                    # return response
                    return response()->json([
                       'code'               => (string)$this->successStatus, 
                       'message'            => 'Upcoming Services Found',
                       'data'               => $upcomingData
                    ]); 
                } else {
                    # return response
                    return response()->json([
                       'code'               => (string)$this->failedStatus, 
                       'message'            => 'No Upcoming Service',
                       'data'               => []
                    ]); 
                }
            } else {
                # return response
                return response()->json([
                   'code'               => (string)$this->failedStatus, 
                   'message'            => 'No Order History',
                   'data'               => []
                ]); 
            }
        } else {
            # return response
            return response()->json([
               'code'               => (string)$this->failedStatus, 
               'message'            => 'User not Found.',
               'data'               => []
            ]); 
        }
    }

    /**
     * @method to get all the Upcoming Orders
     * @param Request
     * @retrun Order Json
     */
    public function pastOrders(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id'            => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $relations = [
            'orders.history',
            'orders.orderServices.service.subCategory',
        ];

        # fetch the User
        $user = $this->user
                     ->with($relations)
                     ->find($request->get('user_id'));

        if($user != '') {
            $orders = $user->orders;
            if($orders->isNotEmpty()) {
                $pastOrders = $orders->filter(function($order) {
                    if($order->is_completed OR $order->is_cancelled) {
                      return $order;
                    }
                });

                # Set pastData
                $pastData = [];
                if($pastOrders->count()) {
                    foreach ($pastOrders as $key => $order) {
                        # fetch Order Service
                        $orderService = $order->orderServices->first();

                        # fetch SubCategory Of Services
                        $subCategory = $orderService->service->subCategory;

                        # Set Arrival time Statement
                        $dateTime = $order->date_time_of_service->format('l, jS M H:i A');

                        $data = [
                            'order_id'          => (string)$order->id ?? '',
                            'unique_order_id'   => (string)$order->unique_order_id ?? '',
                            'order_amount'      => (string)$order->paid_amount ?? '',
                            'subcategory_name'  => (string)$subCategory->name ?? '',
                            'service_date_time' => (string)$dateTime ?? '',
                            'order_status'      => (string)$order->status_string ?? '',
                        ];

                        array_push($pastData, $data);
                    }
                    # return response
                    return response()->json([
                       'code'               => (string)$this->successStatus, 
                       'message'            => 'Past Services Found',
                       'data'               => $pastData
                    ]); 
                } else {
                    # return response
                    return response()->json([
                       'code'               => (string)$this->successStatus, 
                       'message'            => 'No Past Service',
                       'data'               => []
                    ]); 
                }
            } else {
                # return response
                return response()->json([
                   'code'               => (string)$this->successStatus, 
                   'message'            => 'No Order History',
                   'data'               => []
                ]); 
            }
        } else {
            # return response
            return response()->json([
               'code'               => (string)$this->successStatus, 
               'message'            => 'User not Found.',
               'data'               => []
            ]); 
        }
    }
    
    /**
     * @method to fetch all the details of Order
     * @param request
     * @return Order Detail json | Empty
     */
    public function orderDetail(Request $request)
    {
      # Validate request data
      $validator = Validator::make($request->all(), [ 
          'user_id'            => 'required|numeric',
          'order_id'           => 'required|numeric',
      ]);

      # If validator fails return response
      if ($validator->fails()) { 
          return response()->json(['error'=>$validator->errors()], 401);            
      }

      # Set the Relations on User
      $relations = [
            'history',
            'orderServices.service.subCategory',
      ];

      # fetch the User
      $user = $this->user->find($request->get('user_id'));

      if($user != '') {
        $order = $this->order->find($request->get('order_id'));
        if($order != '') {
          $relations = [
          'orderServices.service.subCategory',
          'timeSlot',
          'orderAddress',
          'provider'
          ];

          # ftehc the Updated Order
          $order = $this->order
                        ->with($relations)
                        ->find($order->id);

          # fetch the Time Slot of Order
          $timeSlot = carbon::parse($order->timeSlot->time)->format('H:i A');

          # fetch Order Service
          $orderServices       = $order->orderServices;
          $firstOrderService   = $orderServices->first();

          # fetch SubCategory Of Services
          $subCategory = $firstOrderService->service->subCategory;

          # Set total time needed for order Completion
          $timeTaken = 0;
          foreach ($order->orderServices as $key => $service) {
            $serviceTime = ($service->service_time_taken != '') ? $service->service_time_taken : 0;
            $timeTaken += $serviceTime;
          }

          # Set Booking message
          /*$bookingMessage = 'Your '.($subCategory->name ?? ' ').
                            ' will be assigned 1 hour Before the scheduled time.';*/

          $bookingMessage = 'Provider will assign TIME before scheduled time.';


          # Set Arrival time Statement
          $date = $order->date_of_service->format('l, jS M');

          $arrivalDateTime = $date.' '.$timeSlot;

          # eftch Order Address
          $orderAddress = $order->orderAddress->address;

          # Set order Services Data
          $setOrderServicesData = [];
          foreach ($orderServices as $key => $orderService) {
            $service = $orderService->service;
            $data = [
              'service_icon'        => $service->icon_path ?? '',
              'service_title'       => $service->title ?? '',
              'service_time_taken'  => $orderService->service_time_taken ?? '',
              'quantity'            => $orderService->quantity ?? '',
              'amount'              => $orderService->payable_amount ?? '',
            ];

            array_push($setOrderServicesData, $data);
          }

          # Set provider Details
          if($order->provider != '') {
            $provider = $order->provider;
            $providerDetails = [
              'name'          => $provider->first_name ?? '',
              'mobile_number' => $provider->mobile_number ?? '',
              'image_path'    => $provider->image_path ?? ''
            ];
          } else {
            $providerDetails = [];
          }

          DB::commit();

          # return response
          return response()->json([
             'code'                   => (string)$this->successStatus, 
             'user_id'                => (string)$request->get('user_id'), 
             'order_id'               => (string)$order->id, 
             'unique_order_id'        => (string)$order->unique_order_id ?? '', 
             'order_status'           => (string)$order->status_string ?? '',
             'order_address'          => (string)$orderAddress ?? '',
             'payment_mode'           => (string)$order->payment_mode, 
             'unique_order_id'        => (string)$order->unique_order_id, 
             'booking_message'        => (string)$bookingMessage,
             'arrival_time'           => (string)$arrivalDateTime,
             'total_time'             => (string)$timeTaken.' Minutes',
             'booking_amount'         => (string)$order->paid_amount,
             'sub_total'              => (string)$order->paid_amount,
             'amount_paid'            => (string)$order->paid_amount,
             'order_service_data'     => $setOrderServicesData,
             'provider_avail'         => !empty($providerDetails) ? 'true' : 'false',
             'provider_details'       => $providerDetails,
             'message'                => 'Order has been Placed Succesfully. 
                                          You will Notify about Provider Details before 12 hours of Service.',
             'data'                   => []
          ]);     
        } else {
          # return response
          return response()->json([
             'code'               => (string)$this->successStatus, 
             'message'            => 'Order not Found.',
             'data'               => []
          ]); 
        }
      } else {
        # return response
        return response()->json([
           'code'               => (string)$this->successStatus, 
           'message'            => 'User not Found.',
           'data'               => []
        ]); 
      }
    }

    /**
     * @method to Update Status of Order
     * @param Request $request
     * @return json
     */
    public function cancelOrder(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id'               => 'required|numeric',
            'order_id'              => 'required|numeric',
            'reason'                => 'required|string',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # feth the user
        $user = $this->user
                     ->find($request->get('user_id'));

        # Fetch the Order
        $order = $this->order->find($request->get('order_id')); 

        # Fetch the Order
        $assignedOrderToProvider = $this->assignOrderToProvider
                                        ->where('order_id', $request->get('order_id'))
                                        ->get();

        if($user != '') {
            if($order != '') {
                # Fetch Amount paid By Customer for that order
                $amountToBeCredited = $order->paid_amount;

                # Set Data for order History Creation
                $orderHistoryData = [
                    'order_id'                  => $request->get('order_id'),
                    'order_status_id'           => OrderStatusInterface::CANCELLED,
                    'comment'                   => 'User Cancelled',
                ];

                DB::beginTransaction();

                  if($assignedOrderToProvider->isNotEmpty()) {
                    foreach ($assignedOrderToProvider as $key => $assignedOrder) {
                        # Set Data for assigned Provider History Data
                        $assignedOrderProviderHistoryData = [
                            'order_id'                  => $request->get('order_id'),
                            'assign_orderprovider_id'   => $assignedOrder->id,
                            'assign_order_status_id'    => AssignOrderProviderStatusInterface::CANCELED,
                            'reason'                    => 'User Cancelled',
                      ];

                      $this->assignOrderToProviderHistory->create($assignedOrderProviderHistoryData);
                      $assignedOrder->update(['status' => false]);
                    }
                  }

                  # Set Wallet Data
                  $walletData = [
                      'user_id'           => $order->user_id,
                      'order_id'          => $request->get('order_id'),
                      'transaction_type'  => TransactionTypeInterface::CREDIT,
                      'amount'            => $order->paid_amount,
                  ];


                  $this->orderHistory->create($orderHistoryData);
                  $this->userWallet->create($walletData);

                  # Fetch User
                  $user = $this->user->find($request->get('user_id'));

                  # Notify user for Order Successfully
                  $notifyUser = new NotifyUser();

                  # Set Title message
                  $title = 'Mesee Notification';

                  # Set Message
                  $message = $order->paid_amount. ' has been credit in your wallet account.';

                  # Notify User
                  $notifyUser->notify($user, $title, $message);

                  # Save Send Notification 
                  $data = [
                      'user_id'       => $user->id,
                      'order_id'      => $order->id,
                      'notification'  => $message,
                  ];

                  # Creta UserNotification
                  $this->userNotification->create($data);

                DB::commit();

                # return response
                return response()->json([
                   'code'      => (string)$this->successStatus, 
                   'message'   => 'Order Cancelled Successful',
                   'data'      => []
                ]); 
            } else {
                # return response
                return response()->json([
                   'code'      => (string)$this->failedStatus, 
                   'message'   => 'Order Not Found',
                   'data'      => []
                ]); 
            }
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'No Provider Found',
               'data'      => []
            ]); 
        }
    }
    
    /**
     * @method to fetch all the Condition to Reqchedule the Order
     * @param Request $request
     * @return Json | empty
     */
    public function rescheduleConditions(Request $request)
    {
      # Validate request data
      $validator = Validator::make($request->all(), [ 
          'user_id'            => 'required|numeric',
          'order_id'           => 'required|numeric',
      ]);

      # If validator fails return response
      if ($validator->fails()) { 
          return response()->json(['error'=>$validator->errors()], 401);            
      }

      # fetch the User
      $user = $this->user->find($request->get('user_id'));

      if($user != '') {
        $order = $this->order->find($request->get('order_id'));
        if($order != '') {
          # fetch the Reschedule Model
          $rescheduleModelApplied = $order->reschedule_applied_on_order;

          $rescheduleId = '';
          $amountCharge = '';
          if($rescheduleModelApplied !='') {
            $rescheduleId = $rescheduleModelApplied->id;
            $amountCharge = $rescheduleModelApplied->amount;
          } 
          $rescheduleConditionsData = [];
          $conditions = $this->serviceSettingRescheduleData->all();
          foreach ($conditions as $key => $condition) {
            $data = [
              'id'                  => (string)$condition->id ?? '',
              'description'         => (string)$condition->description ?? '',
              'hours'               => (string)$condition->hours ?? '',
              'amount_charge'       => (string)$condition->amount ?? '',
            ];

            array_push($rescheduleConditionsData, $data);
          }

          # return response
          return response()->json([
             'code'               => (string)$this->successStatus, 
             'message'            => 'Order and its Reschedule Condition.',
             'order_id'           => (string)$order->id ?? '',
             'reschedule_id'      => (string)$rescheduleId ?? '',
             'amount_charge'      => (string)$amountCharge ?? '',
             'data'               => $rescheduleConditionsData
          ]); 
        } else {
          # return response
          return response()->json([
             'code'               => (string)$this->successStatus, 
             'message'            => 'Order not Found.',
             'data'               => []
          ]); 
        }
      } else {
        # return response
        return response()->json([
           'code'               => (string)$this->successStatus, 
           'message'            => 'User not Found.',
           'data'               => []
        ]); 
      }
    }

    /**
     * @method to get all the free time slots on that Day
     * @param Request $request
     * @return json
     */
    public function fetchAllFreeTimeSlotsofProvider(Request $request)
    {
      # Validate request data
      $validator = Validator::make($request->all(), [ 
          'user_id'  => 'required|numeric',
          'order_id' => 'required|numeric',
          'date'     => 'required'        
      ]);

      # If validator fails return response
      if ($validator->fails()) { 
        return response()->json(['error'=>$validator->errors()], 401);            
      }

      # Fetch Date
      $date = Carbon::parse($this->formatDate($request->get('date')));

      # Fetch User
      $user = $this->user->find($request->get('user_id'));

      # Fetch Order
      $order = $this->order
                    ->with('provider.orders')
                    ->find($request->get('order_id'));

      if($user != '') {
        if($order != '') {
          $provider = $order->provider;
          if($provider != '') {
            # fetcf all orders of Provider Served in that Date
            $providerOrders = $provider->orders
                                       ->where('id', '<>', $order->id)
                                       ->filter(function($order) use($date){
                                           $rescheduleDate = $date->toDateString();
                                           $orderActualDate = $order->date_time_of_service->toDateString();
                                           if($rescheduleDate == $orderActualDate) {
                                            return $order;
                                           }
                                        });

            $engagedSlots = collect([]);
            if($providerOrders->isNotEmpty()) {
              foreach ($providerOrders as $key => $order) {
                $noOfSlotsNeeded = (int)round($order->total_minutes_taken_to_complete_order/30)+1;
                $startSlot = $order->time_slot_id;
                $lastSlot = $noOfSlotsNeeded + $startSlot;
                $engagedSlots->push($this->bookingSlot->where('id','>=', $startSlot)->where('id', '<=', $lastSlot)->get());
              }
              
              # fetch all the Engaged Slots of Provider in Date
              $engagedSlots = $engagedSlots->flatten()
                                           ->pluck('id')
                                           ->unique()
                                           ->sort()
                                           ->toArray();

              # Fetch Slots
              $allSlots = $this->bookingSlot->all();
              $slotsData = [];
              foreach ($allSlots as $key => $slot) {
                if(!in_array($slot->id, $engagedSlots)) {
                  $available = "true";
                } else {
                  $available = "false";
                }
               
                $data = [
                  'id'        => (string)$slot->id,
                  'time'      => (string)$slot->time,
                  'is_avail'  => $available,
                ];

                array_push($slotsData, $data);
              }

              # return response
              return response()->json([
                 'code'               => (string)$this->successStatus, 
                 'message'            => 'Slots Found.',
                 'data'               => $slotsData
              ]); 
            } else {
              return $this->fetchTimeSlots();
            }
          } else {
            return $this->fetchTimeSlots();
          }
        } else {
          # return response
          return response()->json([
             'code'               => (string)$this->failedStatus, 
             'message'            => 'Order not Found.',
             'data'               => []
          ]); 
        }
      } else {
        # return response
        return response()->json([
           'code'               => (string)$this->failedStatus, 
           'message'            => 'User not Found.',
           'data'               => []
        ]); 
      }
    }

    /**
     * @method to get Services on Sub category
     * @param Request $request
     * @return Service json
    */
    public function fetchTimeSlots()
    {
        # Fetch all the Booking Slots
        $bookingSlots = $this->bookingSlot->get();

        $bookingSlotData = [];
        if($bookingSlots->isNotEmpty()) {
            foreach ($bookingSlots as $key => $bookingSlot) {
                $data = [
                    'id'        => (string)$bookingSlot->id ?? '',
                    'time'      => (string)$bookingSlot->time ?? '',
                    'status'    => "true",
                ];

                # Push the data
                array_push($bookingSlotData, $data);
            }
            # return response
            return response()->json([
               'code'      => (string)$this->successStatus, 
               'message'   => 'Booking slot found.',
               'data'      => $bookingSlotData
            ]); 
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'Booking slot not found.',
               'data'      => []
            ]); 
        }
    }

    /**
     * @method to Confirm Reschedule the Order
     * @param Request
     * @return Json | 
    */
    public function rescheduleConfirm(Request $request)
    {
      # Validate request data
      $validator = Validator::make($request->all(), [ 
          'user_id'                             => 'required|numeric',
          'order_id'                            => 'required|numeric',
          'reschedule_date'                     => 'required|string',
          'time_slot_id'                        => 'required|numeric',
          'paid_penalty'                        => 'required|string',
          'penalty_paid_from_wallet'            => 'required|string',
          'penalty_paid_online'                 => 'required|string',
      ]);

      # If validator fails return response
      if ($validator->fails()) { 
          return response()->json(['error'=>$validator->errors()], 401);            
      }

      # fetch the User
      $user = $this->user->find($request->get('user_id'));

      # fetch the Time Slot
      $bookingSlot = $this->bookingSlot->find($request->get('time_slot_id'));

      # Set Data Time Of Order
      $dateTime = $this->formatDate($request->get('reschedule_date')).' '.$bookingSlot->time;

      DB::beginTransaction();
      if($request->get('penalty_paid_from_wallet') != '0') {
        $data = [
          'user_id'           => $user->id,
          'order_id'          => $request->get('order_id'),
          'transaction_type'  => TransactionTypeInterface::DEBIT,
          'amount'            => $request->get('penalty_paid_from_wallet'),
        ];

        # Debit from User Wallet
        $this->userWallet->create($data);
      }

      # Get order
      $order = $this->order
                    ->with(['orderAddress.country', 'orderAddress.state', 'orderAddress.city'])
                    ->find($request->get('order_id'));

      # fetch Order Address
      $address = $order->orderAddress;

      # Fetch Data for Reschedule Data
      $rescheduleData = [
        'order_id'                  => $request->get('order_id') ?? '',
        'reschedule_date_time'      => $dateTime ?? '',
        'is_date_time_change'       => true,
        'is_address_change'         => false,
        'reschedule_penalty_paid'   => $request->get('paid_penalty') ?? '',
        'penalty_paid_from_wallet'  => $request->get('penalty_paid_from_wallet') ?? '',
        'penalty_paid_online'       => $request->get('penalty_paid_online') ?? '',
        'address_of_service'        => $address->address ?? '',
        'pin_code_service'          => $address->pin_code ?? '',
        'latitude'                  => $address->latitude ?? '',
        'longitude'                 => $address->longitude ?? '',
        'name'                      => $address->name ?? '',
        'mobile'                    => $address->mobile ?? '',
        'city'                      => $address->city_name ?? '',
        'state'                     => $address->state_name ?? '',
        'country'                   => $address->country_name ?? '',
        'status'                    => true,
        'transaction_id'            => $request->get('transaction_id') ?? '',
        'payment_mode'              => $request->get('payment_mode') ?? '',
      ];

      # create Reschedule Date
      $orderReschedule = $this->orderReschedule->create($rescheduleData);
      $order->update([
        'date_of_service'       => $dateTime,
        'date_time_of_service'  => $dateTime,
        'time_slot_id'          => $request->get('time_slot_id'),
      ]);


      # Fetch Udated Order
      $order = $this->order->find($order->id);

      # Set Arrival time Statement
      $date = $order->date_of_service->format('l, jS M');

      # fetch the Time Slot of Order
      $timeSlot = carbon::parse($order->timeSlot->time)->format('H:i A');

      $arrivalDateTime = $date.' '.$timeSlot;
      
      DB::commit();

      # return response
      return response()->json([
         'code'               => (string)$this->successStatus, 
         'user_id'            => (string)$request->get('user_id'), 
         'order_id'           => (string)$order->id, 
         'unique_order_id'    => (string)$order->unique_order_id, 
         'booking_message'    => (string)'',
         'arrival_time'       => (string)$arrivalDateTime,
         'booking_amount'     => (string)$order->paid_amount,
         'sub_total'          => (string)$order->paid_amount,
         'amount_paid'        => (string)$order->paid_amount,
         'message'            => 'Order has been Reschedule Succesfully.' ,
         'data'               => []
      ]); 

    }
    
    /**
     * @method to fetch all the Orders Related to user
     * @param Request $request
     * @return json
     */
    public function fetchAllOrders(Request $request)
    {
       # Validate request data
      $validator = Validator::make($request->all(), [ 
          'user_id'  => 'required|numeric'
      ]);

      # If validator fails return response
      if ($validator->fails()) { 
          return response()->json(['error'=>$validator->errors()], 401);            
      }

      # Set Relations 
      $relations = ['orders.orderAddress','orders.history'];

      # fetch the User
      $user = $this->user->with($relations)->find($request->get('user_id'));

      if($user != '') {
        $orders = $user->orders->filter(function($order) {
          $completedOrder = $order->history
                                  ->where('order_status_id', OrderStatusInterface::COMPLETED)
                                  ->isNotEmpty();
          if($completedOrder) {
            return $order;
          }
        });
        
        $ordersArray = [];
        if($orders->isNotEmpty()) {
          foreach ($orders as $key => $order) {
            $data = [
              'order_id'                  => (string)$order->id?? '',
              'unique_order_id'           => (string)$order->unique_order_id ?? '',
              'order_status'              => (string)$order->status_string ?? '',
              'user_name'                 => (string)$order->orderAddress->name ?? '',
              'mobile'                    => (string)$order->orderAddress->mobile ?? '',
              'order_date'                => (string)$order->date_time_of_service->format('d-M-Y'),
              'order_time'                => (string)$order->date_time_of_service->format('h:i A'),
              'credit_points'             => '50',
              'amount'                    => (string)$order->paid_amount ?? '',
              'unique_order_id'           => (string)$order->unique_order_id,
              'order_placed_date'         => (string)$order->created_at->format('d-M-Y') ?? '',
              'order_placed_time'         => (string)$order->created_at->format('h:i A') ?? '',
            ];

            array_push($ordersArray, $data);
          }

          # return response
          return response()->json([
             'code'               => (string)$this->successStatus, 
             'message'            => 'Orders Found.',
             'data'               => $ordersArray
          ]); 
        } else {
          # return response
          return response()->json([
             'code'               => (string)$this->successStatus, 
             'message'            => 'Orders not Found.',
             'data'               => []
          ]); 
        }
      } else {
        # return response
        return response()->json([
           'code'               => (string)$this->successStatus, 
           'message'            => 'User not Found.',
           'data'               => []
        ]); 
      }
    }
    
    /**
     * @method to mdat formatter
     * @param Date
     * @retun formatedd Date
     */
    public function formatDate($date)
    {
        # Set Format
        $getDate = str_replace('-', '/', $date);

        # Return Dtae
        $date = date('Y-m-d', strtotime($getDate));

        return $date;
    }
}
