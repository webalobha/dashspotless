<?php

namespace App\Http\Controllers\Api\User;

use DB;
use Validator;
use App\Models\User; 
use App\Models\Dispute; 
use App\Models\UserAddress; 
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Traits\StatusTrait;
use App\Models\SubCategoryVariants; 
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller; 

class AddressController extends Controller
{
  use StatusTrait;

  # Variable to Bind Model
  protected $user;

  # Variable to Bind Model UserAddress
  protected $userAddress;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct(User $user, UserAddress $userAddress)
  {
    $this->user         = $user;
    $this->userAddress  = $userAddress;
  }

  /**
   * @method to store user Address
   * @param Request $request
   * @return Address Json
   */
  public function fetch(Request $request)
  {
    # Validate request data
    $validator = Validator::make($request->all(), [ 
        'user_id'        => 'required|numeric',
    ]);

    # If validator fails return response
    if ($validator->fails()) { 
        return response()->json(['error'=>$validator->errors()], 401);            
    }

    $relation = [
      'address.country',
      'address.state',
      'address.city',
    ];
    # get the user 
    $user = $this->user
                 ->with($relation)
                 ->find($request->get('user_id'));

    if($user != '') {
      $addresses = $user->address;
      $addressData = [];
      if($addresses->isNotEmpty()) {
        foreach ($addresses as $key => $address) {
          # Gather Country, State, City Name
          $country = $address->country;
          $state = $address->state;
          $city = $address->city;
          $countryName = '';
          $stateName = '';
          $cityName = '';
          if($country != '') {
            $countryName = $country->name;
          }
          if($state != '') {
            $stateName = $state->name;
          }
          if($city != '') {
            $cityName = $city->name;
          }
          # gather the Data
          $data = [
            'address_id'                      => (string)$address->id,
            'user_id'                         => (string)$user->id,
            'country'                         => $countryName,
            'state'                           => $stateName,
            'city'                            => $cityName,
            'name'                            => (string)$address->name ?? '',
            'mobile'                          => (string)$address->mobile ?? '',
            'pin_code'                        => (string)$address->pin_code ?? '',
            'address'                         => (string)$address->address ?? '',
            'address_type'                    => (string)$address->address_type_string ?? '',
            'address_other_type_description'  => (string)$address->address_other_type_description ?? '',
            'longitude'                       => (string)$address->longitude ?? '',
            'latitude'                        => (string)$address->latitude ?? '',
          ];

          array_push($addressData, $data);
        }

        # return response
        return response()->json([
           'code'      => (string)$this->successStatus, 
           'message'   => 'Addresses Found.',
           'data'      => $addressData
        ]);
      } else {
        # return response
        return response()->json([
           'code'      => (string)$this->failedStatus, 
           'message'   => 'No Address Found.',
           'data'      => []
        ]);
      }
    } else {
      # return response
      return response()->json([
         'code'      => (string)$this->failedStatus, 
         'message'   => 'No user found on provided user Id.',
         'data'      => []
      ]);
    }
  }

  /**
   * @method to store user Address
   * @param Request $request
   * @return Address Json
   */
  public function store(Request $request)
  {
    # Validate request data
    $validator = Validator::make($request->all(), [ 
        'user_id'        => 'required|numeric',
        'address'        => 'required|string',
        'pin_code'       => 'required|numeric',
        'address_type'   => 'required|string',
    ]);

    # If validator fails return response
    if ($validator->fails()) { 
        return response()->json(['error'=>$validator->errors()], 401);            
    }

    # get the user 
    $user = $this->user->find($request->get('user_id'));

    if($user != '') {
      # get the Address type
      if($request->get('address_type') == 'home') {
        $addressType = 1;
      } elseif ($request->get('address_type') == 'work' OR $request->get('address_type') == 'office') {
        $addressType = 2;
      } else {
        $addressType = 3;
      }

      # gather the Data
      $data = [
        'user_id'                         => $user->id,
        'name'                            => $request->has('name') ? $request->get('name') : '',
        'country_id'                      => $request->has('country_id') ? $request->get('country_id') : 0,
        'state_id'                        => $request->has('state_id') ? $request->get('state_id') : 0,
        'city_id'                         => $request->has('city_id') ? $request->get('city_id') : 0,
        'mobile'                          => $request->has('mobile') ? $request->get('mobile') : '',
        'pin_code'                        => $request->has('pin_code') ? $request->get('pin_code') : '',
        'address'                         => $request->has('address') ? $request->get('address') : '',
        'address_type'                    => $addressType,
        'address_other_type_description'  => $request->has('address_type_desc') ? $request->get('address_type_desc') : '',
        'status'                          => true,
        'longitude'                       => $request->has('longitude') ? $request->get('longitude') : '',
        'latitude'                        => $request->has('latitude') ? $request->get('latitude') : '',
      ];

      # Craete Address for User
      $address = $this->userAddress->create($data);

      # return response
      return response()->json([
         'code'      => (string)$this->successStatus, 
         'message'   => 'user Adrress has been stored Successfully.',
         'data'      => $address
      ]);
    } else {
      # return response
      return response()->json([
         'code'      => (string)$this->failedStatus, 
         'message'   => 'No user found on provided user Id.',
         'data'      => []
      ]);
    }
  }

  /** 
   * @method to fetch address from Address Id
   * @param request $request
   * @return json | Empty
   */
  public function fetchSingleAddress(Request $request)
  {
    # Validate request data
    $validator = Validator::make($request->all(), [ 
        'user_id'           => 'required|numeric',
        'address_id'        => 'required|numeric',
    ]);

    # If validator fails return response
    if ($validator->fails()) { 
        return response()->json(['error'=>$validator->errors()], 401);            
    }

    # get the user 
    $user = $this->user->find($request->get('user_id'));

    if($user != '') {
      $address = $this->userAddress
                      ->with(['country', 'state', 'city'])
                      ->find($request->get('address_id'));
      if($address != '') {
        $data = [
          'user_id'                         => (string)$address->user_id ?? '',
          'country_id'                      => (string)$address->country_id ?? '',
          'country_name'                    => (string)$address->country_name ?? '',
          'state_id'                        => (string)$address->state_id ?? '',
          'state_name'                      => (string)$address->state_name ?? '',
          'city_id'                         => (string)$address->city_id ?? '',
          'city_name'                       => (string)$address->city_name ?? '',
          'mobile'                          => (string)$address->mobile ?? '',
          'name'                            => (string)$address->name ?? '',
          'pin_code'                        => (string)$address->pin_code ?? '',
          'address'                         => (string)$address->address ?? '',
          'address_type'                    => (string)$address->address_type_string ?? '',
          'address_other_type_description'  => (string)$address->address_other_type_description ?? '',
        ];

        # return response
        return response()->json([
           'code'      => (string)$this->successStatus, 
           'message'   => 'Address found.',
           'data'      => $data
        ]);
      } else {
        # return response
        return response()->json([
           'code'      => (string)$this->failedStatus, 
           'message'   => 'No Address found on provided Address Id.',
           'data'      => []
        ]);
      }
    } else {
      # return response
      return response()->json([
         'code'      => (string)$this->failedStatus, 
         'message'   => 'No user found on provided user Id.',
         'data'      => []
      ]);
    }
  }
  

  /**
   * @method to store user Address
   * @param Request $request
   * @return Address Json
   */
  public function update(Request $request)
  {
    # Validate request data
    $validator = Validator::make($request->all(), [ 
        'user_id'         => 'required|numeric',
        'user_address_id' => 'required|string',
    ]);

    # If validator fails return response
    if ($validator->fails()) { 
        return response()->json(['error'=>$validator->errors()], 401);            
    }

    # get the user 
    $user = $this->user->find($request->get('user_id'));

    $addressType = 0;
    if($user != '') {
      $userAddress = $this->userAddress->find($request->get('user_address_id'));

      if($userAddress != '') {
         # get the Address type
        if($request->get('address_type') == 'home') {
          $addressType = 1;
        } elseif (($request->get('address_type') == 'work') OR ($request->get('address_type') == 'office')) {
          $addressType = 2;
        } else {
          $addressType = 3;
        }

        # gather the Data
        $data = [
          'name'                            => ($request->has('name') AND ($request->get('name') != '')) ? 
                                               $request->get('name') : $userAddress->name,
          'country_id'                      => ($request->has('country_id') AND ($request->get('country_id') != ''))                                    ? $request->get('country_id') : 
                                                  $userAddress->country_id,
          'state_id'                        => ($request->has('state_id') AND ($request->get('state_id') != '')) ? 
                                               $request->get('state_id') : $userAddress->country_id,
          'city_id'                         => ($request->has('city_id') AND ($request->get('city_id') != '')) ? 
                                               $request->get('city_id') : $userAddress->city_id,
          'mobile'                          => ($request->has('mobile') AND ($request->get('mobile') != '')) ? 
                                               $request->get('mobile') : $userAddress->mobile,
          'pin_code'                        => ($request->has('pin_code') AND ($request->get('pin_code') != '')) ? 
                                               $request->get('pin_code') : $userAddress->pin_code,
          'address'                         => ($request->has('address') AND ($request->get('address') != '')) ?
                                               $request->get('address') : $userAddress->address,
          'address_type'                    => ($addressType != 0) ? $addressType : $userAddress->address_type,
          'address_other_type_description'  => ($request->has('address_type_desc') AND 
                                                ($request->get('address_type_desc') != '')) ? 
                                               $request->get('address_type_desc') : 
                                               $userAddress->address_other_type_description,
          'status'                          => true,
          'longitude'                       => ($request->has('longitude') AND ($request->get('longitude') != '')) ? 
                                               $request->get('longitude') : $userAddress->longitude,
          'latitude'                        => ($request->has('latitude') AND ($request->get('latitude') != '')) ? 
                                               $request->get('latitude') : $userAddress->latitude,
        ];

        # Craete Address for User
        $address = $userAddress->update($data);

          # Set address relation
        $relation = [
          'country',
          'state',
          'city',
        ];

        # fetch the Updqated User Address
        $userAddress = $this->userAddress
                            ->with($relation)
                            ->find($request->get('user_address_id'));

        # fetch tyhe Country State City                    
        $country = $userAddress->country;
        $state = $userAddress->state;
        $city = $userAddress->city;
        $countryName = '';
        $stateName = '';
        $cityName = '';
        if($country != '') {
          $countryName = $country->name;
        }
        if($state != '') {
          $stateName = $state->name;
        }
        if($city != '') {
          $cityName = $city->name;
        }

        # gather the Data
          $data = [
            'address_id'                      => (string)$userAddress->id,
            'user_id'                         => (string)$user->id,
            'country'                         => (string)$countryName ?? '',
            'state'                           => (string)$stateName ?? '',
            'city'                            => (string)$cityName ?? '',
            'name'                            => (string)$userAddress->name ?? '',
            'mobile'                          => (string)$userAddress->mobile ?? '',
            'pin_code'                        => (string)$userAddress->pin_code ?? '',
            'address'                         => (string)$userAddress->address ?? '',
            'address_type'                    => (string)$userAddress->address_type_string ?? '',
            'address_other_type_description'  => (string)$userAddress->address_other_type_description ?? '',
            'longitude'                       => (string)$userAddress->longitude ?? '',
            'latitude'                        => (string)$userAddress->latitude ?? '',
          ];

        # return response
        return response()->json([
           'code'      => (string)$this->successStatus, 
           'message'   => 'User Address has been Updated.',
           'data'      => $data
        ]);
      } else {
        # return response
        return response()->json([
           'code'      => (string)$this->failedStatus, 
           'message'   => 'No user Address found on provided user address Id.',
           'data'      => []
        ]);
      }
      # return response
      return response()->json([
         'code'      => (string)$this->successStatus, 
         'message'   => 'user Adrress has been stored Successfully.',
         'data'      => $address
      ]);
    } else {
      # return response
      return response()->json([
         'code'      => (string)$this->failedStatus, 
         'message'   => 'No user found on provided user Id.',
         'data'      => []
      ]);
    }
  }

  /**
   * @method to delete the Addres of user
   * @param request
   * @return json
   */
  public function remove(Request $request)
  {
    # Validate request data
    $validator = Validator::make($request->all(), [ 
        'user_id'           => 'required|numeric',
        'user_address_id'   => 'required|string',
    ]);

    # If validator fails return response
    if ($validator->fails()) { 
        return response()->json(['error'=>$validator->errors()], 401);            
    }

    # get the user 
    $user = $this->user->find($request->get('user_id'));

    if($user != '') {
      $userAddress = $this->userAddress->find($request->get('user_address_id'));

      if($userAddress != '') {
        $userAddress->delete();

        # return response
        return response()->json([
           'code'      => (string)$this->successStatus, 
           'message'   => 'User Address deleted.',
           'data'      => []
        ]);
      } else {
        # return response
        return response()->json([
           'code'      => (string)$this->failedStatus, 
           'message'   => 'No user Address found on provided user address Id.',
           'data'      => []
        ]);
      }
    } else {
      # return response
      return response()->json([
         'code'      => (string)$this->failedStatus, 
         'message'   => 'No user found on provided user Id.',
         'data'      => []
      ]);
    }
  }
}
