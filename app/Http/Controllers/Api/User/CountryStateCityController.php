<?php

namespace App\Http\Controllers\Api\User;

use DB;
use Validator;
use App\Models\City; 
use App\Models\State; 
use App\Models\Country; 
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Traits\StatusTrait;
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller; 

class CountryStateCityController extends Controller
{
  use StatusTrait;

  # Variable to Bind Model
  protected $country;

  # Variable to Bind Model
  protected $state;

  # Variable to Bind Model
  protected $city;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct(Country $country, State $state, City $city)
  {
      $this->city     = $city;
      $this->state    = $state;
      $this->country  = $country;
  }

  /**
   * @method to fetch All Countries
   * @param Request $request
   * @return Countries Json
   */
  public function fetchCountries(Request $request)
  {
    # get the user 
    $countries = $this->country->get();
    $countryData = [];
    if($countries->isNotEmpty()) {
      foreach ($countries as $key => $country) {
        $data = [
          'id'    => (string)$country->id ?? '',
          'name'  => (string)$country->name ?? '',
        ];

        array_push($countryData, $data);
      }

      # return response
      return response()->json([
         'code'      => (string)$this->successStatus, 
         'message'   => 'Countries Found.',
         'data'      => $countryData
      ]);
    } else {
      # return response
      return response()->json([
         'code'      => (string)$this->failedStatus, 
         'message'   => 'No Country Found.',
         'data'      => []
      ]);
    }
  }

  /**
   * @method to store user Address
   * @param Request $request
   * @return Address Json
   */
  public function fetchStates(Request $request)
  {
    # Validate request data
    $validator = Validator::make($request->all(), [ 
        'country_id'        => 'required|numeric',
    ]);

    # If validator fails return response
    if ($validator->fails()) { 
        return response()->json(['error'=>$validator->errors()], 401);            
    }

    # get the user 
    $country = $this->country->with('states')->find($request->get('country_id'));

    if($country != '') {
      $states = $country->states;

      if($states->isNotEmpty()) {
        $statesData = [];
        foreach ($states as $key => $state) {
          $data = [
          'id'    => (string)$state->id ?? '',
          'name'  => (string)$state->name ?? '',
        ];

        array_push($statesData, $data);
        }
        # return response
        return response()->json([
           'code'      => (string)$this->successStatus, 
           'message'   => 'States Found.',
           'data'      => $statesData
        ]);
      } else {
        # return response
        return response()->json([
           'code'      => (string)$this->successStatus, 
           'message'   => 'No States on Provide Country Id.',
           'data'      => []
        ]);
      }        
    } else {
      # return response
      return response()->json([
         'code'      => (string)$this->failedStatus, 
         'message'   => 'No Country found on  provided Country Id.',
         'data'      => []
      ]);
    }
  }

  /**
   * @method to store user Address
   * @param Request $request
   * @return Address Json
   */
  public function fetchCities(Request $request)
  {
    # Validate request data
    $validator = Validator::make($request->all(), [ 
        'state_id'        => 'required|numeric',
    ]);

    # If validator fails return response
    if ($validator->fails()) { 
        return response()->json(['error'=>$validator->errors()], 401);            
    }

    # get the state 
    $state = $this->state
                  ->with('cities')
                  ->find($request->get('state_id'));

    if($state != '') {
      $cities = $state->cities;

      if($cities->isNotEmpty()) {
        $citiesData = [];
        foreach ($cities as $key => $city) {
          $data = [
          'id'    => (string)$city->id ?? '',
          'name'  => (string)$city->name ?? '',
        ];

        array_push($citiesData, $data);
        }
        # return response
        return response()->json([
           'code'      => (string)$this->successStatus, 
           'message'   => 'Cities Found.',
           'data'      => $citiesData
        ]);
      } else {
        # return response
        return response()->json([
           'code'      => (string)$this->successStatus, 
           'message'   => 'No City on Provide State Id.',
           'data'      => []
        ]);
      }        
    } else {
      # return response
      return response()->json([
         'code'      => (string)$this->failedStatus, 
         'message'   => 'No State found on  provided State Id.',
         'data'      => []
      ]);
    }
  }
}
