<?php

namespace App\Http\Controllers\Api\User;

use DB;
use Validator;
use App\Models\User; 
use App\Models\Dispute; 
use App\Models\UserAddress; 
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Traits\StatusTrait;
use App\Models\SubCategoryVariants; 
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller; 

class SendTextMessageController extends Controller
{
  use StatusTrait;

  # Variable to Bind Model
  protected $user;

  # Variable to Bind Model UserAddress
  protected $userAddress;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct(User $user, UserAddress $userAddress)
  {
    $this->user         = $user;
    $this->userAddress  = $userAddress;
  }

  /**
   *
   * Block comment
   *
   */
  public function sendOtp($value='')
  {
    # code...
  }
  
}
