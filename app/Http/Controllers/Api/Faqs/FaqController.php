<?php

namespace App\Http\Controllers\Api\Faqs;

use DB;
use Validator;

# Models
use App\Http\Traits\StatusTrait;
use App\Models\TermOfUse;
use App\Models\ContactUs;
use App\Models\HowItWorks;
use App\Models\PrivacyPolicy;
use App\Models\FaqPayingService;
use App\Models\FaqBookingService;
use App\Models\AboutDashspotless;
use App\Models\FaqDashspotlessGuide;

# Controllers
use App\Http\Controllers\Controller; 

# Vendor Classes
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

class FaqController extends Controller 
{
    use StatusTrait;

    # Variable to Bind Model TermOfUse
    protected $termOfUse;

    # Variable to Bind Model ContactUs
    protected $contactUs;

    # Variable to Bind Model HowItWorks
    protected $howItWorks;

    # Variable to Bind Model PrivacyPolicy
    protected $privacyPolicy;

    # Variable to Bind Model FaqPayingService
    protected $faqPayingService;

    # bind FaqBookingService Model
    protected $faqBookingService;

    # bind FaqDashspotlessGuide Model
    protected $faqDashspotlessGuide;

    # bind AboutDashspotless Model
    protected $aboutDashspotless;

    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct(
                                TermOfUse $termOfUse,
                                ContactUs $contactUs,
                                HowItWorks $howItWorks,
                                PrivacyPolicy $privacyPolicy,
                                FaqPayingService $faqPayingService, 
                                FaqBookingService $faqBookingService, 
                                FaqDashspotlessGuide $faqDashspotlessGuide,
                                AboutDashspotless $aboutDashspotless
                                )
    {
        $this->termOfUse                = $termOfUse;
        $this->contactUs                = $contactUs;
        $this->howItWorks               = $howItWorks;
        $this->privacyPolicy            = $privacyPolicy;
        $this->faqPayingService         = $faqPayingService;
        $this->faqBookingService        = $faqBookingService;
        $this->aboutDashspotless        = $aboutDashspotless;
        $this->faqDashspotlessGuide     = $faqDashspotlessGuide;
    }

    /**
     * @method to fetch all the Faqs of Booking Service
     * @param 
     * @return json | []
     */
    public function getBookingService()
    {

        # fetch all the Booking Service Faqs
        $faqBookingServices = $this->faqBookingService->all();

        $faqBookingServiceData = [];
        if($faqBookingServices->isNotEmpty()) {
            foreach ($faqBookingServices as $key => $faqBookingService) {
                $data = [
                    'id'            => $faqBookingService->id,
                    'title'         => $faqBookingService->title ?? '',
                    'description'   => $faqBookingService->description ?? '',
                ];

                array_push($faqBookingServiceData, $data);
            }

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'Faq Found on Booking Service.',
                'data'      => $faqBookingServiceData
            ]); 
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'No Faq Found on Booking Service.',
                'data'      => []
            ]); 
        }
    }

    /**
     * @method to fetch all the Faqs of Dashspotless guide
     * @param 
     * @return json | []
     */
    public function getDashspotlessGuide()
    {
        # fetch all the Dashspotless Guide Faqs
        $faqDashspotlessGuides = $this->faqDashspotlessGuide->all();

        $faqDashspotlessGuideData = [];
        if($faqDashspotlessGuides->isNotEmpty()) {
            foreach ($faqDashspotlessGuides as $key => $faqDashspotlessGuide) {
                $data = [
                    'id'            => $faqDashspotlessGuide->id,
                    'title'         => $faqDashspotlessGuide->title ?? '',
                    'description'   => $faqDashspotlessGuide->description ?? '',
                ];

                array_push($faqDashspotlessGuideData, $data);
            }

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'Faq Found on Dashspotless Guide.',
                'data'      => $faqDashspotlessGuideData
            ]); 
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'No Faq Found on Dashspotless guide.',
                'data'      => []
            ]); 
        }
    }

    /**
     * @method to fetch all the Faqs of Paying Service
     * @param 
     * @return json | []
     */
    public function getPayingService()
    {
        # fetch all the Paying Service Faqs
        $faqPayingServices = $this->faqPayingService->all();

        $faqpayingServiceData = [];
        if($faqPayingServices->isNotEmpty()) {
            foreach ($faqPayingServices as $key => $faqPayingService) {
                $data = [
                    'id'            => $faqPayingService->id,
                    'title'         => $faqPayingService->title ?? '',
                    'description'   => $faqPayingService->description ?? '',
                ];

                array_push($faqpayingServiceData, $data);
            }

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'Faq Found on Paying Service.',
                'data'      => $faqpayingServiceData
            ]); 
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'No Faq Found on Paying Service.',
                'data'      => []
            ]); 
        }
    }

    /**
     * @method to fetch all the About Dashspotless
     * @param 
     * @return json | []
     */
    public function aboutDashspotless()
    {
        # fetch all the Paying Service Faqs
        $aboutDashspotless = $this->aboutDashspotless->all();

        $aboutDashspotlessData = [];
        if($aboutDashspotless->isNotEmpty()) {
            foreach ($aboutDashspotless as $key => $dashspotlessData) {
                $data = [
                    'id'            => $dashspotlessData->id,
                    'title'         => $dashspotlessData->title ?? '',
                    'description'   => $dashspotlessData->description ?? '',
                ];

                array_push($aboutDashspotlessData, $data);
            }

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'About Dashspotless Data Found.',
                'data'      => $aboutDashspotlessData
            ]); 
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'About Dashspotless Data not Found.',
                'data'      => []
            ]); 
        }
    }

    /**
     * @method to fetch all the Term of Use
     * @param 
     * @return json | []
     */
    public function termOfUse()
    {
        # fetch all the Terms of Use
        $termsOfUse = $this->termOfUse->all();

        $termsOfUseData = [];
        if($termsOfUse->isNotEmpty()) {
            foreach ($termsOfUse as $key => $termOfUse) {
                $data = [
                    'id'            => $termOfUse->id,
                    'content'       => $termOfUse->content ?? '',
                ];

                array_push($termsOfUseData, $data);
            }

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'Terms of Use Found.',
                'data'      => $termsOfUseData
            ]); 
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Terms of Use not Found.',
                'data'      => []
            ]); 
        }
    }

     /**
     * @method to fetch all how It Works
     * @param 
     * @return json | []
     */
    public function howItWorks()
    {
        # fetch all the how It Works
        $howItWorks = $this->howItWorks->all();

        $howItWorksData = [];
        if($howItWorks->isNotEmpty()) {
            foreach ($howItWorks as $key => $howItWork) {
                $data = [
                    'id'                => $howItWork->id,
                    'content'           => $howItWork->content ?? '',
                ];

                array_push($howItWorksData, $data);
            }

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'How It Work Data Found.',
                'data'      => $howItWorksData
            ]); 
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'How It Work Data not Found.',
                'data'      => []
            ]); 
        }
    }

     /**
     * @method to fetch all privacy Policy
     * @param 
     * @return json | []
     */
    public function privacyPolicy()
    {
        # fetch all the privacy Policy
        $privacyPolicies = $this->privacyPolicy->all();

        $privacyPolicyData = [];
        if($privacyPolicies->isNotEmpty()) {
            foreach ($privacyPolicies as $key => $privacyPolicy) {
                $data = [
                    'id'            => $privacyPolicy->id,
                    'content'       => $privacyPolicy->content ?? '',
                ];

                array_push($privacyPolicyData, $data);
            }

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'Privacy Policy Data Found.',
                'data'      => $privacyPolicyData
            ]); 
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Privacy Policy Data not Found.',
                'data'      => []
            ]); 
        }
    }

     /**
     * @method to fetch all the contactUs
     * @param 
     * @return json | []
     */
    public function contactUs()
    {
        # fetch all the contact Us
        $contactsUs = $this->contactUs->all();

        $contactsUsData = [];
        if($contactsUs->isNotEmpty()) {
            foreach ($contactsUs as $key => $contactUs) {
                $data = [
                    'id'            => $contactUs->id,
                    'content'         => $contactUs->content ?? '',
                ];

                array_push($contactsUsData, $data);
            }

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'Contact Us Data Found.',
                'data'      => $contactsUsData
            ]); 
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Contact Us Data not Found.',
                'data'      => []
            ]); 
        }
    }
    
}
