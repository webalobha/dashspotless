<?php

namespace App\Http\Controllers\Api\Twilio;

use DB;
use Validator;
use Carbon\Carbon;

use App\Models\User; 
use App\Models\BookingSlot; 
use Twilio\Rest\Client;

use App\Http\Traits\StatusTrait;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller; 

class SmsController extends Controller
{
    use StatusTrait;

    # Variable to Bind Model
    protected $user;

    # Variable to Bind Booking Slot Model
    protected $bookingSlot;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * @method to Send Sms to Mobile Number
     * @param 
     *
     */
    public function sendSms($mobile, $otp)
    {
        # Fetch the Twilio Account Sid
        $accountSid = config('app.twilio')['TWILIO_ACCOUNT_SID'];

        # Fetch the Twilio Auth Token
        $authToken  = config('app.twilio')['TWILIO_AUTH_TOKEN'];

        # Fetch Twili App Id 
        $appSid     = config('app.twilio')['TWILIO_APP_SID'];

        # Set the Client
        $client     = new Client($accountSid, $authToken);
        try
        {
            # Use the client to do fun stuff like send text messages!
            $message = $client->messages->create(
                        # the number you'd like to send the message to
                        '+91'.$mobile,
                        array(
                            // A Twilio phone number you purchased at twilio.com/console
                            'from' => '+12056724298',
                            // the body of the text message you'd like to send
                            'body' => 'OTP: '.$otp
                        )
                     );
            return ;
        }
        catch (Exception $e)
        {
            echo "Error: " . $e->getMessage();
        }
    }
}
