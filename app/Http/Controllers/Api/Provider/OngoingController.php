<?php

namespace App\Http\Controllers\Api\Provider;

use DB;
use Validator;
use Carbon\Carbon;

# Models
use App\Http\Traits\StatusTrait;

use App\Models\Order;
use App\Models\Provider;
use App\Models\UserWallet;
use App\Models\OrderHistory;
use App\Models\AssignOrderToProvider;
use App\Models\AssignOrderToProviderHistory;

# Interface
use App\Http\Interfaces\OrderStatusInterface;
use App\Http\Interfaces\TransactionTypeInterface;
use App\Http\Interfaces\AssignOrderProviderStatusInterface;

# Controllers
use App\Http\Controllers\Controller; 

# Vendor Classes
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

class OngoingController extends Controller implements OrderStatusInterface, TransactionTypeInterface, AssignOrderProviderStatusInterface
{
    use StatusTrait;

    # Variable to Bind Model Order
    protected $order;

    # Variable to Bind Model Provider
    protected $provider;

    # Variable to Bind Model UserWallet
    protected $userWallet;

    # Variable to Bind Model OrderHistory
    protected $orderHistory;

    # Variable to Bind Model AssignOrderToProvider
    protected $assignOrderToProvider;

    # Variable to Bind Model AssignOrderToProviderHistory
    protected $assignOrderToProviderHistory;

    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct(
                                Order $order,
                                Provider $provider,
                                UserWallet $userWallet,
                                OrderHistory $orderHistory,
                                AssignOrderToProvider $assignOrderToProvider,
                                AssignOrderToProviderHistory $assignOrderToProviderHistory
                                )
    {
        $this->order                        = $order;
        $this->provider                     = $provider;
        $this->userWallet                   = $userWallet;
        $this->orderHistory                 = $orderHistory;
        $this->assignOrderToProvider        = $assignOrderToProvider;
        $this->assignOrderToProviderHistory = $assignOrderToProviderHistory;
    }

    /**
     * @method to fetch all the Leads Accepted or Cancelled
     * @param Request $request
     * @return json | []
     */
    public function fetchall(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'   => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # Set Relations
        $relations = [
            'assignOrders.order.user',
            'assignOrders.order.orderAddress',
            'assignOrders.provider',
            'assignOrders.statusHistory',
            'assignOrders.order.orderServices.service.subCategory',
        ];

        # fetch Provider
        $provider = $this->provider
                         ->with($relations)
                         ->find($request->get('provider_id'));

        if($provider != '') {
            $assignOrders = $provider->assignOrders->filter(function($assignOrder){
                                        $statusHistory = $this->assignOrderToProviderHistory
                                                              ->withTrashed()
                                                              ->where('assign_orderprovider_id', $assignOrder->id)
                                                              ->get();

                                        # check Reject Order
                                        $orderNotReject = ($statusHistory->last()->assign_order_status_id != AssignOrderProviderStatusInterface::REJECT);

                                        # Order Accepted By Provider
                                        $orderAccepted = $statusHistory->where('assign_order_status_id', AssignOrderProviderStatusInterface::ACCEPTED)->isNotEmpty();
                                        
                                        if($orderNotReject AND $orderAccepted) {
                                            return $assignOrder;
                                        }
                                    });

            if($assignOrders->isNotEmpty()) {
                $assignOrders = $assignOrders->sortByDesc('id');
                $ongoingArray = [];
                foreach ($assignOrders as $key => $assignOrder) {
                    $order              = $assignOrder->order;
                    $user               = $order->user;
                    $orderService       = $order->orderServices->first();
                    $orderDate          = $order->date_time_of_service->format('d-M-Y');
                    $orderTime          = $order->date_time_of_service->format('h:i A');
                    $userName           = $order->orderAddress->name ?? '';
                    $mobile             = $order->orderAddress->mobile ?? '';
                    $subCategoryName    = $orderService->service->subCategory->name;
                    $orderAmount        = $order->paid_amount ?? '';

                    $data = [
                        'assign_order_provider_id'  => (string)$assignOrder->id,
                        'provider_id'               => (string)$assignOrder->provider_id,
                        'order_id'                  => (string)$assignOrder->order_id,
                        'order_status'              => (string)$order->status_string,
                        'sub_category_name'         => (string)$subCategoryName,
                        'user_name'                 => (string)$userName,
                        'mobile'                    => (string)$mobile,
                        'order_date'                => (string)$orderDate,
                        'order_time'                => (string)$orderTime,
                        'credit_points'             => '50',
                        'amount'                    => (string)$orderAmount,
                        'unique_order_id'           => (string)$order->unique_order_id,
                        'order_placed_date'         => (string)$order->created_at->format('d-M-Y') ?? '',
                        'order_placed_time'         => (string)$order->created_at->format('h:i A') ?? '',
                    ];

                    array_push($ongoingArray, $data);
                }

                # return response
                return response()->json([
                    'code'      => (string)$this->successStatus, 
                    'message'   => 'Ongoing History.',
                    'data'      => $ongoingArray
                 ]);
            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'No Ongoing History.',
                    'data'      => []
                 ]);
            }
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'provider Not Found.',
                'data'      => []
             ]);
        }
    }

    /**
     * @method to fetch all the Leads Accepted or Cancelled
     * @param Request $request
     * @return json | []
     */
    public function fetchToday(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'   => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # Set Relations
        $relations = [
            'assignOrders.order.user',
            'assignOrders.order.orderAddress',
            'assignOrders.provider',
            'assignOrders.statusHistory',
            'assignOrders.order.orderServices.service.subCategory',
        ];

        # fetch Provider
        $provider = $this->provider
                         ->with($relations)
                         ->find($request->get('provider_id'));

        if($provider != '') {
            $assignOrders = $provider->assignOrders->filter(function($assignOrder){
                $orderDate  = $assignOrder->order->date_time_of_service->format('Y-m-d');
                $todayDate  = Carbon::now()->format('Y-m-d');
                                        $statusHistory = $this->assignOrderToProviderHistory
                                                              ->withTrashed()
                                                              ->where('assign_orderprovider_id', $assignOrder->id)
                                                              ->get()
                                                              ->last();
                                        if($statusHistory->assign_order_status_id != AssignOrderProviderStatusInterface::REJECT AND 
                                            $orderDate == $todayDate) {
                                            return $assignOrder;
                                        }
                                    });

            if($assignOrders->isNotEmpty()) {
                $assignOrders = $assignOrders->sortByDesc('id');
                $ongoingArray = [];
                foreach ($assignOrders as $key => $assignOrder) {
                    $order          = $assignOrder->order;
                    $user           = $order->user;
                    $orderService   = $order->orderServices->first();
                    $orderDate          = $order->date_time_of_service->format('d-M-Y');
                    $orderTime          = $order->date_time_of_service->format('h:i A');
                    $userName           = $order->orderAddress->name;
                    $mobile             = $order->orderAddress->mobile;
                    $subCategoryName    = $orderService->service->subCategory->name;
                    $orderAmount        = $order->paid_amount ?? '';

                    $data = [
                        'assign_order_provider_id'  => (string)$assignOrder->id,
                        'provider_id'               => (string)$assignOrder->provider_id,
                        'order_id'                  => (string)$assignOrder->order_id,
                        'order_status'              => (string)$order->status_string,
                        'sub_category_name'         => (string)$subCategoryName,
                        'user_name'                 => (string)$userName,
                        'mobile'                    => (string)$mobile,
                        'order_date'                => (string)$orderDate,
                        'order_time'                => (string)$orderTime,
                        'credit_points'             => '50',
                        'amount'                    => (string)$orderAmount,
                        'unique_order_id'           => (string)$order->unique_order_id,
                        'order_placed_date'         => (string)$order->created_at->format('d-M-Y') ?? '',
                        'order_placed_time'         => (string)$order->created_at->format('h:i A') ?? '',
                    ];

                    array_push($ongoingArray, $data);
                }

                # return response
                return response()->json([
                    'code'      => (string)$this->successStatus, 
                    'message'   => 'Ongoing History.',
                    'data'      => $ongoingArray
                 ]);
            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'No Ongoing History for Today.',
                    'data'      => []
                 ]);
            }
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'provider Not Found.',
                'data'      => []
             ]);
        }
    }

    /**
     * @method to fetch all the Leads Accepted or Cancelled
     * @param Request $request
     * @return json | []
     */
    public function fetchTomorrow(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'   => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # Set Relations
        $relations = [
            'assignOrders.order.user',
            'assignOrders.order.orderAddress',
            'assignOrders.provider',
            'assignOrders.statusHistory',
            'assignOrders.order.orderServices.service.subCategory',
        ];

        # fetch Provider
        $provider = $this->provider
                         ->with($relations)
                         ->find($request->get('provider_id'));

        if($provider != '') {
            $assignOrders = $provider->assignOrders->filter(function($assignOrder){
                $orderDate  = $assignOrder->order->date_time_of_service->format('Y-m-d');
                $tomorrowDate  = Carbon::now()->addDays(1)->format('Y-m-d');
                                        $statusHistory = $this->assignOrderToProviderHistory
                                                              ->withTrashed()
                                                              ->where('assign_orderprovider_id', $assignOrder->id)
                                                              ->get()
                                                              ->last();
                                        if($statusHistory->assign_order_status_id != AssignOrderProviderStatusInterface::REJECT AND 
                                            $orderDate == $tomorrowDate) {
                                            return $assignOrder;
                                        }
                                    });

            if($assignOrders->isNotEmpty()) {
                $assignOrders = $assignOrders->sortByDesc('id');
                $ongoingArray = [];
                foreach ($assignOrders as $key => $assignOrder) {
                    $order          = $assignOrder->order;
                    $user           = $order->user;
                    $orderService   = $order->orderServices->first();
                    $orderDate          = $order->date_time_of_service->format('d-M-Y');
                    $orderTime          = $order->date_time_of_service->format('h:i A');
                    $userName           = $order->orderAddress->name;
                    $mobile             = $order->orderAddress->mobile;
                    $subCategoryName    = $orderService->service->subCategory->name;
                    $orderAmount        = $order->paid_amount ?? '';

                    $data = [
                        'assign_order_provider_id'  => (string)$assignOrder->id,
                        'provider_id'               => (string)$assignOrder->provider_id,
                        'order_id'                  => (string)$assignOrder->order_id,
                        'order_status'              => (string)$order->status_string,
                        'sub_category_name'         => (string)$subCategoryName,
                        'user_name'                 => (string)$userName,
                        'mobile'                    => (string)$mobile,
                        'order_date'                => (string)$orderDate,
                        'order_time'                => (string)$orderTime,
                        'credit_points'             => '50',
                        'amount'                    => (string)$orderAmount,
                        'unique_order_id'           => (string)$order->unique_order_id,
                        'order_placed_date'         => (string)$order->created_at->format('d-M-Y') ?? '',
                        'order_placed_time'         => (string)$order->created_at->format('h:i A') ?? '',
                    ];

                    array_push($ongoingArray, $data);
                }

                # return response
                return response()->json([
                    'code'      => (string)$this->successStatus, 
                    'message'   => 'Ongoing History.',
                    'data'      => $ongoingArray
                 ]);
            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'No Ongoing History for Tomorrow.',
                    'data'      => []
                 ]);
            }
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'provider Not Found.',
                'data'      => []
             ]);
        }
    }

    /**
     * @method to fetch all the Leads Accepted or Cancelled
     * @param Request $request
     * @return json | []
     */
    public function fetchWeekly(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'   => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # Set Relations
        $relations = [
            'assignOrders.order.user',
            'assignOrders.order.orderAddress',
            'assignOrders.provider',
            'assignOrders.statusHistory',
            'assignOrders.order.orderServices.service.subCategory',
        ];

        # fetch Provider
        $provider = $this->provider
                         ->with($relations)
                         ->find($request->get('provider_id'));

        if($provider != '') {
            $assignOrders = $provider->assignOrders->filter(function($assignOrder){
                $orderDate  = $assignOrder->order->date_time_of_service;
                $startDate  = Carbon::now()->startOfWeek();
                $endDate  = Carbon::now()->endOfWeek();
                                        $statusHistory = $this->assignOrderToProviderHistory
                                                              ->withTrashed()
                                                              ->where('assign_orderprovider_id', $assignOrder->id)
                                                              ->get()
                                                              ->last();
                                        if($statusHistory->assign_order_status_id != AssignOrderProviderStatusInterface::REJECT AND 
                                            $orderDate->between($startDate, $endDate)) {
                                            return $assignOrder;
                                        }
                                    });

            if($assignOrders->isNotEmpty()) {
                $assignOrders = $assignOrders->sortByDesc('id');
                $ongoingArray = [];
                foreach ($assignOrders as $key => $assignOrder) {
                    $order          = $assignOrder->order;
                    $user           = $order->user;
                    $orderService   = $order->orderServices->first();
                    $orderDate          = $order->date_time_of_service->format('d-M-Y');
                    $orderTime          = $order->date_time_of_service->format('h:i A');
                    $userName           = $order->orderAddress->name ?? '';
                    $mobile             = $order->orderAddress->mobile ?? '';
                    $subCategoryName    = $orderService->service->subCategory->name;
                     $orderAmount        = $order->paid_amount ?? '';

                    $data = [
                        'assign_order_provider_id'  => (string)$assignOrder->id,
                        'provider_id'               => (string)$assignOrder->provider_id,
                        'order_id'                  => (string)$assignOrder->order_id,
                        'order_status'              => (string)$order->status_string,
                        'sub_category_name'         => (string)$subCategoryName,
                        'user_name'                 => (string)$userName,
                        'mobile'                    => (string)$mobile,
                        'order_date'                => (string)$orderDate,
                        'order_time'                => (string)$orderTime,
                        'credit_points'             => '50',
                        'amount'                    => (string)$orderAmount,
                        'unique_order_id'           => (string)$order->unique_order_id,
                        'order_placed_date'         => (string)$order->created_at->format('d-M-Y') ?? '',
                        'order_placed_time'         => (string)$order->created_at->format('h:i A') ?? '',
                    ];

                    array_push($ongoingArray, $data);
                }

                # return response
                return response()->json([
                    'code'      => (string)$this->successStatus, 
                    'message'   => 'Ongoing History.',
                    'data'      => $ongoingArray
                 ]);
            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'No Ongoing History for Weekly.',
                    'data'      => []
                 ]);
            }
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'provider Not Found.',
                'data'      => []
             ]);
        }
    }

    /**
     * @method to fetch single assign Order Detail
     * @param Request $request
     * @return json | []
     */
    public function ongoingDetail(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'assign_order_id'   => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # Set Relations
        $relations = [
            'order.user',
            'order.orderAddress',
            'provider',
            'statusHistory',
            'order.orderServices.service.subCategory',
        ];

        # fetch Assign Order
        $assignOrder = $this->assignOrderToProvider
                         ->with($relations)
                         ->find($request->get('assign_order_id'));

        if($assignOrder != '') {
                $order              = $assignOrder->order;
                $user               = $order->user;
                $orderService       = $order->orderServices->first();
                $orderDate          = $order->date_time_of_service->format('d-M-Y');
                $orderTime          = $order->date_time_of_service->format('h:i A');
                $userName           = $order->orderAddress->name ?? '';
                $mobile             = $order->orderAddress->mobile ?? '';
                $address            = $order->orderAddress->address ?? '';
                $amount             = $order->paid_amount ?? '';
                $subCategoryName    = $orderService->service->subCategory->name ?? '';

                # fetch Services related to Order
                $orderservices = $order->orderServices;
                $servicesArray = [];

                if($orderservices->isNotEmpty()) {
                    foreach ($orderservices as $key => $orderService) {
                        $service = $orderService->service;
                        $data = [
                            'service_icon'  => (string)$service->icon_path ?? '',
                            'service_name'  => (string)$service->title ?? '',
                            'quantity'      => (string)$orderService->quantity ?? '',
                            'amount'        => (string)$orderService->payable_amount ?? '',
                            'duration'      => (string)$orderService->service_time_taken ?? '',
                        ];

                        array_push($servicesArray, $data);
                    }
                }

                # Set Data
                $data = [
                    'assign_order_provider_id'  => (string)$assignOrder->id,
                    'provider_id'               => (string)$assignOrder->provider_id,
                    'order_id'                  => (string)$assignOrder->order_id,
                    'unique_order_id'           => (string)$order->unique_order_id,
                    'order_status'               => (string)$order->status_string,
                    'sub_category_name'         => (string)$subCategoryName,
                    'user_name'                 => (string)$userName,
                    'mobile'                    => (string)$mobile,
                    'address'                   => (string)$address,
                    'amount'                    => (string)$amount,
                    'order_date'                => (string)$orderDate,
                    'order_time'                => (string)$orderTime,
                    'service_details'           => $servicesArray,
                    'credit_points'             => '50',
                    'order_placed_date'         => (string)$order->created_at->format('d-M-Y') ?? '',
                    'order_placed_time'         => (string)$order->created_at->format('h:i A') ?? '',
                ];

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'Ongoing Lead Detail.',
                'data'      => $data
             ]);
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Assign Order not Found.',
                'data'      => []
             ]);
        }
    }
    
}
