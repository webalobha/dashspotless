<?php

namespace App\Http\Controllers\Api\Provider;

use DB;
use Validator;

# Models
use App\Models\Provider; 
use App\Models\PricePerPoint; 
use App\Models\CreditPointsPackage; 
use App\Models\ProviderNotification;
use App\Models\ProviderBoughtPackage; 
use App\Http\Traits\StatusTrait;

# Interfaces
use App\Http\Interfaces\OrderStatusInterface;

# Controllers
use App\Http\Controllers\Controller; 
use App\Http\Controllers\Api\Provider\NotifyProvider;

# Vendor Classes
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

class CreditPointController extends Controller implements OrderStatusInterface
{
    use StatusTrait;

    # Variable to Bind Model
    protected $provider;

    # bind CreditPointsPackage Model
    protected $creditPointsPackage;

    # bind PricePerPoint Model
    protected $pricePerPoint;

    # bind PricePerPoint Model
    protected $ProviderNotification;

    # bind ProviderBoughtPackage Model
    protected $providerBoughtPackage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
                                Provider $provider,
                                PricePerPoint $pricePerPoint,
                                ProviderNotification $providerNotification,
                                CreditPointsPackage $creditPointsPackage,
                                ProviderBoughtPackage $providerBoughtPackage
                            )
    {
        $this->provider                 = $provider;
        $this->pricePerPoint            = $pricePerPoint;
        $this->creditPointsPackage      = $creditPointsPackage;
        $this->providerNotification     = $providerNotification;
        $this->providerBoughtPackage    = $providerBoughtPackage;
    }

    /**
     * @method to fetch all the Credit Pckages form Backend
     * @param Request $request
     * @return json | []
     */
    
    public function fetchAllPackages(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'           => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        
        # fetch the Provider
        $provider = $this->provider
                         ->with('boughtPackages')
                         ->find($request->get('provider_id'));

        # Provider is Not Avaialble
        if($provider == '') {
             # return response
            return response()->json([
                'code'              => (string)$this->failedStatus, 
                'message'           => 'Provider not Found.',
                'data'              => $creditPackageArray
            ]); 
        }

        # fetch all the Bought Packages
        $boughtPackages = $provider->boughtPackages;

        # Set Total Points
        $totalPointsAvailable = 0;
        if($boughtPackages->isNotEmpty()) {
            $totalPointsAvailable = $boughtPackages->sum('no_of_points');
        } 

        # Fetch all the Available Credit Packages
        $creditPackages = $this->creditPointsPackage->all();

        if($creditPackages->isNotEmpty()) {
            $creditPackageArray = [];
            foreach ($creditPackages as $key => $creditPackage) {
                $data = [
                    'id'                => (string)$creditPackage->id ?? '',
                    'name'              => (string)$creditPackage->name ?? '',
                    'number_of_points'  => (string)$creditPackage->number_of_points ?? '', 
                    'actual_price'      => (string)$creditPackage->actual_price ?? '', 
                    'dicount_title'     => (string)$creditPackage->dicount_title ?? '',
                    'discount_given'    => (string)$creditPackage->discount_string ?? '',
                    'final_amount'      => (string)$creditPackage->discounted_amount,     
                ];

                array_push($creditPackageArray, $data);
            }

            #Set Price PerPoint
            $pricePerPoint = $this->pricePerPoint->all()->isNotEmpty() ? $this->pricePerPoint->all()->first()->price : 50;

            # return response
            return response()->json([
                'code'                  => (string)$this->successStatus, 
                'message'               => 'Package Found.',
                'total_points_avail'    => $totalPointsAvailable,
                'price_per_point'       => $pricePerPoint,
                'data'                  => $creditPackageArray
            ]); 
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'No Package Found.',
                'data'      => []
            ]); 
        }
    }


    /**
     * @method to fetch all the Credit Pckages form Backend
     * @param Request $request
     * @return json | []
     */
    public function buyPackage(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'           => 'required|numeric',
            'credit_package_id'     => 'required|numeric',
            'no_of_points'          => 'required|numeric',
            'actual_price'          => 'required|numeric',
            'discounted_amount'     => 'required|numeric',
            'transaction_id'        => 'required|string',
            'payment_amount'        => 'required|numeric',
            'account_holder_name'   => 'required|string',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        
        # fetch the Provider
        $provider = $this->provider
                         ->find($request->get('provider_id'));

        # Fetch all the Available Credit Packages
        $creditPackages = $this->creditPointsPackage->all();

        if($provider != '') {
           $data = [
                'provider_id'           => $request->get('provider_id') ?? '',
                'credit_package_id'     => $request->get('credit_package_id') ?? '',
                'no_of_points'          => $request->get('no_of_points') ?? '',
                'package_name'          => $request->get('package_name') ?? '',
                'actual_price'          => $request->get('actual_price') ?? '',
                'dicount_title'         => $request->get('dicount_title') ?? '',
                'discount_given'        => $request->get('discount_given') ?? '',
                'discounted_amount'     => $request->get('discounted_amount') ?? '',
                'transaction_id'        => $request->get('transaction_id') ?? '',
                'payment_type'          => $request->get('payment_type') ?? '',
                'payment_amount'        => $request->get('payment_amount') ?? '',
                'card_type'             => $request->get('card_type') ?? '',
                'card_name'             => $request->get('card_name') ?? '',
                'account_holder_name'   => $request->get('account_holder_name') ?? '',
                'bank_name'             => $request->get('bank_name') ?? '',
                'card_number'           => $request->get('card_number') ?? '',
           ];

           # Create the Buy Package
           $this->providerBoughtPackage->create($data);

             # Notify Providder for lEad
            $notifyProvider = new NotifyProvider();

            # Set Title message
            $title = 'Mesee Notification';

            # Set Message
            $message = 'Credit Point Bought Successfully';

            # Notify User
            $notifyProvider->notify($provider, $title, $message);

            # Save Send Notification 
            $data = [
                'provider_id'   => $provider->id,
                'order_id'      => 0,
                'notification'  => $message,
            ];

            # Creta UserNotification
            $this->providerNotification->create($data);

           # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'Package Bought Successfully.',
                'data'      => []
            ]); 
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Provider Not Found.',
                'data'      => []
            ]); 
        }
    }

    /**
     * @method to fetch all the Credit Pckages form Backend
     * @param Request $request
     * @return json | []
     */
    public function fetchProviderBoughtPackages(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'           => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        
        # Relations
        $relations = ['orders.history', 'boughtPackages'];

        # fetch the Provider
        $provider = $this->provider
                         ->with($relations)
                         ->find($request->get('provider_id'));

        if($provider != '') {
            # Fetch all the Packages
           $packages = $provider->boughtPackages;

           # Sort Packages Desc
           $packages = $packages->sortByDesc('id');
           
           if($packages->isNotEmpty()) {
                $packageArray = [];
                foreach ($packages as $key => $package) {
                    $data = [
                        'id'                        => (string)$package->id ?? '',
                        'no_of_points'              => (string)$package->no_of_points ?? '',
                        'package_name'              => (string)$package->package_name ?? '',
                        'actual_price'              => (string)$package->actual_price ?? '',
                        'discount_given'            => (string)$package->discount_given ?? '',
                        'discounted_amount'         => (string)$package->discounted_amount ?? '',
                    ];

                    array_push($packageArray, $data);
                }

                # FEtch available Points in Provider Account
                $totalPoints = (string)$packages->sum('no_of_points');

                # return response
                return response()->json([
                    'code'                      => (string)$this->successStatus, 
                    'message'                   => 'Package Bought yet.',
                    'total_points_avail'        => $totalPoints,
                    'remaining_credit_points'   => (string)$provider->remaining_credit_points ?? '',
                    'data'                      => $packageArray
                ]); 
           } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'No Package Bought yet.',
                    'data'      => []
                ]); 
           }
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Provider Not Found.',
                'data'      => []
            ]); 
        }
    } 

    /**
     * @method to fetch all the Credit Pckages form Backend
     * @param Request $request
     * @return json | []
     */
    public function fetchExpensesCreditPoints(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'           => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        
        # Relations
        $relations = ['orders.history'];

        # fetch the Provider
        $provider = $this->provider
                         ->with($relations)
                         ->find($request->get('provider_id'));

        if($provider != '') {
           # Fetch all the Orders of Provider
            $orders = $provider->orders;

            # Fetch the Orders that are completed by Provider
            $fOrders = $orders->filter(function($order) {
                # fetch History of Order
                $orderHistory = $order->history;
                if($orderHistory->isNotEmpty()) {
                    # fetch order History to check for Completed Order By Provider
                    $completedOrders = $orderHistory->where('order_status_id', OrderStatusInterface::COMPLETED);
                    if($completedOrders->isNotEmpty()) {
                        return $order;
                    }
                }
            });

            $expenseOrderArray = [];
            if($fOrders->isNotEmpty()) {
                foreach ($fOrders as $key => $order) {
                    $data = [
                        'provider_id'               => (string)$provider->id,
                        'order_id'                  => (string)$order->id,
                        'unique_order_id'           => (string)$order->unique_order_id,
                        'credit_point_used'         => (string)$order->credit_points,
                    ];

                    array_push($expenseOrderArray, $data);
                }

                # return response
                return response()->json([
                    'code'                      => (string)$this->successStatus, 
                    'message'                   => 'Credit Expenses',
                    'remaining_credit_points'   => (string)$provider->remaining_credit_points ?? '',
                    'data'                      => $expenseOrderArray
                ]); 

            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'No Credit Use till',
                    'data'      => []
                ]); 
            }
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Provider Not Found.',
                'data'      => []
            ]); 
        }
    }
}
