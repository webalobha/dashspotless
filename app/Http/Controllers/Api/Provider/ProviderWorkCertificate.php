<?php

namespace App\Http\Controllers\Api\Provider;

use DB;
use Validator;

# Models
use App\Http\Traits\StatusTrait;

use App\Models\Provider;
use App\Models\WorkPhotos;
use App\Models\AwardAndCertificate;

# Controllers
use App\Http\Controllers\Controller; 

# Vendor Classes
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

class ProviderWorkCertificate extends Controller 
{
    use StatusTrait;

    # Variable to Bind Model Provider
    protected $provider;

    # Variable to Bind Model WorkPhotos
    protected $workPhotos;

    # Variable to Bind Model AwardAndCertificate
    protected $awardAndCertificate;

    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct(
                                Provider $provider,
                                WorkPhotos $workPhotos,
                                AwardAndCertificate $awardAndCertificate
                                )
    {
        $this->provider                = $provider;
        $this->workPhotos                = $workPhotos;
        $this->awardAndCertificate       = $awardAndCertificate;
    }

    /**
     * @method Upload work photos
     * @param Request $request
     * @return json
     */
    public function uploadWorkPhotos(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'  => 'required|numeric',
            'image'        => 'required|file',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        
        # Fetch file from Request
        $document = $request->file('image');

        # set destination path.
        $destinationpath    = base_path() .'/public/images/provider_award_certificate_work';

        # get file name.
        $filename           = $document->getClientOriginalName();

        # get today date.
        $today_date         = date('d-m-Y');

        # get a random number.
        $random_number      = rand(1111, 9999);
                                           
        # set filname with today date, random number, filename.
        $filenameData       = $today_date . '_' . $random_number .'___'. $filename;

        # file move from current path to destination path.
        $movefilename       = $document->move($destinationpath, $filenameData);

        # Set path for Database
        $databsePathForImage = 'images/provider_award_certificate_work/'.$filenameData;

        # Set data
        $data = [
            'provider_id'       => $request->get('provider_id'),
            'image_name'        => $filename,
            'image_path'        => $databsePathForImage,
        ];

        # Create the Work Photo
        $this->workPhotos->create($data);

        # return response
        return response()->json([
            'code'      => (string)$this->successStatus, 
            'message'   => 'Provider Work Document Uploaded Successfully.',
            'data'      => $data
         ]);
    }

    /**
     * @method to fetch all the workPhotos
     * @param Request $request
     * @return json | []
     */
    public function fetchWorkPhotos(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'  => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # fetch the Provider
        $provider = $this->provider
                         ->with(['workPhotos'])
                         ->find($request->get('provider_id'));

        if($provider != '') {
            # fetch all the work photos of Provider
            $workPhotos = $provider->workPhotos;
            if($workPhotos->isNotEmpty()) {
                $workPhotoArray = [];
                foreach ($workPhotos as $key => $workPhoto) {
                    $data = [
                        'id'            => (string)$workPhoto->id ?? '',
                        'image_name'    => (string)$workPhoto->image_name ?? '',
                        'image_path'    => (string)$workPhoto->image_path ?? '',
                    ];

                    array_push($workPhotoArray, $data);
                }

                # return response
                return response()->json([
                    'code'      => (string)$this->successStatus, 
                    'message'   => 'Providers Work Photos.',
                    'data'      => $workPhotoArray
                ]);
            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'Provider has not uploaded any work Photo',
                    'data'      => []
                 ]);
            }
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Provider not Found with provided Id.',
                'data'      => []
             ]); 
        }              
    }
    

    /**
     * @method Upload work photos
     * @param Request $request
     * @return json
     */
    public function uploadAwardCertificate(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'  => 'required|numeric',
            'image'        => 'required|file',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        
        # Fetch file from Request
        $document = $request->file('image');

        # set destination path.
        $destinationpath    = base_path() .'/public/images/provider_award_certificate_work';

        # get file name.
        $filename           = $document->getClientOriginalName();

        # get today date.
        $today_date         = date('d-m-Y');

        # get a random number.
        $random_number      = rand(1111, 9999);
                                           
        # set filname with today date, random number, filename.
        $filenameData       = $today_date . '_' . $random_number .'___'. $filename;

        # file move from current path to destination path.
        $movefilename       = $document->move($destinationpath, $filenameData);

        # Set path for Database
        $databsePathForImage = 'images/provider_award_certificate_work/'.$filenameData;

        # Set data
        $data = [
            'provider_id'       => $request->get('provider_id'),
            'image_name'        => $filename,
            'image_path'        => $databsePathForImage,
        ];

        # Create the Award Certificate
        $this->awardAndCertificate->create($data);

        # return response
        return response()->json([
            'code'      => (string)$this->successStatus, 
            'message'   => 'Provider Award Certificate Uploaded Successfully.',
            'data'      => $data
         ]);
    }
    

    /**
     * @method to fetch all the workPhotos
     * @param Request $request
     * @return json | []
     */
    public function fetchAwardCertificate(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'  => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # fetch the Provider
        $provider = $this->provider
                         ->with(['awardsCertificate'])
                         ->find($request->get('provider_id'));

        if($provider != '') {
            # fetch all the Award And Certificate of Provider
            $awardCertificates = $provider->awardsCertificate;
            if($awardCertificates->isNotEmpty()) {
                $awardsArray = [];
                foreach ($awardCertificates as $key => $awardCertificate) {
                    $data = [
                        'id'            => (string)$awardCertificate->id ?? '',
                        'image_name'    => (string)$awardCertificate->image_name ?? '',
                        'image_path'    => (string)$awardCertificate->image_path ?? '',
                    ];

                    array_push($awardsArray, $data);
                }

                # return response
                return response()->json([
                    'code'      => (string)$this->successStatus, 
                    'message'   => 'Providers Awards and Certificate.',
                    'data'      => $awardsArray
                ]);
            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'Provider has not uploaded any Award Certificate',
                    'data'      => []
                 ]);
            }
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Provider not Found with provided Id.',
                'data'      => []
             ]); 
        }              
    }
}
