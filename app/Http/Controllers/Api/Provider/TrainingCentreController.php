<?php

namespace App\Http\Controllers\Api\Provider;

use DB;
use Validator;

# Models
use App\Models\Provider; 
use App\Models\TrainingCenter; 
use App\Http\Traits\StatusTrait;

# Controllers
use App\Http\Controllers\Controller; 

# Vendor Classes
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

class TrainingCentreController extends Controller 
{
    use StatusTrait;

    # Variable to Bind Model
    protected $provider;

    # Variable to Bind Model
    protected $trainingCenter;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
                                Provider $provider,
                                TrainingCenter $trainingCenter
                            )
    {
        $this->provider                 = $provider;
        $this->trainingCenter            = $trainingCenter;
    }

    /**
     * @method to fetch all the training centres
     * @param Request $request
     * @return json | []
     */
    
    public function fetch()
    {
        # fetch all the Training Centeres
        $trainingCentres = $this->trainingCenter->all();

        if($trainingCentres->isNotEmpty()) {
            $trainingCentreArray = [];
            foreach ($trainingCentres as $key => $trainingCentre) {
                $url = $trainingCentre->url;
                $splitUrlArray = explode('=', $url);
                $data = [
                    'id'        => $trainingCentre->id,
                    'title'     => $trainingCentre->title ?? '',
                    'url'       => $trainingCentre->url ?? '',
                    'url_id'    => $splitUrlArray[1] ?? '',
                ];

                array_push($trainingCentreArray, $data);
            }

            # return response
            return response()->json([
               'code'               => (string)$this->successStatus, 
               'message'            => 'Training centre Found.',
               'data'               => $trainingCentreArray
            ]); 
        } else {
            # return response
            return response()->json([
               'code'               => (string)$this->failedStatus, 
               'message'            => 'Training centre not Found.',
               'data'               => []
            ]); 
        }
    }
}
