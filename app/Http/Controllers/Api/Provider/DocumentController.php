<?php

namespace App\Http\Controllers\Api\Provider;

use DB;
use Validator;

# Models
use App\Models\Provider; 
use App\Models\ProviderDocument; 
use App\Http\Traits\StatusTrait;
use App\Models\ProviderBankDetail; 
use App\Models\MandatoryDocumentsForProvider; 

# interface
use App\Http\Interfaces\DocumentStatusHistoryInterface;

# Controllers
use App\Http\Controllers\Controller; 

# Vendor Classes
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

class DocumentController extends Controller implements DocumentStatusHistoryInterface
{
    use StatusTrait;

    # Variable to Bind Model
    protected $provider;

    # bind ProviderDocument Model
    protected $providerDocument;

    # bind ProviderBankDetail Model
    protected $providerBankDetail;

    # bind Provider Mandatory Document Model
    protected $providerMandatoryDocument;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
                                Provider $provider, 
                                ProviderDocument $providerDocument,
                                ProviderBankDetail $providerBankDetail,
                                MandatoryDocumentsForProvider $mandatoryDocumentsForProvider
                            )
    {
        $this->provider                             = $provider;
        $this->providerDocument                     = $providerDocument;
        $this->providerBankDetail                   = $providerBankDetail;
        $this->mandatoryDocumentsForProvider        = $mandatoryDocumentsForProvider;
    }

    /**
     *
     * Block comment
     *
     */
    public function getMandatoryDocument()
    {
        # Fetch all the Manad Documents
        $documents = $this->mandatoryDocumentsForProvider->all();

        # Initialize Response Data
        $responseData = [];
        if($documents->isNotEmpty()) {
            foreach ($documents as $key => $document) {
                $data = [
                    'id'    => (string)$document->id ?? '',
                    'name'  => (string)$document->name ?? '',
                ];

                # Push Data
                array_push($responseData, $data);
            }

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'Mandatory Documents Found.',
                'data'      => $responseData
             ]); 
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'No Document Found.',
                'data'      => []
             ]); 
        }
    }
    

    /**
     * @method to uplaod the Document of Provider
     * @param request
     * @return Json
     */
    public function uploadDocument(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'       => 'required|numeric',
            'doc_ids'           => 'required|string',
            'doc_images'        => 'required|array',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        
        # fetch the Provider
        $provider = $this->provider
                         ->with(['documents'])
                         ->find($request->get('provider_id'));

        $documents = [];

        #dd($request->all());
        $docIds = explode(',', $request->get('doc_ids'));
        $docImages = $request->file('doc_images');

        # check if doc_ids and doc images count is Same
        if(count($docIds) != count($docImages)) {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Please Upload same number of document images as number of Documents.',
                'data'      => []
             ]); 
        }

        # Set the Document Data
        foreach ($docIds as $key => $docId) {
            $documents[$docId] = $docImages[$key];
        }

        if($provider != '') {
            foreach ($documents as $key => $document) {
                # set destination path.
                $destinationpath    = base_path() .'/public/images/provider_documents';

                # get file name.
                $filename           = $document->getClientOriginalName();

                # get today date.
                $today_date         = date('d-m-Y');

                # get a random number.
                $random_number      = rand(1111, 9999);

                # set filname with today date, random number, filename.
                $filenameData       = $today_date . '_' . $random_number .'___'. $filename;

                # file move from current path to destination path.
                $movefilename       = $document->move($destinationpath, $filenameData);

                # Set path for Database
                $databsePathForImage = 'images/provider_documents/'.$filenameData;

                # Set data
                $data = [
                    'provider_id'           => $provider->id,
                    'document_id'           => $key,
                    'document_name'         => $filename,
                    'document_image_path'   => $databsePathForImage,
                ];

                # fetch if Provider already have that document on same Document Id
                $documentExist = $provider->documents->where('document_id', $key);

                # if document already Exist than delete it
                if($documentExist->isNotEmpty()) {
                      $this->providerDocument
                           ->where('provider_id', $provider->id)
                           ->where('document_id', $key)
                           ->delete();
                }
              
                # Store Data
                $this->providerDocument->create($data);
            }
            # return response
                return response()->json([
                    'code'      => (string)$this->successStatus, 
                    'message'   => 'Provider Documets has been Updated Successfully.',
                    'data'      => []
                 ]);
        } else {
             # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Provider not Found with provided Id.',
                'data'      => []
             ]); 
        }
    }

    /**
     * @method to get all the uploaded Documents
     * @param Request $request
     * @return Json Data of Provider Documents
     */
    public function getUploadDocument(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'       => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        
        # fetch the Provider
        $provider = $this->provider
                         ->with(['documents.histories', 'documents.documentName'])
                         ->find($request->get('provider_id'));

        # provider Document Data
        $providerDocumentData = [];
        if($provider != '') {
            $providerDocuments = $provider->documents;
            if($providerDocuments->isNotEmpty()) {
                foreach ($providerDocuments as $key => $providerDocument) {
                    $status = ($providerDocument->history AND $providerDocument->history->isNotEmpty()) ?
                              $providerDocument->history->last()->status : 'Pending';
                    $reason = ($providerDocument->history AND $providerDocument->history->isNotEmpty()) ?
                              $providerDocument->history->last()->reason  : '';

                    $data = [
                        'document_id'               => (string)$providerDocument->document_id,
                        'provider_document_id'      => (string)$providerDocument->id ?? '',
                        'document_image_path'       => (string)$providerDocument->document_image_path ?? '',
                        'document_name'             => (string)$providerDocument->documentName->name ?? '',
                        'status'                    => $status,
                        'reason'                    => $reason,
                    ];

                    # push the Data
                    array_push($providerDocumentData, $data);
                }

                # return response
                return response()->json([
                    'code'      => (string)$this->successStatus, 
                    'message'   => 'Provider documents Found.',
                    'data'      => $providerDocumentData
                 ]);
            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'Provider is not uploaded any document.',
                    'data'      => []
                 ]);
            }
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Provider not Found with provided Id.',
                'data'      => []
             ]);
        }
    }

    /**
     * @method to Fetch Provider Status 
     * @param Request $request
     * @return Json | true
    */
    public function documentStatus(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'           => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # Set bthe relations
        $relations = ['documents.histories', 'documents.documentName'];

        # feth the Provider
        $provider = $this->provider
                         ->with($relations)
                         ->find($request->get('provider_id'));

        # fetch all the Uploaded Documents By Provider
        $mandatoryDocuments = $this->mandatoryDocumentsForProvider->all();

        # eftch all the Uploaded Documents
        $uplodedDocuments = $provider->documents;

        # If none of Document has been uploded by Provider
        if($uplodedDocuments->count() == 0) {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'No Document Uploded Yet',
                'status'    => 'false',
                'data'      => []
            ]); 
        }

        if(($uplodedDocuments->count() > 0) AND ($uplodedDocuments->count() < $mandatoryDocuments->count() )) {
            $documentNotUploded = [];
            foreach ($mandatoryDocuments as $key => $mandatoryDocument) {
               $uplodDocumentOnMandatoryDocumnetId = $uplodedDocuments->where('document_id', $mandatoryDocument->id);
               if($uplodDocumentOnMandatoryDocumnetId->isEmpty()) {
                $data = [
                            'id'    => (string)$mandatoryDocument->id,
                            'name'  => (string)$mandatoryDocument->name,
                          ];
                array_push($documentNotUploded, $data);
               }
            }

            # return response
            return response()->json([
                'code'                  => (string)$this->failedStatus, 
                'message'               => 'Document Not Uploded yet.',
                'status'                => 'false',
                'data'                  => $documentNotUploded
            ]); 
        }

        $acceptedDocumentCount = 0;
        $rejectedDocumentCount = 0;
        $rejectedDocument = [];
        $acceptanceStillProcessDocument = [];
        if($uplodedDocuments->count() >= $mandatoryDocuments->count() ) {
            
            foreach ($mandatoryDocuments as $key => $mandatoryDocument) {
              # fetch all the Document on mandatory document Id
              $documentOnMandatoryDocumentId = $uplodedDocuments->where('document_id', $mandatoryDocument->id);

              if($documentOnMandatoryDocumentId->isNotEmpty()) {
                foreach ($documentOnMandatoryDocumentId as $key => $uploadDocument) {
                  $documentStatusHistory = $uploadDocument->histories;
                  if($documentStatusHistory->isNotEmpty()) {
                    foreach ($documentStatusHistory as $key => $status) {
                      if($status->document_status_id == DocumentStatusHistoryInterface::ACCEPT) {
                        $acceptedDocumentCount = $acceptedDocumentCount + 1;
                      } elseif ($status->document_status_id == DocumentStatusHistoryInterface::REJECT) {
                          $rejectedDocumentCount = $rejectedDocumentCount + 1;
                          $data = [
                            'id'    => (string)$uploadDocument->documentName->id,
                            'name'  => (string)$uploadDocument->documentName->name,
                          ];
                          array_push($rejectedDocument, $data);
                      } 
                    }
                  } else {
                    $data = [
                            'id'    => (string)$uploadDocument->documentName->id,
                            'name'  => (string)$uploadDocument->documentName->name,
                          ];
                    array_push($acceptanceStillProcessDocument, $data);
                  }
                }
              }
            }
        }

        if($acceptedDocumentCount == $mandatoryDocuments->count()) {
            # return response
            return response()->json([
                'code'                  => (string)$this->successStatus, 
                'message'               => 'Document Acceepted.',
                'status'                => 'true',
                'data'                  => []
            ]); 
        }

        if(!empty($acceptanceStillProcessDocument)) {
            # return response
            return response()->json([
                'code'                  => (string)$this->successStatus, 
                'message'               => 'Document Acceptance Still in Process.',
                'status'                => 'false',
                'data'                  => $acceptanceStillProcessDocument,
            ]); 
        }

        if(!empty($rejectedDocument)) {
            # return response
            return response()->json([
                'code'                  => (string)$this->failedStatus, 
                'message'               => 'Document Rejected.',
                'status'                => 'false',
                'data'                  => $rejectedDocument
            ]); 
        }
    }

    /**
     * @method to Update Bank Details
     * @param Request $request
     * @return json  | []
     */
    public function updateBankDetails(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'           => 'required|numeric',
            'bank_name'             => 'required|string',
            'ifsc_code'             => 'required|string',
            'account_number'        => 'required|numeric',
            'account_holder_name'   => 'required|string',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # feth the Provider
        $provider = $this->provider
                         ->with('bankDetail')
                         ->find($request->get('provider_id'));

        if($provider != '') {
            $availableBankDetail = $provider->bankDetail;
           # $availableBankDetail = $bankDetail->isNotEmpty() ? $bankDetail->first() : '';
            if($availableBankDetail != '') {
                $data = [
                    'provider_id'           => $request->get('provider_id'),

                    'bank_name'             => (($request->has('bank_name')) AND ($request->get('bank_name') != '')) ? 
                                               $request->get('bank_name') : $availableBankDetail->bank_name,

                    'ifsc_code'             => (($request->has('ifsc_code')) AND ($request->get('ifsc_code') != '')) ? 
                                               $request->get('ifsc_code') : $availableBankDetail->ifsc_code,

                    'account_number'        => (($request->has('account_number')) AND ($request->get('account_number') != '')) ? 
                                               $request->get('account_number') : $availableBankDetail->account_number,

                    'account_holder_name'   => (($request->has('account_holder_name')) AND ($request->get('account_holder_name') != '')) ? 
                                                $request->get('account_holder_name') : $availableBankDetail->account_holder_name,
                ];

                $availableBankDetail->update($data); 
                $providerBankdetail = $this->providerBankDetail->where('id', $availableBankDetail->id)->get();
            } else {
                $data = [
                    'provider_id'           => $request->get('provider_id'),
                    'bank_name'             => $request->get('bank_name'),
                    'ifsc_code'             => $request->get('ifsc_code'),
                    'account_number'        => $request->get('account_number'),
                    'account_holder_name'   => $request->get('account_holder_name')
                ];

                $providerBankdetail = $this->providerBankDetail->create($data);
            }

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'Provider Bank Detail Updated',
                'data'      => $providerBankdetail
             ]);
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Provider not Found with provided Id.',
                'data'      => []
             ]);
        }
    }

    /**
     * @method To fetch all the Bank Details
     * @param Request $reauest
     * @return json | []
    */
    public function fetchBankDetails(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'           => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # feth the Provider
        $provider = $this->provider
                         ->with('bankDetail')
                         ->find($request->get('provider_id'));

        if($provider != '') {
            $bankDetail = $provider->bankDetail;
            if($bankDetail != '') {
                $data = [
                    'id'                    => (string)$bankDetail->id ?? '',
                    'bank_name'             => (string)$bankDetail->bank_name ?? '',
                    'ifsc_code'             => (string)$bankDetail->ifsc_code ?? '',
                    'account_number'        => (string)$bankDetail->account_number ?? '',
                    'account_holder_name'   => (string)$bankDetail->account_holder_name ?? '',
                ];

                # return response
                return response()->json([
                    'code'      => (string)$this->successStatus, 
                    'message'   => 'Bank Detail Attached with this Account.',
                    'data'      => $data
                 ]);
            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'No Bank Detail Attached with this Account.',
                    'data'      => []
                 ]);
            }
        } else {
           # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Provider not Found with provided Id.',
                'data'      => []
             ]); 
        }
    }

}
