<?php

namespace App\Http\Controllers\Api\Provider;

use Exception;
use App\Models\User; 
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller; 

class NotifyProvider extends Controller
{
  # Variable to Bind Model
  protected $user;

  # Variable to Bind title
  protected $title;

  # Variable to Bind message
  protected $message;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
  }

  public function notify($provider, $title = '', $message= '')
  {
    # define the Server Api Key for Sending N
    $providerKey =  env('SERVER_ANDROID_PROVIDER_API_KEY');


    # Fetch The User Device Token
    $tokens = [$provider->device_token];

    #dd($tokens, $user, $title, $message);
    # Set th Header
    $header = [ 
      'Authorization: Key=' . $providerKey,
      'Content-Type: Application/json' 
    ];

    # Set the Message
    $msg = [
      'title' => (string)$title,
      'body'  => (string)$message,
      'icon'  => asset('logo/logo.PNG'),
      'image' => asset('logo/logo.PNG'),
    ];
    #dd($msg);
    # Set the Payload
    $payload = [
      'registration_ids'  => $tokens,
      'data'              => $msg,
    ];

    # initialize the Curl
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($payload),
      CURLOPT_HTTPHEADER => $header
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return ;
  }
}
