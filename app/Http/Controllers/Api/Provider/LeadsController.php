<?php

namespace App\Http\Controllers\Api\Provider;

use DB;
use Validator;
use Carbon\Carbon;

# Models
use App\Http\Traits\StatusTrait;

use App\Models\Order;
use App\Models\Provider;
use App\Models\UserWallet;
use App\Models\OrderHistory;
use App\Models\UserNotification;
use App\Models\AssignOrderToProvider;
use App\Models\AssignOrderToProviderHistory;

# Interface
use App\Http\Interfaces\OrderStatusInterface;
use App\Http\Interfaces\TransactionTypeInterface;
use App\Http\Interfaces\AssignOrderProviderStatusInterface;

# Controllers
use App\Http\Controllers\Controller; 
use App\Http\Controllers\Api\User\NotifyUser;

# Vendor Classes
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

class LeadsController extends Controller implements OrderStatusInterface, TransactionTypeInterface, AssignOrderProviderStatusInterface
{
    use StatusTrait;

    # Variable to Bind Model Order
    protected $order;

    # Variable to Bind Model Provider
    protected $provider;

    # Variable to Bind Model UserWallet
    protected $userWallet;

    # Variable to Bind Model userNotification
    protected $userNotification;

    # Variable to Bind Model OrderHistory
    protected $orderHistory;

    # Variable to Bind Model AssignOrderToProvider
    protected $assignOrderToProvider;

    # Variable to Bind Model AssignOrderToProviderHistory
    protected $assignOrderToProviderHistory;

    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct(
                                Order $order,
                                Provider $provider,
                                UserWallet $userWallet,
                                OrderHistory $orderHistory,
                                UserNotification $userNotification,
                                AssignOrderToProvider $assignOrderToProvider,
                                AssignOrderToProviderHistory $assignOrderToProviderHistory
                                )
    {
        $this->order                        = $order;
        $this->provider                     = $provider;
        $this->userWallet                   = $userWallet;
        $this->orderHistory                 = $orderHistory;
        $this->userNotification             = $userNotification;
        $this->assignOrderToProvider        = $assignOrderToProvider;
        $this->assignOrderToProviderHistory = $assignOrderToProviderHistory;
    }

    /**
     * @method fetch leads for Provider
     * @param Request $request
     * @return json | []
     */
    public function fetchLeads(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'   => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # Set Relations
        $relations = [
            'assignOrders.order.user',
            'assignOrders.provider',
            'assignOrders.order.orderServices.service.subCategory',
            'boughtPackages',
            'orders.history',
        ];

        # fetch Provider
        $provider = $this->provider
                         ->with($relations)
                         ->find($request->get('provider_id'));

        # fetch the Assigned order to Provider
        $assignedOrders = $provider->assignOrders->filter(function($assignOrder) {
            if($assignOrder->status) {
                return $assignOrder;
            }
        });

        # Fetch all the Leads which are in Searching Mode
        if($assignedOrders->isNotEmpty()) {
            $newLeads = [];
            foreach ($assignedOrders as $key => $assignedOrder) {
                $orderId = $assignedOrder->order_id;
                $orderStatusHistory = $this->assignOrderToProviderHistory->where('order_id', $orderId)->get();

                # Fetch if other Provider Accepted order
                $accepted = $orderStatusHistory->where('assign_order_status_id', AssignOrderProviderStatusInterface::ACCEPTED);

                # Fetch if other Provider Initiate Order
                $initiate = $orderStatusHistory->where('assign_order_status_id', AssignOrderProviderStatusInterface::INITIATED);
                # Fetch if other Provider Complete Order
                $complete = $orderStatusHistory->where('assign_order_status_id', AssignOrderProviderStatusInterface::COMPLETE);

                # Order Date should be Future Date
                $order = $assignedOrder->order;

                # Validate oRder
                $validOrder = $order->date_time_of_service->gt(Carbon::now());


                if($accepted->isEmpty() AND $initiate->isEmpty() AND $complete->isEmpty() AND $validOrder) {
                    $order              = $assignedOrder->order;
                    $orderService       = $order->orderServices->first();
                    $orderDate          = $order->date_time_of_service->format('d-M-Y');
                    $orderTime          = $order->date_time_of_service->format('h:i A');
                    $userName           = $order->orderAddress->name ?? '';
                    $subCategoryName    = $orderService->service->subCategory->name;
                    $data = [
                        'assign_order_provider_id'  => (string)$assignedOrder->id,
                        'provider_id'               => (string)$assignedOrder->provider_id,
                        'order_id'                  => (string)$assignedOrder->order_id,
                        'unique_order_id'           => (string)$order->unique_order_id,
                        'sub_category_name'         => (string)$subCategoryName,
                        'user_name'                 => (string)$userName,
                        'order_date'                => (string)$orderDate,
                        'order_time'                => (string)$orderTime,
                        'credit_points'             => '50',
                    ];

                    array_push($newLeads, $data);
                }
            }

            if(!empty($newLeads)) {
                # return response
                return response()->json([
                    'code'                      => (string)$this->successStatus, 
                    'message'                   => 'Leads Found.',
                    'remaining_credit_points'   => (string)$provider->remaining_credit_points ?? '',
                    'data'                      => $newLeads

                ]);
            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'No Lead Found.',
                    'remaining_credit_points'   => (string)$provider->remaining_credit_points ?? '',
                    'data'      => []
                ]);
            }
        } else {
            # return response
            return response()->json([
                'code'                      => (string)$this->failedStatus, 
                'remaining_credit_points'   => (string)$provider->remaining_credit_points ?? '',
                'message'                   => 'No Lead Found.',
                'data'                      => []
             ]);
        }
    }

    /**
     * @method fetch leads for Provider
     * @param Request $request
     * @return json | []
     */
    public function fetchLeadDetail(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'                => 'required|numeric',
            'assign_order_provider_id'   => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # Set Relations
        $relations = [
            'order.user',
            'order.orderAddress',
            'provider',
            'order.orderServices.service.subCategory',
        ];

        # fetch Provider
        $provider = $this->provider
                         ->find($request->get('provider_id'));

        # fetch the Assigned order to Provider
        $assignedOrder = $this->assignOrderToProvider
                              ->with($relations)
                              ->find($request->get('assign_order_provider_id'));

        # Fetch all the Leads which are in Searching Mode
        if($assignedOrder != '') {
            $order              = $assignedOrder->order;
            $orderService       = $order->orderServices;
            $orderDate          = $order->date_time_of_service->format('d-M-Y');
            $orderTime          = $order->date_time_of_service->format('h:i A');
            $userName           = $order->orderAddress->name ?? '';
            $orderAmount        = $order->paid_amount ?? '';
            $subCategoryName    = $orderService->first()->service->subCategory->name;
            $serviceAddress     = $order->orderAddress->address ?? '';


            # fetch Services related to Order
            $orderservices = $orderService;
            $servicesArray = [];

            if($orderservices->isNotEmpty()) {
                foreach ($orderservices as $key => $orderService) {
                    $service = $orderService->service;
                    $data = [
                        'service_icon'  => (string)$service->icon_path ?? '',
                        'service_name'  => (string)$service->title ?? '',
                        'quantity'      => (string)$orderService->quantity ?? '',
                        'amount'        => (string)$orderService->payable_amount ?? '',
                        'duration'      => (string)$orderService->service_time_taken ?? '',
                    ];

                    array_push($servicesArray, $data);
                }
            }
            $data = [
                'assign_order_provider_id'  => (string)$assignedOrder->id,
                'provider_id'               => (string)$assignedOrder->provider_id,
                'order_id'                  => (string)$assignedOrder->order_id,
                'unique_order_id'           => (string)$order->unique_order_id,
                'sub_category_name'         => (string)$subCategoryName,
                'user_name'                 => (string)$userName,
                'order_date'                => (string)$orderDate,
                'order_time'                => (string)$orderTime,
                'amount'                    => (string)$orderAmount,
                'credit_points'             => '50',
                'service_address'           => (string)$serviceAddress,
                'service_details'           => $servicesArray,
                'order_placed_date'         => (string)$order->created_at->format('d-M-Y') ?? '',
                'order_placed_time'         => (string)$order->created_at->format('h:i A') ?? '',
            ];

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'Lead Details.',
                'data'      => $data
             ]);
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'lead not Found',
                'data'      => []
             ]);
        }
    }

    /**
     * @method fetch leads for Provider
     * @param Request $request
     * @return json | []
     */
    public function acceptLead(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'                => 'required|numeric',
            'assign_order_provider_id'   => 'required|numeric',
            'order_id'                   => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # Set Relations
        $relations = [
            'assignOrders.order.user',
            'assignOrders.provider',
            'assignOrders.order.orderServices.service.subCategory',
        ];

        DB::beginTransaction();

        # fetch Provider
        $provider = $this->provider
                         ->with($relations)
                         ->find($request->get('provider_id'));

        # Fetch all the Assign Orders Provider History 
        $orderStatusHistory = $this->assignOrderToProviderHistory
                                    ->where('order_id', $request->get('order_id'))
                                    ->get();

        # Fetch if other Provider Accepted order
        $accepted = $orderStatusHistory->where('assign_order_status_id', AssignOrderProviderStatusInterface::ACCEPTED);

        # Fetch if other Provider Initiate Order
        $initiate = $orderStatusHistory->where('assign_order_status_id', AssignOrderProviderStatusInterface::INITIATED);

        # Fetch if other Provider Complete Order
        $complete = $orderStatusHistory->where('assign_order_status_id', AssignOrderProviderStatusInterface::COMPLETE);

        # Fetch all the Leads which are in Searching Mode
        if($accepted->isEmpty() AND $initiate->isEmpty() AND $complete->isEmpty()) {
            $data = [
                'order_id'                  => $request->get('order_id') ?? '' ,
                'assign_orderprovider_id'   => $request->get('assign_order_provider_id') ?? '' ,
                'assign_order_status_id'    => AssignOrderProviderStatusInterface::ACCEPTED,
                'reason'                    => 'Accepted' ,
            ];

            # Update assign Order Status 
            $this->assignOrderToProviderHistory->create($data);

            # Update provider on Order
            $order = $this->order
                          ->with('user')
                          ->find($request->get('order_id'));

             # Fetch User
            $user = $order->user;

            # Notify user for Order Successfully
            $notifyUser = new NotifyUser();

            # Set Title message
            $title = 'Mesee Order Notification';

            # Set Message
            $message = 'Provider Accepted Your Order.';

            # Notify User
            $notifyUser->notify($user, $title, $message);

            # Save Send Notification 
            $data = [
                'user_id'       => $user->id,
                'order_id'      => $order->id,
                'notification'  => $message,
            ];

            # Creta UserNotification
            $this->userNotification->create($data);


            $order->update(['provider_id' => $provider->id]);

            # fetch all the assignOrderTOProvider beside Accept One Delete Them
            $freeAssignOrdersProviders = $this->assignOrderToProvider
                                              ->with('statusHistory')
                                              ->where('provider_id', '<>', $provider->id)
                                              ->where('order_id', $request->get('order_id'))
                                              ->get();

            # Delete if Not Empty
            if($freeAssignOrdersProviders->isNotEmpty()) {
                foreach ($freeAssignOrdersProviders as $key => $freeAssignOrdersProvider) {
                    $assignOrderProviderStatusHistory = $freeAssignOrdersProvider->statusHistory;
                    if($assignOrderProviderStatusHistory->isNotEmpty()) {
                        foreach ($assignOrderProviderStatusHistory as $key => $assignOrderProviderStatus) {
                            $assignOrderProviderStatus->forceDelete();
                        }
                    }

                    $freeAssignOrdersProvider->forceDelete();
                }
            }

            # update the Order Staus
            $data = [
                'order_id'          => $request->get('order_id') ?? '',
                'order_status_id'   => OrderStatusInterface::PICKED_BY_PROVIDER,
                'comment'           => 'Accepted'
            ];

            # Update Order Status
            $this->orderHistory->create($data);

            DB::commit();

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'Order Accepted Successfully.',
                'data'      => []
             ]);
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Lead accepted by Someone Else wait for other Lead.',
                'data'      => []
             ]);
        }
    }

    /**
     * @method fetch leads for Provider
     * @param Request $request
     * @return json | []
     */
    public function rejectLead(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'                => 'required|numeric',
            'assign_order_provider_id'   => 'required|numeric',
            'order_id'                   => 'required|numeric',
            #'reason'                     => 'required|string',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }


        # fetch Provider
        $provider = $this->provider
                         ->find($request->get('provider_id'));

        if($provider != '') {

            # Fetch the Assigned Order want to Reject
            $assignOrder = $this->assignOrderToProvider
                                ->with('statusHistory')
                                ->active()
                                ->find($request->get('assign_order_provider_id'));

            if($assignOrder != '') {

                DB::beginTransaction();

                # Update the Status of Lead to Reject
                $data = [
                    'order_id'                  => $request->get('order_id') ?? '',
                    'assign_orderprovider_id'   => $request->get('assign_order_provider_id') ?? '',
                    'assign_order_status_id'    => AssignOrderProviderStatusInterface::REJECT,
                    'reason'                    => $request->get('reason') ?? '',
                ];

                # Update Status History of Assigned Order
                $this->assignOrderToProviderHistory->create($data);

                # fetch all the History of Assigned Order
                $assignOrderStatusHistory = $assignOrder->statusHistory;

                # Delete all the Assign Orders History on Assign Order
                foreach ($assignOrderStatusHistory as $key => $assignOrderStatus) {
                    $assignOrderStatus->delete();
                }

                # Disable this Assign Provider Model
                $assignOrder->update(['status' => false]);

                /*# Update the Order History
                $data = [
                    'order_id'          => $request->get('order_id'),
                    'order_status_id'   => OrderStatusInterface::REJECTED_BY_PROVIDER,
                    'comment'           => 'Provider Reject.',
                ];

                # Update Order Status to rejected By Provider
                $this->orderHistory->create($data);*/

                # Process to Again Try to assign the Provider to Service
                # Fet Order First
                $order = $this->order->find($request->get('order_id'));

                # fetch Service Address of Order
                $orderAddress = $order->orderAddress;

                # Fetch the Lat long from Order address 
                $latitude = $orderAddress->latitude;
                $longitude = $orderAddress->longitude;
                $distance = 20;

                # fetch Providers
                $providers = $this->provider
                                  ->where('id','<>', $provider->id)
                                  #->active()
                                  #->whereRaw("(1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
                                  ->limit(5)
                                  ->get();
                DB::commit();

                # If Provider Found Than Search New Provider and Assign to Providers
                if($providers->isNotEmpty()) {
                    DB::beginTransaction();
                    foreach ($providers as $key => $provider) {
                       $data = [
                         'order_id'    => $order->id,
                         'provider_id' => $provider->id,
                       ];

                       # Searching provider and Set order to Provider 
                       $assignedOrderProvider = $this->assignOrderToProvider->create($data);

                       # Set Assigned Provider Status to Searching
                       $data = [
                         'order_id'                => $order->id,
                         'assign_orderprovider_id' => $assignedOrderProvider->id,
                         'assign_order_status_id'  => AssignOrderProviderStatusInterface::SEARCHING,
                         'reason'                  => 'Searching Provider with Order',
                       ];

                       # Create Status for searching Provider for Status
                       $this->assignOrderToProviderHistory->create($data);
                    }

                    # Update Order Status to Searching Provider
                    # Update the Order History
                    $data = [
                        'order_id'          => $request->get('order_id'),
                        'order_status_id'   => OrderStatusInterface::SEARCHING_PROVIDER,
                        'comment'           => 'Searching Provider for Order.',
                    ];

                    # Update Order Status to rejected By Provider
                    $this->orderHistory->create($data);
                    DB::commit();
                    # return response
                    return response()->json([
                        'code'      => (string)$this->successStatus, 
                        'message'   => 'Lead has been Rejected but we are trying to search New Provider for you, Be patient.',
                        'data'      => []
                     ]);
                } else {
                    DB::beginTransaction();
                    # Return the Money to User Wallet and Cancel the Order
                    $data = [
                        'user_id'           => $order->user_id,
                        'order_id'          => $order->id,
                        'transaction_type'  => TransactionTypeInterface::Credit,
                        'amount'            => $order->paid_amount,
                    ];

                    # Credit Payment Amount to Wallet
                    $this->userWallet->create($data);

                    # Cancel the Order as Provider Not Found
                    $data = [
                        'order_id'          => $request->get('order_id'),
                        'order_status_id'   => OrderStatusInterface::CANCELLED,
                        'comment'           => 'Order Cancel due to Provider not Found. Money will credit ',
                    ];

                    # Update Order Status to rejected By Provider
                    $this->orderHistory->create($data);
                    DB::commit();
                    # return response
                    return response()->json([
                        'code'      => (string)$this->successStatus, 
                        'message'   => 'Lead has been Rejected and Cancelled Due to No Provider Found.',
                        'data'      => []
                     ]);
                }

            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'Assign Order Not Found.',
                    'data'      => []
                 ]);
            }
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'provider Not Found.',
                'data'      => []
             ]);
        }
    }
}
