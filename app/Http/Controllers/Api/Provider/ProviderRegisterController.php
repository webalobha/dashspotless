<?php

namespace App\Http\Controllers\Api\Provider;

use DB;
use Validator;
use App\Models\Provider; 
use App\Models\Category; 
use App\Models\ProviderOtp; 
use App\Models\SubCategory; 
use App\Models\ProviderAddress; 
use App\Models\ProviderCategory; 
use App\Models\ProviderDocument; 
use App\Http\Traits\StatusTrait;
use App\Models\ProviderSubCategory;
use App\Models\MandatoryDocumentsForProvider; 

use App\Http\Interfaces\DocumentStatusHistoryInterface;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller; 

class ProviderRegisterController extends Controller implements DocumentStatusHistoryInterface
{
    use StatusTrait;

    # Variable to Bind Model
    protected $provider;

    # bind ProviderOtp Model
    protected $providerOtp;

    # bind Category Model
    protected $category;

    # bind Category Model
    protected $subCategory;

    # bind ProviderDocument Model
    protected $providerDocument;

    # bind ProviderCategory Model
    protected $providerCategory;

    # bind ProviderSubCategory Model
    protected $providerSubCategory;

    # bind Provider Mandatory Document Model
    protected $providerMandatoryDocument;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
                                Provider $provider, 
                                Category $category,
                                ProviderOtp $providerOtp,
                                SubCategory $subCategory,
                                ProviderDocument $providerDocument,
                                ProviderCategory $providerCategory,
                                ProviderSubCategory $providerSubCategory,
                                MandatoryDocumentsForProvider $mandatoryDocumentsForProvider
                            )
    {
        $this->provider                             = $provider;
        $this->category                             = $category;
        $this->providerOtp                          = $providerOtp;
        $this->subCategory                          = $subCategory;
        $this->providerDocument                     = $providerDocument;
        $this->providerCategory                     = $providerCategory;
        $this->providerSubCategory                  = $providerSubCategory;
        $this->mandatoryDocumentsForProvider        = $mandatoryDocumentsForProvider;
    }

    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'mobile_number'     => 'required|numeric',
            'device_type'       => 'required|string',
            'device_token'      => 'required|string',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        DB::beginTransaction();

        # Fetch all the inputs
        $input =  $request->all(); 

        # Check if email Already Exist
        $mobileNumber = $input['mobile_number'];

        # check provider already Exist on that Email
        $providers = $this->provider->where('mobile_number', $mobileNumber)->get();

        
        # return response if User already exist on requested Email
        if($providers->isNotEmpty()) {
            $otp = mt_rand(1000,9999);

            # fetch the First Provider
            $provider = $providers->first();

            # Update provider Device Type
            $provider->update([
                'device_type'   => $request->get('device_type'),
                'device_token'  => $request->get('device_token'),
            ]);

            # Set the success message after User creation 
            $data = [
                'provider_id'       =>  (string)$provider->id,
                'mobile_number'     =>  (string)$provider->mobile_number,
                'otp'               =>  (string)$otp
            ];

            $otpData   = ['provider_id' => $provider->id, 'otp' => $otp];

            # Create Provider Otp Model
            $this->providerOtp->create($otpData);

            DB::commit();

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'Provider already exist!!!',
                'data'      =>  $data
             ]); 
        } else {
            # Create User Model
            $provider =  $this->provider->create([
                'mobile_number' => $mobileNumber,
                'device_type'   => $request->get('device_type'),
                'device_token'  => $request->get('device_token'),
            ]);

            #Set data for User Otp
            $otp    = mt_rand(1000,9999);
            $data   = ['provider_id' => $provider->id, 'otp' => $otp];

            # Create Provider Otp Model
            $this->providerOtp->create($data);

            # Set the success message after User creation 
            $data = [
                'mobile_number'     =>  $provider->mobile_number,
                'provider_id'       =>  (string)$provider->id,
                'otp'               =>  (string)$otp
            ];

            DB::commit();

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'Provider has been register Successfully!!!',
                'data'      => $data
             ]); 

        }
    }

    /**
     * @method to verify Users Request Otp
     * 
     * @return Otp Verified or Not
     */
    public function verifyOtp(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'mobile_number'     => 'required|numeric',
            'otp'               => 'required|string',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # Fetch all the inputs
        $input =  $request->all(); 

        # Check if email Already Exist
        $mobileNumber = $input['mobile_number'];

        # check provider already Exist on that Email
        $providers = $this->provider
                      ->with(['otp', 'category', 'subCategory', 'addresses', 'documents'])
                      ->where('mobile_number', $mobileNumber)
                      ->get();

        # return response if Provider already exist on requested Email
        if($providers->isNotEmpty()) {
            # fetch Provider
            $provider = $providers->first();

            # fetch the Otp of Providers
            $providerOtps = $provider->otp;
            if($providerOtps->isNotEmpty() ) {
                $lastOtp = $providerOtps->last()->otp;
                if($lastOtp == $request->get('otp')) {
                    $token = $this->generateToken();

                    # Update provider token
                    $provider->update(['api_token' => $token]);

                    # Set the Data
                    $data = [
                        'token'             => $token,
                        'mobile_number'     => $mobileNumber,
                        'provider_id'       => $provider->id
                    ];

                    #Fetchthe Provider category Data
                    $providerCategory = $provider->category;
                    $providerCategoryData = 'false';
                    if($providerCategory != '') {
                        $providerCategoryData = 'true';
                    } 
                    #Fetchthe Provider Sub category Data
                    $providerSubCategories = $provider->subCategory;
                    $providerSubCategoryData = 'false';
                    if($providerSubCategories->isNotEmpty()) {
                        $providerSubCategoryData = 'true';
                    } 

                    #Fetchthe Provider Address Data
                    $providerAddresses = $provider->addresses;
                    $providerAddressesData = 'false';
                    if($providerAddresses->isNotEmpty()) {
                        $providerAddressesData = 'true';
                    } 

                    #Fetchthe Provider Document Data
                    $providerDocuments = $provider->documents;
                    $providerDocumentData = 'false';
                    if($providerDocuments->isNotEmpty()) {
                        $providerDocumentData = 'true';
                    } 

                    # return response
                    return response()->json([
                        'code'                  => (string)$this->successStatus, 
                        'message'               => 'Otp has been Verified.',
                        'token'                 => $token,
                        'status'                => $provider->status ? 'true' : 'false',
                        'name'                  => $provider->first_name ?? '',
                        'email'                 => $provider->email ?? '',
                        'mobile_number'         => $provider->mobile_number ?? '',
                        'latitude'              => $provider->latitude ?? '',
                        'longitude'             => $provider->longitude ?? '',
                        'category'              => $providerCategoryData,
                        'subcategories'         => $providerSubCategoryData,
                        'addresses'             => $providerAddressesData,
                        'documents'             => $providerDocumentData,
                        'data'                  => $data
                     ]);
                } else {
                    # return response
                    return response()->json([
                        'code'      => (string)$this->failedStatus, 
                        'message'   => 'Otp does not match.',
                        'data'      => []
                     ]);
                }
            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'Otp Not Found.',
                    'data'      => []
                 ]);
            }
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Provider not Found with this Mobile Number.',
                'data'      => []
             ]); 
        }

    }
    
    /**
     * function to generate the Token
     * 
     * @return Token
     */
    public function generateToken()
    {
        # Set the token 
        $token = Str::random(60);

        # Hash Token
        $hashToken = hash('sha256', $token);

        # return the Hash Token 
        return $hashToken;
    }

    /**
     * @method to Update Provider Profile
     * @param $request
     * @return Json 
     */
    public function updateProfile(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'     => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # fetch the Provider
        $provider = $this->provider->find($request->get('provider_id'));

        if($provider != '') {
            $emailExist = $this->provider
                               ->where('id','<>', $request->get('prvider_id'))
                               ->where('email',$request->get('email'))
                               ->get();

            if($emailExist->isEmpty()) {
                 # Fetch the field
                $data = [
                            'first_name' => $request->get('name') ?? '',
                            'email'      => $request->get('email') ?? '',
                            'gender'     => $request->get('gender') ?? ''
                        ];

                # Update Provider
                $provider->update($data);
                $data['provider_id'] = $provider->id;
                # return response
                return response()->json([
                    'code'      => (string)$this->successStatus, 
                    'message'   => 'Provider Data has been Updated',
                    'data'      => $data
                ]);
            } else {
                #return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'This Email already in Use.',
                    'data'      => []
                ]); 
            }
        } else {
            #return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Provider not Found with provided Id.',
                'data'      => []
             ]); 
        }
    }

    /**
     * @method to Update Provider Profile
     * @param $request
     * @return Json 
     */
    public function updateProfileImage(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'     => 'required|numeric',
            'image'           => 'mimes:jpeg,jpg,png,gif|required|max:10000'
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # Fetch Profile Image
        $profileImage = $request->file('image');
        
        # fetch the Provider
        $provider = $this->provider->find($request->get('provider_id'));

        if($provider != '') {
            DB::beginTransaction();

            # set destination path.
            $destinationpath    = base_path() .'/public/images/provider_documents';

            # get file name.
            $filename           = str_replace(' ', '', $profileImage->getClientOriginalName());

            # get today date.
            $today_date         = date('d-m-Y');

            # get a random number.
            $random_number      = rand(1111, 9999);

            # set filname with today date, random number, filename.
            $filenameData       = $today_date . '_' . $random_number .'___'. $filename;

            # file move from current path to destination path.
            $movefilename       = $profileImage->move($destinationpath, $filenameData);

            # Set path for Database
            $databsePathForImage = 'images/provider_documents/'.$filenameData;

            $provider->update(['image_name' => $filename, 'image_path' => $databsePathForImage]);

            DB::commit();

            #return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'Provider Profile Image has been Updated',
                'data'      => []
             ]); 
        } else {
            #return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Provider not Found with provided Id.',
                'data'      => []
             ]); 
        }
    }

    /**
     * @method to Fetch profile of Provider
     * @param Request $request 
     * @return json | []
     */
    public function fetchProfile(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'   => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # fetch the Provider
        $provider = $this->provider->find($request->get('provider_id'));

        if($provider != '') {
            # return response
            return response()->json([
                'code'                  => (string)$this->successStatus, 
                'message'               => 'Provider Profile Information.',
                'status'                => $provider->status ? 'true' : 'false',
                'name'                  => $provider->first_name ?? '',
                'email'                 => $provider->email ?? '',
                'mobile_number'         => $provider->mobile_number ?? '',
                'latitude'              => $provider->latitude ?? '',
                'longitude'             => $provider->longitude ?? '',
                'address'               => $provider->address ?? '',
                'postal_code'           => $provider->postal_code ?? '',
                'image_path'            => $provider->image_path ?? '',
                'data'                  => [] 
             ]);
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Provider not Found with provided Id.',
                'data'      => []
             ]); 
        }
    }
    

    /**
     * @method to update the location of Provider
     * @param request
     * @return json 
     */
    public function updateLocation(Request $request)
    {
       # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'   => 'required|numeric',
            'latitude'      => 'required|string',
            'longitude'     => 'required|string',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # fetch the Provider
        $provider = $this->provider->find($request->get('provider_id'));

        if($provider != '') {
            # Fetch the Postak Code and Address from Lat long
            $fetchAddressPostalCode = $this->fetchAddressFromLatLong($request->get('latitude'), $request->get('longitude'));
            $data = [
                'latitude'      => $request->get('latitude'),
                'longitude'     => $request->get('longitude'),
                'address'       => $fetchAddressPostalCode['address'],
                'postal_code'   => $fetchAddressPostalCode['postalCode'],
            ];
            # Update the provider Location
            $provider->update($data);
            $data['provider_id'] = $provider->id;
            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'Provider location has been Updated Successfully.',
                'data'      => $data
             ]);
        } else {
             # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Provider not Found with provided Id.',
                'data'      => []
             ]); 
        }
    }

      /**
     * @method to update the location of Provider Manually
     * @param request
     * @return json 
     */
    public function updateLocationManual(Request $request)
    {
       # Validate request data
        $validator = Validator::make($request->all(), [ 
            'address'       => 'required|string',
            'pin_code'      => 'required|numeric',
            'provider_id'   => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # fetch the Provider
        $provider = $this->provider->find($request->get('provider_id'));

        # fetch the Latitude and longitude
        $latitude  = '';
        $longitude = '';
        $fullAddress = $request->get('address').' '.$request->get('pin_code');
        
        # fetch latitude and Longitude
        $latLong = $this->geocode($fullAddress);

        if($provider != '') {
           if($latLong AND !empty($latLong)) {
                $latitude = $latLong[0];
                $longitude = $latLong[1];

                #update the Provider Latitude and longitude
                $provider->update([
                    'latitude'      => $latitude,
                    'longitude'     => $longitude,
                    'address'       => $request->get('address'),
                    'postal_code'   => $request->get('pin_code'),
                ]);

                $provider = $this->provider->find($request->get('provider_id'));
                # return response
                return response()->json([
                    'code'      => (string)$this->successStatus, 
                    'message'   => 'Provider Loaction has been updated Successfully.',
                    'data'      => [
                        'latitude'      => $latitude,
                        'longitude'     => $longitude,
                        'address'       => $provider->address,
                        'postal_code'   => $provider->postal_code,
                    ]
                 ]);
           } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'Latitude and Longitude not found on provided Address.',
                    'data'      => []
                 ]); 
           }
        } else {
             # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Provider not Found with provided Id.',
                'data'      => []
             ]); 
        }
    }

        /**
     * @method to fetch the Lat, Long From Address String
     * @param Address String 
     * @return lat, long 
     */
    public function geocode($address)
    {
        # url encode the address
        $address = urlencode($address);

        # Define the Api Key for Google Api
        $apiKey = 'AIzaSyC3tI2o08AKcPpSK40CbVIqMPzbjPHiglA';

        # google map geocode api url
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key=$apiKey ";

        # get the json response
        $resp_json = file_get_contents($url);

        # decode the json
        $resp = json_decode($resp_json, true);

        # response status will be 'OK', if able to geocode given address
        if($resp['status']=='OK') {
            # get the important data
            $lati = isset($resp['results'][0]['geometry']['location']['lat']) ? $resp['results'][0]['geometry']['location']['lat'] : "";
            $longi = isset($resp['results'][0]['geometry']['location']['lng']) ? $resp['results'][0]['geometry']['location']['lng'] : "";
            $formatted_address = isset($resp['results'][0]['formatted_address']) ? $resp['results'][0]['formatted_address'] : "";

            # verify if data is complete
            if($lati && $longi && $formatted_address) {
                # put the data in the array
                $data_arr = array();

                # push Data in empty array
                array_push(
                $data_arr,
                $lati,
                $longi,
                $formatted_address
                );

                # return the data Array
                return $data_arr;
            } else {
                return false;
            }
        } else {
            echo "<strong>ERROR: {$resp['status']}</strong>";
            return false;
        }
    }

    /**
     * @method to Fetch address from Lat long
     * @param $latitude, $longitude
     * @return $address
     */
    public function fetchAddressFromLatLong($latitude,$longitude)
    {
        if(!empty($latitude) && !empty($longitude)){
            #Send request and receive json data by address
            $geocodeFromLatLong = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&sensor=false&key=AIzaSyC3tI2o08AKcPpSK40CbVIqMPzbjPHiglA'); 
            $output = json_decode($geocodeFromLatLong);
            $status = $output->status;
            
            #Get address from json data
            $address = ($status=="OK") ? $output->results[1]->formatted_address : '';

            if($output->status == "OK" AND !empty($output)){
                $addressComponents = $output->results[0]->address_components;
                foreach($addressComponents as $addrComp) {
                    if($addrComp->types[0] == 'postal_code') {
                        //Return the zipcode
                        $postalCode =  $addrComp->long_name;
                    }
                }
            } else {
                $postalCode =  '';
            }

            //Return address of the given latitude and longitude
            if(!empty($address)) {
                $address =  $address;
            } else {
                $address =  '';
            }
        } 

        $data = [
            'postalCode' => $postalCode,
            'address'    => $address,
        ];

        return $data;
    }
    
    /**
     * @method to fetch all the Categories
     * 
     * @return Json data
     */
    public function categories(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id' => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # feth the Provider
        $provider = $this->provider->find($request->get('provider_id'));

        # check for provider
        if($provider != '') {
            $categories = $this->getCategories();
            if($categories->isNotEmpty()) {
                $categoriesData = [];
                foreach ($categories as $key => $category) {
                    $data = [
                        'category_id'   => (string)$category->id,
                        'name'          => (string)$category->name,
                        'avtar_path'    => (string)$category->avtar_path,
                    ];

                    array_push($categoriesData, $data);
                }

                # return response
                return response()->json([
                    'code'      => (string)$this->successStatus, 
                    'message'   => 'Categories Found.',
                    'data'      => $categoriesData
                 ]); 

            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'No category Found.',
                    'data'      => []
                ]); 
            }
        } else {
           # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Provider not Found with provided Id.',
                'data'      => []
             ]); 
        }
    }

    /**
     * fetch all the categories avail in System
     * 
     * @return Category Collection
     */
    public function getCategories()
    {
        # Fetch all the Active Categories
        $categories = $this->category->active()->get();

        # return 
        return $categories;
    }

    /**
     * @method to store Provider category
     * @param request
     * @return json of Sub categories
     */
    public function storeCategory(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'   => 'required|numeric',
            'category_id'   => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # feth the Provider
        $provider = $this->provider->find($request->get('provider_id'));

        # fetch the Category
        $category = $this->category
                         ->with('subCategories')
                         ->find($request->get('category_id'));
        
        # If Catgeory is Not Found
        if($category == '') {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Requested Category is Not Found in records.',
                'data'      => []
             ]);
        }   

        # fetch all the Sub category of category
        $subCategories = $category->subCategories;
        $subCategoriesData = [];
        # check for provider
        if($provider != '') {
            if($subCategories->isNotEmpty()) {
                 $providerCategoryExist = ProviderCategory::where('provider_id', $provider->id)
                                                #->where('category_id', $request->get('category_id'))
                                                ->get();
                if($providerCategoryExist->isEmpty()) {
                    $data = [
                    'provider_id' => $provider->id,
                    'category_id' => $request->get('category_id'),
                   ];

                   # store Provider category
                   $this->providerCategory->create($data);

                   foreach ($subCategories as $key => $subCategory) {
                      $data = [
                        'sub_category_id'   => (string)$subCategory->id ?? '',
                        'sub_category_name' => (string)$subCategory->name ?? '',
                        'sub_category_icon' => (string)$subCategory->avtar_path ?? '',
                      ];

                      # Push data to Subcategory
                      array_push($subCategoriesData, $data);
                   }

                   # return response
                    return response()->json([
                        'code'      => (string)$this->successStatus, 
                        'message'   => 'Provider Category has been Stored Successfully.',
                        'data'      => $subCategoriesData
                     ]); 
                } else {
                    $data = [
                    'provider_id' => (string)$provider->id,
                    'category_id' => (string)$request->get('category_id'),
                   ];

                   # store Provider category
                   $providerCategoryExist->first()->update($data);

                   foreach ($subCategories as $key => $subCategory) {
                      $data = [
                        'sub_category_id'   => (string)$subCategory->id ?? '',
                        'sub_category_name' => (string)$subCategory->name ?? '',
                        'sub_category_icon' => (string)$subCategory->avtar_path ?? '',
                      ];

                      # Push data to Subcategory
                      array_push($subCategoriesData, $data);
                   }

                   # return response
                    return response()->json([
                        'code'      => (string)$this->successStatus, 
                        'message'   => 'Provider Category has been Updated Successfully.',
                        'data'      => $subCategoriesData
                     ]); 
                }
            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'No Sub Category Found On Provided category.',
                    'data'      => []
                 ]); 
            }
        } else {
           # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Provider not Found with provided Id.',
                'data'      => []
             ]); 
        }

    }

    /**
     * @method to store Sub categories of Provider
     * @param $request
     * @return json 
     */
    public function storeSubCategory(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'           => 'required|numeric',
            'category_id'           => 'required|numeric',
            'sub-category-ids'      => 'required|string',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        
        # fetch the Sub category ids and Convert to Array
        $subcategoryIds = explode(',', $request->get('sub-category-ids'));

        # feth the Provider
        $provider = $this->provider->find($request->get('provider_id'));

        # fetch Category id form request
        $categoryId = $request->get('category_id');

        # check for provider
        if($provider != '') {
            foreach ($subcategoryIds as $key => $subcategoryId) {
                $providerSubCategory = $this->providerSubCategory
                                            ->where('provider_id', $provider->id)
                                            ->where('category_id','<>', $categoryId)
                                            ->pluck('id');

                # First Delete all the Previous Sub Category
                $this->providerSubCategory->whereIn('id', $providerSubCategory)->delete();

                # Check wheter already present in Databse
                    $data = [
                        'provider_id'       => $provider->id,
                        'sub_category_id'   => $subcategoryId,
                        'category_id'       => $categoryId,
                    ];

                    # create Provider Sub category
                    $this->providerSubCategory->create($data);
            }
            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'Provider Sub categories has been updated Successfully.',
                'data'      => []
            ]); 
        } else{
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'Provider not Found with provided Id.',
                'data'      => []
             ]);
        }
    }

    /**
     * @method to Fetch Provider Status 
     * @param Request $request
     * @return Json | true
    */
    public function documentStatus(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'           => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # Set bthe relations
        $relations = ['documents.histories', 'documents.documentName'];

        # feth the Provider
        $provider = $this->provider
                         ->with($relations)
                         ->find($request->get('provider_id'));

        # fetch all the Uploaded Documents By Provider
        $mandatoryDocuments = $this->mandatoryDocumentsForProvider->all();

        # eftch all the Uploaded Documents
        $uplodedDocuments = $provider->documents;

        # If none of Document has been uploded by Provider
        if($uplodedDocuments->count() == 0) {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'No Document Found',
                'status'    => 'false',
                'data'      => []
            ]); 
        }

        if(($uplodedDocuments->count() > 0) AND ($uplodedDocuments->count() < $mandatoryDocuments->count() )) {
            $documentNotUploded = [];
            foreach ($mandatoryDocuments as $key => $mandatoryDocument) {
               $uplodDocumentOnMandatoryDocumnetId = $uplodedDocuments->where('document_id', $mandatoryDocument->id);
               if($uplodDocumentOnMandatoryDocumnetId->isEmpty()) {
                array_push($documentNotUploded, $mandatoryDocument->name);
               }
            }

            # return response
            return response()->json([
                'code'                  => (string)$this->failedStatus, 
                'message'               => 'Document Not Uploded yet.',
                'documents'             => $documentNotUploded,
                'status'                => 'false',
                'data'                  => []
            ]); 
        }

        $acceptedDocumentCount = 0;
        $rejectedDocumentCount = 0;
        $rejectedDocument = [];
        $acceptanceStillProcessDocument = [];
        if($uplodedDocuments->count() >= $mandatoryDocuments->count() ) {
            
            foreach ($mandatoryDocuments as $key => $mandatoryDocument) {
              # fetch all the Document on mandatory document Id
              $documentOnMandatoryDocumentId = $uplodedDocuments->where('document_id', $mandatoryDocument->id);

              if($documentOnMandatoryDocumentId->isNotEmpty()) {
                foreach ($documentOnMandatoryDocumentId as $key => $uploadDocument) {
                  $documentStatusHistory = $uploadDocument->histories;
                  if($documentStatusHistory->isNotEmpty()) {
                    foreach ($documentStatusHistory as $key => $status) {
                      if($status->document_status_id == DocumentStatusHistoryInterface::ACCEPT) {
                        $acceptedDocumentCount = $acceptedDocumentCount + 1;
                      } elseif ($status->document_status_id == DocumentStatusHistoryInterface::REJECT) {
                          $rejectedDocumentCount = $rejectedDocumentCount + 1;
                          array_push($rejectedDocument, $uploadDocument->documentName->name);
                      } 
                    }
                  } else {
                    array_push($acceptanceStillProcessDocument, $uploadDocument->documentName->name);
                  }
                }
              }
            }
        }

        if($acceptedDocumentCount == $mandatoryDocuments->count()) {
            # return response
            return response()->json([
                'code'                  => (string)$this->successStatus, 
                'message'               => 'Document Acceepted.',
                'documents'             => [],
                'status'                => 'true',
                'data'                  => []
            ]); 
        }

        if(!empty($acceptanceStillProcessDocument)) {
            # return response
            return response()->json([
                'code'                  => (string)$this->failedStatus, 
                'message'               => 'Document Acceptance Still in Process.',
                'document'              => $acceptanceStillProcessDocument,
                'status'                => 'true',
                'data'                  => []
            ]); 
        }

        if(!empty($rejectedDocument)) {
            # return response
            return response()->json([
                'code'                  => (string)$this->failedStatus, 
                'message'               => 'Document Rejected.',
                'document'              => $rejectedDocument,
                'status'                => 'false',
                'data'                  => []
            ]); 
        }
    }
}
