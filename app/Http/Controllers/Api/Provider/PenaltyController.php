<?php

namespace App\Http\Controllers\Api\Provider;

use DB;
use Validator;

# Models
use App\Models\Provider; 
use App\Http\Traits\StatusTrait;

# Interfaces
use App\Http\Interfaces\OrderStatusInterface;

# Controllers
use App\Http\Controllers\Controller; 

# Vendor Classes
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

class PenaltyController extends Controller implements OrderStatusInterface
{
    use StatusTrait;

    # Variable to Bind Model
    protected $provider;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
                                Provider $provider
                            )
    {
        $this->provider                 = $provider;
    }

    /**
     * @method to fetch all the Credit Pckages form Backend
     * @param Request $request
     * @return json | []
     */
    
    public function fetchPenalty(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'           => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        
        # fetch the Provider
        $provider = $this->provider
                         ->with('penalties.order')
                         ->find($request->get('provider_id'));

        # Provider is Not Avaialble
        if($provider != '') {
            # Fetch the Penalty of Provider
            $penalties = $provider->penalties;
            $penaltyArray = [];
            if($penalties->isNotEmpty()) {
                foreach ($penalties as $key => $penalty) {
                    $data = [
                        'id'                => (string)$penalty->id,
                        'penalty_points'    => (string)$penalty->penalty_points,
                        'order_id'          => (string)$penalty->order->id,
                        'unique_order_id'   => (string)$penalty->order->unique_order_id,
                        'reason'            => (string)$penalty->comment,
                    ];

                    array_push($penaltyArray, $data);
                }
                # return response
                return response()->json([
                    'code'      => (string)$this->successStatus, 
                    'message'   => 'Penalty Found.',
                    'data'      => $penaltyArray
                ]); 
            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'No Penalty Found.',
                    'data'      => []
                ]); 
            }
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'No Package Found.',
                'data'      => []
            ]); 
        }
    }
}
