<?php

namespace App\Http\Controllers\Api\Provider;

use DB;
use Validator;
use Carbon\Carbon;

# Models
use App\Http\Traits\StatusTrait;

use App\Models\User;
use App\Models\Order;
use App\Models\Provider;
use App\Models\UserWallet;
use App\Models\OrderHistory;
use App\Models\ProviderPenalty;
use App\Models\UserNotification;
use App\Models\AssignOrderToProvider;
use App\Models\AssignOrderToProviderHistory;

# Interface
use App\Http\Interfaces\OrderStatusInterface;
use App\Http\Interfaces\TransactionTypeInterface;
use App\Http\Interfaces\AssignOrderProviderStatusInterface;

# Controllers
use App\Http\Controllers\Controller; 
use App\Http\Controllers\Api\User\NotifyUser;

# Vendor Classes
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

class OrderController extends Controller implements OrderStatusInterface,
                                                    TransactionTypeInterface,
                                                    AssignOrderProviderStatusInterface
{
    use StatusTrait;

    # Variable to Bind Model Order
    protected $order;

    # Variable to Bind Model Provider
    protected $provider;

    # Variable to Bind Model UserWallet
    protected $userWallet;

    # Variable to Bind Model ProviderPenalty
    protected $providerPenalty;

    # Variable to Bind Model userNotification
    protected $userNotification;

    # Variable to Bind Model OrderHistory
    protected $orderHistory;

    # Variable to Bind Model AssignOrderToProvider
    protected $assignOrderToProvider;

    # Variable to Bind Model AssignOrderToProviderHistory
    protected $assignOrderToProviderHistory;

    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct(
                                User $user,
                                Order $order,
                                Provider $provider,
                                UserWallet $userWallet,
                                OrderHistory $orderHistory,
                                ProviderPenalty $providerPenalty,
                                UserNotification $userNotification,
                                AssignOrderToProvider $assignOrderToProvider,
                                AssignOrderToProviderHistory $assignOrderToProviderHistory
                                )
    {
        $this->user                         = $user;
        $this->order                        = $order;
        $this->provider                     = $provider;
        $this->userWallet                   = $userWallet;
        $this->orderHistory                 = $orderHistory;
        $this->providerPenalty              = $providerPenalty;
        $this->userNotification             = $userNotification;
        $this->assignOrderToProvider        = $assignOrderToProvider;
        $this->assignOrderToProviderHistory = $assignOrderToProviderHistory;
    }

    /**
     * @method to Update Status of Order
     * @param Request $request
     * @return json
     */
    public function updateOrderStatus(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'           => 'required|numeric',
            'assigned_provider_id'  => 'required|numeric',
            'order_id'              => 'required|numeric',
            'status'                => 'required|string',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # feth the provider
        $provider = $this->provider
                         ->find($request->get('provider_id'));

        if($provider != '') {
            # FEtch the Status
            $status = strtolower($request->get('status'));
            $message = '';
            if($status == 'ontheway') {
                $orderStatus = OrderStatusInterface::ON_THE_WAY;
                $assignOrderStatus = AssignOrderProviderStatusInterface::ON_TH_WAY;
                $message = 'Provider On the Way';
            } elseif ($status == 'inprogress') {
                $orderStatus = OrderStatusInterface::IN_PROGRESS;
                $assignOrderStatus = AssignOrderProviderStatusInterface::IN_PROGRES;
                $message = 'Order is in Progress';
            } elseif ($status == 'completed') {
               $orderStatus = OrderStatusInterface::COMPLETED;
                $assignOrderStatus = AssignOrderProviderStatusInterface::COMPLETE;
                $message = 'Order is Completed.';
            } else {
                # return response
                return response()->json([
                   'code'      => (string)$this->failedStatus, 
                   'message'   => 'Invalid Status Provided',
                   'data'      => []
                ]); 
            }

            $order = $this->order
                          ->with('user')
                          ->find($request->get('order_id'));
            # Fetch User
            $user = $order->user;
            #$user = $this->user->find(76);

            # Notify user for Order Successfully
            $notifyUser = new NotifyUser();

            # Set Title message
            $title = 'Mesee Order Notification';

            # Notify User
            $notifyUser->notify($user, $title, $message);

            # Save Send Notification 
            $data = [
                'user_id'       => $user->id,
                'order_id'      => $order->id,
                'notification'  => $message,
            ];

            # Creta UserNotification
            $this->userNotification->create($data);

            # Set Data for order History Creation
            $orderHistoryData = [
                'order_id'                  => $request->get('order_id'),
                'order_status_id'           => $orderStatus,
                'comment'                   => 'Provider Update',
            ];

            # Set Data for assigned Provider History Data
            $assignedOrderProviderData = [
                'order_id'                  => $request->get('order_id'),
                'assign_orderprovider_id'   => $request->get('assigned_provider_id'),
                'assign_order_status_id'    => $assignOrderStatus,
                'reason'                    => 'Provider Update',
            ];

            

            DB::beginTransaction();
                $this->orderHistory->create($orderHistoryData);
                $this->assignOrderToProviderHistory->create($assignedOrderProviderData);
            DB::commit();

            # return response
            return response()->json([
               'code'      => (string)$this->successStatus, 
               'message'   => 'Order Status has been Updated',
               'data'      => []
            ]); 
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'No Provider Found',
               'data'      => []
            ]); 
        }
    }

    /**
     * @method to Update Status of Order
     * @param Request $request
     * @return json
     */
    public function cancelOrder(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'           => 'required|numeric',
            'assigned_provider_id'  => 'required|numeric',
            'order_id'              => 'required|numeric',
            'reason'                => 'required|string',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # feth the provider
        $provider = $this->provider
                         ->find($request->get('provider_id'));

        # Fetch the Order
        $order = $this->order->find($request->get('order_id')); 

        # Fetch the Order
        $assignedOrderToProvider = $this->assignOrderToProvider->find($request->get('assigned_provider_id'));

        if($provider != '') {
            if($order != '') {
                # Fetch Amount paid By Customer for that order
                $amountToBeCredited = $order->paid_amount;

                # Set Data for order History Creation
                $orderHistoryData = [
                    'order_id'                  => $request->get('order_id'),
                    'order_status_id'           => OrderStatusInterface::CANCELLED,
                    'comment'                   => 'Provider Cancelled',
                ];

                # Set Data for assigned Provider History Data
                $assignedOrderProviderHistoryData = [
                    'order_id'                  => $request->get('order_id'),
                    'assign_orderprovider_id'   => $request->get('assigned_provider_id'),
                    'assign_order_status_id'    => AssignOrderProviderStatusInterface::CANCELED,
                    'reason'                    => 'Provider Cancelled',
                ];

                # Set Wallet Data
                $walletData = [
                    'user_id'           => $order->user_id,
                    'order_id'          => $request->get('order_id'),
                    'transaction_type'  => TransactionTypeInterface::CREDIT,
                    'amount'            => $order->paid_amount,
                ];

                # Set Data for assigned Provider Penalty Data
                $providerPenaltyData = [
                    'order_id'          => $order->id,
                    'provider_id'       => $provider->id,
                    'penalty_points'    => 5,
                    'comment'           => 'Provider Canceled Order',
                ];

                DB::beginTransaction();
                    $assignedOrderToProvider->update(['status' => false]);
                    $this->orderHistory->create($orderHistoryData);
                    $this->assignOrderToProviderHistory->create($assignedOrderProviderHistoryData);
                    $this->userWallet->create($walletData);
                    $this->providerPenalty->create($providerPenaltyData);
                DB::commit();

                # return response
                return response()->json([
                   'code'      => (string)$this->successStatus, 
                   'message'   => 'Order Cancelled Successfully',
                   'data'      => []
                ]); 
            } else {
                # return response
                return response()->json([
                   'code'      => (string)$this->failedStatus, 
                   'message'   => 'Order Not Found',
                   'data'      => []
                ]); 
            }
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'No Provider Found',
               'data'      => []
            ]); 
        }
    }

    /**
     * @method to fetch all the Cancelled orders of Providers
     * @param Request $request
     * @return json
     */
    public function fetchCancel(Request $request)
    {
       # Validate request data
        $validator = Validator::make($request->all(), [ 
            'provider_id'           => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # feth the provider
        $provider = $this->provider
                         ->with(['assignOrders.statusHistory','assignOrders.order'])
                         ->find($request->get('provider_id'));

        if($provider != '') {
            # FEtch Assign Orders to proivider
            $assignOrdersToProvider = $provider->assignOrders;

            # Fetch the orders which are cancelled by Provider
            $cancelledOrders = $assignOrdersToProvider->filter(function($assignOrder) {
                                # Fetch Assign Order History
                                $history = $assignOrder->statusHistory
                                                       ->where('assign_order_status_id', AssignOrderProviderStatusInterface::CANCELED)
                                                       ->isNotEmpty();
                                if($history) {
                                    return $assignOrder;
                                }
                            }); 

            $cancelledOrderArray = [];
            if($cancelledOrders->isNotEmpty()) {
                foreach ($cancelledOrders as $key => $cancelledOrder) {
                    $order          = $cancelledOrder->order;
                    $user           = $order->user;
                    $orderService   = $order->orderServices->first();
                    $orderDate          = $order->date_time_of_service->format('d-M-Y');
                    $orderTime          = $order->date_time_of_service->format('h:i A');
                    $userName           = $order->orderAddress->name;
                    $mobile             = $order->orderAddress->mobile;
                    $subCategoryName    = $orderService->service->subCategory->name;
                    $orderAmount        = $order->paid_amount ?? '';

                    $data = [
                        'assign_order_provider_id'  => (string)$cancelledOrder->id,
                        'provider_id'               => (string)$cancelledOrder->provider_id,
                        'order_id'                  => (string)$cancelledOrder->order_id,
                        'order_status'              => (string)$order->status_string,
                        'sub_category_name'         => (string)$subCategoryName,
                        'user_name'                 => (string)$userName,
                        'mobile'                    => (string)$mobile,
                        'order_date'                => (string)$orderDate,
                        'order_time'                => (string)$orderTime,
                        'credit_points'             => '50',
                        'amount'                    => (string)$orderAmount,
                        'unique_order_id'           => (string)$order->unique_order_id,
                        'order_placed_date'         => (string)$order->created_at->format('d-M-Y') ?? '',
                        'order_placed_time'         => (string)$order->created_at->format('h:i A') ?? '',
                    ];

                    array_push($cancelledOrderArray, $data);
                }

                # return response
                return response()->json([
                   'code'      => (string)$this->successStatus, 
                   'message'   => 'Cancelled Order Found',
                   'data'      => $cancelledOrderArray
                ]);
            } else {
                # return response
                return response()->json([
                   'code'      => (string)$this->failedStatus, 
                   'message'   => 'No Cancelled Order Found',
                   'data'      => []
                ]); 
            }
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'No Provider Found',
               'data'      => []
            ]); 
        }
    }
    
}
 