<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;

class UserAPi
{
    /**  
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        # fetch The User Id
        $userId = $request->header('id');
        
        # get the user Token
        $apiToken = User::where('id', $userId)->pluck('api_token')->first();

        # get Request Token
        $authorizationToken = $request->header('Authorization');
        
        # Chek Api Token for vallidation
        if($apiToken == $authorizationToken){
          return $next($request);
        }

        return response()->json([
        'responseMessage' => 'Unauthenticated User.',
        'responseCode'    => '0',
        ]);

        return $next($request);
    }
}
