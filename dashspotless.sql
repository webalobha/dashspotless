-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 24, 2020 at 01:58 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dashspotless`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, NULL, NULL, 'admin@gmail.com', '$2y$10$khzM91XVrdCfloJmJTuJM.8waTOA9QAw15qlJONgXjtv2RtLG5diK', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `main_services`
--

CREATE TABLE `main_services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avtar_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avtar_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `added_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `deleted_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `main_service_banners`
--

CREATE TABLE `main_service_banners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `main_service_id` int(10) UNSIGNED NOT NULL,
  `avtar_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avtar_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `added_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `deleted_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `main_service_description`
--

CREATE TABLE `main_service_description` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `main_service_id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `added_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `deleted_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `main_service_main_title`
--

CREATE TABLE `main_service_main_title` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `main_service_id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `added_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `deleted_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_03_24_043355_create_admin_table', 1),
(4, '2020_03_24_044910_create_user_address_table', 1),
(5, '2020_03_24_045747_create_table_user_avtars', 1),
(6, '2020_03_24_050643_create_providers_table', 1),
(7, '2020_03_24_050844_create_provider_addresses_table', 1),
(8, '2020_03_24_051006_create_table_provider_avtars', 1),
(9, '2020_03_24_051308_create_provider_vehicle_informations', 1),
(10, '2020_03_24_060337_create_table_main_services', 1),
(11, '2020_03_24_062119_create_table_main_services_banners', 1),
(12, '2020_03_24_062505_create_table_main_service_main_title', 1),
(13, '2020_03_24_062638_create_table_main_service_description', 1),
(14, '2020_03_24_063500_create_table_sub_services', 1),
(15, '2020_03_24_064549_create_table_packages', 1),
(16, '2020_03_24_072526_create_table_package_steps', 1),
(17, '2020_03_24_072817_create_table_package_detail_titles', 1),
(18, '2020_03_24_072912_create_table_package_detail_title_steps', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations1`
--

CREATE TABLE `migrations1` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations1`
--

INSERT INTO `migrations1` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_taken_minutes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `added_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `deleted_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `package_detail_titles`
--

CREATE TABLE `package_detail_titles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `package_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `added_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `deleted_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `package_detail_title_steps`
--

CREATE TABLE `package_detail_title_steps` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `package_id` int(10) UNSIGNED NOT NULL,
  `package_detail_title_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `added_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `deleted_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `package_steps`
--

CREATE TABLE `package_steps` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `package_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `added_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `deleted_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets1`
--

CREATE TABLE `password_resets1` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `providers`
--

CREATE TABLE `providers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `age` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` int(10) UNSIGNED DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `android_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ios_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `provider_addresses`
--

CREATE TABLE `provider_addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `state_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pin_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `landmark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_type` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `address_other_type_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `provider_avtars`
--

CREATE TABLE `provider_avtars` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `avtar_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avtar_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `provider_vehicle_informations`
--

CREATE TABLE `provider_vehicle_informations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `provider_id` int(10) UNSIGNED NOT NULL,
  `vehicle_type` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `vehicle_owner_first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_owner_middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_owner_last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_chasis_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_image_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rc_front_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rc_back_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `insur_certi_front_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `insur_certi_back_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poll_certifi_front_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poll_certifi_back_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_status` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `verified_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `added_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sub_services`
--

CREATE TABLE `sub_services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `main_service_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avtar_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avtar_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `added_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `deleted_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `age` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` int(10) UNSIGNED DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `android_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ios_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users1`
--

CREATE TABLE `users1` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users1`
--

INSERT INTO `users1` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mr. Admin', 'admin@gmail.com', NULL, '$2y$10$R0zzcXIuyp8kc0Ots2ubz.E37jwXXAsODB0v2IjFPppAn/YxU2ad6', NULL, '2020-03-23 01:47:53', '2020-03-23 01:47:53');

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE `user_addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `state_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pin_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `landmark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_type` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `address_other_type_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_avtars`
--

CREATE TABLE `user_avtars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `avtar_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avtar_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_first_name_index` (`first_name`),
  ADD KEY `admin_middle_name_index` (`middle_name`),
  ADD KEY `admin_last_name_index` (`last_name`),
  ADD KEY `admin_email_index` (`email`);

--
-- Indexes for table `main_services`
--
ALTER TABLE `main_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `main_services_name_index` (`name`),
  ADD KEY `main_services_avtar_name_index` (`avtar_name`),
  ADD KEY `main_services_avtar_path_index` (`avtar_path`),
  ADD KEY `main_services_status_index` (`status`),
  ADD KEY `main_services_added_by_index` (`added_by`),
  ADD KEY `main_services_updated_by_index` (`updated_by`),
  ADD KEY `main_services_deleted_by_index` (`deleted_by`);

--
-- Indexes for table `main_service_banners`
--
ALTER TABLE `main_service_banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `main_service_banners_main_service_id_index` (`main_service_id`),
  ADD KEY `main_service_banners_avtar_name_index` (`avtar_name`),
  ADD KEY `main_service_banners_avtar_path_index` (`avtar_path`),
  ADD KEY `main_service_banners_status_index` (`status`),
  ADD KEY `main_service_banners_added_by_index` (`added_by`),
  ADD KEY `main_service_banners_updated_by_index` (`updated_by`),
  ADD KEY `main_service_banners_deleted_by_index` (`deleted_by`);

--
-- Indexes for table `main_service_description`
--
ALTER TABLE `main_service_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `main_service_description_main_service_id_index` (`main_service_id`),
  ADD KEY `main_service_description_added_by_index` (`added_by`),
  ADD KEY `main_service_description_updated_by_index` (`updated_by`),
  ADD KEY `main_service_description_deleted_by_index` (`deleted_by`);

--
-- Indexes for table `main_service_main_title`
--
ALTER TABLE `main_service_main_title`
  ADD PRIMARY KEY (`id`),
  ADD KEY `main_service_main_title_main_service_id_index` (`main_service_id`),
  ADD KEY `main_service_main_title_added_by_index` (`added_by`),
  ADD KEY `main_service_main_title_updated_by_index` (`updated_by`),
  ADD KEY `main_service_main_title_deleted_by_index` (`deleted_by`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations1`
--
ALTER TABLE `migrations1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `packages_title_index` (`title`),
  ADD KEY `packages_price_index` (`price`),
  ADD KEY `packages_time_taken_minutes_index` (`time_taken_minutes`),
  ADD KEY `packages_added_by_index` (`added_by`),
  ADD KEY `packages_updated_by_index` (`updated_by`),
  ADD KEY `packages_deleted_by_index` (`deleted_by`);

--
-- Indexes for table `package_detail_titles`
--
ALTER TABLE `package_detail_titles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_detail_titles_package_id_index` (`package_id`),
  ADD KEY `package_detail_titles_title_index` (`title`),
  ADD KEY `package_detail_titles_added_by_index` (`added_by`),
  ADD KEY `package_detail_titles_updated_by_index` (`updated_by`),
  ADD KEY `package_detail_titles_deleted_by_index` (`deleted_by`);

--
-- Indexes for table `package_detail_title_steps`
--
ALTER TABLE `package_detail_title_steps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_detail_title_steps_package_id_index` (`package_id`),
  ADD KEY `package_detail_title_steps_package_detail_title_id_index` (`package_detail_title_id`),
  ADD KEY `package_detail_title_steps_title_index` (`title`),
  ADD KEY `package_detail_title_steps_added_by_index` (`added_by`),
  ADD KEY `package_detail_title_steps_updated_by_index` (`updated_by`),
  ADD KEY `package_detail_title_steps_deleted_by_index` (`deleted_by`);

--
-- Indexes for table `package_steps`
--
ALTER TABLE `package_steps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_steps_package_id_index` (`package_id`),
  ADD KEY `package_steps_title_index` (`title`),
  ADD KEY `package_steps_description_index` (`description`),
  ADD KEY `package_steps_added_by_index` (`added_by`),
  ADD KEY `package_steps_updated_by_index` (`updated_by`),
  ADD KEY `package_steps_deleted_by_index` (`deleted_by`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `password_resets1`
--
ALTER TABLE `password_resets1`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `providers`
--
ALTER TABLE `providers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `providers_first_name_index` (`first_name`),
  ADD KEY `providers_middle_name_index` (`middle_name`),
  ADD KEY `providers_last_name_index` (`last_name`),
  ADD KEY `providers_country_code_index` (`country_code`),
  ADD KEY `providers_mobile_number_index` (`mobile_number`),
  ADD KEY `providers_email_index` (`email`),
  ADD KEY `providers_status_index` (`status`),
  ADD KEY `providers_age_index` (`age`),
  ADD KEY `providers_gender_index` (`gender`),
  ADD KEY `providers_latitude_index` (`latitude`),
  ADD KEY `providers_longitude_index` (`longitude`),
  ADD KEY `providers_android_key_index` (`android_key`),
  ADD KEY `providers_ios_key_index` (`ios_key`);

--
-- Indexes for table `provider_addresses`
--
ALTER TABLE `provider_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provider_addresses_user_id_index` (`user_id`),
  ADD KEY `provider_addresses_country_id_index` (`country_id`),
  ADD KEY `provider_addresses_state_id_index` (`state_id`),
  ADD KEY `provider_addresses_city_index` (`city`),
  ADD KEY `provider_addresses_mobile_index` (`mobile`),
  ADD KEY `provider_addresses_pin_code_index` (`pin_code`),
  ADD KEY `provider_addresses_landmark_index` (`landmark`),
  ADD KEY `provider_addresses_address_other_type_description_index` (`address_other_type_description`),
  ADD KEY `provider_addresses_status_index` (`status`);

--
-- Indexes for table `provider_avtars`
--
ALTER TABLE `provider_avtars`
  ADD KEY `provider_avtars_user_id_index` (`user_id`),
  ADD KEY `provider_avtars_avtar_name_index` (`avtar_name`),
  ADD KEY `provider_avtars_avtar_path_index` (`avtar_path`);

--
-- Indexes for table `provider_vehicle_informations`
--
ALTER TABLE `provider_vehicle_informations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provider_vehicle_informations_provider_id_index` (`provider_id`),
  ADD KEY `provider_vehicle_informations_vehicle_type_index` (`vehicle_type`),
  ADD KEY `provider_vehicle_informations_vehicle_owner_first_name_index` (`vehicle_owner_first_name`),
  ADD KEY `provider_vehicle_informations_vehicle_owner_middle_name_index` (`vehicle_owner_middle_name`),
  ADD KEY `provider_vehicle_informations_vehicle_owner_last_name_index` (`vehicle_owner_last_name`),
  ADD KEY `provider_vehicle_informations_vehicle_chasis_number_index` (`vehicle_chasis_number`),
  ADD KEY `provider_vehicle_informations_vehicle_number_index` (`vehicle_number`),
  ADD KEY `provider_vehicle_informations_vehicle_image_name_index` (`vehicle_image_name`),
  ADD KEY `provider_vehicle_informations_vehicle_image_path_index` (`vehicle_image_path`),
  ADD KEY `provider_vehicle_informations_rc_front_image_index` (`rc_front_image`),
  ADD KEY `provider_vehicle_informations_rc_back_image_index` (`rc_back_image`),
  ADD KEY `provider_vehicle_informations_insur_certi_front_image_index` (`insur_certi_front_image`),
  ADD KEY `provider_vehicle_informations_insur_certi_back_image_index` (`insur_certi_back_image`),
  ADD KEY `provider_vehicle_informations_poll_certifi_front_image_index` (`poll_certifi_front_image`),
  ADD KEY `provider_vehicle_informations_poll_certifi_back_image_index` (`poll_certifi_back_image`);

--
-- Indexes for table `sub_services`
--
ALTER TABLE `sub_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_services_main_service_id_index` (`main_service_id`),
  ADD KEY `sub_services_name_index` (`name`),
  ADD KEY `sub_services_avtar_name_index` (`avtar_name`),
  ADD KEY `sub_services_avtar_path_index` (`avtar_path`),
  ADD KEY `sub_services_status_index` (`status`),
  ADD KEY `sub_services_added_by_index` (`added_by`),
  ADD KEY `sub_services_updated_by_index` (`updated_by`),
  ADD KEY `sub_services_deleted_by_index` (`deleted_by`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_first_name_index` (`first_name`),
  ADD KEY `users_middle_name_index` (`middle_name`),
  ADD KEY `users_last_name_index` (`last_name`),
  ADD KEY `users_country_code_index` (`country_code`),
  ADD KEY `users_mobile_number_index` (`mobile_number`),
  ADD KEY `users_email_index` (`email`),
  ADD KEY `users_status_index` (`status`),
  ADD KEY `users_age_index` (`age`),
  ADD KEY `users_gender_index` (`gender`),
  ADD KEY `users_latitude_index` (`latitude`),
  ADD KEY `users_longitude_index` (`longitude`),
  ADD KEY `users_android_key_index` (`android_key`),
  ADD KEY `users_ios_key_index` (`ios_key`);

--
-- Indexes for table `users1`
--
ALTER TABLE `users1`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_addresses_user_id_index` (`user_id`),
  ADD KEY `user_addresses_country_id_index` (`country_id`),
  ADD KEY `user_addresses_state_id_index` (`state_id`),
  ADD KEY `user_addresses_city_index` (`city`),
  ADD KEY `user_addresses_mobile_index` (`mobile`),
  ADD KEY `user_addresses_pin_code_index` (`pin_code`),
  ADD KEY `user_addresses_landmark_index` (`landmark`),
  ADD KEY `user_addresses_address_other_type_description_index` (`address_other_type_description`),
  ADD KEY `user_addresses_status_index` (`status`);

--
-- Indexes for table `user_avtars`
--
ALTER TABLE `user_avtars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_avtars_user_id_index` (`user_id`),
  ADD KEY `user_avtars_avtar_name_index` (`avtar_name`),
  ADD KEY `user_avtars_avtar_path_index` (`avtar_path`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `main_services`
--
ALTER TABLE `main_services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_service_banners`
--
ALTER TABLE `main_service_banners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_service_description`
--
ALTER TABLE `main_service_description`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_service_main_title`
--
ALTER TABLE `main_service_main_title`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `migrations1`
--
ALTER TABLE `migrations1`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `package_detail_titles`
--
ALTER TABLE `package_detail_titles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `package_detail_title_steps`
--
ALTER TABLE `package_detail_title_steps`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `package_steps`
--
ALTER TABLE `package_steps`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `providers`
--
ALTER TABLE `providers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `provider_addresses`
--
ALTER TABLE `provider_addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `provider_vehicle_informations`
--
ALTER TABLE `provider_vehicle_informations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_services`
--
ALTER TABLE `sub_services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users1`
--
ALTER TABLE `users1`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_addresses`
--
ALTER TABLE `user_addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_avtars`
--
ALTER TABLE `user_avtars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
