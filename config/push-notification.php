<?php

return array(

    'appNameIOS'     => array(
        'environment' =>'development',
        'certificate' =>'/path/to/certificate.pem',
        'passPhrase'  =>'password',
        'service'     =>'apns'
    ),
    'AndroidUser' => array(
        'environment' => env('ANDROID_USER_ENV', 'production'),
        'apiKey'      => 'AAAA71GSrtg:APA91bExULGMYeGSLmxlhcq7RDsu6MOLf2h15OL6JNjOCLw0GvKfuhVK8AABpOdjVhBwBbu2K8SYagM-gneZHJN2e2hxV1ae4fZWufrRRwrc3DwNaazkWVV1mah56hr97Qc7X9BTywr6',
        'service'     => 'gcm'
    ),
    'AndroidProvider' => array(
        'environment' => env('ANDROID_PROVIDER_ENV', 'production'),
        'apiKey'      => 'AAAAaS6ONwM:APA91bGQt_fmGGFHn4ZrBiceztepNYOO7C9Z4pPlaMjV6RUPRWSe_wLrJqlBPQKAZY7SZt5JJ8I-oo9dNFVbAwPte_uedhI4ZLQkgeRiA3QFnbP7TTJq71HVurFGNWnWwp7NZ08Hrso2',
        'service'     => 'gcm'
    )

);




